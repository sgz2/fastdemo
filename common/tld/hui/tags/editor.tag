<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="id"  rtexprvalue="true" %>
<%@ attribute name="name"  rtexprvalue="true" %>
<%@ attribute name="value"  rtexprvalue="true" %>
<%@ attribute name="required" rtexprvalue="false" %>
<div class="row cl">
    <label class="form-label col-xs-4 col-sm-3">
        <c:if test="${not empty required}">
            <span class="c-red">*</span>
        </c:if>
        ${name}：</label>
    <div class="formControls col-xs-8 col-sm-9">
        <script id="${id}" type="text/plain" style="width:100%;height:400px;"><jsp:doBody /></script>
    </div>
</div>