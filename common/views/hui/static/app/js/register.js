/**
 * 初始化详情对话框
 */
var UserInfoDlg = {
    userInfoData : {},
    formId: "userForm"
};

/**
 * 清除数据
 */
UserInfoDlg.clearData = function() {
    this.userInfoData = {};
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserInfoDlg.set = function(key, val) {
    this.userInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
    return this;
}

/**
 * 设置对话框中的数据
 *
 * @param key 数据的名称
 * @param val 数据的具体值
 */
UserInfoDlg.get = function(key) {
    return $("#" + key).val();
}

/**
 * 关闭此对话框
 */
UserInfoDlg.close = function() {
    location.href = "login";
}

/**
 * 收集数据
 */
UserInfoDlg.collectData = function() {
    this
        .set('id')
        .set('username')
        .set('password')
        .set('name')
        .set('sex')
        .set('headImg')
        .set('intro')
        .set('address')
        .set('phone')
        .set('email')
        .set('createTime')
        .set('updateTime');
}

UserInfoDlg.validate = function () {
    $('#'+this.formId).data("bootstrapValidator").resetForm();
    $('#'+this.formId).bootstrapValidator('validate');
    return $('#'+this.formId).data('bootstrapValidator').isValid();
}

/**
 * 提交添加
 */
UserInfoDlg.addSubmit = function() {

    this.clearData();
    this.collectData();

    if(!this.validate()){
        return;
    }

    //提交信息
    $.post(BBQ.ctxPath + "/register", this.userInfoData ,function(data){
        if(data.code==1){
            BBQ.success("注册成功!");
            setTimeout(function () {
                location.href = "login";
            }, 1000);
        }else{
             BBQ.info(data.msg);
        }
    });
}
/**
 * 提交修改
 */
UserInfoDlg.editSubmit = function() {

    this.clearData();
    this.collectData();

    if(!this.validate()){
        return;
    }

    //提交信息
    $.post(BBQ.ctxPath + "/user/profile/update",this.userInfoData, function(data){
        if(data.code==1){
            BBQ.success("修改成功!");
        }else{
             BBQ.info(data.msg);
        }
    });
}
$(function() {

    var validSetting = {};
    BBQ.notEmpty(validSetting, "username");
    BBQ.notEmpty(validSetting, "password");
    BBQ.notEmpty(validSetting, "name");
    BBQ.initValidator(UserInfoDlg.formId, validSetting);

    var avatarUp = new $WebUpload("headImg");
    avatarUp.setUploadBarId("progressBar");
    avatarUp.init();

});
