<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FastDemo - adminlte</title>

    <link href="https://cdn.bootcss.com/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.js"></script>
    <script src="https://cdn.bootcss.com/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<jsp:include page="common/nav.jsp"/>
<div class="container">
    <div class="row">
        <jsp:include page="common/left.jsp"/>
        <div class="col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading">xx栏目</div>
                <div class="panel-body">

                    <c:forEach var="rowData" items="${pageData.rows}">
                        <div class="media">
                            <div class="media-left">
                                <a href="/detail/${fastdemo_tagMap['row_to_detail'].linkLayerId}?id=${rowData[fastdemo_tagMap['row_to_detail'].parameters]}">
                                    <img class="media-object" src="..." alt="...">
                                </a>
                            </div>
                            <div class="media-body">
                                <h4 class="media-heading">${rowData.title}</h4>
                                ${rowData.description}
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </div>
            <nav aria-label="Page navigation">
                <ul class="pagination">
                    <li>
                        <a href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                        </a>
                    </li>
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">4</a></li>
                    <li><a href="#">5</a></li>
                    <li>
                        <a href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>
</body>
</html>