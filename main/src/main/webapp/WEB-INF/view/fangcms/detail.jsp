<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FastDemo - adminlte</title>

    <link href="https://cdn.bootcss.com/twitter-bootstrap/3.4.1/css/bootstrap.min.css" rel="stylesheet">
    <script src="https://cdn.bootcss.com/jquery/2.2.4/jquery.js"></script>
    <script src="https://cdn.bootcss.com/twitter-bootstrap/3.4.1/js/bootstrap.min.js"></script>

</head>
<body>
<jsp:include page="common/nav.jsp"/>
<div class="container">
    <div class="row">
        <jsp:include page="common/left.jsp"/>
        <div class="col-sm-9">
            <div class="panel panel-default">
                <div class="panel-heading">xx栏目</div>
                <div class="panel-body">

                    详情内容
                </div>
            </div>

        </div>
    </div>
</div>
</body>
</html>