<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="container">
    Logo
    <nav class="navbar navbar-default">
        <ul class="nav navbar-nav">
            <c:forEach items="${fastdemoMenuList}" var="menuDto">
            <li><a href="/layer/${menuDto.menu.layerId}">${menuDto.menu.name}</a></li>
            </c:forEach>
            <c:if test="${not empty fastdemoMenuInclude}">
                <c:forEach items="${fastdemoMenuInclude}" var="menuIncludeData">
                    <li><a href="/layer/${fastdemoMenuIncludeLink.linkLayerId}?${fastdemoMenuIncludeLink.parameters}=${menuIncludeData.id}">${menuIncludeData.name}</a></li>
                </c:forEach>
            </c:if>
        </ul>
    </nav>
</div>