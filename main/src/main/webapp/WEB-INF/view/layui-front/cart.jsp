<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${seo.mainName}</title>
    <link rel="stylesheet" type="text/css" href="/static/main.css">
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <script type="text/javascript" src="/static/layui/layui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
</head>
<body>

<jsp:include page="common/header_front.jsp"/>



<div class="header">
    <div class="headerLayout w1200">
        <div class="headerCon">
            <h1 class="mallLogo">
                <a href="#" title="星巴克">
                    <img src="/res/static/img/logo.png">
                </a>
            </h1>
            <div class="mallSearch">
                <form action="" class="layui-form" novalidate>
                    <input type="text" name="title" required  lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入需要的商品">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">
                        <i class="layui-icon layui-icon-search"></i>
                    </button>
                    <input type="hidden" name="" value="">
                </form>
            </div>
        </div>
    </div>
</div>


<div class="content content-nav-base shopcart-content">
    <jsp:include page="common/nav.jsp"/>
    <div class="banner-bg w1200">
    </div>
    <div class="cart w1200">
        <div class="cart-table-th">
            <div class="th th-chk">
                <div class="select-all">
                    <div class="cart-checkbox">
                        <input class="check-all check" id="allCheckked" type="checkbox" value="true">
                    </div>
                    <label>&nbsp;&nbsp;全选</label>
                </div>
            </div>
            <div class="th th-item">
                <div class="th-inner">
                    商品
                </div>
            </div>
            <div class="th th-price">
                <div class="th-inner">
                    单价
                </div>
            </div>
            <div class="th th-amount">
                <div class="th-inner">
                    数量
                </div>
            </div>
            <div class="th th-sum">
                <div class="th-inner">
                    小计
                </div>
            </div>
            <div class="th th-op">
                <div class="th-inner">
                    操作
                </div>
            </div>
        </div>
        <div class="OrderList">
            <div class="order-content" id="list-cont">
                    <ul class="item-content layui-clear">
                        <li class="th th-chk">
                            <div class="select-all">
                                <div class="cart-checkbox">
                                    <input id="id" class="CheckBoxShop check"  type="checkbox" num="all" name="select-all" value="true">
                                </div>
                            </div>
                        </li>
                        <li class="th th-item">
                            <div class="item-cont">
                                <a href="javascript:;"><img src="/res/static/img/paging_img1.jpg" alt=""></a>
                                <div class="text">
                                    <div class="title">name</div>
                                </div>
                            </div>
                        </li>
                        <li class="th th-price">
                            <span class="th-su">price</span>
                        </li>
                        <li class="th th-amount">
                            <div class="box-btn layui-clear">
                                <div class="less layui-btn">-</div>
                                <input class="Quantity-input" type="" name="" value="${item.number}" disabled="disabled">
                                <div class="add layui-btn">+</div>
                            </div>
                        </li>
                        <li class="th th-sum">
                            <span class="sum">0</span>
                        </li>
                        <li class="th th-op">
                            <span class="dele-btn">删除</span>
                        </li>
                    </ul>
            </div>
        </div>

        <div class="FloatBarHolder layui-clear">
            <div class="th th-chk">
                <div class="select-all">
                    <div class="cart-checkbox">
                        <input class="check-all check" name="select-all" type="checkbox"  value="true">
                    </div>
                    <label>&nbsp;&nbsp;已选<span class="Selected-pieces">0</span>件</label>
                </div>
            </div>
            <div class="th batch-deletion">
                <span class="batch-dele-btn">批量删除</span>
            </div>
            <div class="th Settlement">
                <button type="button" id="jieSuan" class="layui-btn">结算</button>
            </div>
            <div class="th total">
                <p>应付：<span class="pieces-total">0</span></p>
            </div>
        </div>

        <div id="demo0" style="text-align: center;"></div>
    </div>
</div>


<!-- 模版导入数据 -->
<script type="text/html" id="demo">
  {{# layui.each(d.data,function(index,item){}}
    <ul class="item-content layui-clear">
      <li class="th th-chk">
        <div class="select-all">
          <div class="cart-checkbox">
            <input id="{{item.id}}" class="CheckBoxShop check" id="" type="checkbox" num="all" name="select-all" value="true">
          </div>
        </div>
      </li>
      <li class="th th-item">
        <div class="item-cont">
          <a href="javascript:;"><img src="../res/static/img/paging_img1.jpg" alt=""></a>
          <div class="text">
            <div class="title">宝宝T恤棉质小衫</div>
            <p><span>粉色</span>  <span>130</span>cm</p>
          </div>
        </div>
      </li>
      <li class="th th-price">
        <span class="th-su">189.00</span>
      </li>
      <li class="th th-amount">
        <div class="box-btn layui-clear">
          <div class="less layui-btn">-</div>
          <input class="Quantity-input" type="" name="" value="1" disabled="disabled">
          <div class="add layui-btn">+</div>
        </div>
      </li>
      <li class="th th-sum">
        <span class="sum">189.00</span>
      </li>
      <li class="th th-op">
        <span class="dele-btn">删除</span>
      </li>
    </ul>
  {{# });}}
</script>

<script>
    layui.config({ base: '/static/'}).use(['laypage','jquery','laytpl','car'],function(){
        var laypage = layui.laypage,
            $ = layui.$;
        var car = layui.car;
        laytpl = layui.laytpl;

        loadData();

        function loadData(curr){
            var url = "/list/${layer.id}"
            $.post(url,{curr:curr},function (resp) {
                if (resp.code == 0) {
                    if(curr){
                        buildItems(resp);
                    }else{
                        renderPage(resp);
                    }
                }
            })
        }

        function renderPage(resp){
            laypage.render({
                elem: 'demo0'
                ,count: resp.count //数据总数
                ,jump:function (obj,first) {
                    //obj包含了当前分页的所有参数，比如：
                    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                    console.log(obj.limit); //得到每页显示的条数
                    if(!first){
                        loadData(obj.curr)
                    }else{
                        buildItems(resp);
                    }
                }
            });
        }

        function buildItems(resp) {
            var getTpl = demo.innerHTML
                ,view = document.getElementById('list-cont');
            laytpl(getTpl).render(resp, function(html){
                view.innerHTML = html;
            });
            car.init();
        }


        $("#jieSuan").on('click', jiesuan);

        function jiesuan() {
            var uls = document.getElementById('list-cont').getElementsByTagName('ul');//每一行
            var itemArr = [];
            for(var i = 0; i < uls.length;i++){
                if(uls[i].getElementsByTagName('input')[0].checked){
                    var id = parseInt(uls[i].getElementsByTagName('input')[0].id);
                    itemArr.push(id)
                }
            }
            console.log(itemArr);
            if(itemArr.length<1){
                layer.msg("未选择任何商品");
                return;
            }
            var url = "?m=cart&ids="+itemArr.join(",");
            location.href = "/order_confirm"+url;
        }

    });
</script>
</body>
</html>
