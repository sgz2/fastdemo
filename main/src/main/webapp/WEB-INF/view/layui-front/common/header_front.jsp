<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="site-nav-bg">
    <div class="site-nav w1200">
        <p class="sn-back-home">
            <i class="layui-icon layui-icon-home"></i>
            <a href="/">首页</a>
        </p>
        <div class="sn-quick-menu">
            <c:if test="${not empty loginUser && loginRole == 'user'}">
                <div class="login"><a href="javascript:;">欢迎：${loginUser.showname}</a></div>
                <div class="login"><a href="/order">个人资料</a></div>
                <div class="login"><a href="/order">我的订单</a></div>
                <div class="login"><a href="/cart">购物车</a>
                    <span></span>
                </div>
            </c:if>
            <c:if test="${empty loginUser || loginRole != 'user'}">
                <div class="login"><a href="/login">登录</a></div>
                <div class="login"><a href="/register">注册</a></div>
            </c:if>

        </div>
    </div>
</div>
