<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="main-nav">
    <div class="inner-cont0">
        <div class="inner-cont1 w1200">
            <div class="inner-cont2">
                <c:forEach items="${fastdemoMenuList}" var="menuDto">
                    <c:choose>
                        <c:when test="${fn:length(menuDto.childs)>0}">

                        </c:when>
                        <c:otherwise>
                            <a href="/layer/${menuDto.menu.layerId}">${menuDto.menu.name}</a>
                        </c:otherwise>
                    </c:choose>

                </c:forEach>
            </div>
        </div>
    </div>
</div>
