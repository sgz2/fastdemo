<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/static/main.css">
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <script type="text/javascript" src="/static/layui/layui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    <style>
        .content .category-banner {
            background: #0e0b2d;
        }
    </style>
</head>
<body id="list-cont">
<jsp:include page="common/header_front.jsp"/>

<div class="header">
    <div class="headerLayout w1200">
        <div class="headerCon">
            <h1 class="mallLogo">
                <a href="#" title="星巴克咖啡">
                    <img src="/res/static/img/logo.png">
                </a>
            </h1>
            <div class="mallSearch">
                <form action="" class="layui-form" novalidate>
                    <input type="text" name="title" required  lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入需要的商品">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">
                        <i class="layui-icon layui-icon-search"></i>
                    </button>
                    <input type="hidden" name="" value="">
                </form>
            </div>
        </div>
    </div>
</div>


<div class="content">
    <jsp:include page="common/nav.jsp"/>
    <div class="category-con">
        <div class="category-inner-con w1200">
            <div class="category-type">
                <h3>全部分类</h3>
            </div>
            <div class="category-tab-content">
                <div class="nav-con">
                    <ul class="normal-nav layui-clear" style="height:447px;">
                        <li class="nav-item">
                            <div class="title">咖啡产品</div>
                            <p><a href="#">星巴克臻选™咖啡</a>
                                <a href="#">咖啡豆</a>
                                <a href="#">免煮咖啡</a></p>
                            <i class="layui-icon layui-icon-right"></i>
                        </li>
                        <li class="nav-item">
                            <div class="title">美食</div>
                            <p><a href="#">烘焙</a>
                                <a href="#">蛋糕&甜品</a></p>
                            <i class="layui-icon layui-icon-right"></i>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="category-banner">
            <div class="w1200">
                <img width="1200" src="/res/static/img/banner1.jpg">
            </div>
        </div>
    </div>

    <div class="product-cont w1200" id="product-cont">
        <div class="product-item product-item1 layui-clear">
            <div class="left-title">
                <h4><i>1F</i></h4>
                <img src="/res/static/img/icon_gou.png">
                <h5>咖啡</h5>
            </div>
            <div class="right-cont">
                <a href="javascript:;" class="top-img"><img src="/res/static/img/img12.jpg" alt=""></a>
                <div class="img-box">
                    <c:forEach var="goods" items="${list}">
                        <a title="${goods.name}" href="/details?id=${goods.id}"><img src="${goods.headImg}"></a>
                    </c:forEach>
                </div>
            </div>
        </div>
        <div class="product-item product-item2 layui-clear">
            <div class="left-title">
                <h4><i>2F</i></h4>
                <img src="/res/static/img/icon_gou.png">
                <h5>零食</h5>
            </div>
            <div class="right-cont">
                <a href="javascript:;" class="top-img"><img src="/res/static/img/img122.jpg" alt=""></a>
                <div class="img-box">
                    <c:forEach var="goods" items="${list2}">
                        <a title="${goods.name}" href="/details?id=${goods.id}"><img src="${goods.headImg}"></a>
                    </c:forEach>
                </div>
            </div>
        </div>
    </div>

</div>

<jsp:include page="common/footer_front.jsp"/>
<script type="text/javascript">

    layui.config({
        base: '/res/static/js/util/' //你存放新模块的目录，注意，不是layui的模块目录
    }).use(['mm','carousel'],function(){
        var carousel = layui.carousel,
            mm = layui.mm;
        var option = {
            elem: '#test1'
            ,width: '100%' //设置容器宽度
            ,arrow: 'always'
            ,height:'298'
            ,indicator:'none'
        }
        carousel.render(option);

    });
</script>
</body>
</html>
