<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="/static/main.css">
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <script type="text/javascript" src="/static/layui/layui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">

    <style>
        .content .category-banner {
            background: #0e0b2d;
        }
    </style>
</head>
<body>
<jsp:include page="common/header_front.jsp"/>

<div class="header">
    <div class="headerLayout w1200">
        <div class="headerCon">
            <h1 class="mallLogo">
                <a href="#" title="星巴克咖啡">
                    <img src="/res/static/img/logo.png">
                </a>
            </h1>
            <div class="mallSearch">
                <form action="" class="layui-form" novalidate>
                    <input type="text" name="title" required  lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入需要的商品">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">
                        <i class="layui-icon layui-icon-search"></i>
                    </button>
                    <input type="hidden" name="" value="">
                </form>
            </div>
        </div>
    </div>
</div>


<div class="content content-nav-base commodity-content">
    <jsp:include page="common/nav.jsp"/>
    <div class="commod-cont-wrap">
        <div class="commod-cont w1200 layui-clear">
        <div class="left-nav">
            <div class="title">所有分类</div>
            <div class="list-box">
                <dl>
                    <dt>奶粉辅食</dt>
                    <dd><a href="javascript:;">进口奶粉</a></dd>
                    <dd><a href="javascript:;">宝宝辅食</a></dd>
                    <dd><a href="javascript:;">营养品</a></dd>
                </dl>
            </div>
        </div>
        <div class="right-cont-wrap">
            <div class="right-cont">
                <div class="sort layui-clear">
                    <a class="active" href="javascript:;" event = 'volume'>销量</a>
                    <a href="javascript:;" event = 'price'>价格</a>
                    <a href="javascript:;" event = 'newprod'>新品</a>
                    <a href="javascript:;" event = 'collection'>收藏</a>
                </div>
                <div class="prod-number">
                    <span>200个</span>
                </div>
                <div class="cont-list layui-clear" id="list-cont">
                    <div class="item">
                        <div class="img">
                            <a href="javascript:;"><img src="../res/static/img/paging_img1.jpg"></a>
                        </div>
                        <div class="text">
                            <p class="title">森系小清新四件套</p>
                            <p class="price">
                                <span class="pri">￥200</span>
                                <span class="nub">1266付款</span>
                            </p>
                        </div>
                    </div>
                </div>
                <div id="demo0" style="text-align: center;"></div>
            </div>
        </div>
    </div>

    </div>
</div>

<jsp:include page="common/footer_front.jsp"/>

<!-- 模版引擎导入 -->
<script type="text/html" id="demo">
  {{# layui.each(d.data,function(index,item){}}
    <div class="item">
      <div class="img">
        <a href="/detail/${fastdemo_tagMap['row_to_detail'].linkLayerId}?oid={{item.id}}"><img src="{{item.img}}"></a>
      </div>
      <div class="text">
          <a href="/detail/${fastdemo_tagMap['row_to_detail'].linkLayerId}?oid={{item.id}}">
              <p class="title">{{item.title}}</p>
          </a>
          <p class="price">
              <span class="pri">{{item.pri}}</span>
              <span class="nub">{{item.nub}}</span>
          </p>
      </div>
    </div>
  {{# }); }}
</script>

<script type="text/javascript">

    layui.use(['laypage','jquery','laytpl'],function(){
        var laypage = layui.laypage,
            $ = layui.$;
        laytpl = layui.laytpl;

        loadData();

        function loadData(curr){
            var url = "/list/${layer.id}"
            $.post(url,{curr:curr},function (resp) {
                if (resp.code == 0) {
                    if(curr){
                        buildItems(resp);
                    }else{
                        renderPage(resp);
                    }
                }
            })
        }

        function renderPage(resp){
            laypage.render({
                elem: 'demo0'
                ,count: resp.count //数据总数
                ,jump:function (obj,first) {
                    //obj包含了当前分页的所有参数，比如：
                    console.log(obj.curr); //得到当前页，以便向服务端请求对应页的数据。
                    console.log(obj.limit); //得到每页显示的条数
                    if(!first){
                        loadData(obj.curr)
                    }else{
                        buildItems(resp);
                    }
                }
            });
        }
        
        function buildItems(resp) {
            var getTpl = demo.innerHTML
                ,view = document.getElementById('list-cont');
            laytpl(getTpl).render(resp, function(html){
                view.innerHTML = html;
            });
        }
    });
</script>
</body>
</html>
