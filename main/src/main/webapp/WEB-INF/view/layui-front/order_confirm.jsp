<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${seo.mainName}</title>
    <link rel="stylesheet" type="text/css" href="/static/main.css">
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <script type="text/javascript" src="/static/layui/layui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
</head>
<body>
<style>
    .shopcart-content .order-content .th-item {
        left: 0;
    }
    .th-price{
        /*line-height: 24px;*/
    }
    .shopcart-content .order-content .th-amount .box-btn {
        padding-left: 80px;
    }
    .shopcart-content .order-content .th-amount input {
        width: 46px;
        height: 24px;
    }
    .shopcart-content .item-content {
        height:80px;
    }
    .shopcart-content .order-content .th-amount input{
        border:none;
    }
    .shopcart-content .item-content {
        padding: 9px 0;
    }
    .shopcart-content .FloatBarHolder .Settlement button.layui-btn {
        width: 160px;
        height: 50px;
        line-height: 50px;
    }
    .shopcart-content .th-attr {
        width: 240px;
        min-height: 24px;
        text-align: left;
        padding-left: 90px;
        box-sizing: border-box;
    }
</style>
<jsp:include page="common/header_front.jsp"/>



<div class="header">
    <div class="headerLayout w1200">
        <div class="headerCon">
            <h1 class="mallLogo">
                <a href="#" title="星巴克">
                    <img src="/res/static/img/logo.png">
                </a>
            </h1>
            <div class="mallSearch">
                <form action="" class="layui-form" novalidate>
                    <input type="text" name="title" required  lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入需要的商品">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">
                        <i class="layui-icon layui-icon-search"></i>
                    </button>
                    <input type="hidden" name="" value="">
                </form>
            </div>
        </div>
    </div>
</div>


<div class="content content-nav-base shopcart-content">
    <jsp:include page="common/nav.jsp"/>
    <div class="banner-bg w1200">
    </div>
    <div class="w1200">
        <form id="infoForm" action="/order_confirm_sure" method="post">
            <div>
                <label>姓名</label>
                <input name="name" type="text">
            </div>
            <div>
                <label>联系电话</label>
                <input name="phone" type="text">
            </div>
            <div>
                <label>地址</label>
                <input name="address" type="text">
            </div>
        </form>
    </div>
    <div class="cart w1200">
        <div class="cart-table-th">
            <div class="th th-item">
                <div class="th-inner">
                    商品
                </div>
            </div>
            <div class="th th-attr">
                <div class="th-inner">
                    商品属性
                </div>
            </div>
            <div class="th th-price">
                <div class="th-inner">
                    单价
                </div>
            </div>
            <div class="th th-amount">
                <div class="th-inner">
                    数量
                </div>
            </div>
            <div class="th th-sum">
                <div class="th-inner">
                    小计
                </div>
            </div>
        </div>
        <div class="OrderList">
            <div class="order-content" id="list-cont">
                <c:forEach items="${fastdemo_cartList}" var="item">
                    <ul class="item-content layui-clear">
                        <li class="th th-item">
                            <div class="item-cont">
                                <a href="javascript:;"><img style="width: 60px;height:60px;" src="${item.goods.headImg}" alt=""></a>
                                <div class="text">
                                    <div class="title">${item.goods.name}</div>
                                </div>
                            </div>
                        </li>
                        <li class="th th-attr">
                            <div class="item-cont">
                                <div class="text">

                                </div>
                            </div>
                        </li>
                        <li class="th th-price">
                            <span class="th-su">${item.goods.price}</span>
                        </li>
                        <li class="th th-amount">
                            <div class="box-btn layui-clear">
                                <input class="Quantity-input" type="" name="" value="${item.number}" disabled="disabled">
                            </div>
                        </li>
                        <li class="th th-sum">
                            <span class="sum">${item.goods.price * item.number}</span>
                        </li>
                    </ul>
                </c:forEach>
            </div>
        </div>
        <div class="FloatBarHolder layui-clear">
            <div class="th total">
                <p>合计：<span class="pieces-total">${total}</span></p>
            </div>
        </div>
        <div class="FloatBarHolder layui-clear">

            <div class="th Settlement">
                <button type="button" id="submit" lay-filter="*" lay-submit class="layui-btn">提交订单</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">


    layui.use(['jquery','element','form'],function(){
        var $ = layui.$
            ,form = layui.form
            ,element = layui.element;

        form.on('submit(*)', function(data){
            console.log(data.elem) //被执行事件的元素DOM对象，一般为button对象
            console.log(data.form) //被执行提交的form对象，一般在存在form标签时才会返回
            console.log(data.field) //当前容器的全部表单字段，名值对形式：{name: value}
            var url = "/order_confirm_sure";
            var obj = data.field;
            obj.m= '${fastdemo_m}';
            obj.ids= '${fastdemo_ids}';
            $.post(url,data.field,function (resp) {
                console.log(resp);
            });
            return false; //阻止表单跳转。如果需要表单跳转，去掉这段即可。
        });

    });
</script>
</body>
</html>
