<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>${seo.mainName}</title>
    <link rel="stylesheet" type="text/css" href="/static/main.css">
    <link rel="stylesheet" type="text/css" href="/static/layui/css/layui.css">
    <script type="text/javascript" src="/static/layui/layui.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
</head>
<body>
<style>
    .datails-content .detail .item {
        border: 1px solid #ececec;
        padding: 15px;
        min-height: 300px;
    }
    .datails-content .detail {
        float: none;
    }
</style>
<jsp:include page="common/header_front.jsp"/>

<div class="header">
    <div class="headerLayout w1200">
        <div class="headerCon">
            <h1 class="mallLogo">
                <a href="#" title="星巴克">
                    <img src="../res/static/img/logo.png">
                </a>
            </h1>
            <div class="mallSearch">
                <form action="" class="layui-form" novalidate>
                    <input type="text" name="title" required  lay-verify="required" autocomplete="off" class="layui-input" placeholder="请输入需要的商品">
                    <button class="layui-btn" lay-submit lay-filter="formDemo">
                        <i class="layui-icon layui-icon-search"></i>
                    </button>
                    <input type="hidden" name="" value="">
                </form>
            </div>
        </div>
    </div>
</div>


<div class="content content-nav-base datails-content">
    <jsp:include page="common/nav.jsp"/>
    <div class="data-cont-wrap w1200">
        <div class="crumb">
            <a href="javascript:;">首页</a>
            <span>></span>
            <a href="javascript:;">所有商品</a>
            <span>></span>
            <a href="javascript:;">产品详情</a>
        </div>
        <div class="product-intro layui-clear">
            <div class="preview-wrap">
                <a href="javascript:;">
                    <img width="400" height="400" src="${obj.headImg}">
                </a>
            </div>
            <div class="itemInfo-wrap">
                <div class="itemInfo">
                    <div class="title">
                        <h4>${obj.title} </h4>
                    </div>
                    <div class="summary">
                        <p class="activity"><span>价格</span><strong class="price"><i>￥</i>${obj.price}</strong></p>
                    </div>
                    <div class="choose-attrs">

                        <div class="number layui-clear">
                            <span class="title">数&nbsp;&nbsp;&nbsp;&nbsp;量</span>
                            <div class="number-cont"><span class="cut btn">-</span>
                                <input onkeyup="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}"
                                    id="number"   onafterpaste="if(this.value.length==1){this.value=this.value.replace(/[^1-9]/g,'')}else{this.value=this.value.replace(/\D/g,'')}" maxlength="4" type="" name="" value="1">
                                <span class="add btn">+</span></div></div>
                    </div>
                    <div class="choose-btns">
                        <button class="layui-btn layui-btn-primary purchase-btn">立刻购买</button>
                        <button id="joinCart" class="layui-btn  layui-btn-danger car-btn">
                            <i class="layui-icon layui-icon-cart-simple"></i>加入购物车</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="layui-clear">
            <div class="detail">
                <h4>详情</h4>
                <div class="item">
                   ${obj.description}
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">


    layui.use(['jquery','layer'],function(){
        var $ = layui.$;
        var layer = layui.layer;

        var cur = $('.number-cont input').val();
        $('.number-cont .btn').on('click',function(){
            if($(this).hasClass('add')){
                cur++;

            }else{
                if(cur > 1){
                    cur--;
                }
            }
            $('.number-cont input').val(cur)
        })

        $('#joinCart').on('click',joinCart)

        function joinCart(){
            var url = "/add/${fastdemo_orderMap.order_addcart.linkLayerId}";
            var sendData = {
                goodsId:'${obj.id}'
                ,number:$("#number").val()
            }
            $.post(url, sendData, function (data) {
                if(data.code==1){
                    layer.msg("加入购物车成功");
                }else{
                    layer.msg(data.msg);
                    location.href = data.data;
                }
            });
        }

    });
</script>


</body>
</html>