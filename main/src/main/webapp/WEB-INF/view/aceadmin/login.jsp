<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Login Page - ${seo.mainName}</title>
    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="/static/assets/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/static/assets/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="/static/assets/css/ace.min.css" />
    <link rel="stylesheet" href="/static/assets/css/ace-rtl.min.css" />
</head>

<body class="login-layout">
<div class="main-container">
    <div class="main-content">
        <div class="row">
            <div class="col-sm-10 col-sm-offset-1">
                <div class="login-container">
                    <div class="center">
                        <h1>
                            <i class="ace-icon fa fa-leaf green"></i>
                            <span class="red">Express</span>
                            <span class="white" id="id-text2">管理系统</span>
                        </h1>
                        <h4 class="blue" id="id-company-text">&copy; LZY</h4>
                    </div>

                    <div class="space-6"></div>

                    <div class="position-relative">
                        <div id="login-box" class="login-box visible widget-box no-border">
                            <div class="widget-body">
                                <div class="widget-main">
                                    <h4 class="header blue lighter bigger">
                                        <i class="ace-icon fa fa-coffee green"></i>
                                        输入登录信息
                                    </h4>

                                    <div class="space-6"></div>

                                    <form id="loginForm">
                                        <fieldset>
                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input id="username" name="username" type="text" class="form-control" placeholder="Username" />
                                                    <i class="ace-icon fa fa-user"></i>
                                                </span>
                                            </label>

                                            <label class="block clearfix">
                                                <span class="block input-icon input-icon-right">
                                                    <input id="password" name="password" type="password" class="form-control" placeholder="Password" />
                                                    <i class="ace-icon fa fa-lock"></i>
                                                </span>
                                            </label>

                                            <div class="space"></div>

                                            <div class="clearfix">
                                                <label class="inline">
                                                    <c:if test="${not empty layers && fn:length(layers)>=2}">
                                                        <select name="" id="role" class="form-control">
                                                            <c:forEach items="${layers}" var="layer" varStatus="status">
                                                                <option value="${layer.roleCode}">${layer.name}</option>
                                                            </c:forEach>
                                                        </select>
                                                    </c:if>
                                                </label>

                                                <button id="login" onclick="Login.login()" type="button" class="width-35 pull-right btn btn-sm btn-primary">
                                                    <i class="ace-icon fa fa-key"></i>
                                                    <span class="bigger-110">Login</span>
                                                </button>
                                            </div>

                                            <div class="space-4"></div>
                                        </fieldset>
                                    </form>

                                    <div class="space-6"></div>

                                </div><!-- /.widget-main -->
                            </div><!-- /.widget-body -->
                        </div><!-- /.login-box -->

                    </div><!-- /.signup-box -->
                </div><!-- /.position-relative -->
            </div>
        </div><!-- /.col -->
    </div><!-- /.row -->
</div><!-- /.main-content -->
</div><!-- /.main-container -->

<!-- basic scripts -->


<script src="/static/assets/js/jquery-2.1.4.min.js"></script>
<script src="/static/plugins/validate/bootstrapValidator.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>
<script src="/static/adminlte/app/js/BBQ.js"></script>

<!-- inline scripts related to this page -->
<script type="text/javascript">

    var Login = {
        formId: 'loginForm',
        loginData : {}
    }

    /**
     * 清除数据
     */
    Login.clearData = function() {
        this.loginData = {};
    }

    Login.set = function(key, val) {
        this.loginData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
        return this;
    }

    Login.collectData = function() {
        this
            .set('username')
            .set('password')
            .set('role');
    }


    Login.validate = function(){
        $('#'+this.formId).data("bootstrapValidator").resetForm();
        $('#'+this.formId).bootstrapValidator('validate');
        return $('#'+this.formId).data('bootstrapValidator').isValid();
    }

    Login.login = function(){

        this.clearData();
        this.collectData();

        if(!this.validate()){
            return;
        }
        var url = "/login";
        $.post(url,Login.loginData,function (data) {
            if(data.code==1){
                BBQ.info("登录成功");
                setTimeout(function () {
                    location.href = "/index";
                }, 500);
            }else{
                BBQ.error(data.msg);
            }
        })


    }
    jQuery(function() {
        var validSetting = {};
        BBQ.notEmpty(validSetting, "username");
        BBQ.notEmpty(validSetting, "password");
        BBQ.initValidator(Login.formId, validSetting);
    });


</script>
</body>
</html>

