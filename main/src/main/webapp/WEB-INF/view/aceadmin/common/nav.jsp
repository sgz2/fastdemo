<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div id="sidebar" class="sidebar responsive ace-save-state">
    <script type="text/javascript">
        try{ace.settings.loadState('sidebar')}catch(e){}
    </script>

    <div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <button class="btn btn-success">
                <i class="ace-icon fa fa-signal"></i>
            </button>

            <button class="btn btn-info">
                <i class="ace-icon fa fa-pencil"></i>
            </button>

            <button class="btn btn-warning">
                <i class="ace-icon fa fa-users"></i>
            </button>

            <button class="btn btn-danger">
                <i class="ace-icon fa fa-cogs"></i>
            </button>
        </div>

        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <span class="btn btn-success"></span>

            <span class="btn btn-info"></span>

            <span class="btn btn-warning"></span>

            <span class="btn btn-danger"></span>
        </div>
    </div><!-- /.sidebar-shortcuts -->
        <ul class="nav nav-list">
            <li class="">
                <a href="/welcome">
                    <i class="menu-icon fa fa-tachometer"></i>
                    <span class="menu-text"> Dashboard </span>
                </a>
                <b class="arrow"></b>
            </li>

            <c:forEach items="${fastdemoMenuList}" var="menuDto">
                <c:choose>
                    <c:when test="${fn:length(menuDto.childs)>0}">
                        <li class="layui-nav-item layui-nav-itemed">
                            <a class="" href="javascript:;">${menuDto.menu.name}</a>
                            <dl class="layui-nav-child">
                                <c:forEach items="${menuDto.childs}" var="child">
                                    <li class="">
                                        <a href="/layer/${child.menu.layerId}">
                                            <i class="menu-icon fa fa-list-alt"></i>
                                            <span class="menu-text"> ${child.menu.name} </span>
                                        </a>
                                        <b class="arrow"></b>
                                    </li>
                                </c:forEach>

                            </dl>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="">
                            <a href="/layer/${menuDto.menu.layerId}">
                                <i class="menu-icon fa fa-list-alt"></i>
                                <span class="menu-text"> ${menuDto.menu.name} </span>
                            </a>
                            <b class="arrow"></b>
                        </li>
                    </c:otherwise>
                </c:choose>

            </c:forEach>
        </ul><!-- /.nav-list -->



    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
    <script>
        $(".nav-list a").each(function () {
            var _ = $(this);
            _.attr("data-href", _.attr("href"));
            _.attr("href", "javascript:;");
        });
        $(".nav-list a").on('click', function () {
            var _ = $(this);
            var url = _.attr("data-href");
            if(url){
                $.get(url,function (html) {
                    $("#main-content").html(html);
                })
            }
        });
    </script>
</div>
