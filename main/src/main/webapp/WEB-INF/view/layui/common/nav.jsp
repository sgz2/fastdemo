<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
        <!-- 左侧导航区域（可配合layui已有的垂直导航） -->
        <ul class="layui-nav layui-nav-tree"  lay-filter="test">
<c:forEach items="${fastdemoMenuList}" var="menuDto">
    <c:choose>
        <c:when test="${fn:length(menuDto.childs)>0}">
            <li class="layui-nav-item layui-nav-itemed">
                <a class="" href="javascript:;">${menuDto.menu.name}</a>
                <dl class="layui-nav-child">
                    <c:forEach items="${menuDto.childs}" var="child">
                        <dd><a href="/layer/${child.menu.layerId}">${child.menu.name}</a></dd>
                    </c:forEach>

                </dl>
            </li>
        </c:when>
        <c:otherwise>
            <li class="layui-nav-item"><a href="/layer/${menuDto.menu.layerId}">${menuDto.menu.name}</a></li>
        </c:otherwise>
    </c:choose>

</c:forEach>
        </ul>
    </div>
</div>
