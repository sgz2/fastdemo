<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-header">
    <div class="layui-logo">${fastdemo_name.shortname}</div>
    <!-- 头部区域（可配合layui已有的水平导航） -->

    <ul class="layui-nav layui-layout-right">
        <li class="layui-nav-item">
            <a href="javascript:;">
                <img src="http://t.cn/RCzsdCq" class="layui-nav-img">
                ${loginUser.showname}
            </a>
            <dl class="layui-nav-child">
                <dd><a href="/profile">基本资料</a></dd>
                <dd><a href="/password">修改密码</a></dd>
            </dl>
        </li>
        <li class="layui-nav-item"><a href="/logout">登出</a></li>
    </ul>
</div>
