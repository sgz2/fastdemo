<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/static/layui/css/layui.css" media="all">
    <title>${fastdemo_name.shortname}</title>
    <style>
        .login-wrapper {
            overflow: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: #eee;
        }
        .login-body{
            width: 339px;
            margin: 0 auto;
            padding: 120px 0 20px 0;
        }
        .layui-link {
            color: #029789 !important;
        }
        .login-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: center;
            line-height: 30px;
            color: rgba(0, 0, 0, 0.7) !important;
            padding-bottom: 20px;
        }

    </style>
</head>
<body>

<div class="login-wrapper">

    <div class="login-header">
        <h1>Fastdemo-Layui预览模板</h1>
    </div>

    <div class="login-body">
        <div class="layui-card">
            <div class="layui-card-header">注册</div>
            <div class="layui-card-body">
                <form class="layui-form layui-form-pane" action="">
                    <c:forEach var="column" items="${columnList}">
                            <c:choose>
                                <c:when test="${column.type=='from-server'}"></c:when>
                                <c:when test="${column.type=='img'}">
                                    <div class="layui-form-item ">
                                        <label class="layui-form-label"> ${column.title}：</label>
                                        <div class="layui-input-block">
                                            <div class="layui-col-sm4">
                                                <div id="${column.field}PreId">
                                                    <div>
                                                        <img width="100px" height="100px" />
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="layui-col-sm4">
                                                <div class="head-scu-btn upload-btn" id="${column.field}BtnId">
                                                    <i class="fa fa-upload"></i>&nbsp;上传
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" id="${column.field}" name="${column.field}" value="${obj[currentValue]}"/>
                                    </div>
                                    <script>
                                        var avatarUp = new $WebUpload("${column.field}");
                                        avatarUp.setUploadBarId("progressBar");
                                        avatarUp.init();
                                    </script>
                                </c:when>
                                <c:otherwise>
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">
                                                ${column.title}
                                            <span style="color: red;">*</span>
                                        </label>
                                        <div class="layui-input-block">
                                            <input id="${column.field}" name="${column.field}"
                                                   type="text" class="layui-input"
                                                <%--lay-verify="required" required=""--%>
                                            >
                                        </div>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                    </c:forEach>

                    <c:if test="${not empty layers && fn:length(layers)>=2}">
                        <div class="layui-form-item" pane>
                            <label class="layui-form-label">角色</label>
                            <div class="layui-input-block">
                                <c:forEach items="${layers}" var="layer" varStatus="status">
                                    <input type="radio" name="role" value="${layer.roleCode}" title="${layer.name}" ${status.index==0?"checked":""}>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>

                    <div class="layui-form-item">
                        <a href="login" class="layui-link">登录</a>
                    </div>
                    <div class="layui-form-item">
                        <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="formDemo">注册</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="login-footer">
        <p>© 2019 <a href="www.saoft.com">一克金代码</a>版权所有</p>
    </div>
</div>

<script src="/static/layui/layui.js"></script>
<script>
    //Demo
    layui.use(['form','jquery'], function(){
        var form = layui.form;
        var $ = layui.jquery;

        //监听提交
        form.on('submit(formDemo)', function(data){
            var url = "/register";
            $.post(url, data.field, function (resp) {
                if(resp.code==1){
                    location.href = "login";
                }else{
                    layer.msg(resp.msg);
                }
            });
            return false;
        });
    });
</script>

</body>
</html>