<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/static/layui/css/layui.css" media="all">
    <title>${fastdemo_name.shortname}</title>
    <style>
        .login-wrapper {
            overflow: auto;
            position: absolute;
            top: 0;
            bottom: 0;
            left: 0;
            right: 0;
            background: #eee;
        }
        .login-body{
            width: 339px;
            margin: 0 auto;
            padding: 120px 0 20px 0;
        }
        .layui-link {
            color: #029789 !important;
        }
        .login-footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            text-align: center;
            line-height: 30px;
            color: rgba(0, 0, 0, 0.7) !important;
            padding-bottom: 20px;
        }

    </style>
</head>
<body>

<div class="login-wrapper">

    <div class="login-header">
        <h1>${fastdemo_name.fullname}</h1>
    </div>

    <div class="login-body">
        <div class="layui-card">
            <div class="layui-card-header">用户登录</div>
            <div class="layui-card-body">
                <form class="layui-form layui-form-pane" action="">
                    <div class="layui-form-item">
                        <label class="layui-form-label">登录名</label>
                        <div class="layui-input-block">
                            <input type="text" name="username" required  lay-verify="required" placeholder="请输入标题" autocomplete="off" class="layui-input">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">密码</label>
                        <div class="layui-input-block">
                            <input type="password" name="password" required lay-verify="required" placeholder="请输入密码" autocomplete="off" class="layui-input">
                        </div>
                    </div>

                    <c:if test="${not empty layers && fn:length(layers)>=2}">
                        <div class="layui-form-item" pane>
                            <label class="layui-form-label">角色</label>
                            <div class="layui-input-block">
                                <c:forEach items="${layers}" var="layer" varStatus="status">
                                    <input type="radio" name="role" value="${layer.roleCode}" title="${layer.name}" ${status.index==0?"checked":""}>
                                </c:forEach>
                            </div>
                        </div>
                    </c:if>

                    <div class="layui-form-item">
                        <c:if test="${not empty layers && fn:length(registerLayers)>0}">
                        <a href="register" class="layui-link">账号注册</a>
                        </c:if>
                    </div>
                    <div class="layui-form-item">
                        <button class="layui-btn layui-btn-fluid" lay-submit lay-filter="formDemo">登录</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="login-footer">
        <p>${fastdemo_name.footer}</p>
    </div>
</div>

<script src="/static/layui/layui.js"></script>
<script>
    //Demo
    layui.use(['form','jquery'], function(){
        var form = layui.form;
        var $ = layui.jquery;

        //监听提交
        form.on('submit(formDemo)', function(data){
            var url = "/login";
            $.post(url, data.field, function (resp) {
                if(resp.code==1){
                    location.href = "index";
                }else{
                    layer.msg(resp.msg);
                }
            });
            return false;
        });
    });
</script>

</body>
</html>