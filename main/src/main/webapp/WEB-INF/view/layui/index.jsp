<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>${fastdemo_name.shortname}</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/static/fastdemo-layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <jsp:include page="common/header.jsp"/>
    <jsp:include page="common/nav.jsp"/>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div class="layui-fluid">
            <div class="layui-row layui-col-space15">
                <c:forEach var="count" items="${idxCount}">
                <div class="layui-col-sm6 layui-col-md3">
                    <div class="layui-card">
                        <div class="layui-card-header">
                            ${count.title}
                            <span class="layui-badge layui-bg-blue"></span>
                        </div>
                        <div class="layui-card-body">
                            <p>${count.total}</p>
                        </div>
                    </div>
                </div>
                </c:forEach>
            </div>
            <div class="layui-row layui-col-space15">
                <div class="layui-col-sm6">
                    <div class="layui-card">
                        <div class="layui-card-header">个人信息</div>
                        <div class="layui-card-body">
                            <table class="layui-table">
                                <c:forEach var="item" items="${infoItem}">
                                    <c:set var="currentValue">${item.field}</c:set>
                                    <tr>
                                        <td>${item.title}</td>
                                        <td>${obj[currentValue]}</td>
                                    </tr>
                                </c:forEach>
                            </table>
                        </div>
                    </div>

                </div>
                <c:if test="${not empty hasWeather && hasWeather}">
                    <div class="layui-col-sm6">
                        <div class="layui-card">
                            <div class="layui-card-header">天气</div>
                            <div class="layui-card-body">
                                <!-- weather -->
                                <div id="weather-view-he"></div>
                                <script>
                                    WIDGET = {ID: 'J0tHTTE5wx'};
                                </script>
                                <script type="text/javascript" src="https://apip.weatherdt.com/view/static/js/r.js?v=1111"></script>
                            </div>
                        </div>

                    </div>
                </c:if>
            </div>
        </div>
    </div>

    <jsp:include page="common/footer.jsp"/>
</div>
<script src="/static/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use(['element','table'], function(){
        var element = layui.element;
        var table = layui.table;

        //第一个实例
        table.render({
            elem: '#demo'
            ,height: 312
            ,url: '/demo/table/user/' //数据接口
            ,page: true //开启分页
            ,cols: [[ //表头
                {field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
                ,{field: 'username', title: '用户名', width:80}
                ,{field: 'sex', title: '性别', width:80, sort: true}
                ,{field: 'city', title: '城市', width:80}
                ,{field: 'sign', title: '签名', width: 177}
                ,{field: 'experience', title: '积分', width: 80, sort: true}
                ,{field: 'score', title: '评分', width: 80, sort: true}
                ,{field: 'classify', title: '职业', width: 80}
                ,{field: 'wealth', title: '财富', width: 135, sort: true}
            ]]
        });
    });
</script>
</body>
</html>