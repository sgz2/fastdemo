<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>${fastdemo_name.shortname}</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/static/fastdemo-layui.css">
    <script src="/static/layui/common/ProjectCommon.js"></script>
    <script src="/static/layui/common/jquery.min.js"></script>
    <script src="/static/layui/common/webuploader/webuploader.js"></script>
    <script src="/static/layui/common/web-upload-object.js?v=0.2"></script>
    <script src="/static/laydate/laydate.js?v=0.2"></script>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <jsp:include page="common/header.jsp"/>
    <jsp:include page="common/nav.jsp"/>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div class="layui-nav-title1">
            <blockquote class="layui-elem-quote layui-nav-header">${out.name} - 编辑</blockquote>
        </div>
        <div class="layui-nav-content">
            <div class="layui-fluid">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <form action="" class="layui-form add-form">
                            <c:forEach var="column" items="${columnList}">
                                <c:set var="currentValue">${column.field}</c:set>
                                <c:if test="${column.hiddenFlag}">
                                    <input type="hidden" name="${column.field}" value="${obj[currentValue]}">
                                </c:if>
                                <c:if test="${!column.hiddenFlag}">
                                    <c:choose>
                                        <c:when test="${column.type=='from-server'}"></c:when>
                                        <c:when test="${column.type=='img'}">
                                            <div class="layui-form-item ">
                                                <label class="layui-form-label"> ${column.title}
                                                    <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                        <span style="color: red;">*</span>
                                                    </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <div class="layui-col-sm4">
                                                        <div id="${column.field}PreId">
                                                            <div>
                                                                <img width="100px" height="100px" src="${obj[currentValue]}" />
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="layui-col-sm4">
                                                        <div class="head-scu-btn upload-btn" id="${column.field}BtnId">
                                                            <i class="fa fa-upload"></i>&nbsp;上传
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" id="${column.field}" name="${column.field}" value="${obj[currentValue]}"/>
                                            </div>
                                            <script>
                                                var avatarUp = new $WebUpload("${column.field}");
                                                avatarUp.setUploadBarId("progressBar");
                                                avatarUp.init();
                                            </script>
                                        </c:when>
                                        <c:when test="${column.type=='radio'}" >
                                            <%--判断kv> ft>--%>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">
                                                    ${column.title}
                                                        <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                            <span style="color: red;">*</span>
                                                        </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <c:forEach var="kv" items="${column.kvMap}">
                                                        <input type="radio" name="${column.field}" value="${kv.key}" title="${kv.value}"
                                                            ${obj[currentValue]==kv.key?"checked='checked'":""}
                                                        >
                                                    </c:forEach>
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:when test="${column.type=='select'}" >
                                            <%--判断kv> ft>--%>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">
                                                        ${column.title}
                                                            <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                                <span style="color: red;">*</span>
                                                            </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <select name="${column.field}" id="${column.field}" class="layui-select">
                                                        <option value=""></option>
                                                        <c:forEach var="kv" items="${column.kvMap}">
                                                            <option value="${kv.key}"  ${obj[currentValue]==kv.key?"selected='selected'":""}>${kv.value}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:when test="${column.type=='fk-select'}" >
                                            <%--判断kv> ft>--%>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">
                                                        ${column.title}
                                                            <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                                <span style="color: red;">*</span>
                                                            </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <select name="${column.field}" id="${column.field}" class="layui-select">
                                                        <option value=""></option>
                                                        <c:forEach var="kv" items="${column.kvList}">
                                                            <option value="${kv[column.kvKey]}"  ${obj[kv[column.kvKey]]==kv.key?"selected='selected'":""}>${kv[column.kvValue]}</option>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                            </div>
                                        </c:when>
                                        <c:when test="${column.type=='date'}" >
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">${column.title}
                                                    <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                        <span style="color: red;">*</span>
                                                    </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <input id="${column.field}" name="${column.field}"   value="${obj[currentValue]}"
                                                           type="text" class="layui-input">
                                                </div>
                                            </div>
                                            <script>
                                                laydate.render({
                                                    elem: '#${column.field}' //指定元素
                                                });
                                            </script>
                                        </c:when>
                                        <c:when test="${column.type=='datetime'}" >
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">${column.title}
                                                    <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                        <span style="color: red;">*</span>
                                                    </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <input id="${column.field}" name="${column.field}"   value="${obj[currentValue]}"
                                                           type="text" class="layui-input">
                                                </div>
                                            </div>
                                            <script>
                                                laydate.render({
                                                    elem: '#${column.field}' //指定元素
                                                    ,type: 'datetime'
                                                });
                                            </script>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="layui-form-item">
                                                <label class="layui-form-label">
                                                        ${column.title}
                                                            <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                                <span style="color: red;">*</span>
                                                            </c:if>
                                                </label>
                                                <div class="layui-input-block">
                                                    <input id="${column.field}" name="${column.field}"
                                                           value="${obj[currentValue]}"
                                                           type="text" class="layui-input"
                                                    <c:if test="${ not empty column.mustFlag && column.mustFlag}">
                                                        lay-verify="required" required=""
                                                    </c:if>

                                                    >
                                                </div>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </c:if>
                            </c:forEach>
                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-filter="btnSubmit" lay-submit="">&emsp;提交&emsp;</button>
                                    <button class="layui-btn layui-btn-primary" type="button" id="backupPage">&emsp;返回&emsp;</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="common/footer.jsp"/>
</div>
<script src="/static/layui/layui.js"></script>
<script>
    layui.use(['element','form','jquery'], function(){
        var element = layui.element;
        var $ = layui.jquery;
        var form = layui.form;

        var FDObj = {
            outId:'${fastdemo_return_to_list_layerId}'
        }

        form.on('submit(btnSubmit)', function (data) {
            var url = "/edit/${layer.id}";

            $.post(url,data.field,function (resp) {

                location.href = "/list/"+FDObj.outId;
            })
            return false;
        });

        $("#backupPage").click(function () {
            location.href = "/list/"+FDObj.outId;
        });
    });
</script>
</body>
</html>