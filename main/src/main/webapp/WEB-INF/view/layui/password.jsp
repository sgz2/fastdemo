<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>${fastdemo_name.shortname}</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/static/fastdemo-layui.css">

    <script src="/static/layui/common/ProjectCommon.js"></script>
    <script src="/static/layui/common/jquery.min.js"></script>
    <script src="/static/layui/common/webuploader/webuploader.js"></script>
    <script src="/static/layui/common/web-upload-object.js?v=0.2"></script>
    <script>
        ProjectCommon.addCtx("/layui");
    </script>
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <jsp:include page="common/header.jsp"/>
    <jsp:include page="common/nav.jsp"/>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div class="layui-nav-title1">
            <blockquote class="layui-elem-quote layui-nav-header">${layer.name}</blockquote>
        </div>
        <div class="layui-nav-content">
            <div class="layui-fluid">
                <div class="layui-card">
                    <div class="layui-card-body">

                        <form action="" class="layui-form add-form">
                            <div class="layui-form-item">
                                <label class="layui-form-label">
                                    旧密码
                                    <span style="color: red;">*</span>
                                </label>
                                <div class="layui-input-block">
                                    <input id="password" name="password"
                                           type="password" class="layui-input"
                                    lay-verify="required" required=""
                                    >
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label class="layui-form-label">
                                    新密码
                                    <span style="color: red;">*</span>
                                </label>
                                <div class="layui-input-block">
                                    <input id="newPassword" name="newPassword"
                                           type="password" class="layui-input"
                                           lay-verify="required" required=""
                                    >
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <label class="layui-form-label">
                                    重复密码
                                    <span style="color: red;">*</span>
                                </label>
                                <div class="layui-input-block">
                                    <input id="reNewPassword" name="reNewPassword"
                                           type="password" class="layui-input"
                                           lay-verify="required|repassword" required=""
                                    >
                                </div>
                            </div>

                            <div class="layui-form-item">
                                <div class="layui-input-block">
                                    <button class="layui-btn" lay-filter="btnSubmit" lay-submit="">&emsp;保存&emsp;</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="common/footer.jsp"/>
</div>
<script src="/static/layui/layui.js"></script>
<script>
    layui.use(['element','form','jquery'], function(){
        var element = layui.element;
        var $ = layui.jquery;
        var form = layui.form;
        form.verify({
            repassword: function (value,item) {
                if(value!=$("#newPassword").val()){
                    return "与新密码不一致";
                }
            }
        });
        form.on('submit(btnSubmit)', function (data) {
            var url = "password";

            $.post(url,data.field,function (resp) {
                if (resp.code == 1) {
                    layer.msg("修改成功");
                }else{
                    layer.msg(resp.msg);
                }
            })
            return false;
        });

    });
</script>
</body>
</html>