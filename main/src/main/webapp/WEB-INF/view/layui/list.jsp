<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>${fastdemo_name.shortname}</title>
    <link rel="stylesheet" href="/static/layui/css/layui.css">
    <link rel="stylesheet" href="/static/fastdemo-layui.css">
</head>
<body class="layui-layout-body">
<div class="layui-layout layui-layout-admin">

    <jsp:include page="common/header.jsp"/>
    <jsp:include page="common/nav.jsp"/>

    <div class="layui-body">
        <!-- 内容主体区域 -->
        <div class="layui-nav-title1">
            <blockquote class="layui-elem-quote layui-nav-header">${layer.name}</blockquote>
        </div>
        <div class="layui-nav-content">
            <div class="layui-fluid">
                <div class="layui-card">
                    <div class="layui-card-body">
                        <div class="layui-form">
                            <div class="layui-form-item">

                                    <c:forEach var="query" items="${fastdemo_queryList}">
                                        <div class="layui-inline">
                                        <c:choose>
                                            <c:when test="${query.type=='select'}">
                                                <select name="${query.field}" id="${query.field}" class="layui-select">
                                                    <option value="">${query.title}</option>
                                                    <c:forEach var="kv" items="${query.kvMap}">
                                                        <option value="${kv.key}" >${kv.value}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:when>
                                            <c:when test="${query.type=='fk-select'}">
                                                <select name="${query.field}" id="${query.field}" class="layui-select">
                                                    <option value="">${query.title}</option>
                                                    <c:forEach var="kv" items="${query.kvList}">
                                                        <option value="${kv[query.kvKey]}"  >${kv[query.kvValue]}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:when>
                                            <c:otherwise>
                                                <input type="text" placeholder="${query.title}" id="${query.field}" class="layui-input">
                                            </c:otherwise>
                                        </c:choose>
                                        </div>
                                    </c:forEach>
                                <div class="layui-inline">
                                    <c:if test="${not empty fastdemo_queryList && fn:length(fastdemo_queryList)>0}">
                                        <button class="layui-btn" id="searchBtn">搜索</button>
                                    </c:if>

                                    <c:forEach var="linkDTO" items="${fastdemo_links}">
                                        <c:if test="${linkDTO.layer.type=='create'}">
                                            <button class="layui-btn" fd-layer-id="${linkDTO.layer.id}" id="addBtn">${linkDTO.layerLink.title}</button>
                                        </c:if>
                                    </c:forEach>

                                </div>
                            </div>
                        </div>

                        <table id="demoTable" lay-filter="demoTable"></table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <jsp:include page="common/footer.jsp"/>
</div>


<script type="text/html" id="tableBar">
<c:forEach items="${fastdemo_rowLinks}" var="row">
    <a class="layui-btn layui-btn-${row.layerLink.cssClass} layui-btn-xs" id="fd-${row.layerLink.parameters}" fd-layer-id="${row.layer.id}" lay-event="${row.layerLink.parameters}">${row.layerLink.title}</a>
</c:forEach>

</script>

<script src="/static/layui/layui.js"></script>
<script>
    //JavaScript代码区域
    layui.use(['element','table','jquery'], function(){
        var $ = layui.jquery;
        var table = layui.table;

        var FDObj = {
            tableId:"demoTable",
            outId:'${fastdemo_layerId}'
        }

        FDObj.initColumn = function () {
            return[[
                {checkbox:true}
            <c:forEach var="column" items="${columnList}">
                <c:choose>
                <c:when test="${not empty column.hiddenFlag && column.hiddenFlag}">

                </c:when>
                    <c:when test="${column.type=='img'}">
                ,{title: '${column.title}', width:'${column.width==null?80:column.width}'
                    ,templet: function (d) {
                        console.log(d);
                        return '<img src="'+d.${column.field}+'" width="80" height="80" />';
                    }}
                    </c:when>

                <c:when test="${column.type=='select' || column.type=='radio'}">
                ,{title: '${column.title}', width:'${column.width==null?80:column.width}'
                    ,templet: function (d) {
                        var obj = {};
                        <c:forEach var="kv" items="${column.kvMap}">
                        obj['${kv.key}'] = '${kv.value}';
                        </c:forEach>
                        return obj[d.${column.field}];
                    }}
                </c:when>
                <c:when test="${column.type=='fk-select'}">
                ,{title: '${column.title}', width:'${column.width==null?80:column.width}'
                    ,templet: function (d) {
                        var obj = {};
                        <c:forEach var="kv" items="${column.kvList}">
                        obj['${kv[column.kvKey]}'] = '${kv[column.kvValue]}';
                        </c:forEach>
                        var value = obj[d.${column.field}];
                        return !!value ? value : '-';

                    }}
                </c:when>
                    <c:otherwise>
                        ,{field: '${column.field}', title: '${column.title}', width:'${column.width==null?80:column.width}'}
                    </c:otherwise>
                </c:choose>
            </c:forEach>
                <c:if test="${fn:length(fastdemo_rowLinks)>0 }">
                , {align: 'center', toolbar: '#tableBar', title: '操作', minWidth: 200}
                </c:if>

            ]]
        }


        FDObj.search = function () {
            var queryData = {};
            <c:forEach var="query" items="${fastdemo_queryList}">
            queryData['${query.field}'] = $("#${query.field}").val();
            </c:forEach>
            table.reload(FDObj.tableId, {where: queryData});
        };

        /**
         * 弹出添加
         */

        FDObj.openAddFDObj = function (id) {

            location.href = "/add/"+id
        };

        FDObj.onEditFDObj = function (data,layerId) {
            var oid = data.id;
            location.href = "/edit/"+layerId+"?oid="+oid;
        };

        FDObj.onDeleteFDObj = function (data,layerId) {
            var url = "/delete/"+layerId
            var oid = data.id;
            $.post(url, {'oid':oid}, function (resp) {
                location.reload();
            });
        };

        //第一个实例
        table.render({
            elem: '#'+FDObj.tableId
            ,method: 'post'
            ,height: 500
            ,url: '/list/${fastdemo_layerId}' //数据接口
            ,page: true //开启分页
            ,cols: FDObj.initColumn()
        });


        /*新增*/
        $("#addBtn").click(function () {
            FDObj.openAddFDObj($(this).attr("fd-layer-id"));
        });

        $("#searchBtn").click(FDObj.search);

        /*修改 删除*/
        table.on('tool(' + FDObj.tableId + ')', function (obj) {
            var data = obj.data;
            var layEvent = obj.event;

            if (layEvent === 'edit') {
                FDObj.onEditFDObj(data,$("#fd-edit").attr("fd-layer-id"));
            } else if (layEvent === 'delete') {
                FDObj.onDeleteFDObj(data,$("#fd-delete").attr("fd-layer-id"));
            } 
        });
    });
</script>
</body>
</html>