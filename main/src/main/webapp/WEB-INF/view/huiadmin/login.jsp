<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <meta charset="utf-8">
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
    <meta http-equiv="Cache-Control" content="no-siteapp" />

    <link href="/static/huiadmin/app/css/H-ui.min.css" rel="stylesheet" type="text/css" />
    <link href="/static/huiadmin/app/css/H-ui.login.css" rel="stylesheet" type="text/css" />
    <link href="/static/huiadmin/app/css/style.css" rel="stylesheet" type="text/css" />
    <link href="/static/huiadmin/app/css/iconfont.css" rel="stylesheet" type="text/css" />

    <title>后台登录 - ${seo.mainName}</title>
    <style>
        #loginform{
            width: 40%;
            padding: 20px;
            background-color: rgba(0,0,0,0.5);
        }
    </style>
</head>
<body class="childrenBody">
<div class="header"></div>
<div class="loginWraper">
    <div id="loginform" class="loginBox">
        <form class="form form-horizontal" action="/login" onsubmit="return login();" method="post">
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60d;</i></label>
                <div class="formControls col-xs-8">
                    <input id="username" name="username" type="text" placeholder="账户" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">
                <label class="form-label col-xs-3"><i class="Hui-iconfont">&#xe60e;</i></label>
                <div class="formControls col-xs-8">
                    <input id="password" name="password" type="password" placeholder="密码" class="input-text size-L">
                </div>
            </div>
            <div class="row cl">

            </div>
            <div class="row cl" style="color:white;">
                <div class="formControls col-xs-8 col-xs-offset-3">

                    <c:if test="${not empty layers && fn:length(layers)>=2}">
                        <c:forEach items="${layers}" var="layer" varStatus="status">
                            <div class="radio-box">
                                <input type="radio" id="role" ${status.index==0?"checked='checked'":""} name="role" value="${layer.roleCode}">
                                <label style="color: #fff;" for="${layer.roleCode}">${layer.name}</label>
                            </div>
                        </c:forEach>
                    </c:if>
                </div>
            </div>
            <div class="row cl">
                <div class="formControls col-xs-8 col-xs-offset-3">
                    <button type="submit" class="btn btn-success radius size-L">&nbsp;登&nbsp;&nbsp;&nbsp;&nbsp;录&nbsp;</button>
                    <button type="button" onclick="register();" class="btn btn-default radius size-L">&nbsp;注&nbsp;&nbsp;&nbsp;&nbsp;册&nbsp;</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="footer">Copyright 你的公司名称 by ${seo.mainName} v1.0</div>

<script type="text/javascript" src="/static/plugins/jquery/jquery.min.js"></script>
<script type="text/javascript" src="/static/plugins/layer/layer.js"></script>
<script type="text/javascript" src="/static/plugins/h-ui/js/H-ui.min.js"></script>
<script>
    function login() {
        var data = {
            "username":$("#username").val(),
            "password":$("#password").val(),
            "role":$("#role").val(),
        }
        var url = "/login";
        $.post(url, data, function (resp) {
            if(resp.code==1){
                location.href = "index";
            }else{
                layer.msg(resp.msg);
            }
        });
        return false;
    }
    
    function register() {
        location.href = "register";
    }
</script>
</body>
</html>
