<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib tagdir="/WEB-INF/tags/huiadmin" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <jsp:include page="common/meta.jsp"/>
    <title>公告新增 - ${seo.mainName}</title>
</head>
<body class="childrenBody">
<article class="page-container">
    <div class="form form-horizontal" id="productForm">

        <c:forEach var="column" items="${columnList}" varStatus="status">
            <form:input required="true" id="${column.field}" name="${column.title}"/>
        </c:forEach>

        <form:submit>
            <form:button btnCss="primary" name="提交" id="ensure" icon="fa-check" onclick="InfoDlg.addSubmit()"/>
            <form:button btnCss="danger" name="取消" id="cancel" icon="fa-eraser" onclick="InfoDlg.close()"/>
        </form:submit>
    </div>
</article>
<jsp:include page="common/footer.jsp"/>
<script>
    /**
     * 初始化详情对话框
     */
    var InfoDlg = {
        productInfoData : {},
        formId: "productForm"
    };

    /**
     * 清除数据
     */
    InfoDlg.clearData = function() {
        this.productInfoData = {};
    }

    /**
     * 设置对话框中的数据
     *
     * @param key 数据的名称
     * @param val 数据的具体值
     */
    InfoDlg.set = function(key, val) {
        this.productInfoData[key] = (typeof val == "undefined") ? $("#" + key).val() : val;
        return this;
    }

    /**
     * 设置对话框中的数据
     *
     * @param key 数据的名称
     * @param val 数据的具体值
     */
    InfoDlg.get = function(key) {
        return $("#" + key).val();
    }

    /**
     * 关闭此对话框
     */
    InfoDlg.close = function() {
        parent.layer.close(window.parent.LocalObject.layerIndex);
    }

    /**
     * 收集数据
     */
    InfoDlg.collectData = function() {
        this
        <c:forEach var="column" items="${columnList}">
            .set('${column.field}')
        </c:forEach>

    }

    InfoDlg.validate = function () {
        $('#'+this.formId).data("bootstrapValidator").resetForm();
        $('#'+this.formId).bootstrapValidator('validate');
        return $('#'+this.formId).data('bootstrapValidator').isValid();
    }

    /**
     * 提交添加
     */
    InfoDlg.addSubmit = function() {

        this.clearData();
        this.collectData();

        if(!this.validate()){
            return;
        }

        //提交信息
        $.post("/add/${layer.id}", this.productInfoData ,function(data){
            if(data.code==1){
                BBQ.success("添加成功!");
                window.parent.LocalObject.table.refresh();
                InfoDlg.close();
            }else{
                BBQ.info(data.msg);
            }
        });
    }

    $(function() {

        var validSetting = {};
        <c:forEach var="column" items="${columnList}">
        <c:if test="${column.mustFlag}">
        BBQ.notEmpty(validSetting, "${column.field}");
        </c:if>
        </c:forEach>
        BBQ.initValidator(InfoDlg.formId, validSetting);
    });

</script>
</body>
</html>