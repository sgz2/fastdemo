<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <title>${layer.name} - ${seo.mainName}</title>
    <jsp:include page="common/meta.jsp"/>
</head>
<body>
<nav class="breadcrumb">
    <i class="Hui-iconfont">&#xe67f;</i> 首页
    <span class="c-gray en">&gt;</span>${layer.name}
    <a class="btn btn-success radius r" style="line-height:1.6em;margin-top:3px"
       href="javascript:location.replace(location.href);" title="刷新" >
        <i class="Hui-iconfont">&#xe68f;</i></a>
</nav>
<div class="page-container">
    <div class="text-c">
    </div>
    <div class="cl pd-5 bg-1 bk-gray mt-20">
        <span class="l">
            <button class="btn btn-primary" type="button" onclick="LocalObject.openAdd()">新增</button>
            <button class="btn btn-primary"  type="button" onclick="LocalObject.openDetail()">修改</button>
            <button class="btn btn-primary"  type="button" onclick="LocalObject.delete()">删除</button>
        </span>
    </div>
    <div class="mt-20">
        <table class="table table-border table-bordered table-hover table-bg table-sort" id="LocalObjectTable"></table>
    </div>
</div>


<jsp:include page="common/footer.jsp"/>
<script>
    /**
     * 管理初始化
     */
    var LocalObject = {
        id: "LocalObjectTable",	//表格id
        seItem: null,		//选中的条目
        table: null,
        layerIndex: -1
    };
    /**
     * 初始化表格的列
     */
    LocalObject.initColumn = function () {
        return [
            {field: 'selectItem', radio: true}
            <c:forEach var="column" items="${columnList}">
            <c:choose>
            <c:when test="${not empty column.hiddenFlag && column.hiddenFlag}">
            </c:when>

            <c:when test="${column.type=='img'}">
            ,{title: '${column.title}', width:'${column.width==null?80:column.width}'
                ,formatter: function (value) {
                    return '<img src="/'+value+'" width="80" height="80" />';
                }}
            </c:when>
            <c:otherwise>
            ,{title: '${column.title}', field: '${column.field}', visible: true, align: 'center', valign: 'middle',width:'${column.width==null?80:column.width}'}
            </c:otherwise>
            </c:choose>
            </c:forEach>
        ];
    };


    /**
     * 检查是否选中
     */
    LocalObject.check = function () {
        var selected = $('#' + this.id).bootstrapTable('getSelections');
        if(selected.length == 0){
            BBQ.info("请先选中表格中的某一记录！");
            return false;
        }else{
            LocalObject.seItem = selected[0];
            return true;
        }
    };
    /**
     * 点击添加
     */
    LocalObject.openAdd = function () {
        var index = layer.open({
            type: 2,
            title: '添加',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: "/add/${fastdemo_linkMap['link_to_add'].linkLayerId}"
        });
        this.layerIndex = index;
    };
    /**
     * 打开查看详情
     */
    LocalObject.openDetail = function () {
        if (this.check()) {
            var index = layer.open({
                type: 2,
                title: '详情',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content:  '/edit/${fastdemo_tagMap['row_to_update'].linkLayerId}?oid=' + LocalObject.seItem.id
            });
            this.layerIndex = index;
        }
    };
    /**
     * 删除
     */
    LocalObject.delete = function () {
        if (this.check()) {
            $.post("/delete/${fastdemo_tagMap['row_to_delete'].linkLayerId}",{'oid':this.seItem.id}, function (data) {
                if(data.code==1){
                    BBQ.success("删除成功!");
                    LocalObject.table.refresh();
                }else{
                    BBQ.info(data.msg);
                }
            });
        }
    };
    /**
     * 查询列表
     */
    LocalObject.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        LocalObject.table.refresh({query: queryData});
    };

    $(function () {
        var defaultColunms = LocalObject.initColumn();
        var table = new BSTable(LocalObject.id, "/list/${layer.id}", defaultColunms);
        table.setPaginationType("server");
        LocalObject.table = table.init();
    });

</script>
</body>
</html>