<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />

<link rel="stylesheet" href="/static/plugins/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/plugins/bootstrap-table/dist/bootstrap-table.min.css">
<link rel="stylesheet" type="text/css" href="/static/plugins/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="/static/plugins/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="/static/plugins/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="/static/plugins/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="/static/plugins/h-ui.admin/css/style.css" />

<script src="/static/plugins/jquery/jquery.min.js"></script>
<style>
    .fixed-table-container {
        height: auto !important;
        border-top: none;
        border-bottom: none;
        padding-bottom: 0!important;
    }
</style>
