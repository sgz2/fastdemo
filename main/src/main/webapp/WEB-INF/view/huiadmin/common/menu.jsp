<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<aside class="Hui-aside">
    <div class="menu_dropdown bk_2">
        <c:forEach items="${fastdemoMenuList}" var="menuDto">
            <c:choose>
                <c:when test="${fn:length(menuDto.childs)>0}">
                    <dl id="menu-member">
                        <dt><i class="Hui-iconfont">&#xe60d;</i> ${menuDto.menu.name}<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
                        <dd>
                            <ul>
                            <c:forEach items="${menuDto.childs}" var="child">
                                <li><a data-href="/layer/${child.menu.layerId}" data-title="${child.menu.name}" href="javascript:;">${child.menu.name}</a></li>
                            </c:forEach>
                            </ul>
                        </dd>
                    </dl>
                </c:when>
                <c:otherwise>
                    <dl id="menu-member">
                        <dt><i class="Hui-iconfont">&#xe60d;</i> <a data-href="/layer/${menuDto.menu.layerId}" data-title="${menuDto.menu.name}" href="javascript:;">${menuDto.menu.name}</a></dt>
                    </dl>
                </c:otherwise>
            </c:choose>

        </c:forEach>
    </div>
</aside>
