<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!--_footer 作为公共模版分离出去-->
<script src="/static/plugins/h-ui/js/H-ui.js"></script>
<script src="/static/plugins/h-ui.admin/js/H-ui.admin.js"></script>

<script src="/static/plugins/webuploader/webuploader.min.js"></script>
<script src="/static/plugins/bootstrap-table/1.12.1/bootstrap-table.js"></script>
<script src="/static/plugins/bootstrap-table/1.12.1/extensions/export/bootstrap-table-export.min.js"></script>
<script src="/static/plugins/bootstrap-table/1.12.1/locale/bootstrap-table-zh-CN.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>
<script src="/static/plugins/laydate/laydate.js"></script>
<script src="/static/plugins/validate/bootstrapValidator.min.js"></script>
<script src="/static/plugins/validate/zh_CN.js"></script>
<script src="/static/huiadmin/app/js/bootstrap-table-object.js?v=0.5"></script>
<script src="/static/huiadmin/app/js/web-upload-object.js?v=0.132"></script>
<script src="/static/huiadmin/app/js/BBQ.js?v=0.4"></script>

<!--/_footer 作为公共模版分离出去-->