<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>${seo.mainName} Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/static/plugins/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/static/plugins/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/static/plugins/Ionicons/css/ionicons.min.css">
    <link rel="stylesheet" href="/static/plugins/adminlte/dist/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/static/plugins/iCheck/square/blue.css">

</head>
<body class="hold-transition login-page" style="background: #d2d6de">
<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>${seo.mainName}</b></a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">${msg}</p>

        <form action="/login" id="loginForm" onsubmit="return login();" method="post">
            <div class="form-group has-feedback">
                <input type="text" id="username" name="username" class="form-control" placeholder="编号">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" id="password" name="password" class="form-control" placeholder="密码">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>

            <div class="row">
                <div class="col-xs-8">
                    <c:if test="${not empty layers && fn:length(layers)>=2}">
                        <select class="form-control" name="role" id="role">
                                <c:forEach items="${layers}" var="layer" varStatus="status">
                                    <option value="${layer.roleCode}">${layer.name}</option>
                                </c:forEach>
                        </select>
                    </c:if>
                </div>
                <!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">登录</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="/static/plugins/jquery/dist/jquery.min.js"></script>
<script src="/static/plugins/layer/layer.js"></script>
<script src="/static/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/static/plugins/validate/bootstrapValidator.min.js"></script>
<script src="/static/plugins/validate/zh_CN.js"></script>

<script>
    function login() {
        var data = {
            "username":$("#username").val(),
            "password":$("#password").val(),
            "role":$("#role").val(),
        }
        var url = "/login";
        $.post(url, data, function (resp) {
            if(resp.code==1){
                location.href = "index";
            }else{
                layer.msg(resp.msg);
            }
        });
        return false;
    }
    var validateFields = {
        username: {
            validators: {
                notEmpty: {
                    message: '不能为空'
                }
            }
        },
        password: {
            validators: {
                stringLength: {
                    min: 6,
                    max: 20,
                    message: '密码长度6-20！'
                },
                notEmpty: {
                    message: '不能为空'
                }
            }
        },
    }

    $('#loginForm').bootstrapValidator({
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: validateFields,
        live: 'enabled',
        message: '该字段不能为空'
    });
</script>
</body>
</html>

