<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<link rel="stylesheet" href="/static/plugins/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" href="/static/plugins/bootstrap-table/dist/bootstrap-table.min.css">
<link rel="stylesheet" href="/static/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/static/plugins/Ionicons/css/ionicons.min.css">
<link rel="stylesheet" href="/static/plugins/adminlte/dist/css/AdminLTE.min.css">
<link rel="stylesheet" href="/static/plugins/adminlte/dist/css/skins/_all-skins.min.css">
<link rel="stylesheet" href="/static/plugins/webuploader/webuploader.css?v=0.1">
<link rel="stylesheet" href="/static/plugins/laydate/theme/default/laydate.css?v=0.1">
<link rel="stylesheet" href="/static/adminlte/app/css/style.css?v=0.1">
<script src="/static/plugins/jquery/dist/jquery.min.js"></script>
