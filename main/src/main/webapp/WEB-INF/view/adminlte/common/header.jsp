<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header class="main-header">
    <!-- Logo -->
    <a href="/" class="logo">
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>${seo.mainName}</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="/static/app/images/bbq.png" class="user-image" alt="User Image">
                        <span class="hidden-xs">${loginUser.showname}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="/static/app/images/bbq.png" class="img-circle" alt="User Image">
                            <p>
                                <small></small>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="/profile" class="J_menuItem btn btn-default btn-flat">个人资料</a>
                            </div>
                            <div class="pull-right">
                                <a href="/password" class="J_menuItem btn btn-default btn-flat">修改密码</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>


<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <ul class="sidebar-menu" data-widget="tree">
            <li class="header">MAIN NAVIGATION</li>
            <c:forEach items="${fastdemoMenuList}" var="menuDto">
                <c:choose>
                    <c:when test="${fn:length(menuDto.childs)>0}">
                        <li class="layui-nav-item layui-nav-itemed">
                            <a class="" href="javascript:;">${menuDto.menu.name}</a>
                            <dl class="layui-nav-child">
                                <c:forEach items="${menuDto.childs}" var="child">
                                    <dd><a href="/layer/${child.menu.layerId}">${child.menu.name}</a></dd>
                                </c:forEach>

                            </dl>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li><a class="J_menuItem" href="/layer/${menuDto.menu.layerId}"> <i class="fa fa-dashboard"></i> <span>${menuDto.menu.name}</span></a></li>
                    </c:otherwise>
                </c:choose>

            </c:forEach>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>
