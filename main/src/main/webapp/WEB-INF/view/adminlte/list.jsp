<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>FastDemo - adminlte</title>
    <jsp:include page="common/link.jsp"/>
</head>
<body class="hold-transition skin-blue sidebar-mini">
<section class="content">
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">${layer.name}</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div id="LocalObjectTableToolbar">
                    <c:if test="${fn:length(fastdemo_rowLinks)>0 }">
                        <button class="btn btn-primary" type="button" onclick="LocalObject.openAdd()">新增</button>
                        <c:if test="${not empty fastdemo_tagMap['row_to_update']}">
                            <button class="btn btn-primary"  type="button" onclick="LocalObject.openDetail()">修改</button>
                        </c:if>
                        <c:if test="${not empty fastdemo_tagMap['row_to_delete']}">
                            <button class="btn btn-primary"  type="button" onclick="LocalObject.delete()">删除</button>
                        </c:if>

                    </c:if>
                     </div>
                    <table id="LocalObjectTable"></table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->
</section>
<!-- Site wrapper -->

<jsp:include page="common/script.jsp"/>
<script>
    /**
     * 管理初始化
     */
    var LocalObject = {
        id: "LocalObjectTable",	//表格id
        seItem: null,		//选中的条目
        table: null,
        layerIndex: -1
    };
    /**
     * 初始化表格的列
     */
    LocalObject.initColumn = function () {
        return [
            {field: 'selectItem', radio: true}
            <c:forEach var="column" items="${columnList}">
                <c:choose>
            <c:when test="${not empty column.hiddenFlag && column.hiddenFlag}">
            </c:when>

                <c:when test="${column.type=='img'}">
                ,{title: '${column.title}', width:'${column.width==null?80:column.width}'
                    ,formatter: function (value) {
                        return '<img src="/'+value+'" width="80" height="80" />';
                    }}
                </c:when>
                <c:otherwise>
                    ,{title: '${column.title}', field: '${column.field}', visible: true, align: 'center', valign: 'middle',width:'${column.width==null?80:column.width}'}
                </c:otherwise>
                </c:choose>
            </c:forEach>
        ];
    };


    /**
     * 检查是否选中
     */
    LocalObject.check = function () {
        var selected = $('#' + this.id).bootstrapTable('getSelections');
        if(selected.length == 0){
            BBQ.info("请先选中表格中的某一记录！");
            return false;
        }else{
            LocalObject.seItem = selected[0];
            return true;
        }
    };
    /**
     * 点击添加
     */
    LocalObject.openAdd = function () {
        var index = layer.open({
            type: 2,
            title: '添加',
            area: ['800px', '420px'], //宽高
            fix: false, //不固定
            maxmin: true,
            content: "/add/${fastdemo_linkMap['link_to_add'].linkLayerId}"
        });
        this.layerIndex = index;
    };
    /**
     * 打开查看详情
     */
    LocalObject.openDetail = function () {
        if (this.check()) {
            var index = layer.open({
                type: 2,
                title: '详情',
                area: ['800px', '420px'], //宽高
                fix: false, //不固定
                maxmin: true,
                content:  '/edit/${fastdemo_tagMap['row_to_update'].linkLayerId}?oid=' + LocalObject.seItem.id
            });
            this.layerIndex = index;
        }
    };
    /**
     * 删除
     */
    LocalObject.delete = function () {
        if (this.check()) {
            $.post("/delete/${fastdemo_tagMap['row_to_delete'].linkLayerId}",{'oid':this.seItem.id}, function (data) {
                if(data.code==1){
                    BBQ.success("删除成功!");
                    LocalObject.table.refresh();
                }else{
                    BBQ.info(data.msg);
                }
            });
        }
    };
    /**
     * 查询列表
     */
    LocalObject.search = function () {
        var queryData = {};
        queryData['condition'] = $("#condition").val();
        LocalObject.table.refresh({query: queryData});
    };

    $(function () {
        var defaultColunms = LocalObject.initColumn();
        var table = new BSTable(LocalObject.id, "/list/${layer.id}", defaultColunms);
        table.setPaginationType("server");
        LocalObject.table = table.init();
    });

</script>
</body>
</html>