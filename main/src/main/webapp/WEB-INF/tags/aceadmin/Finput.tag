<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="id"  rtexprvalue="true" %>
<%@ attribute name="name"  rtexprvalue="true" %>
<%@ attribute name="value"  rtexprvalue="true" %>
<%@ attribute name="style" rtexprvalue="true" %>
<%@ attribute name="type" rtexprvalue="true" %>
<h2>${name}</h2>
<label>
    <input class="txt_input txt_input2" id="${id}" name="${id}" value="${value}" type="${not empty type?type:'text'}"
            <c:if test="${not empty style}">
                style="${style}"
            </c:if>
    />
</label>