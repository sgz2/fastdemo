<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="id"  rtexprvalue="true" %>
<%@ attribute name="name"  rtexprvalue="true" %>
<%@ attribute name="value" rtexprvalue="true" %>
<div class="row cl">
    <label class="form-label col-xs-4 col-sm-3">${name}：</label>
    <div class="col-sm-4">
        <div id="${id}PreId">
            <div>
                <img width="100px" height="100px"
                <c:if test="${empty value}">
                     src="/static/app/images/default.gif">
                </c:if>
                <c:if test="${not empty value}">
                    src="/${value}"  >
                </c:if>
            </div>
        </div>
    </div>
</div>