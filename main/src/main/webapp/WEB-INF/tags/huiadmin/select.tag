<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="id"  rtexprvalue="true" %>
<%@ attribute name="name"  rtexprvalue="true" %>
<%@ attribute name="options"  rtexprvalue="true" type="java.util.List" %>
<%@ attribute name="value"  rtexprvalue="true" %>
<%@ attribute name="readonly"  rtexprvalue="true" %>
<%@ attribute name="style" rtexprvalue="true" %>
<%@ attribute name="disabled" rtexprvalue="true" %>
<%@ attribute name="underline" rtexprvalue="true" %>
<%@ attribute name="required" rtexprvalue="false" %>
<div class="row cl">
    <label class="form-label col-xs-4 col-sm-3">
        <c:if test="${not empty required}">
            <span class="c-red">*</span>
        </c:if>
        ${name}：</label>
    <div class=" col-xs-8 col-sm-9">
        <div class="formControls">
        <select class="form-control" id="${id}" name="${id}" value="${value}"
                <c:if test="${not empty readonly}">
                    readonly="${readonly}"
                </c:if>
                <c:if test="${not empty style}">
                    style="${style}"
                </c:if>
                <c:if test="${not empty disabled}">
                    disabled="${disabled}"
                </c:if>
        >
            <jsp:doBody />
            <c:if test="${not empty options}">
                <c:forEach items="${options}" var="itemObjet">
                    <option value="${itemObjet.ikey}">${itemObjet.ivalue}</option>
                </c:forEach>
            </c:if>
        </select>
    </div>
    </div>
</div>