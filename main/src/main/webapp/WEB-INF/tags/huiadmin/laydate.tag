<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ tag pageEncoding="UTF-8" %>
<%@ attribute name="id"  rtexprvalue="true" %>
<%@ attribute name="name"  rtexprvalue="true" %>
<%@ attribute name="value"  rtexprvalue="true" type="java.util.Date" %>
<%@ attribute name="readonly"  rtexprvalue="true" %>
<%@ attribute name="style" rtexprvalue="true" %>
<%@ attribute name="type" rtexprvalue="true" %>
<%@ attribute name="disabled" rtexprvalue="true" %>
<%@ attribute name="underline" rtexprvalue="true" %>
<%@ attribute name="required" rtexprvalue="false" %>
<div class="row cl">
    <label class="form-label col-xs-4 col-sm-3">
        <c:if test="${not empty required}">
            <span class="c-red">*</span>
        </c:if>
        ${name}：</label>
    <div class="formControls col-xs-8 col-sm-9">
        <input class="form-control" id="${id}" name="${id}" value="<fmt:formatDate value="${value}" type="both"/>" type="${not empty type?type:'text'}"
        <c:if test="${not empty readonly}">
               readonly="${readonly}"
        </c:if>
        <c:if test="${not empty style}">
               style="${style}"
        </c:if>
        <c:if test="${not empty disabled}">
               disabled="${disabled}"
        </c:if>
        >
    </div>
</div>
<script>
$(document).ready(function () {
    laydate.render({
        elem: '#${id}'
    });
});
</script>