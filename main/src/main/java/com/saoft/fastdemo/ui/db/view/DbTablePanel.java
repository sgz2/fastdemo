package com.saoft.fastdemo.ui.db.view;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.db.model.JTableInfoModel;
import com.saoft.fastdemo.ui.db.model.TableListModel;
import com.saoft.fastdemo.ui.shared.ButtonUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component("DbTablePanel")
public class DbTablePanel extends JPanel {

    private JList<TableInfo> tableInfoJList;

    JTableInfoModel tableInfoModel;

    private JButton mf = new JButton("添加");
    private JButton edit = new JButton("编辑");
    private JButton delete = new JButton("删除");
    private JButton local = new JButton("本地导入");
    private JButton remote = new JButton("远程添加");


    @Autowired
    DbTablePanel(JTableInfoModel tableInfoModel){
        this.tableInfoModel = tableInfoModel;
        initComponents();
        after();
    }

    void after(){
        ButtonUtil.convertIconButton(mf,ButtonUtil.ADD_TABLE);
        ButtonUtil.convertIconButton(edit,ButtonUtil.EDIT_TABLE);
        ButtonUtil.convertIconButton(delete,ButtonUtil.DELETE_TABLE);
        ButtonUtil.convertIconButton(local,ButtonUtil.IMPORT_TABLE);
    }

    private void initComponents() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        //添加表
        JPanel tool = new JPanel();
        tool.setLayout(new BoxLayout(tool, BoxLayout.X_AXIS));

        tool.add(mf);
        tool.add(edit);
        tool.add(delete);
        tool.add(local);
//        tool.add(remote);

        add(tool);


        //数据表列表
        tableInfoJList = new JList(tableInfoModel);
        tableInfoJList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        tableInfoJList.setModel(new TableListModel());
        JScrollPane paneWithTable = new JScrollPane(tableInfoJList);
        add(paneWithTable, BorderLayout.CENTER);
    }



    public JButton getLocaleBtn() {
        return local;
    }

    public JList<TableInfo> getTableInfoJList() {
        return tableInfoJList;
    }

    public JButton getMf() {
        return mf;
    }

    public JButton getEdit() {
        return edit;
    }

    public JButton getDelete() {
        return delete;
    }

    public JButton getLocal() {
        return local;
    }

    public JButton getRemote() {
        return remote;
    }
}
