package com.saoft.fastdemo.ui.main;

import com.saoft.fastdemo.util.CheckProjUtil;
import com.saoft.fastdemo.system.ChooserUtil;
import com.saoft.fastdemo.system.FastdemoProperties;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.db.DBTableController;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.PrefUtil;
import com.saoft.fastdemo.ui.main.about.About;
import com.saoft.fastdemo.ui.main.export.ExportFrame;
import com.saoft.fastdemo.ui.main.export.ExportFrameController;
import com.saoft.fastdemo.ui.main.login.LoginFrame;
import com.saoft.fastdemo.ui.main.title.TitleSettingFrame;
import com.saoft.fastdemo.ui.main.view.*;
import com.saoft.fastdemo.ui.main.view.Action;
import com.saoft.fastdemo.ui.main.view.Menu;
import com.saoft.fastdemo.ui.outline.OutlineController;
import com.saoft.fastdemo.ui.preview.view.PreviewPanel;
import com.saoft.fastdemo.ui.shared.controller.AbstractPanelController;
import com.saoft.fastdemo.util.BrowseUtil;
import com.saoft.fastdemo.util.VersionUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.io.File;

@Component("TopController")
public class TopController extends AbstractPanelController {

    @Value("${server.port}")
    private Integer port;

    @Autowired
    DBTableController dbTableController;

    @Autowired
    OutlineController outlineController;

    @Autowired
    PreferencesUtil preferencesUtil;

    private JPanel jPanel;

    @Autowired
    private Action action;

    @Autowired
    Menu menu;

    @Autowired
    LoginFrame loginFrame;

    @Autowired
    TitleSettingFrame titleSettingFrame;

    @PostConstruct
    void init(){
        jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.add(menu);
        jPanel.add(new JSeparator());
        jPanel.add(action);

        menu.getRefresh().addActionListener(e -> refresh());
        menu.getOpen().addActionListener(e -> open());
        menu.getNewCreate().addActionListener(e -> newCreate());
        menu.getOtherSave().addActionListener(e -> otherSave());
        menu.getExportSetting().addActionListener(e -> export());
        menu.getHelp().addActionListener(e -> help());
        menu.getAbout().addActionListener(e -> about());
        menu.getTitleSetting().addActionListener(e -> titleSetting());

        menu.getLogin().addActionListener(e -> login());
        menu.getLogout().addActionListener(e -> logout());
        menu.getRegister().addActionListener(e -> regsiter());

        action.getBrower().addActionListener(e -> bowerBtn());
        action.getRefresh().addActionListener(e -> refresh());
        action.getExport().addActionListener(e -> export());

        titleSettingFrame.getOkButton().addActionListener(e -> saveTitleSetting());
        titleSettingFrame.getCancelButton().addActionListener(e -> titleSettingFrame.setVisible(false));
    }

    //标题设置面板
    private void titleSetting() {
        //加载数据
        String s = PrefUtil.get(PrefUtil.title_full_name);
        String shortname = PrefUtil.get(PrefUtil.title_short_name);
        String footer = PrefUtil.get(PrefUtil.title_footer);

        if (s != null) {
            titleSettingFrame.getFull_name().setText(s);
        }

        if (shortname != null) {
            titleSettingFrame.getShort_name().setText(shortname);
        }

        if (footer != null) {
            titleSettingFrame.getFooter().setText(footer);
        }

        titleSettingFrame.setVisible(true);
    }

    //保存设置
    private void saveTitleSetting(){
        String text = titleSettingFrame.getFull_name().getText();
        String shortname = titleSettingFrame.getShort_name().getText();
        String footer = titleSettingFrame.getFooter().getText();

        PrefUtil.set(PrefUtil.title_full_name,text);
        PrefUtil.set(PrefUtil.title_short_name,shortname);
        PrefUtil.set(PrefUtil.title_footer,footer);

        ConsoleController.log("标题设置成功！");

        titleSettingFrame.setVisible(false);
    }

    private void about() {

        About about = S.getBean(About.class);
        about.setVisible(true);
        //查询最新版本
        String serverVersion = VersionUtil.getServerVersion();
        about.getServer_version().setText(serverVersion);
    }

    @Override
    public JPanel getPanel() {
        return jPanel;
    }

    private void help(){
        S.getBean(HelpFrame.class).setVisible(true);
    }

    private void export(){
        S.getBean(ExportFrame.class).setVisible(true);
    }

    public void render_online(){
        menu.render_online();
    }

    private void logout() {
        menu.render_login();
    }

    private void login(){
        loginFrame.setVisible(true);
    }

    private void regsiter(){
        FastdemoProperties bean = S.getBean(FastdemoProperties.class);
        String apiServer = bean.getApiServer();
        String url = apiServer+"/register";
        BrowseUtil.openUrl(url);
    }

    /**
     * 浏览器打开
     */
    void bowerBtn(){
        String text = "http://localhost:"+port;
        BrowseUtil.openUrl(text);
    }

    void open(){
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        ChooserUtil.acceptFd(chooser);
        chooser.showOpenDialog(S.getBean(MainFrame.class));
        File file = chooser.getSelectedFile();
        if (file != null) {
            String path = file.getAbsolutePath();
            String s = path.replaceAll("\\\\", "/");
            String dbUrl = CheckProjUtil.getH2Url(s);
            S.replaceDataSource(dbUrl);
            //设置临时变量
            preferencesUtil.setH2File(s);
            //刷新界面
            refresh();
        }
    }

    void otherSave(){
        JOptionPane.showMessageDialog(null,"未实现");
    }

    void newCreate(){

        S.getBean(NewProjectFrame.class).setVisible(true);
    }

    public void refresh(){
        dbTableController.initByDb();
        outlineController.initByDb();

        S.getBean(ExportFrameController.class).initByDB();

        S.getBean(MainFrame.class).setSubTitle(preferencesUtil.getH2File());
    }


}
