package com.saoft.fastdemo.ui.main;

import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.controller.AbstractFrameController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component("MainController")
public class MainController extends AbstractFrameController {

    private MainFrame mainFrame;

    @Autowired
    PreferencesUtil preferencesUtil;

    @Override
    public void prepareAndOpenFrame() {
        if(mainFrame==null){
            mainFrame = S.getBean(MainFrame.class);
        }
        mainFrame.setSubTitle(preferencesUtil.getH2File());
        mainFrame.setVisible(true);
    }
}
