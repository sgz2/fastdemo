package com.saoft.fastdemo.ui.shared;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RegUtil {

    public static boolean hasNumber(String company) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(company);
        if (m.find()) {
            return true;
        }
        return false;
    }
}
