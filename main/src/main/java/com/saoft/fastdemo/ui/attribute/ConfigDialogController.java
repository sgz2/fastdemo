package com.saoft.fastdemo.ui.attribute;

import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.ui.attribute.layerupdate.LayeUpdateFrameController;
import com.saoft.fastdemo.ui.attribute.menu.MenuFrameController;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component("ConfigDialogController")
public class ConfigDialogController {

    public void bindLayerConfig(Layer layer){
        String type = layer.getType();
        if (StringUtils.hasLength(type)) {
            if ("menu".equals(type)) {
                showMenuFrame(layer);
            }else{
                showLayerUpdateFrame(layer);
            }
        }
    }

    private void showMenuFrame(Layer layer){
        MenuFrameController controller = new MenuFrameController(layer);
        controller.initByDb();
        controller.show();
    }


    private void showLayerUpdateFrame(Layer layer){
        LayeUpdateFrameController controller = new LayeUpdateFrameController();
        controller.initByLayer(layer);
        controller.show();
    }

}
