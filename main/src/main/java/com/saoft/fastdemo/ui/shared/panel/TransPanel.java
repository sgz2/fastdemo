package com.saoft.fastdemo.ui.shared.panel;

import javax.swing.*;

/*背景透明*/
public class TransPanel extends JPanel {

    public TransPanel() {
        setOpaque(false);
        setBackground(null);
    }
}
