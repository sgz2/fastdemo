package com.saoft.fastdemo.ui.db.view.menu.insertmethod.bean;

public class ServiceDaoData {

    private String dao;
    private String dao_xml;
    private String service;
    private String service_impl;

    public String getDao() {
        return dao;
    }

    public void setDao(String dao) {
        this.dao = dao;
    }

    public String getDao_xml() {
        return dao_xml;
    }

    public void setDao_xml(String dao_xml) {
        this.dao_xml = dao_xml;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getService_impl() {
        return service_impl;
    }

    public void setService_impl(String service_impl) {
        this.service_impl = service_impl;
    }
}
