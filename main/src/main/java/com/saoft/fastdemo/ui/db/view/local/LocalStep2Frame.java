/*
 * Created by JFormDesigner on Sun Jun 16 21:05:13 CST 2019
 */

package com.saoft.fastdemo.ui.db.view.local;

import com.saoft.fastdemo.ui.db.model.DBListModel;
import com.saoft.fastdemo.ui.shared.button.MFrame;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("LocalStep2Frame")
public class LocalStep2Frame extends MFrame {
    public LocalStep2Frame() {
        setSubTitle("选择数据库");
        initComponents();
        init();
    }

    void init(){
        dbList.setModel(new DBListModel());
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        scrollPane1 = new EmptyBorderScrollPane();
        dbList = new JList();
        panel2 = new JPanel();
        pbtn = new JButton();
        nbtn = new JButton();
        cbtn = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(dbList);
        }
        contentPane.add(scrollPane1);

        //======== panel2 ========
        {
            panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));

            //---- pbtn ----
            pbtn.setText("\u4e0a\u4e00\u6b65");
            panel2.add(pbtn);

            //---- nbtn ----
            nbtn.setText("\u4e0b\u4e00\u6b65");
            panel2.add(nbtn);

            //---- cbtn ----
            cbtn.setText("\u53d6\u6d88");
            panel2.add(cbtn);
        }
        contentPane.add(panel2);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JScrollPane scrollPane1;
    private JList dbList;
    private JPanel panel2;
    private JButton pbtn;
    private JButton nbtn;
    private JButton cbtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JButton getPbtn() {
        return pbtn;
    }

    public JButton getCbtn() {
        return cbtn;
    }

    public JButton getNbtn() {
        return nbtn;
    }

    public JList getDbList() {
        return dbList;
    }
}
