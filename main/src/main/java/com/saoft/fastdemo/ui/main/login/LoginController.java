package com.saoft.fastdemo.ui.main.login;

import cn.hutool.core.map.MapUtil;
import cn.hutool.http.HttpUtil;
import com.alibaba.fastjson.JSON;
import com.saoft.fastdemo.system.FastdemoProperties;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.TopController;
import com.saoft.fastdemo.web.bean.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.util.HashMap;

@Component("UI_LoginController")
public class LoginController {

    @Autowired
    LoginFrame loginFrame;

    @Autowired
    PreferencesUtil preferencesUtil;

    TopController topController;

    @PostConstruct
    public void init(){
        loginFrame.getLoginBtn().addActionListener(e -> login());
    }

    public void login(){
        String username = loginFrame.getUsername().getText();
        char[] password = loginFrame.getPassword().getPassword();
        if (StringUtils.hasLength(username) && password != null && password.length > 0) {
            FastdemoProperties bean = S.getBean(FastdemoProperties.class);
            String apiServer = bean.getApiServer();
            String url = apiServer+"/api/login";
            HashMap<String, Object> map = MapUtil.newHashMap();
            map.put("username", username);
            map.put("password", new String(password));
            try {
                String post = HttpUtil.post(url, map, 10000);
                //post 返回内容

                Msg msg = JSON.parseObject(post, Msg.class);
                if (msg.getCode() == 1) {
                    //成功 1保存token 2切花已登录状态
//                    System.out.println(post);
                    preferencesUtil.setToken(msg.getData().toString(),username);
                    preferencesUtil.setUsername(username);

                    if (topController == null) {
                        topController = S.getBean(TopController.class);
                    }
                    topController.render_online();
                    loginFrame.setVisible(false);
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null,"登录失败");
            }
        }else{

//            JOptionPane.showMessageDialog(null,"登录失败");
            JOptionPane.showMessageDialog(null,"内容不能为空");
        }

    }

}
