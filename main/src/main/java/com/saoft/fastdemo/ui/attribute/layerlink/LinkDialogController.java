package com.saoft.fastdemo.ui.attribute.layerlink;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.ui.shared.FormUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.util.List;

@Component("LinkDialogController")
public class LinkDialogController {

    @Autowired
    LayerLinkFrame layerLinkFrame;

    @Autowired
    EditLayerLink editLayerLink;

    @PostConstruct
    private void init(){
        ControllerUtil.registerAction(layerLinkFrame.getAddBtn(),e->add());
        ControllerUtil.registerAction(layerLinkFrame.getDeleteBtn(),e->delete());
        ControllerUtil.registerAction(layerLinkFrame.getOkButton(),e->hide());
        ControllerUtil.registerAction(layerLinkFrame.getCancelButton(),e->hide());


        ControllerUtil.registerAction(editLayerLink.getOkButton(),e->save());
        ControllerUtil.registerAction(editLayerLink.getCancelButton(),e->hideEdit());
    }

    private void save() {
        Layer layer = layerLinkFrame.getLayer();
        LayerLink update = new LayerLink();
        update.setLayerId(layer.getId());
        FormUtil.extractData(editLayerLink,update);
        update.insertOrUpdate();

        editLayerLink.setVisible(false);
        bindLayerLink(layer);
    }

    private void hideEdit() {
        editLayerLink.setVisible(false);
    }

    private void hide() {
        layerLinkFrame.setVisible(false);
    }

    private void delete() {
        JTable linkTable = layerLinkFrame.getLinkTable();
        LayerLinkTableModel model = (LayerLinkTableModel) linkTable.getModel();
        model.removeRow(linkTable);
    }

    private void add() {
        FormUtil.bindData(editLayerLink,new LayerLink());
        editLayerLink.setVisible(true);
    }

    public void bindLayerLink(Layer layer) {
        List<LayerLink> layerLinks = new LayerLink()
                .selectList(new QueryWrapper<LayerLink>().eq(DBConstants.layer_id, layer.getId()));
        LayerLinkTableModel model = (LayerLinkTableModel) layerLinkFrame.getLinkTable().getModel();
        model.clear();
        model.addEntities(layerLinks);
        layerLinkFrame.setLayer(layer);
        layerLinkFrame.setVisible(true);
    }
}
