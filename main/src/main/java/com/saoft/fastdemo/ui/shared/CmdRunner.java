package com.saoft.fastdemo.ui.shared;


import java.io.*;
import java.util.LinkedList;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;

/**
 * Created by saoft on 2018/9/26.
 */
public class CmdRunner {

    public static Process run(LinkedList<String> actCmd, String dir) throws InterruptedException, IOException {
        return run(actCmd, dir, CmdRunner::callback);
    }

    public static Process run(LinkedList<String> actCmd, String dir,Consumer<String> consumer) throws InterruptedException, IOException {
        boolean isWindows = System.getProperty("os.name")
                .toLowerCase().startsWith("windows");
        ProcessBuilder builder = new ProcessBuilder();
        LinkedList<String> cmd = new LinkedList<>();

        if (isWindows) {
            cmd.add("cmd.exe");
            cmd.add("/c");
        } else {
            cmd.add("sh");
            cmd.add("-c");
        }
        cmd.addAll(actCmd);
        builder.command(actCmd);
        if (dir != null) {
            builder.directory(new File(dir));
        }
        Process process = builder.start();
        StreamGobbler streamGobbler =
                new StreamGobbler(process.getInputStream(), consumer);
        new ThreadPoolExecutor(1, 1,
                0L, TimeUnit.MILLISECONDS,
                new LinkedBlockingQueue<>()).submit(streamGobbler);

        return process;
    }

    private static void callback(String outline) {
        System.out.println(outline);
    }

    private static class StreamGobbler implements Runnable {
        private InputStream inputStream;
        private Consumer<String> consumer;

        public StreamGobbler(InputStream inputStream, Consumer<String> consumer) {
            this.inputStream = inputStream;
            this.consumer = consumer;
        }

        @Override
        public void run() {
            try {
                new BufferedReader(new InputStreamReader(inputStream,"UTF-8")).lines()
                        .forEach(consumer);
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }
    }

//    public static void main(String[] args) throws IOException, InterruptedException {
//        String dir = "D:/dev";
//        LinkedList<String> objects = new LinkedList<>();
//        objects.add("svn");
//        objects.add("diff");
//        objects.add("--old=t2.txt");
//        objects.add("--new=t1.txt");
//
//        Process run = run(objects, dir, new Test()::out);
//        int i = run.waitFor();
//        System.out.println("end");
//        System.out.println(i);
//    }
//
//    private static class Test{
//
//        public void out(String string) {
//            System.out.println(string);
//        }
//    }
}
