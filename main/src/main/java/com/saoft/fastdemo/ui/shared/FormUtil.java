package com.saoft.fastdemo.ui.shared;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;

import javax.swing.*;
import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

public class FormUtil {

    /**
     * 绑定数据 到 form表单
     * @param form
     * @param data
     */
    public static void bindData(Object form,Object data){
        Class<?> aClass = data.getClass();
        try {
            Map<String, Field> formMap = getTableFieldMap(form);
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                TableId idann = field.getAnnotation(TableId.class);
                TableField annotation = field.getAnnotation(TableField.class);
                if (annotation != null || idann!=null) {
                    //form的text
                    Field declaredField = null;

                    if (annotation != null) {
                        declaredField = formMap.get(annotation.value());
                    }else{
                        declaredField = formMap.get(idann.value());
                    }

                    if(declaredField!=null){
                       if( declaredField.getType().equals(JTextField.class)){
                            declaredField.setAccessible(true);
                            field.setAccessible(true);

                            JTextField o = (JTextField) declaredField.get(form);
                            Object o1 = field.get(data);
                            if (o1 != null) {
                                o.setText(o1.toString());
                            }else{
                                o.setText("");
                            }
                        }
                        if( declaredField.getType().equals(JCheckBox.class)){
                            declaredField.setAccessible(true);
                            field.setAccessible(true);

                            JCheckBox o = (JCheckBox) declaredField.get(form);
                            Object o1 = field.get(data);
                            o.setSelected(false);
                            if (o1 != null) {
                                o.setSelected((Boolean) o1);
                            }
                        }

                        if( declaredField.getType().equals(JComboBox.class)){
                            declaredField.setAccessible(true);
                            field.setAccessible(true);

                            JComboBox o = (JComboBox) declaredField.get(form);
                            Object o1 = field.get(data);
                            if (o1 instanceof String) {
                                o.setSelectedItem(o1);
                            }
                        }
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static Map<String,Field> getTableFieldMap(Object object){
        Map<String, Field> map = new HashMap<>();
        Class<?> aClass = object.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            TableField annotation = field.getAnnotation(TableField.class);
            if (annotation != null) {
                map.put(annotation.value(), field);
            }
        }
        return map;
    }
    /**
     * 抽取form表单 数据到rltData
     * @param form
     * @param rltData
     */
    public static void extractData(Object form,Object rltData){
        Class<?> aClass = rltData.getClass();
        Map<String, Field> formMap = getTableFieldMap(form);
        Field[] fields = aClass.getDeclaredFields();
        for (Field field : fields) {
            TableField annotation = field.getAnnotation(TableField.class);
            if (annotation != null) {
                //form的text
                Field declaredField = null;
                try {
                    declaredField = formMap.get(annotation.value());
                    if(declaredField!=null){
                        if( declaredField.getType().equals(JTextField.class)){
                            declaredField.setAccessible(true);
                            JTextField textField = (JTextField) declaredField.get(form);

                            String text = textField.getText();
                            field.setAccessible(true);

                            if(field.getType().equals(String.class)){
                                field.set(rltData,text);
                            }
                            if(field.getType().equals(Integer.class)){
                                int i = Integer.parseInt(text);
                                field.set(rltData,i);
                            }
                            if(field.getType().equals(Double.class)){
                                double i = Double.parseDouble(text);
                                field.set(rltData,i);
                            }

                        }

                        /*复选框*/
                        if( declaredField.getType().equals(JCheckBox.class)){
                            declaredField.setAccessible(true);
                            JCheckBox textField = (JCheckBox) declaredField.get(form);

                            boolean selected = textField.isSelected();
                            field.setAccessible(true);

                            if(field.getType().equals(Boolean.class)){
                                field.set(rltData,selected);
                            }
                        }

                        /*下拉框*/
                        if( declaredField.getType().equals(JComboBox.class)){
                            declaredField.setAccessible(true);
                            JComboBox textField = (JComboBox) declaredField.get(form);

                            Object selectedItem = textField.getSelectedItem();
                            if (selectedItem instanceof String) {
                                String selected = (String)selectedItem ;
                                field.setAccessible(true);
                                if(field.getType().equals(String.class)){
                                    field.set(rltData,selected);
                                }
                            }

                        }
                    }

                } catch (Exception e) {
                   continue;
                }

            }
        }

    }
}
