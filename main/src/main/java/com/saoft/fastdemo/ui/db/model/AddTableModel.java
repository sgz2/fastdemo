package com.saoft.fastdemo.ui.db.model;

import com.saoft.fastdemo.bean.JColumn;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

public class AddTableModel extends DefaultTableModel<JColumn> {

    public AddTableModel() {

    }

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "名",
                "类型",
                "长度",
                "小数点",
                "不是null",
                "键"
        };
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return true;
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        JColumn column = entities.get(rowIndex);

        switch (columnIndex) {
            case 0:
                column.setName(aValue.toString());
                break;
            case 1:
                column.setType(aValue.toString());
                break;
            case 2:
                column.setLength(Integer.parseInt(aValue.toString()));
                break;
            case 3:
                column.setPoint(Integer.parseInt(aValue.toString()));
                break;
            case 4:
                column.setIsnull(aValue.toString());
                break;
            case 5:
                column.setKey(aValue.toString());
        }

    }



    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        JColumn column = entities.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return column.getName();
            case 1:
                return column.getType();
            case 2:
                return column.getLength();
            case 3:
                return column.getPoint();
            case 4:
                return column.getIsnull();
            case 5:
                return column.getKey();
            default:
                return "";
        }

    }

}
