package com.saoft.fastdemo.ui.db.util;

import com.saoft.fastdemo.Application;

import java.util.prefs.Preferences;

public class LocalPref {

    /*localdb*/
    public final static String localdb_ip = "localdb_ip";
    public final static String localdb_port = "localdb_port";
    public final static String localdb_username = "localdb_username";
    public final static String localdb_password = "localdb_password";

    public static String get(String code,String defaultValue) {
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        String s = pref.get(code, defaultValue);
        return s;
    }

    public static void set(String nToken, String musername){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        pref.put(nToken, musername);
    }
}
