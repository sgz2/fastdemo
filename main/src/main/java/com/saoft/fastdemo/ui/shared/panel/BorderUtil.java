package com.saoft.fastdemo.ui.shared.panel;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

public class BorderUtil {

    //标题栏的上下一条线
    public static Border createTitleBorder(){
        return BorderFactory.createMatteBorder(1,0,1,0,Color.darkGray);
    }
}
