package com.saoft.fastdemo.ui.shared.form;

public interface IdAble {

    String getId();

    Object getValue();
}
