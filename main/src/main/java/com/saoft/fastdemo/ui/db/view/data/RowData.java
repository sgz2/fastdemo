package com.saoft.fastdemo.ui.db.view.data;

import javax.swing.*;

public class RowData {

    private String id;
    private String title;
    private Object value;
    private boolean isnotnull;
    private JComponent component;

    public RowData() {
    }

    public RowData(String id, String title, Object value, boolean isnotnull, JComponent component) {
        this.id = id;
        this.title = title;
        this.isnotnull = isnotnull;
        this.component = component;
        setValue(value);
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        if (value != null) {
            this.value = value;
            if (value instanceof Boolean && component instanceof JCheckBox) {
                ((JCheckBox) component).setSelected((Boolean) value);
            } else if (component instanceof JTextField) {
                ((JTextField) component).setText(value.toString());
            }
        }
    }

    public boolean isIsnotnull() {
        return isnotnull;
    }

    public void setIsnotnull(boolean isnotnull) {
        this.isnotnull = isnotnull;
    }

    public JComponent getComponent() {
        return component;
    }

    public void setComponent(JComponent component) {
        this.component = component;
    }
}
