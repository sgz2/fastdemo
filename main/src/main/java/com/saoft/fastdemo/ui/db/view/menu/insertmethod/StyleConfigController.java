package com.saoft.fastdemo.ui.db.view.menu.insertmethod;

import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;

@Component("StyleConfigController")
public class StyleConfigController {

    @Autowired
    StyleConfig styleConfig;

    @Autowired
    InsertMethod insertMethod;

    @PostConstruct
    private void init(){

        ControllerUtil.registerAction(insertMethod.getStyleBtn(),e->loadProperties());

        ControllerUtil.registerAction(styleConfig.getOkButton(),e->save());
        ControllerUtil.registerAction(styleConfig.getCancelButton(),e->styleConfig.setVisible(false));


    }

    void loadProperties(){
        {
            String dao = PrefUtil.get(PrefUtil.name_dao);
            if (!StringUtils.hasLength(dao)) {
                PrefUtil.set(PrefUtil.name_dao,dao);
            }else{
                styleConfig.getDao().setText(dao);
            }
        }

        {
            String name_dao_xml = PrefUtil.get(PrefUtil.name_dao_xml);
            if (!StringUtils.hasLength(name_dao_xml)) {
                PrefUtil.set(PrefUtil.name_dao_xml,name_dao_xml);
            }else{
                styleConfig.getDao_xml().setText(name_dao_xml);
            }
        }

        {
            String name_service = PrefUtil.get(PrefUtil.name_service);
            if (!StringUtils.hasLength(name_service)) {
                PrefUtil.set(PrefUtil.name_service,name_service);
            }else{
                styleConfig.getService().setText(name_service);
            }
        }

        {
            String name_service_impl = PrefUtil.get(PrefUtil.name_service_impl);
            if (!StringUtils.hasLength(name_service_impl)) {
                PrefUtil.set(PrefUtil.name_service_impl,name_service_impl);
            }else{
                styleConfig.getService_impl().setText(name_service_impl);
            }
        }

        {
            String name_style = PrefUtil.get(PrefUtil.name_style);
            if (!StringUtils.hasLength(name_style)) {
                PrefUtil.set(PrefUtil.name_style,name_style);
            }else{
                styleConfig.getStyle().setSelectedItem(name_style);
            }
        }


        styleConfig.setVisible(true);
    }

    void save(){
        String dao = styleConfig.getDao().getText();
        String ser = styleConfig.getService().getText();
        String dao_impl = styleConfig.getDao_xml().getText();
        String ser_impl = styleConfig.getService_impl().getText();
        String style = styleConfig.getStyle().getSelectedItem().toString();

        PrefUtil.set(PrefUtil.name_dao,dao);
        PrefUtil.set(PrefUtil.name_dao_xml,dao_impl);
        PrefUtil.set(PrefUtil.name_service,ser);
        PrefUtil.set(PrefUtil.name_service_impl,ser_impl);
        PrefUtil.set(PrefUtil.name_style,style);
        styleConfig.setVisible(false);
    }

}
