package com.saoft.fastdemo.ui.outline;

import cn.hutool.core.util.ArrayUtil;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.*;
import com.saoft.fastdemo.db.entity.Menu;
import com.saoft.fastdemo.db.util.IdUtil;
import com.saoft.fastdemo.service.LayerService;
import com.saoft.fastdemo.service.RoleService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.system.W;
import com.saoft.fastdemo.ui.attribute.AttributeController;
import com.saoft.fastdemo.ui.attribute.ConfigDialogController;
import com.saoft.fastdemo.ui.attribute.menu.MenuLayerKeyValue;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.PrefUtil;
import com.saoft.fastdemo.ui.outline.model.ControllerTreeModel;
import com.saoft.fastdemo.ui.outline.model.LayerGroupNode;
import com.saoft.fastdemo.ui.outline.view.OutLinePanel;
import com.saoft.fastdemo.ui.outline.view.RolePanel;
import com.saoft.fastdemo.ui.outline.view.RolePopupMenu;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.ui.shared.controller.AbstractPanelController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.ui.shared.controller.DeleteController;
import com.saoft.fastdemo.ui.shared.util.JTreeUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;
import java.util.List;

@Component("OutlineController")
public class OutlineController extends AbstractPanelController {

    @Autowired
    OutLinePanel outLinePanel;

    @Autowired
    ConfigDialogController configDialogController;

    @Autowired
    SeriesComponent seriesComponent;

    @Autowired
    DeleteController deleteController;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    RolePopupMenu rolePopupMenu;


    @Autowired
    LayerService layerService;

    @Autowired
    RoleService roleService;

    @Override
    public JComponent getPanel() {
        return outLinePanel;
    }

    @PostConstruct
    private void init(){
        ControllerUtil.registerAction(outLinePanel.getAddRole(),e->addRole());

//        rolePopupMenu.addMenuItem("预览", e-> preview());
        rolePopupMenu.addSeparator();
        rolePopupMenu.addMenuItem("新建组", e-> newGroup());
        rolePopupMenu.addMenuItem("加入组", e-> joinGroup());
        rolePopupMenu.addMenuItem("重命名", e-> rename());
        rolePopupMenu.addSeparator();
        rolePopupMenu.addMenuItem("删除层", e-> deleteOut());
        rolePopupMenu.addMenuItem("管理端编辑", e-> serverItemEdit());
        rolePopupMenu.addSeparator();
        rolePopupMenu.addMenuItem("加入菜单", e-> appendMenu());


        outLinePanel.getRoleTabbedPane().addChangeListener(e -> {
            Object source = e.getSource();
            int selectedIndex = outLinePanel.getRoleTabbedPane().getSelectedIndex();
            if (selectedIndex >= 0) {
                PrefUtil.set(W.OutLinePanel_Tab,selectedIndex+"");
            }
        });

    }

    void rename(){
        JTree jTree = roleService.currentControllerTree();
        DefaultMutableTreeNode component = (DefaultMutableTreeNode) jTree.getLastSelectedPathComponent();
        Object userObject = component.getUserObject();
        if (userObject instanceof LayerGroupNode) {
            LayerGroupNode node = (LayerGroupNode) userObject;
            LayerGroup layerGroup = node.getLayerGroup();
            String name = layerGroup.getName();
            String renameValue = JOptionPane.showInputDialog("重命名", name);
            if (StringUtils.hasLength(renameValue)) {
                layerGroup.setName(renameValue);
                layerService.updateGroupName(layerGroup);
            }
        } else if (userObject instanceof Layer) {
            Layer layer = (Layer) userObject;
            String renameValue = JOptionPane.showInputDialog("重命名", layer.getName());
            if (StringUtils.hasLength(renameValue)) {
                layer.setName(renameValue);
                layerService.updateLayer(layer);
            }
        }

        initByDb();
    }

    void newGroup(){
        String zhuming = JOptionPane.showInputDialog("组名");
        if (!StringUtils.hasLength(zhuming)) {
            zhuming = "新建组";
        }

        LayerGroup layerGroup = new LayerGroup();
        layerGroup.setName(zhuming);
        String roleCode = roleService.currentRoleCode();
        layerGroup.setRoleCode(roleCode);

        RolePanel rolePanel = roleService.currentRolePanel();
        JTree controlerTree = rolePanel.getControlerTree();

        TreePath[] paths = controlerTree.getSelectionPaths();
        List<Layer> layerList = new ArrayList<>();
        if (paths != null) {
            for (TreePath path : paths) {
//                controlerTree.getComponentAt()
                //todo 只移动顶级的目录
                DefaultMutableTreeNode component = (DefaultMutableTreeNode) path.getLastPathComponent();
                Object userObject = component.getUserObject();
//                TreePath parentPath = path.getParentPath();
//                Object parent = parentPath.getLastPathComponent();
                if (userObject instanceof Layer) {
                    layerList.add((Layer) userObject);
                }
            }
        }

        layerService.newGroup(layerGroup, layerList);

        initByDb();
    }

    void joinGroup() {
        String roleCode = roleService.currentRoleCode();
        List<LayerGroup> layerGroups = layerService.roleGroupList(roleCode);
        layerGroups.add(new LayerGroup());
//        String[] options = { "OK", "Cancel", "Help" };
        int selection = JOptionPane.showOptionDialog(null, "选择组", "选择组",
                JOptionPane.YES_NO_OPTION, JOptionPane.PLAIN_MESSAGE, null, layerGroups.toArray(),
                layerGroups.get(0));
        if (selection >= 0) {
            LayerGroup layerGroup = layerGroups.get(selection);
            Layer currentLayer = layerService.getCurrentLayer();
            if (currentLayer != null) {
                currentLayer.setGroupId(layerGroup.getId());
                layerService.updateLayer(currentLayer);
            }
        }

        initByDb();
    }


    /**
     * 右键将层加入菜单
     */
    void appendMenu(){

        Layer layer = layerService.getCurrentLayer();
        if (layer == null) {
            return;
        }
        Menu menu = new Menu();
        menu.setRole(roleService.currentRoleCode());
        menu.setLayerId(layer.getId());

        menu.setCode(layer.getTableName()+":"+layer.getType());
        menu.setName(layer.getName());

        //设置url
        String type = layer.getType();
        String url = layer.getRoleCode() + "/" +
                layer.getTableName() + "/" +
                layer.getTableName() + "_" + MenuLayerKeyValue.map.get(type)
                ;
        menu.setUrl(url);

        menu.insert();
    }

    void serverItemEdit(){
        RolePanel rolePanel = roleService.currentRolePanel();
        JTree tree = rolePanel.getControlerTree();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
        Object userObject = node.getUserObject();
        if(node.getUserObject() instanceof Layer){
            configDialogController.bindLayerConfig((Layer) userObject);
        }
    }


    public void layerClick(MouseEvent e,JTree controlerTree){

        TreePath[] paths = controlerTree.getSelectionPaths();
        TreePath pathForLocation = controlerTree.getPathForLocation(e.getX(), e.getY());
        TreePath[] append = ArrayUtil.append(paths, pathForLocation);
        controlerTree.setSelectionPaths(append);

        DefaultMutableTreeNode node = (DefaultMutableTreeNode) controlerTree.getLastSelectedPathComponent();
        if(node == null){
            return;
        }

        Object nodeUserObject = node.getUserObject();

        if (nodeUserObject instanceof LayerGroupNode) {
            if (e.getButton() == MouseEvent.BUTTON3) {
                rolePopupMenu.setVisible(true);
                Point point = e.getPoint();

                rolePopupMenu.show(controlerTree,point.x,point.y);
            }
        }

        if(node.getUserObject() instanceof Layer){
            Layer userObject = (Layer) node.getUserObject();
            S.b(AttributeController.class).showAttr(userObject);
            //双击打开配置
            if (e.getClickCount() == 2) {
//                preview();
                configDialogController.bindLayerConfig(userObject);
            }
            //右键  加入菜单 删除层，删除role
            if (e.getButton() == MouseEvent.BUTTON3) {
                rolePopupMenu.setVisible(true);
                Point point = e.getPoint();
                rolePopupMenu.show(controlerTree,point.x,point.y);
            }
        }

    }

    /**
     * 从数据库初始化
     * */
    public void initByDb(){
        outLinePanel.getRoleTabbedPane().removeAll();
        List<Role> roles = new Role().selectAll();

        for (Role role : roles) {
            RolePanel rolePanel = new RolePanel();
            rolePanel.setRole(role);
            ControllerUtil.registerAction(rolePanel.getViewTpl(),(e)->selectTplBtn());
            ControllerUtil.registerAction(rolePanel.getSaveRoleBtn(),(e)->saveRole());
            ControllerUtil.registerAction(rolePanel.getDeleteRoleBtn(),(e)->deleteRole());

            JTabbedPane tabbedPane = rolePanel.getTabbedPane();
            tabbedPane.addChangeListener(e -> {
                        int selectedIndex = tabbedPane.getSelectedIndex();
                        if (selectedIndex >= 0) {
                            PrefUtil.set(W.RolePanel_Tab, tabbedPane.getSelectedIndex() + "");
                        }
                    });

            tabbedPane.setSelectedIndex(PrefUtil.getInteger(W.RolePanel_Tab));

            JTree controlerTree = rolePanel.getControlerTree();
            initControllerTree(controlerTree,role);

            //角色信息
            rolePanel.getRoleIdTxt().setText(role.getCode());
            rolePanel.getRoleNameTxt().setText(role.getName());
            rolePanel.getType().setSelectedItem(role.getType());
            rolePanel.getTemplateCode().setSelectedItem(role.getTemplateCode());

            outLinePanel.getRoleTabbedPane().addTab(role.getCode(),rolePanel);
        }

        JTabbedPane roleTabbedPane = outLinePanel.getRoleTabbedPane();
        roleTabbedPane.setSelectedIndex(PrefUtil.getInteger(W.OutLinePanel_Tab));

    }

    private void initControllerTree(JTree controlerTree, Role role){
        //group
        List<LayerGroup> layerGroupList = layerService.roleGroupList(role.getCode());
        //多层树形
        //tree的列表
        List<Layer> outs = layerService.roleLayerList(role.getCode());

        ControllerTreeModel model = (ControllerTreeModel) controlerTree.getModel();

        //带分组的层
        Map<Integer, LinkedList<Layer>> groupMap = new HashMap<>();


        for (Layer layer : outs) {
            if(layer.getGroupId()==null){
                model.add(layer);
            }else{
                MapUtil.putListValue(groupMap,layer.getGroupId(),layer);
            }
        }

        List<LayerGroupNode> topNode = new LinkedList<>();
        Map<Integer, LayerGroupNode> nodeMap = new HashMap<>();
        for (LayerGroup value : layerGroupList) {
            LayerGroupNode node = new LayerGroupNode();
            node.setLayerGroup(value);
            nodeMap.put(value.getId(), node);
            if (value.getParentId() == null) {
                topNode.add(node);//根路径
            }
        }
        for (LayerGroup value : layerGroupList) {
            Integer parentId = value.getParentId();
            if (parentId!=null) {
                LayerGroupNode parentNode = nodeMap.get(parentId);
                LayerGroupNode nodeVal = nodeMap.get(value.getId());
                parentNode.getChilds().add(nodeVal);
            }
            Integer id = value.getId();
            LinkedList<Layer> layers = groupMap.get(id);
            LayerGroupNode node = nodeMap.get(id);
            if (layers != null) {
                node.getChilds().addAll(layers);
            }
        }

        for (LayerGroupNode node : topNode) {
            model.addGroup(node, null);
        }
        //文件夹添加
        controlerTree.setRootVisible(true);
        TreePath pathForRow = controlerTree.getPathForRow(0);
        JTreeUtil.expandAll(controlerTree, pathForRow, true);

        controlerTree.expandRow(0);
        controlerTree.setRootVisible(false);

        controlerTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                layerClick(e, controlerTree);
            }
        });


    }


    /**
     * 更新Out
     * @param id
     */
    public void updateControllerTree(Integer id){
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        JTree controlerTree = rolePanel.getControlerTree();
        ControllerTreeModel model = (ControllerTreeModel) controlerTree.getModel();

        Object component = controlerTree.getLastSelectedPathComponent();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) component;

        if (node != null) {
            Object userObject = node.getUserObject();
            if (userObject instanceof Layer) {
                Layer o = (Layer) userObject;
                Layer layer = layerService.getById(id);
                BeanUtils.copyProperties(layer,o);
            }
        }

        model.reload();
        controlerTree.updateUI();
    }

    /**
     * 添加一个角色
     * */
    private void addRole(){
        RolePanel rolePanel = new RolePanel();
        ControllerUtil.registerAction(rolePanel.getViewTpl(),(e)->selectTplBtn());
        ControllerUtil.registerAction(rolePanel.getSaveRoleBtn(),(e)->saveRole());

        JTree controlerTree = rolePanel.getControlerTree();
        controlerTree.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                layerClick(e, controlerTree);
            }
        });

        outLinePanel.getRoleTabbedPane().addTab("newrole",rolePanel);
    }

    /**
     * 删除角色
     * */
    private void deleteRole(){
        JTabbedPane tabbedPane = outLinePanel.getRoleTabbedPane();
        RolePanel rolePanel = (RolePanel) tabbedPane.getSelectedComponent();

        try {
            Role role = rolePanel.getRole();
            int i = JOptionPane.showConfirmDialog(null, "确认删除角色：" + role.getCode() + "吗？");
            if (i == JOptionPane.YES_OPTION) {
                deleteController.deleteRole(role.getCode());
                tabbedPane.remove(rolePanel);
            }
        } catch (Exception e) {
            //刚刚新建没有保存的角色
            tabbedPane.remove(rolePanel);
        }

    }

    /*删除层*/
    private void deleteOut(){
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        JTree controlerTree = rolePanel.getControlerTree();

        Object component = controlerTree.getLastSelectedPathComponent();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) component;
        Object userObject = node.getUserObject();

        layerService.deleteLayer(userObject);
        initByDb();
    }

    /**
     * 添加一个控制层
     * */
    public void addControllerTreeLayer(TemplateItem item,String tableName){

        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        JTree controlerTree = rolePanel.getControlerTree();
        ControllerTreeModel model = (ControllerTreeModel) controlerTree.getModel();

        //添加一条数据到数据库
        Layer layer = new Layer();
        layer.setName(item.getName());
        layer.setType(item.getCode());
        layer.setRoleCode(rolePanel.getRole().getCode());
        layer.setVisiableFlag(true);
        layer.insert();
        layer.setTableName(tableName);

        Integer okgfd_out = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);
        layer.setId(okgfd_out);

        model.add(layer);
        controlerTree.updateUI();
    }

    /**
     * 添加一套crud_series login_series
     * @param item
     * @param tablename
     */
    public void addSeriesLayer(TemplateItem item,String tablename){
        String code = item.getCode();
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        if ("crud_series".equals(code)) {

            seriesComponent.addCrud(tablename, rolePanel.getRole().getCode());

        } else if ("login_series".equals(code)) {
            seriesComponent.addLogin(tablename, rolePanel.getRole().getCode());
        }
        initByDb();
        //
    }

    /**
     * 保存角色
     */
    void saveRole(){
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        JTextField roleIdTxt = rolePanel.getRoleIdTxt();
        JTextField roleNameTxt = rolePanel.getRoleNameTxt();

        Role role1 = rolePanel.getRole();
        Object selectedItem = rolePanel.getType().getSelectedItem();
        String text = roleIdTxt.getText();
        String roleNameTxtText = roleNameTxt.getText();
        Object code = rolePanel.getTemplateCode().getSelectedItem();

        Role role = new Role();
        if (role1 != null) {
            role.setId(role1.getId());
        }
        role.setCode(text);
        role.setName(roleNameTxtText);
        if (selectedItem != null) {
            role.setType(selectedItem.toString());
        }
        if (code != null) {
            role.setTemplateCode(code.toString());
        }

        role.insertOrUpdate();
    }

    //todo 私有模板
    private void selectTplBtn() {

//        componentController.initComponentUI(list);
    }
}
