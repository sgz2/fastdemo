package com.saoft.fastdemo.ui.shared.model;

import com.baomidou.mybatisplus.extension.activerecord.Model;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public abstract class DefaultTableModel<T> extends AbstractTableModel {

    protected List<T> entities = new ArrayList<>();

    public abstract String[] getColumnLabels();

    @Override
    public int getRowCount() {
        return entities.size();
    }

    @Override
    public int getColumnCount() {
        return getColumnLabels().length;
    }

    @Override
    public String getColumnName(int column) {
        String[] strings = getColumnLabels();
        return strings[column];
    }

    public void addEntity(T entity) {
        entities.add(entity);
        fireTableDataChanged();
    }

    public void addEntities(List<T> entities) {
        this.entities.addAll(entities);
        fireTableDataChanged();
    }

    public T getEntityByRow(int rowIndex) {
        return entities.get(rowIndex);
    }

    public void removeRow(JTable table) {
        int[] selectedRows = table.getSelectedRows();
        if (selectedRows != null) {
            List<T> lsit = new ArrayList<>();
            for (int selectedRow : selectedRows) {
                T t = entities.get(selectedRow);
                lsit.add(t);
            }
            for (T t : lsit) {
                if(t instanceof Model){
                    ((Model) t).deleteById();
                }
                entities.remove(t);
            }
            fireTableDataChanged();
        }
    }


    public void clear() {
        entities.clear();
    }
}
