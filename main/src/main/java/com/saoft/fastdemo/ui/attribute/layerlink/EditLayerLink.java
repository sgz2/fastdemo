/*
 * Created by JFormDesigner on Fri Jul 19 17:12:14 CST 2019
 */

package com.saoft.fastdemo.ui.attribute.layerlink;

import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
@Component("EditLayerLink")
public class EditLayerLink extends MDialog {

    private LayerLink layerLink;

    public EditLayerLink(LayerLinkFrame owner) {
        super(owner);
        initComponents();
        after();
    }

    void after(){
        tag.addItem("");

        tag.addItem("include_data");
        tag.addItem("data_link");//数据内容连接的层

        tag.addItem("link_to_add");

        tag.addItem("row_to_update");
        tag.addItem("row_to_delete");
        tag.addItem("row_to_detail");

        tag.addItem("return_to_list");

        tag.addItem("accept_from_row");


    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public LayerLink getLayerLink() {
        return layerLink;
    }

    public void setLayerLink(LayerLink layerLink) {
        this.layerLink = layerLink;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        label1 = new JLabel();
        link_layer_id = new JTextField();
        label2 = new JLabel();
        title = new JTextField();
        label4 = new JLabel();
        tag = new JComboBox();
        label3 = new JLabel();
        parameters = new JTextField();
        label5 = new JLabel();
        css_class = new JTextField();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridLayout(5, 2));

                //---- label1 ----
                label1.setText("\u76ee\u6807\u5c42");
                contentPanel.add(label1);
                contentPanel.add(link_layer_id);

                //---- label2 ----
                label2.setText("\u663e\u793a\u540d");
                contentPanel.add(label2);
                contentPanel.add(title);

                //---- label4 ----
                label4.setText("\u4f4d\u7f6e\u6807\u8bb0");
                contentPanel.add(label4);
                contentPanel.add(tag);

                //---- label3 ----
                label3.setText("\u53c2\u6570");
                contentPanel.add(label3);
                contentPanel.add(parameters);

                //---- label5 ----
                label5.setText("cssClass");
                contentPanel.add(label5);
                contentPanel.add(css_class);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JLabel label1;
    @TableField(value = DBConstants.link_layer_id)
    private JTextField link_layer_id;
    private JLabel label2;
    @TableField(value = DBConstants.title)
    private JTextField title;
    private JLabel label4;
    @TableField(value = DBConstants.tag)
    private JComboBox tag;
    private JLabel label3;
    @TableField(value = DBConstants.parameters)
    private JTextField parameters;
    private JLabel label5;
    @TableField(value = DBConstants.css_class)
    private JTextField css_class;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
