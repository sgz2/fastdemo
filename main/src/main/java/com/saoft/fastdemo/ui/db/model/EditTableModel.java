package com.saoft.fastdemo.ui.db.model;

import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

public class EditTableModel extends DefaultTableModel<JTableColumn> {

    public EditTableModel() {

    }

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "列",
                "注释",
                "类型",
                "允许为空",
                "默认值",
                "主键"
        };
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

//    @Override
//    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
//        JTableColumn column = entities.get(rowIndex);
//
//        switch (columnIndex) {
//            case 0:
//                column.setName(aValue.toString());
//                break;
//            case 1:
//                column.setType(aValue.toString());
//                break;
//            case 2:
//                column.set(Integer.parseInt(aValue.toString()));
//                break;
//            case 3:
//                column.setPoint(Integer.parseInt(aValue.toString()));
//                break;
//            case 4:
//                column.setIsnull(aValue.toString());
//                break;
//            case 5:
//                column.setKey(aValue.toString());
//        }
//
//    }



    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        JTableColumn column = entities.get(rowIndex);

        switch (columnIndex) {
            case 0:
                return column.getName();
            case 1:
                return column.getShowName();
            case 2:
                return column.getType();
            case 3:
                return column.getIfNull();
            case 4:
                return column.getDefaultValue();
            case 5:
                return column.getKeyFlag();
            default:
                return "";
        }

    }

}
