/*
 * Created by JFormDesigner on Wed Aug 28 10:59:26 CST 2019
 */

package com.saoft.fastdemo.ui.main.view;

import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("HelpFrame")
public class HelpFrame extends MDialog {
    public HelpFrame(MainFrame owner) {
        super(owner);
        setSubTitle("帮助");
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel1 = new JPanel();
        label3 = new JLabel();
        label1 = new JLabel();
        label2 = new JLabel();

        //======== this ========
        setMinimumSize(new Dimension(400, 100));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== panel1 ========
        {
            panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));

            //---- label3 ----
            label3.setText("\u7f51\u7ad9\uff1ahttp://www.saoft.com");
            panel1.add(label3);

            //---- label1 ----
            label1.setText("\u8054\u7cfbQQ:631471497");
            panel1.add(label1);

            //---- label2 ----
            label2.setText("\u90ae\u7bb1\uff1asaoft@foxmail.com");
            panel1.add(label2);
        }
        contentPane.add(panel1, BorderLayout.NORTH);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel1;
    private JLabel label3;
    private JLabel label1;
    private JLabel label2;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
