package com.saoft.fastdemo.ui.db.util;

import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.db.entity.JTableInfo;

import java.util.List;

public class TableUtil {

    public static TableInfo toLocal(JTableInfo info){
        TableInfo info1 = new TableInfo();

        return info1;
    }

    public static JTableInfo toDb(TableInfo info){
        JTableInfo info1 = new JTableInfo();

        return info1;
    }

    public static String tableColumn(TableInfo info) {
        List<TableField> fields = info.getFields();
        StringBuffer buffer = new StringBuffer();
        for (TableField field : fields) {
            buffer.append("`");
            buffer.append(field.getName());
            buffer.append("`,");
        }
        String substring = buffer.substring(0, buffer.length() - 1);
        return substring;
    }
}
