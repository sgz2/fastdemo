package com.saoft.fastdemo.ui.main.export;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Setting;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.ui.shared.form.IdAble;
import com.saoft.fastdemo.ui.shared.form.IdInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.util.*;
import java.util.List;


@Component("ExportFrameController")
public class ExportFrameController {

    @Autowired
    ExportFrame exportFrame;

    @Autowired
    PreferencesUtil preferencesUtil;



    @PostConstruct
    private void init() {
        ControllerUtil.registerAction(exportFrame.getOkButton(), e -> save());
        ControllerUtil.registerAction(exportFrame.getCancelButton(), e -> cancel());
    }

    public void initByDB() {
        initPanel();
    }

    private void initPanel() {
        List<Setting> sort_code = new Setting().selectList(
                new QueryWrapper<Setting>()
                        .in(DBConstants.group_code,"base","db","strategy","pkg","javaname")
                        .orderByAsc(DBConstants.sort_code));

        Map<String, LinkedList<Setting>> map = new LinkedHashMap<>();
        for (Setting setting : sort_code) {
            MapUtil.putListValue(map, setting.getGroupCode(), setting);
        }

        JTabbedPane setting = exportFrame.getSetting();
        Set<String> strings = map.keySet();
        for (String string : strings) {
            ExportForm form = new ExportForm();
            form.setForm(map.get(string));
            setting.add(string, form);
        }
    }

    private void cancel(){
        exportFrame.setVisible(false);
    }

    /**
     * 保存设置
     */
    private void save() {
        JTabbedPane setting = exportFrame.getSetting();
        java.awt.Component[] components = setting.getComponents();
        for (java.awt.Component component : components) {
            if (component instanceof ExportForm) {
                List<IdAble> values = ((ExportForm) component).getValues();
                for (IdAble value : values) {
                    if (value instanceof IdInput) {
                        String id = value.getId();
                        String text = ((IdInput) value).getText();

                        Setting st = new Setting();
                        st.setCode(id);
                        st.update(new UpdateWrapper<Setting>()
                                .eq(DBConstants.code, id).set(DBConstants.value,text));

                    }
                }
            }
        }
        JOptionPane.showMessageDialog(null,"修改成功");
    }








}
