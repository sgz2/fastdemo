/*
 * Created by JFormDesigner on Wed Aug 21 18:02:11 CST 2019
 */

package com.saoft.fastdemo.ui.db.view.data;

import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
@Component("DataForm")
public class DataForm extends MDialog {

    private java.util.List<RowData> dataList = new ArrayList<>();

    public DataForm(MainFrame owner) {
        super(owner);
        initComponents();
    }

    public void clean(){
        dataList = new ArrayList<>();
        leftPanel.removeAll();
        centerPanel.removeAll();
    }

    /*添加一行*/
    public void addItem(RowData rowData){
        dataList.add(rowData);
        leftPanel.add(new JLabel(rowData.getTitle()));
        centerPanel.add(rowData.getComponent());
        JLabel r = new JLabel();
        if (rowData.isIsnotnull()) {
            r.setText("*");
            r.setForeground(Color.red);
        }
        rightPanel.add(r);
    }

    /*获取数据*/
    public Map<String,Object> extraData(){
        Map<String, Object> data = new HashMap<>();

        for (RowData rowData : dataList) {
            JComponent component = rowData.getComponent();
            if (component instanceof JTextField) {
                String text = ((JTextField) component).getText();
                data.put(rowData.getId(), text);
            } else if (component instanceof JCheckBox) {
                boolean selected = ((JCheckBox) component).isSelected();
                data.put(rowData.getId(), selected);
            } else if (component instanceof JComboBox) {
                Object item = ((JComboBox) component).getSelectedItem();
                data.put(rowData.getId(), item);
            }
        }

        return data;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        leftPanel = new JPanel();
        centerPanel = new JPanel();
        rightPanel = new JPanel();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(300, 400));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new BorderLayout());

                //======== leftPanel ========
                {
                    leftPanel.setMinimumSize(new Dimension(80, 0));
                    leftPanel.setMaximumSize(null);
                    leftPanel.setLayout(new GridLayout(0, 1, 5, 5));
                }
                contentPanel.add(leftPanel, BorderLayout.WEST);

                //======== centerPanel ========
                {
                    centerPanel.setLayout(new GridLayout(0, 1, 5, 5));
                }
                contentPanel.add(centerPanel, BorderLayout.CENTER);

                //======== rightPanel ========
                {
                    rightPanel.setMinimumSize(new Dimension(20, 0));
                    rightPanel.setLayout(new GridLayout(0, 1, 5, 5));
                }
                contentPanel.add(rightPanel, BorderLayout.EAST);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JPanel leftPanel;
    private JPanel centerPanel;
    private JPanel rightPanel;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
