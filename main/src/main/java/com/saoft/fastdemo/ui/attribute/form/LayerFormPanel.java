package com.saoft.fastdemo.ui.attribute.form;

import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.ui.shared.form.FormPanel;

import javax.swing.*;

public class LayerFormPanel extends FormPanel {

    private Layer layer;


    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    @Override
    public void end() {
        super.end();
    }
}
