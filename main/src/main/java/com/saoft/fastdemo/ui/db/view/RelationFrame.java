/*
 * Created by JFormDesigner on Tue Jun 18 22:01:32 CST 2019
 */

package com.saoft.fastdemo.ui.db.view;

import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.db.entity.TableRelation;
import com.saoft.fastdemo.ui.db.view.local.dto.RelationDTO;
import com.saoft.fastdemo.ui.shared.button.MFrame;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
@Component("RelationFrame")
public class RelationFrame extends MFrame {

    private TableInfo fromTable;

    public RelationFrame() {
        initComponents();
    }

    public TableInfo getFromTable() {
        return fromTable;
    }

    public TableRelation getRelation(){
        TableRelation relation = new TableRelation();
        relation.setFtableId(fromTable.getName());

        TableField selectedItem = (TableField) formSel.getSelectedItem();
        relation.setFcolumnId(selectedItem.getName());

        TableInfo toS = (TableInfo) toTableSel.getSelectedItem();
        relation.setTtableId(toS.getName());

        TableField toSe = (TableField) toSel.getSelectedItem();
        relation.setTcolumnId(toSe.getName());

        RelationDTO dto = (RelationDTO) rSel.getSelectedItem();
        relation.setType(dto.getCode());

        relation.setName(nameTxt.getText());

        return relation;
    }

    public void setFromTable(TableInfo fromTable) {
        this.fromTable = fromTable;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        panel1 = new JPanel();
        label5 = new JLabel();
        nameTxt = new JTextField();
        label1 = new JLabel();
        rSel = new JComboBox();
        label2 = new JLabel();
        formSel = new JComboBox();
        label3 = new JLabel();
        toTableSel = new JComboBox();
        label4 = new JLabel();
        toSel = new JComboBox();
        buttonBar = new JPanel();
        okBtn = new JButton();
        celBtn = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== panel1 ========
            {
                panel1.setLayout(new GridBagLayout());
                ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0};
                ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0};
                ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 1.0E-4};
                ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- label5 ----
                label5.setText("\u7f16\u7801");
                panel1.add(label5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(nameTxt, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label1 ----
                label1.setText("\u5173\u7cfb");
                panel1.add(label1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(rSel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 100, 0));

                //---- label2 ----
                label2.setText("\u5b57\u6bb5");
                panel1.add(label2, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(formSel, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label3 ----
                label3.setText("\u53c2\u8003\u8868");
                panel1.add(label3, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(toTableSel, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 0), 0, 0));

                //---- label4 ----
                label4.setText("\u53c2\u8003\u5b57\u6bb5");
                panel1.add(label4, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                panel1.add(toSel, new GridBagConstraints(1, 4, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(panel1, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okBtn ----
                okBtn.setText("OK");
                buttonBar.add(okBtn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- celBtn ----
                celBtn.setText("Cancel");
                buttonBar.add(celBtn, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel panel1;
    private JLabel label5;
    private JTextField nameTxt;
    private JLabel label1;
    private JComboBox rSel;
    private JLabel label2;
    private JComboBox formSel;
    private JLabel label3;
    private JComboBox toTableSel;
    private JLabel label4;
    private JComboBox toSel;
    private JPanel buttonBar;
    private JButton okBtn;
    private JButton celBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JButton getOkBtn() {
        return okBtn;
    }

    public JButton getCelBtn() {
        return celBtn;
    }

    public JComboBox getrSel() {
        return rSel;
    }

    public JComboBox getFormSel() {
        return formSel;
    }

    public JComboBox getToTableSel() {
        return toTableSel;
    }

    public JComboBox getToSel() {
        return toSel;
    }
}
