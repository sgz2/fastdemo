/*
 * Created by JFormDesigner on Tue Sep 17 11:34:55 CST 2019
 */

package com.saoft.fastdemo.ui.db.view.menu.insertmethod;

import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
@Component("InsertMethod")
public class InsertMethod extends MDialog {

    public InsertMethod(MainFrame owner) {
        super(owner);
        initComponents();
        after();
    }

    void after(){
        crud.addItem("select");
        crud.addItem("update");
        crud.addItem("insert");
        crud.addItem("delete");
    }

    public JTextField getDao() {
        return dao;
    }

    public JTextField getDao_xml() {
        return dao_xml;
    }

    public JTextField getService_impl() {
        return service_impl;
    }

    public JTextField getService() {
        return service;
    }

    public JButton getReload() {
        return reload;
    }

    public JButton getStyleBtn() {
        return styleBtn;
    }

    public JButton getDictoryBtn() {
        return dictoryBtn;
    }

    public JComboBox getCrud() {
        return crud;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public JCheckBox getDao_chk() {
        return dao_chk;
    }

    public JCheckBox getService_chk() {
        return service_chk;
    }

    public JCheckBox getDao_xml_chk() {
        return dao_xml_chk;
    }

    public JCheckBox getService_impl_chk() {
        return service_impl_chk;
    }

    public JTextField getName1() {
        return name;
    }

    public JTextField getBack() {
        return back;
    }

    public JTextField getParm() {
        return parm;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        panel1 = new JPanel();
        label11 = new JLabel();
        panel2 = new JPanel();
        dictoryBtn = new JButton();
        panel3 = new JPanel();
        styleBtn = new JButton();
        panel4 = new JPanel();
        reload = new JButton();
        separator3 = new JSeparator();
        label12 = new JLabel();
        label9 = new JLabel();
        label4 = new JLabel();
        dao = new JTextField();
        dao_chk = new JCheckBox();
        label5 = new JLabel();
        dao_xml = new JTextField();
        dao_xml_chk = new JCheckBox();
        label7 = new JLabel();
        service = new JTextField();
        service_chk = new JCheckBox();
        label8 = new JLabel();
        service_impl = new JTextField();
        service_impl_chk = new JCheckBox();
        separator4 = new JSeparator();
        label13 = new JLabel();
        label1 = new JLabel();
        name = new JTextField();
        label2 = new JLabel();
        back = new JTextField();
        label3 = new JLabel();
        parm = new JTextField();
        label6 = new JLabel();
        crud = new JComboBox();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== panel1 ========
            {
                panel1.setLayout(new GridBagLayout());
                ((GridBagLayout)panel1.getLayout()).columnWidths = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)panel1.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)panel1.getLayout()).columnWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
                ((GridBagLayout)panel1.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- label11 ----
                label11.setText("\u914d\u7f6e\uff1a");
                panel1.add(label11, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== panel2 ========
                {
                    panel2.setLayout(new BoxLayout(panel2, BoxLayout.X_AXIS));

                    //---- dictoryBtn ----
                    dictoryBtn.setText("\u76ee\u6807\u76ee\u5f55\u914d\u7f6e");
                    panel2.add(dictoryBtn);
                }
                panel1.add(panel2, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== panel3 ========
                {
                    panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));

                    //---- styleBtn ----
                    styleBtn.setText("\u6587\u4ef6\u540d\u98ce\u683c\u914d\u7f6e");
                    panel3.add(styleBtn);
                }
                panel1.add(panel3, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //======== panel4 ========
                {
                    panel4.setLayout(new BoxLayout(panel4, BoxLayout.X_AXIS));

                    //---- reload ----
                    reload.setText("\u91cd\u65b0\u52a0\u8f7d\u6587\u4ef6");
                    panel4.add(reload);
                }
                panel1.add(panel4, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(separator3, new GridBagConstraints(1, 4, 3, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label12 ----
                label12.setText("\u76ee\u6807\u6587\u4ef6\u540d\uff1a");
                panel1.add(label12, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label9 ----
                label9.setText("\u8f93\u5165\u6846\u5185\u6709\u7c7b\u540d\uff0c\u8868\u793a\u627e\u5230\u4e86\u76ee\u6807\u6587\u4ef6");
                panel1.add(label9, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label4 ----
                label4.setText("dao");
                panel1.add(label4, new GridBagConstraints(1, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 50, 0));

                //---- dao ----
                dao.setEditable(false);
                panel1.add(dao, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 200, 0));

                //---- dao_chk ----
                dao_chk.setText("\u63d2\u5165");
                dao_chk.setSelected(true);
                panel1.add(dao_chk, new GridBagConstraints(3, 6, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label5 ----
                label5.setText("dao_xml");
                panel1.add(label5, new GridBagConstraints(1, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- dao_xml ----
                dao_xml.setEditable(false);
                panel1.add(dao_xml, new GridBagConstraints(2, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- dao_xml_chk ----
                dao_xml_chk.setText("\u63d2\u5165");
                dao_xml_chk.setSelected(true);
                panel1.add(dao_xml_chk, new GridBagConstraints(3, 7, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label7 ----
                label7.setText("service");
                panel1.add(label7, new GridBagConstraints(1, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- service ----
                service.setEditable(false);
                panel1.add(service, new GridBagConstraints(2, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- service_chk ----
                service_chk.setText("\u63d2\u5165");
                service_chk.setSelected(true);
                panel1.add(service_chk, new GridBagConstraints(3, 8, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label8 ----
                label8.setText("service_impl");
                panel1.add(label8, new GridBagConstraints(1, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- service_impl ----
                service_impl.setEditable(false);
                panel1.add(service_impl, new GridBagConstraints(2, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- service_impl_chk ----
                service_impl_chk.setText("\u63d2\u5165");
                service_impl_chk.setSelected(true);
                panel1.add(service_impl_chk, new GridBagConstraints(3, 9, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(separator4, new GridBagConstraints(1, 10, 3, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label13 ----
                label13.setText("\u63d2\u5165\u5185\u5bb9\uff1a");
                panel1.add(label13, new GridBagConstraints(1, 11, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label1 ----
                label1.setText("\u65b9\u6cd5\u540d\uff1a");
                panel1.add(label1, new GridBagConstraints(1, 12, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(name, new GridBagConstraints(2, 12, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label2 ----
                label2.setText("\u8fd4\u56de\u503c\uff1a");
                panel1.add(label2, new GridBagConstraints(1, 13, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(back, new GridBagConstraints(2, 13, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));

                //---- label3 ----
                label3.setText("\u53c2\u6570\uff1a");
                panel1.add(label3, new GridBagConstraints(1, 14, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 0, 0));
                panel1.add(parm, new GridBagConstraints(2, 14, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 5, 5), 200, 0));

                //---- label6 ----
                label6.setText("\u65b9\u6cd5\u7c7b\u578b\uff1a");
                panel1.add(label6, new GridBagConstraints(1, 15, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
                panel1.add(crud, new GridBagConstraints(2, 15, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));
            }
            dialogPane.add(panel1, BorderLayout.NORTH);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel panel1;
    private JLabel label11;
    private JPanel panel2;
    private JButton dictoryBtn;
    private JPanel panel3;
    private JButton styleBtn;
    private JPanel panel4;
    private JButton reload;
    private JSeparator separator3;
    private JLabel label12;
    private JLabel label9;
    private JLabel label4;
    private JTextField dao;
    private JCheckBox dao_chk;
    private JLabel label5;
    private JTextField dao_xml;
    private JCheckBox dao_xml_chk;
    private JLabel label7;
    private JTextField service;
    private JCheckBox service_chk;
    private JLabel label8;
    private JTextField service_impl;
    private JCheckBox service_impl_chk;
    private JSeparator separator4;
    private JLabel label13;
    private JLabel label1;
    private JTextField name;
    private JLabel label2;
    private JTextField back;
    private JLabel label3;
    private JTextField parm;
    private JLabel label6;
    private JComboBox crud;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
