package com.saoft.fastdemo.ui.db;

import com.saoft.fastdemo.bean.JColumn;
import com.saoft.fastdemo.ui.db.model.AddTableModel;
import com.saoft.fastdemo.ui.db.view.AddTable;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 添加数据表
 */
@Component("AddTableController")
public class AddTableController {

    @Autowired
    AddTable addTable;

    @PostConstruct
    public void init(){

        ControllerUtil.registerAction(addTable.getAddBtn(), (e)-> addColumn());
        ControllerUtil.registerAction(addTable.getDeleteBtn(), (e)-> deleteColumn());
    }

    private void addColumn(){
        AddTableModel model = (AddTableModel) addTable.getTable1().getModel();
        model.addEntity(new JColumn());
    }

    private void deleteColumn(){
        AddTableModel model = (AddTableModel) addTable.getTable1().getModel();
        model.removeRow(addTable.getTable1());
    }
}
