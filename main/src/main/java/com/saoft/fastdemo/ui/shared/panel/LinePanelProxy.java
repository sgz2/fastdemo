package com.saoft.fastdemo.ui.shared.panel;

import com.saoft.fastdemo.ui.shared.GBC;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public class LinePanelProxy {

    private JPanel panel;
    private java.util.List<Component> componentList = new ArrayList<>();
    private GridBagLayout bagLayout = new GridBagLayout();

    public LinePanelProxy(JPanel panel) {
        this.panel = panel;
        panel.setLayout(bagLayout);
    }

    public void add(Component comp){
        componentList.add(comp);
        panel.add(comp);
    }

    public void left(){
        for (int i = 0; i < componentList.size(); i++) {
            Component component = componentList.get(i);
            bagLayout.addLayoutComponent(component, new GBC(i, 0));
        }

        int col = componentList.size();
        JLabel label = new JLabel("");
        bagLayout.addLayoutComponent(label,
                new GBC(col, 0)
                .setWeight(1,1)
                .setFill(GBC.HORIZONTAL));
        panel.add(label);
    }

    public void right(){
        JLabel label = new JLabel("");
        bagLayout.addLayoutComponent(label,
                new GBC(0, 0)
                        .setWeight(1,1)
                        .setFill(GBC.HORIZONTAL));
        panel.add(label);

        for (int i = 0; i < componentList.size(); i++) {
            Component component = componentList.get(i);
            bagLayout.addLayoutComponent(component, new GBC((i+1), 0));
        }
    }

    public void top(){
        for (int i = 0; i < componentList.size(); i++) {
            Component component = componentList.get(i);
            bagLayout.addLayoutComponent(component, new GBC(0, i));
        }

        int col = componentList.size();
        JLabel label = new JLabel("");
        bagLayout.addLayoutComponent(label,
                new GBC(0, col)
                        .setWeight(1,1)
                        .setFill(GBC.HORIZONTAL));
        panel.add(label);
    }

    public void bottom(){
        JLabel label = new JLabel("");
        bagLayout.addLayoutComponent(label,
                new GBC(0, 0)
                        .setWeight(1,1)
                        .setFill(GBC.HORIZONTAL));
        panel.add(label);

        for (int i = 0; i < componentList.size(); i++) {
            Component component = componentList.get(i);
            bagLayout.addLayoutComponent(component, new GBC(0,(i+1)));
        }
    }
}
