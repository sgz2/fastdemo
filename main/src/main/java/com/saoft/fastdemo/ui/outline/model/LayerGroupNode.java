package com.saoft.fastdemo.ui.outline.model;

import com.saoft.fastdemo.db.entity.LayerGroup;

import java.util.LinkedList;
import java.util.List;

public class LayerGroupNode {

    private LayerGroup layerGroup;

    private List childs = new LinkedList();

    public LayerGroup getLayerGroup() {
        return layerGroup;
    }

    public void setLayerGroup(LayerGroup layerGroup) {
        this.layerGroup = layerGroup;
    }

    public List getChilds() {
        return childs;
    }

    public void setChilds(List childs) {
        this.childs = childs;
    }

    @Override
    public String toString() {
        if (layerGroup != null) {
            return layerGroup.getName();
        }
        return super.toString();
    }
}
