package com.saoft.fastdemo.ui.outline;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.*;
import com.saoft.fastdemo.db.util.IdUtil;
import com.saoft.fastdemo.ui.shared.RegUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("SeriesComponent")
public class SeriesComponent {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 增加一套crud
     * @param tableName
     */
    public void addCrud(String tableName,String role){
        JTableInfo tableInfo = new JTableInfo()
                .selectOne(new QueryWrapper<JTableInfo>().eq(DBConstants.name,tableName));

        List<JTableColumn> tableColumns = new JTableColumn()
                .selectList(new QueryWrapper<JTableColumn>().eq(DBConstants.table_name, tableName));

        String showName = tableInfo.getShowName();

        LayerGroup layerGroup = new LayerGroup();
        layerGroup.setName(showName+"管理");
        layerGroup.setRoleCode(role);
        layerGroup.insert();

        Integer layer_group = IdUtil.maxId(DBConstants.okgfd_layer_group, jdbcTemplate);


        Integer createLayerId = create(layer_group, showName, role, tableName, tableColumns);
        Integer deleteLayerId = delete(layer_group, showName, role, tableName,tableColumns);
        Integer updateLayerId = update(layer_group, showName, role, tableName, tableColumns);
        Integer listLayerId = list(layer_group, showName, role, tableName, tableColumns);



        //新增页面 连接到列表页面
        LayerLink layerLink = new LayerLink();
        layerLink.setLayerId(createLayerId);
        layerLink.setLinkLayerId(listLayerId);
        layerLink.setTitle("列表");
        layerLink.setTag("return_to_list");
        layerLink.insert();

        //编辑页面 连接到列表页面
        LayerLink layerUpdate = new LayerLink();
        layerUpdate.setLayerId(updateLayerId);
        layerUpdate.setLinkLayerId(listLayerId);
        layerUpdate.setTitle("列表");
        layerUpdate.setTag("return_to_list");
        layerUpdate.insert();

        //列表连接
        LayerLink listLink = new LayerLink();
        listLink.setLayerId(listLayerId);
        listLink.setLinkLayerId(createLayerId);
        listLink.setTitle("新增");
        listLink.setTag("link_to_add");
        listLink.insert();

        //列表连接到编辑
        LayerLink listUpdateLink = new LayerLink();
        listUpdateLink.setLayerId(listLayerId);
        listUpdateLink.setLinkLayerId(updateLayerId);
        listUpdateLink.setTitle("修改");
        listUpdateLink.setTag("row_to_update");
        listUpdateLink.setParameters("edit");
        listUpdateLink.setCssClass("primary");
        listUpdateLink.insert();

        //列表连接到删除
        LayerLink listDeleteLink = new LayerLink();
        listDeleteLink.setLayerId(listLayerId);
        listDeleteLink.setLinkLayerId(deleteLayerId);
        listDeleteLink.setTitle("删除");
        listDeleteLink.setTag("row_to_delete");
        listDeleteLink.setParameters("delete");
        listDeleteLink.setCssClass("danger");
        listDeleteLink.insert();

    }

    private Integer create(Integer layer_group, String showName
                        ,String role
            , String tableName,List<JTableColumn> tableColumns){

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "新增");
        layer.setTableName(tableName);
        layer.setType("create");
        layer.setRoleCode(role);
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);
        shibie(tableColumns, layerId, role);
        return layerId;
    }
    private Integer delete(Integer layer_group, String showName
            ,String role
            , String tableName,List<JTableColumn> tableColumns){
        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "删除");
        layer.setTableName(tableName);
        layer.setType("delete");
        layer.setRoleCode(role);
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);
        return layerId;
    }


    private Integer update(Integer layer_group, String showName
            ,String role
            , String tableName,List<JTableColumn> tableColumns){
        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "修改");
        layer.setTableName(tableName);
        layer.setType("update");
        layer.setRoleCode(role);
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);

        shibie(tableColumns, layerId, role);

        return layerId;

    }
    private Integer list(Integer layer_group, String showName
            , String role
            , String tableName, List<JTableColumn> tableColumns){

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "列表");
        layer.setTableName(tableName);
        layer.setType("list");
        layer.setRoleCode(role);
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);

        shibie(tableColumns, layerId,role);
        return layerId;
    }


    /**
     * 识别
     */
    private void shibie(List<JTableColumn> tableColumns, Integer layerId, String role){

        for (JTableColumn tableColumn : tableColumns) {
            String columnName = tableColumn.getName();
            LayerItem layerItem = new LayerItem();
            layerItem.setField(tableColumn.getName());
            layerItem.setTitle(tableColumn.getShowName());
            layerItem.setLayerId(layerId);

            //todo 相似字段处理 在设置里面，设置新增忽略字段名列表 黑名单

            String ifNull = tableColumn.getIfNull();
            if("YES".equals(ifNull)){
                layerItem.setMustFlag(true);
            }

            if(tableColumn.getType().equals("datetime")){
                layerItem.setType("from-server");
                layerItem.setTypeKvlist("{type:datetime}");
            }

            if(columnName.contains(role)){
                layerItem.setType("from-server");
                layerItem.setTypeKvlist("{type:user_id}");
            }

            if (columnName.contains("_id")) {
                layerItem.setType("fk-select");
                layerItem.setTypeKvlist("{t:" + columnName.substring(0, columnName.length() - 3)+"}");
            }

            if(columnName.contains("_img")){
                layerItem.setType("img");
            }

            if (columnName.contains(" ") && RegUtil.hasNumber(columnName)){
                layerItem.setType("select");
                int i = columnName.indexOf(" ");
                String title = columnName.substring(0, i);
                String kv = columnName.substring(i + 1, columnName.length());

                layerItem.setTitle(title);
                layerItem.setTypeKvlist(kv);
            }

            if (layerItem.getTitle().length() > 2) {
                layerItem.setWidth(120);
            }

            //主键
            String keyFlag = tableColumn.getKeyFlag();
            if("true".equals(keyFlag)){
                layerItem.setType("id");
                layerItem.setHiddenFlag(true);
            }

            layerItem.insert();

        }

    }

    public void addLogin(String tableName, String role) {
        JTableInfo tableInfo = new JTableInfo()
                .selectOne(new QueryWrapper<JTableInfo>().eq(DBConstants.name,tableName));

        List<JTableColumn> tableColumns = new JTableColumn()
                .selectList(new QueryWrapper<JTableColumn>().eq(DBConstants.table_name, tableName));

        String showName = tableInfo.getShowName();

        LayerGroup layerGroup = new LayerGroup();
        layerGroup.setName(showName+"登录组");
        layerGroup.setRoleCode(role);
        layerGroup.insert();

        Integer layer_group = IdUtil.maxId(DBConstants.okgfd_layer_group, jdbcTemplate);


        check(layer_group, showName, role, tableName,tableColumns);
        register(layer_group, showName, role, tableName,tableColumns);
        profile(layer_group, showName, role, tableName,tableColumns);
        updatePassword(layer_group, showName, role, tableName,tableColumns);
        //登录 check 注册 create 个人资料 update 修改密码 update
    }

    private void updatePassword(Integer layer_group, String showName
            , String role
            , String tableName, List<JTableColumn> tableColumns) {

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "修改密码");
        layer.setTableName(tableName);
        layer.setType("update");
        layer.setRoleCode(role);
        layer.setTag("update_password");
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);

        for (JTableColumn tableColumn : tableColumns) {
            String columnName = tableColumn.getName();

            if(columnName.contains("password") || columnName.contains("pwd")){
                LayerItem layerItem = new LayerItem();
                layerItem.setField(tableColumn.getName());
                layerItem.setTitle(tableColumn.getShowName());
                layerItem.setLayerId(layerId);
                layerItem.insert();
            }



        }

    }

    private void profile(Integer layer_group, String showName
            , String role
            , String tableName, List<JTableColumn> tableColumns) {

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "资料");
        layer.setTableName(tableName);
        layer.setType("update");
        layer.setRoleCode(role);
        layer.setTag("profile");
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);

        shibie(tableColumns, layerId, role);

    }

    private void check(Integer layer_group, String showName
            ,String role
            , String tableName,List<JTableColumn> tableColumns){

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "登录");
        layer.setTableName(tableName);
        layer.setType("check");
        layer.setRoleCode(role);
        layer.setTag("login");
        layer.insert();

    }

    private void register(Integer layer_group, String showName
            ,String role
            , String tableName,List<JTableColumn> tableColumns){

        Layer layer = new Layer();
        layer.setGroupId(layer_group);
        layer.setName(showName + "注册");
        layer.setTableName(tableName);
        layer.setType("create");
        layer.setRoleCode(role);
        layer.setTag("register");
        layer.insert();

        Integer layerId = IdUtil.maxId(DBConstants.okgfd_layer, jdbcTemplate);

        shibie(tableColumns, layerId, role);

    }

}
