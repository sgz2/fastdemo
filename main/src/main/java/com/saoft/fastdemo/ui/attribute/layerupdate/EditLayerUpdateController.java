package com.saoft.fastdemo.ui.attribute.layerupdate;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.service.LayerService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.attribute.layerhelp.LayerHelp;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.FormUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.StringUtils;


import javax.annotation.PostConstruct;
import java.awt.*;
import java.util.List;
import java.util.stream.Collectors;

@org.springframework.stereotype.Component("EditLayerUpdateController")
public class EditLayerUpdateController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    EditLayerUpdate editLayerUpdate;

    @Autowired
    PreviewController previewController;


    @Autowired
    LayerService layerService;

    @PostConstruct
    void init(){
        ControllerUtil.registerAction(editLayerUpdate.getHelpBtn(), e -> help());
        ControllerUtil.registerAction(editLayerUpdate.getKvBtn(), e -> kvSplash());
        ControllerUtil.registerAction(editLayerUpdate.getOkButton(), e -> save());
        ControllerUtil.registerAction(editLayerUpdate.getCancelButton(), e -> hide());
    }

    private void help() {
        LayerHelp layerHelpFrame = new LayerHelp(null);
        layerHelpFrame.setVisible(true);
    }

    /*修改表名下的所有配置*/
    void kvSplash(){
        LayerItem layerUpdate = editLayerUpdate.getLayerItem();
        LayerItem update = new LayerItem();
        FormUtil.extractData(editLayerUpdate,update);

        if(layerUpdate!=null){
            update.setId(layerUpdate.getId());
            //如果要溅射的 type kvtypelist 不为空
            if (StringUtils.hasLength(update.getType())) {
                Integer layerId = layerUpdate.getLayerId();
                String tableName = new Layer().selectById(layerId).getTableName();
                List<Layer> layerList = layerService.listByTableName(tableName);

                List<Integer> collect = layerList.stream()
                        .map(Layer::getId)
                        .collect(Collectors.toList());

                LayerItem subUpdate = new LayerItem();
                subUpdate.setType(update.getType());
                subUpdate.setTypeKvlist(update.getTypeKvlist());
                subUpdate.setTitle(update.getTitle());

                subUpdate.update(new QueryWrapper<LayerItem>()
                        .in(DBConstants.layer_id, collect)
                        .eq(DBConstants.field,update.getField())
                );

                //修改所有系统表名的该列配置
//                String ids = StringUtils.collectionToDelimitedString(collect, ",");
//                String sql = "update %s set %s=?,%s=? where %s in ("+ids+") and %s=?";
//                String format = String.format(sql
//                        , DBConstants.okgfd_layer_item
//                        , DBConstants.type
//                        , DBConstants.type_kvlist
//                        , DBConstants.field);
//                jdbcTemplate.update(format
//                        , update.getType()
//                        , update.getTypeKvlist()
//                        , update.getField());
            }
        }
    }

    void save(){
        LayerItem layerUpdate = editLayerUpdate.getLayerItem();
        LayerItem update = new LayerItem();
        if(layerUpdate!=null){
            update.setId(layerUpdate.getId());
        }

        Component currentTab = previewController.getCurrentTab();
        LayerUpdateFrame layerUpdateFrame = (LayerUpdateFrame) currentTab;
        Integer id = layerUpdateFrame.getLayer().getId();
        update.setLayerId(id);
        FormUtil.extractData(editLayerUpdate,update);
        update.insertOrUpdate();

        LayeUpdateFrameController controller = layerUpdateFrame.getController();
        controller.initByLayer(layerUpdateFrame.getLayer());
        hide();
    }

    void hide(){
        editLayerUpdate.setVisible(false);
    }
}
