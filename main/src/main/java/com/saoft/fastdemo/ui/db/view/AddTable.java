/*
 * Created by JFormDesigner on Thu Jun 06 18:35:39 CST 2019
 */

package com.saoft.fastdemo.ui.db.view;

import com.saoft.fastdemo.bean.JColumn;
import com.saoft.fastdemo.ui.db.model.AddTableModel;
import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;
import org.springframework.stereotype.Component;

import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("AddTable")
public class AddTable extends JPanel {
    public AddTable() {
        initComponents();
        initTable();
        after();
    }

    void after(){
        ButtonUtil.convertIconButton(addBtn, ButtonUtil.PLUS);
    }

    //获取表格
    public JTable getTable1() {
        return table1;
    }

    public JButton getAddBtn() {
        return addBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    private void initTable(){
        AddTableModel model = new AddTableModel();
        table1.setModel(model);
        JColumn column = new JColumn();
        column.setIsnull("NO");
        column.setLength(11);
        column.setName("id");
        column.setType("int");


        model.addEntity(column);
        model.fireTableDataChanged();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel1 = new JPanel();
        addBtn = new JButton();
        deleteBtn = new JButton();
        tabbedPane1 = new JTabbedPane();
        panel2 = new JPanel();
        scrollPane1 = new EmptyBorderScrollPane();
        table1 = new JTable();
        panel3 = new JPanel();

        //======== this ========
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        //======== panel1 ========
        {
            panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));

            //---- addBtn ----
            addBtn.setText("\u6dfb\u52a0\u5b57\u6bb5");
            panel1.add(addBtn);

            //---- deleteBtn ----
            deleteBtn.setText("\u5220\u9664\u5b57\u6bb5");
            panel1.add(deleteBtn);
        }
        add(panel1);

        //======== tabbedPane1 ========
        {

            //======== panel2 ========
            {
                panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(table1);
                }
                panel2.add(scrollPane1);
            }
            tabbedPane1.addTab("\u5b57\u6bb5", panel2);

            //======== panel3 ========
            {
                panel3.setLayout(new BoxLayout(panel3, BoxLayout.X_AXIS));
            }
            tabbedPane1.addTab("\u5176\u4ed6", panel3);
        }
        add(tabbedPane1);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel1;
    private JButton addBtn;
    private JButton deleteBtn;
    private JTabbedPane tabbedPane1;
    private JPanel panel2;
    private JScrollPane scrollPane1;
    private JTable table1;
    private JPanel panel3;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
