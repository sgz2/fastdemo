package com.saoft.fastdemo.ui.outline.model;


import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import java.util.LinkedList;
import java.util.List;

public class ControllerTreeModel extends DefaultTreeModel {

    private LinkedList entitys = new LinkedList();

    public ControllerTreeModel() {
        super(new DefaultMutableTreeNode("Root"));
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();

//        root.add(new DefaultMutableTreeNode("Test"));
//        entitys.add("Test");
    }


    public void add(Object value){
        DefaultMutableTreeNode root = (DefaultMutableTreeNode) getRoot();
        root.add(new DefaultMutableTreeNode(value));

        entitys.add(value);
    }

    public void addGroup(LayerGroupNode node,DefaultMutableTreeNode parent){
        if (parent == null) {
            parent = (DefaultMutableTreeNode) getRoot();
        }


        DefaultMutableTreeNode treeNode = new DefaultMutableTreeNode(node);
        parent.add(treeNode);

        List childs = node.getChilds();
        for (Object child : childs) {
            if (child instanceof LayerGroupNode) {
                addGroup((LayerGroupNode) child,treeNode);
            }else{
                treeNode.add(new DefaultMutableTreeNode(child));
            }
        }

    }

}
