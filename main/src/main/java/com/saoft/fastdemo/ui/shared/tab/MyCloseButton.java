package com.saoft.fastdemo.ui.shared.tab;

import javax.swing.*;
import java.awt.*;

public class MyCloseButton extends JButton {
    public MyCloseButton() {
        super("x");
        setBorder(BorderFactory.createEmptyBorder());
        setFocusPainted(false);
        setBorderPainted(false);
        setContentAreaFilled(false);
        setRolloverEnabled(false);
    }
    @Override
    public Dimension getPreferredSize() {
        return new Dimension(16, 16);
    }
}
