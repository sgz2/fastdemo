package com.saoft.fastdemo.ui.shared;

import cn.hutool.core.map.MapUtil;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;

import java.util.HashMap;
import java.util.Map;

public class UiRequestUtil {

    public static Map<String, String> headers() {
        PreferencesUtil bean = S.getBean(PreferencesUtil.class);
        HashMap<String, String> map = MapUtil.newHashMap();
        map.put("Token", bean.getToken());
        return map;
    }
}
