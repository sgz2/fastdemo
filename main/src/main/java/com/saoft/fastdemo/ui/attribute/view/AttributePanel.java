package com.saoft.fastdemo.ui.attribute.view;

import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.panel.BorderUtil;
import com.saoft.fastdemo.ui.shared.panel.TransPanel;
import org.springframework.stereotype.Component;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

@Component("AttributePanel")
public class AttributePanel extends JPanel {

    private JPanel top;
    private JLabel titleText;
    private JPanel content;

    private JButton saveBtn;
    private JButton linkBtn;
    private JButton configBtn;



    public JButton getSaveBtn() {
        return saveBtn;
    }

    public JPanel getContent() {
        return content;
    }

    public JButton getLinkBtn() {
        return linkBtn;
    }

    public JButton getConfigBtn() {
        return configBtn;
    }

    public AttributePanel() {
        initComponents();
        after();
    }

    public void after(){
        ButtonUtil.convertIconButton(linkBtn, ButtonUtil.ATTR_LINK);
        ButtonUtil.convertIconButton(configBtn, ButtonUtil.ATTR_CONFIG);
        ButtonUtil.convertIconButton(saveBtn, ButtonUtil.ATTR_SAVE);
    }

    public void replace(JComponent component) {
        content.removeAll();
        content.add(component,BorderLayout.CENTER);
        this.updateUI();
    }


    private void initComponents() {
        top = new JPanel();
        titleText = new JLabel();
        content = new JPanel();



        JPanel rightBtns = new TransPanel();
        rightBtns.setLayout(new BoxLayout(rightBtns, BoxLayout.X_AXIS));
        //======== this ========
        setLayout(new BorderLayout());

        //======== top ========
        {
            top.setLayout(new BorderLayout());
//            top.setBackground(Color.darkGray);
            top.setBorder(BorderUtil.createTitleBorder());
            //---- title ----
            titleText.setText("属性");
            top.add(titleText,BorderLayout.WEST);

            {
                configBtn = new JButton("配置");
                linkBtn = new JButton("连接");
                saveBtn = new JButton("保存");

                rightBtns.add(configBtn);
                rightBtns.add(linkBtn);
                rightBtns.add(saveBtn);

                JButton rightClose = new JButton("RightClose");
                ButtonUtil.convertIconButton(rightClose, ButtonUtil.RIGHT_CLOSE);
                rightBtns.add(rightClose);
            }
            top.add(rightBtns, BorderLayout.EAST);
        }
        add(top,BorderLayout.NORTH);
        //======== content ========
        {
            content.setLayout(new BorderLayout());
            content.setPreferredSize(new Dimension(20,300));
        }
        add(content,BorderLayout.CENTER);
    }
}
