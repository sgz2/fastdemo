package com.saoft.fastdemo.ui.shared;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class ButtonUtil {

    /*图标名字*/
    public final static String PLUS = "plus";
    public final static String ADD_TABLE = "add_table";
    public final static String EDIT_TABLE = "edit_table";
    public final static String DELETE_TABLE = "delete_table";
    public final static String IMPORT_TABLE = "import_table";
    public final static String USER = "user";

    public final static String ATTR_LINK = "attr_link";
    public final static String REFRESH = "refresh";
    public final static String RUN = "run";
    public static final String EXPORT = "export";


    public final static String ATTR_SAVE = "attr_save";
    public final static String ATTR_CONFIG = "attr_config";

    public final static String RIGHT_CLOSE = "right_close";
    public final static String LEFT_CLOSE = "left_close";
    public static final String LOGIN = "login";
    public static final String LOGOUT = "logout";


    /**
     * 设置按钮为图标类型
     * @param btn
     * @param imgType 图标样式或者说用哪个图标
     */
    public static void convertIconButton(JButton btn,String imgType){
        setIcon(btn, imgType,true);
        String text = btn.getText();
        btn.setToolTipText(text);//把原有文字设置为鼠标放上后才有效果
        btn.setText("");//不显示文字
    }

    /**
     * 为按钮添加图标
     * @param btn
     * @param imgType 图标样式或者说用哪个图标
     */
    public static void setIcon(JButton btn,String imgType){
        setIcon(btn, imgType,false);
    }
    public static void setIcon(JButton btn,String imgType,boolean hover){
        try {
            BufferedImage read = ImageIO.read(ButtonUtil.class.getResource("/icon/"+imgType+".png"));
            btn.setIcon(new ImageIcon(read));//设置图标
            if(hover){
                BufferedImage read2 = ImageIO.read(ButtonUtil.class.getResource("/icon/"+imgType+"@hover.png"));
                btn.setRolloverIcon(new ImageIcon(read2));//设置鼠标放上的图标
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        btn.setMargin(new Insets(0,0,0,0));//设置边距
        btn.setBorderPainted(false);//不绘制边框
        btn.setFocusPainted(false);//选中后不绘制边框
        btn.setContentAreaFilled(false);//不绘制按钮区域
    }
}
