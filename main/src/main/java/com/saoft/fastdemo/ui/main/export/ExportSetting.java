package com.saoft.fastdemo.ui.main.export;

/**
 * 导出设置
 */
public class ExportSetting {

    private String project_name;
    private String position;
    private String author = "saoft";


    private String username = "root";
    private String password = "root";
    private String database = "db_test";
    private String type = "mysql";


    private String naming = "caml";
    private String table_prefix;
    private String column_prefix;

    private String base_pkg = "com.saoft";
    private String entity_pkg = "entity";
    private String dao_pkg = "dao";
    private String daoimpl_pkg = "dao.impl";
    private String service_pkg = "service";
    private String serviceimpl_pkg = "service.impl";
    private String controller_pkg ="controller";

    private String entity_name = "%s";
    private String dao_name = "%sDao";
    private String daoimpl_name = "%sDaoImpl";
    private String service_name = "%sService";
    private String serviceimpl_name = "%sServiceImpl";
    private String controller_name = "%sController";


    private String maven;
    private String framework;

    public String getMaven() {
        return maven;
    }

    public void setMaven(String maven) {
        this.maven = maven;
    }

    public String getFramework() {
        return framework;
    }

    public void setFramework(String framework) {
        this.framework = framework;
    }

    public String getProject_name() {
        return project_name;
    }

    public void setProject_name(String project_name) {
        this.project_name = project_name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNaming() {
        return naming;
    }

    public void setNaming(String naming) {
        this.naming = naming;
    }

    public String getTable_prefix() {
        return table_prefix;
    }

    public void setTable_prefix(String table_prefix) {
        this.table_prefix = table_prefix;
    }

    public String getColumn_prefix() {
        return column_prefix;
    }

    public void setColumn_prefix(String column_prefix) {
        this.column_prefix = column_prefix;
    }

    public String getBase_pkg() {
        return base_pkg;
    }

    public void setBase_pkg(String base_pkg) {
        this.base_pkg = base_pkg;
    }

    public String getEntity_pkg() {
        return entity_pkg;
    }

    public void setEntity_pkg(String entity_pkg) {
        this.entity_pkg = entity_pkg;
    }

    public String getDao_pkg() {
        return dao_pkg;
    }

    public void setDao_pkg(String dao_pkg) {
        this.dao_pkg = dao_pkg;
    }

    public String getDaoimpl_pkg() {
        return daoimpl_pkg;
    }

    public void setDaoimpl_pkg(String daoimpl_pkg) {
        this.daoimpl_pkg = daoimpl_pkg;
    }

    public String getService_pkg() {
        return service_pkg;
    }

    public void setService_pkg(String service_pkg) {
        this.service_pkg = service_pkg;
    }

    public String getServiceimpl_pkg() {
        return serviceimpl_pkg;
    }

    public void setServiceimpl_pkg(String serviceimpl_pkg) {
        this.serviceimpl_pkg = serviceimpl_pkg;
    }

    public String getController_pkg() {
        return controller_pkg;
    }

    public void setController_pkg(String controller_pkg) {
        this.controller_pkg = controller_pkg;
    }

    public String getEntity_name() {
        return entity_name;
    }

    public void setEntity_name(String entity_name) {
        this.entity_name = entity_name;
    }

    public String getDao_name() {
        return dao_name;
    }

    public void setDao_name(String dao_name) {
        this.dao_name = dao_name;
    }

    public String getDaoimpl_name() {
        return daoimpl_name;
    }

    public void setDaoimpl_name(String daoimpl_name) {
        this.daoimpl_name = daoimpl_name;
    }

    public String getService_name() {
        return service_name;
    }

    public void setService_name(String service_name) {
        this.service_name = service_name;
    }

    public String getServiceimpl_name() {
        return serviceimpl_name;
    }

    public void setServiceimpl_name(String serviceimpl_name) {
        this.serviceimpl_name = serviceimpl_name;
    }

    public String getController_name() {
        return controller_name;
    }

    public void setController_name(String controller_name) {
        this.controller_name = controller_name;
    }
}
