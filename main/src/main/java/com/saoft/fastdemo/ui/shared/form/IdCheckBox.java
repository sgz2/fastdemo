package com.saoft.fastdemo.ui.shared.form;

import javax.swing.*;
import java.util.Objects;

public class IdCheckBox extends JCheckBox implements IdAble {

    private String id;

    @Override
    public String getId() {
        return id;
    }

    @Override
    public Object getValue() {
        return getSelectedObjects();
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdCheckBox that = (IdCheckBox) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
