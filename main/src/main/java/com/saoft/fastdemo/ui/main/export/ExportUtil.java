package com.saoft.fastdemo.ui.main.export;

public class ExportUtil {


    /**
     * 处理多余的斜线
     * @param s
     */
    public static String replaceLine(String s) {
        return s.replaceAll("//", "/");
    }
}
