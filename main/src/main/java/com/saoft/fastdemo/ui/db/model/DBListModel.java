package com.saoft.fastdemo.ui.db.model;

import com.saoft.fastdemo.ui.shared.model.SaoftDefaultListModel;
import org.springframework.stereotype.Component;

@Component("DBListModel")
public class DBListModel extends SaoftDefaultListModel<String> {

}
