package com.saoft.fastdemo.ui.outline.view;

import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * 初始化 表格的右键菜单
 */
@Component("RolePopupMenu")
public class RolePopupMenu extends JPopupMenu {


    /**
     * 添加菜单并编辑
     * @param title
     * @param listener
     */
    public JMenuItem addMenuItem(String title, ActionListener listener){
        JMenuItem item = new JMenuItem(title);
        item.addActionListener(listener);
        return add(item);
    }


    public void addSubMenu(JMenu parent,String title, ActionListener listener){
        JMenuItem item = new JMenuItem(title);
        parent.add(item);
        item.addActionListener(listener);
    }

    public JMenu addMenu(String title) {
        JMenu item = new JMenu(title);
        add(item);
        return item;
    }
}
