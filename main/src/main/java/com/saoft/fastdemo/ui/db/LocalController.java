package com.saoft.fastdemo.ui.db;

import com.saoft.dbinfo.DbInfoBuilder;
import com.saoft.dbinfo.IDbUpdate;
import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.dbinfo.updates.H2Update;
import com.saoft.dbinfo.updates.MysqlUpdate;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.JTableInfo;
import com.saoft.fastdemo.system.RuntimeDataSource;
import com.saoft.fastdemo.ui.db.model.DBListModel;
import com.saoft.fastdemo.ui.db.model.TableListModel;
import com.saoft.fastdemo.ui.db.view.DbTablePanel;
import com.saoft.fastdemo.ui.db.view.local.LocalDBConfig;
import com.saoft.fastdemo.ui.db.view.local.LocalStep2Frame;
import com.saoft.fastdemo.ui.db.view.local.LocalStep3Frame;
import com.saoft.fastdemo.ui.db.view.local.dto.DBConfigDTO;
import com.saoft.fastdemo.ui.db.view.local.util.DBUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.util.List;

/**
 * 本地导入
 */
@Component("LocalController")
public class LocalController {

    LocalDBConfig localStep1Frame;
    LocalStep2Frame localStep2Frame;
    LocalStep3Frame localStep3Frame;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    DbTablePanel dbTablePanel;

    @Autowired
    public LocalController(LocalDBConfig localStep1Frame, LocalStep2Frame localStep2Frame, LocalStep3Frame localStep3Frame) {
        this.localStep1Frame = localStep1Frame;
        this.localStep2Frame = localStep2Frame;
        this.localStep3Frame = localStep3Frame;

        bind();
    }

    void bind(){
        ControllerUtil.registerAction(localStep1Frame.getOkButton(),(e)-> next1());
        ControllerUtil.registerAction(localStep1Frame.getCancelButton(),(e)-> localStep1Frame.setVisible(false));

        ControllerUtil.registerAction(localStep2Frame.getPbtn(),(e)-> pre2());
        ControllerUtil.registerAction(localStep2Frame.getNbtn(),(e)-> next2());
        ControllerUtil.registerAction(localStep2Frame.getCbtn(),(e)-> localStep2Frame.setVisible(false));

        ControllerUtil.registerAction(localStep3Frame.getPbtn(),(e)-> pre3());
        ControllerUtil.registerAction(localStep3Frame.getFbtn(),(e)-> finish());
        ControllerUtil.registerAction(localStep3Frame.getCbtn(),e->localStep3Frame.setVisible(false));

        localStep1Frame.loadData();
    }




    void next1(){

        DBConfigDTO data = localStep1Frame.getData();

        //todo 输入值验证
        try {
            List<String> allDB = DBUtil.getAllDB(data);
            JList dbList = localStep2Frame.getDbList();
            DBListModel model = (DBListModel) dbList.getModel();
            model.clear();
            model.addEntities(allDB);

            localStep1Frame.setVisible(false);
            localStep2Frame.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null,"数据库连接失败");
        }

    }



    void pre2(){

        //
        localStep1Frame.setVisible(true);
        localStep2Frame.setVisible(false);
    }
    void next2(){

        DBConfigDTO data = localStep1Frame.getData();

        Object value = localStep2Frame.getDbList().getSelectedValue();
        if (value == null) {
            JOptionPane.showMessageDialog(null,"请选择数据库");
            return;
        }
        DbInfoBuilder builder = new DbInfoBuilder(DBUtil.getConn(data,value.toString()));
        List<TableInfo> allTable = builder.getTableInfoList();

        JList tlist = localStep3Frame.getTlist();
        TableListModel model = (TableListModel) tlist.getModel();
        model.clear();
        model.addEntities(allTable);

        localStep3Frame.setVisible(true);
        localStep2Frame.setVisible(false);
    }


    void pre3(){
        localStep3Frame.setVisible(false);
        localStep2Frame.setVisible(true);
    }
    void finish(){
        //导入表
        List selectedValuesList = localStep3Frame.getTlist().getSelectedValuesList();

        for (Object o : selectedValuesList) {
            TableInfo tableInfo = (TableInfo) o;
            //加入数据表
            JTableInfo info = new JTableInfo();
            info.setName(tableInfo.getName());
            info.setShowName(tableInfo.getComment());

            info.insert();

            List<TableField> fields = tableInfo.getFields();
            for (TableField field : fields) {
                JTableColumn column = new JTableColumn();
                column.setTableName(tableInfo.getName());
                column.setName(field.getName());
                column.setShowName(field.getComment());
                column.setType(field.getType());
                column.setKeyFlag(field.isKeyFlag()+"");
                column.insert();
            }

            //建表
            RuntimeDataSource dataSource = (RuntimeDataSource) jdbcTemplate.getDataSource();
            HikariDataSource dataSource1 = (HikariDataSource) dataSource.getDataSource();
            String driverClassName = dataSource1.getDriverClassName();
            IDbUpdate update = new MysqlUpdate();
            if ("org.h2.Driver".equals(driverClassName)) {
                 update = new H2Update();
            }
            String table = update.createTable(tableInfo);
            jdbcTemplate.execute(table);

        }

        TableListModel model = (TableListModel) dbTablePanel.getTableInfoJList().getModel();
        model.addEntities(selectedValuesList);

        localStep3Frame.setVisible(false);
    }

    public void preOpen(){
        localStep1Frame.setVisible(true);
    }
}
