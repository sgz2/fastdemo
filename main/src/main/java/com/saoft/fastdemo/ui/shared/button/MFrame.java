package com.saoft.fastdemo.ui.shared.button;


import com.saoft.fastdemo.Application;

import javax.swing.*;
import java.awt.*;

public class MFrame extends JFrame {

    public MFrame() throws HeadlessException {
        setTitle(Application.M_PROJECT_NAME);
        this.setIconImages(ImageComponent.imageList);
    }

    /**
     * 设置二级标题
     * @param subTitle
     */
    public void setSubTitle(String subTitle){
        setTitle(subTitle + " - " + Application.M_PROJECT_NAME);
    }
}
