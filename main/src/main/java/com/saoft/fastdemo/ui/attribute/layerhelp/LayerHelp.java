/*
 * Created by JFormDesigner on Thu Sep 12 12:10:52 CST 2019
 */

package com.saoft.fastdemo.ui.attribute.layerhelp;

import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;
import org.springframework.core.io.ClassPathResource;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class LayerHelp extends JDialog {


    public LayerHelp(JDialog owner) {
        super(owner);
        initComponents();
        after();
    }

    private void after(){
        ClassPathResource resource = new ClassPathResource("/help/layeritem_help.html");
        try {
            content.setPage(resource.getURL());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        scrollPane1 = new EmptyBorderScrollPane();
        content = new JEditorPane();

        //======== this ========
        setMinimumSize(new Dimension(500, 600));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(content);
        }
        contentPane.add(scrollPane1, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JScrollPane scrollPane1;
    private JEditorPane content;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
