package com.saoft.fastdemo.ui.shared.button;

import com.saoft.fastdemo.util.BrowseUtil;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class HrefButton extends JButton {

    private String url;

    public HrefButton(String url) {
        this.url = url;

        Border emptyBorder = BorderFactory.createEmptyBorder();
        setBorder(emptyBorder);

        addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BrowseUtil.openUrl(url);
            }
        });
    }


}
