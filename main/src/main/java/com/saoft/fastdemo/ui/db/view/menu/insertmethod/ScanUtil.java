package com.saoft.fastdemo.ui.db.view.menu.insertmethod;

import com.saoft.fastdemo.ui.db.view.menu.insertmethod.bean.ServiceDaoData;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.List;

public class ScanUtil {

    public static ServiceDaoData scan(String path){
        File file = new File(path);
        List<String> list = new ArrayList<>();

        if (file.isDirectory()){
            scanFile(file,list);
        }

        ServiceDaoData data = new ServiceDaoData();

        list.stream()
                .map(s -> s.replaceAll("\\\\", "/"))
                .filter(s -> s.contains("/src/"))
                .forEach(s -> {
                    //todo 其他匹配
                    if (s.endsWith("/dao")) {
                        data.setDao(s);
                    }

                    if (s.endsWith("/dao/impl")) {
                        data.setDao_xml(s);
                    }

                    if (s.endsWith("/service")) {
                        data.setService(s);
                    }

                    if (s.endsWith("/service/impl")) {
                        data.setService_impl(s);
                    }
                });

        return data;

    }

    static void scanFile(File file,List<String> list){
        File[] files = file.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });
        list.add(file.getAbsolutePath());
        for (File file1 : files) {
            scanFile(file1, list);
        }
    }
}
