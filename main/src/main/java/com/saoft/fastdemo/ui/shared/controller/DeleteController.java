package com.saoft.fastdemo.ui.shared.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.*;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("DeleteController")
public class DeleteController {

    public void deleteRole(String role) {
        //delete role
        new Role().delete(new QueryWrapper<Role>().eq(DBConstants.code, role));
        //delete group
        new LayerGroup().delete(new QueryWrapper<LayerGroup>().eq(DBConstants.role_code, role));
        //delete layer
        deleteLayerByRole(role);
        //delete menu
        new Menu().delete(new QueryWrapper<Menu>().eq(DBConstants.role, role));

    }

    public void deleteLayer(Layer layer){
        if (layer != null && layer.getId() != null) {
            Integer layerId = layer.getId();
            new LayerItem().delete(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
            new LayerLink().delete(new QueryWrapper<LayerLink>().eq(DBConstants.layer_id, layerId));
        }
    }

    private void deleteLayerByRole(String role){
        List<Layer> layerList = new Layer().selectList(new QueryWrapper<Layer>().eq(DBConstants.role_code, role));
        for (Layer layer : layerList) {
            deleteLayer(layer);
        }
    }
}
