package com.saoft.fastdemo.ui.preview.view;

import com.saoft.fastdemo.uic.CloseableTabbedPaneLayerUI;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component("PreviewPanel")
public class PreviewPanel extends JPanel {

    private JTabbedPane jTabbedPane = new PreviuewJTabbedPane();


    public PreviewPanel(){
        initComponents();
        setBorder(BorderFactory.createEmptyBorder());
        jTabbedPane.setBorder(BorderFactory.createEmptyBorder());
    }

    public void initComponents(){
        JPanel preview = new JPanel();


        JPanel nav = new JPanel();


        preview.setLayout(new BoxLayout(preview, BoxLayout.Y_AXIS));
        nav.setLayout(new BoxLayout(nav, BoxLayout.X_AXIS));
        nav.setPreferredSize(new Dimension(1000,30));
        nav.add(new JLabel("WELCOME"));

        preview.add(nav);
        preview.add(new JLabel("将idea的根目录设置到环境变量Path中可以导出后默认打开项目"));
        setLayout(new BorderLayout());

        CloseableTabbedPaneLayerUI ui = new CloseableTabbedPaneLayerUI();
        ui.setTarget(jTabbedPane);
        add(new JLayer<JTabbedPane>(jTabbedPane, ui));
//        jTabbedPane.addTab("welcome", preview);
//
//        jTabbedPane.getComponents()
    }

    public JTabbedPane getjTabbedPane() {
        return jTabbedPane;
    }

    public void setjTabbedPane(JTabbedPane jTabbedPane) {
        this.jTabbedPane = jTabbedPane;
    }
}
