package com.saoft.fastdemo.ui.main.export;

import com.saoft.fastdemo.db.entity.Setting;
import com.saoft.fastdemo.ui.shared.GBC;
import com.saoft.fastdemo.ui.shared.form.IdAble;
import com.saoft.fastdemo.ui.shared.form.IdCheckBox;
import com.saoft.fastdemo.ui.shared.form.IdInput;
import org.springframework.util.StringUtils;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class ExportForm extends JPanel {

    private List<IdAble> values = new ArrayList();

    private GridBagLayout gridBag = new GridBagLayout();

    public ExportForm() {
        setLayout(gridBag);
    }

    public List<IdAble> getValues() {
        return values;
    }

    public void setForm(List<Setting> list){

        for (int i = 0; i < list.size(); i++) {
            Setting row = list.get(i);
            //title
            setTitle(row.getName(), i);
            //content
            JTextField field = setInput(row.getCode(), row.getValue(), i);
            if("position".equals(row.getCode())){
                JButton button = setChooser("", i);
                button.addActionListener(e -> {
                    JFileChooser chooser = new JFileChooser();
                    chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    chooser.showOpenDialog(null);
                    File file = chooser.getSelectedFile();
                    if (file != null) {
                        String absolutePath = file.getAbsolutePath();
                        field.setText(absolutePath);
                    }
                });
            }else{
                //tips
                setInfo(row.getMemo(),i);
            }

        }

        end(list.size());
    }


    private void setTitle(String title,int row){
        JLabel label = new JLabel(title);
        gridBag.addLayoutComponent(label, new GBC(0,row,1,1).setIpad(20,5).
                setFill(GBC.NONE).setWeight(0, 0));
        add(label);
    }

    private JTextField setInput(String id,String value,int row){
        IdInput field = new IdInput();
        field.setId(id);
        field.setText(value);

        if ("id".equals(id)) {
            field.setEditable(false);
        }

        gridBag.addLayoutComponent(field, new GBC(1,row,1,1).setIpad(120,5).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        values.add(field);
        add(field);
        return field;
    }

    private void setCheckbox(String id,Boolean value,int row){
        IdCheckBox field = new IdCheckBox();
        field.setId(id);
        field.setSelected(value==null?false:value);
        gridBag.addLayoutComponent(field, new GBC(1,row,1,1).setIpad(120,5).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        values.add(field);
        add(field);
    }

    public void setInfo(String info,int row){
        String text = info;
        if (info!=null && info.length() > 10) {
            text = info.substring(0, 10)+"...";
        }
        JLabel label = new JLabel(text);
        label.setToolTipText(info);
        gridBag.addLayoutComponent(label, new GBC(2,row,1,1).setIpad(60,5).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        add(label);
    }

    public JButton setChooser(String info,int row){
        JButton label = new JButton("...");
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
        panel.add(label);
        gridBag.addLayoutComponent(panel, new GBC(2,row,1,1).setIpad(120,5).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        add(panel);
        return label;
    }

    public void end(int rows){
        add(new JLabel(), new GBC(3, rows, 1, 1).setWeight(1, 1).setFill(GBC.BOTH));
    }

}
