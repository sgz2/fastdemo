package com.saoft.fastdemo.ui.attribute;

import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.*;
import com.saoft.fastdemo.service.LayerService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.system.W;
import com.saoft.fastdemo.ui.attribute.form.*;
import com.saoft.fastdemo.ui.attribute.layerlink.LinkDialogController;
import com.saoft.fastdemo.ui.attribute.view.AttributePanel;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.PrefUtil;
import com.saoft.fastdemo.ui.outline.OutlineController;
import com.saoft.fastdemo.ui.shared.ObjectUtil;
import com.saoft.fastdemo.ui.shared.controller.AbstractPanelController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.ui.shared.form.IdAble;
import com.saoft.fastdemo.ui.shared.form.IdCheckBox;
import com.saoft.fastdemo.ui.shared.form.IdComboBox;
import com.saoft.fastdemo.ui.shared.form.IdInput;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Component("AttributeController")
public class AttributeController extends AbstractPanelController {

    @Autowired
    AttributePanel panel;

    @Autowired
    OutlineController outlineController;

    @Autowired
    LayerService layerService;

    private Layer currentLayer;

    @PostConstruct
    void init(){
        ControllerUtil.registerAction(panel.getSaveBtn(),(event)->savePropValue());
        ControllerUtil.registerAction(panel.getLinkBtn(),(event)->bindLink());
        ControllerUtil.registerAction(panel.getConfigBtn(),(event)->bindConfig());

    }


    @Override
    public JPanel getPanel() {
        return panel;
    }

    public Layer getCurrentLayer() {
        return currentLayer;
    }

    void savePropValue(){
        Component[] components = panel.getContent().getComponents();
        if (components.length == 1 && components[0] instanceof PropValuePanel) {
            PropValuePanel propValuePanel = (PropValuePanel) components[0];
            //基础属性
            Layer layer = propValuePanel.getLayer();
            Layer update = new Layer();
            update.setId(layer.getId());

            List<IdAble> idInputList = propValuePanel.getIdInputList();
            for (IdAble idAble : idInputList) {
                String id = idAble.getId();

                if (idAble instanceof IdInput) {
                    String text = ((IdInput)idAble).getText();
                    ObjectUtil.setValue(update,id,text);
                } else if (idAble instanceof IdCheckBox) {
                    boolean selected = ((IdCheckBox)idAble).isSelected();
                    ObjectUtil.setValue(update,id,selected);
                } else if (idAble instanceof IdComboBox) {
                    Object selected = ((IdComboBox)idAble).getSelectedItem();
                    ObjectUtil.setValue(update,id,selected);
                }

            }
            layerService.updateLayer(update);

            outlineController.updateControllerTree(layer.getId());
        }
    }

    public void showAttr(Layer layer) {
        this.currentLayer = layer;
        PropValuePanel propValuePanel = new PropValuePanel();
        propValuePanel.setLayer(layer);
        if (layer != null) {
            JScrollPane scrollPane = baseProperties(layer,propValuePanel.getIdInputList());
            propValuePanel.addTab("基础属性",scrollPane);
        }


        //监听选中
        propValuePanel
                .addChangeListener(e ->
                        PrefUtil.set(W.PropValuePanel_Tab
                                ,propValuePanel.getSelectedIndex()+""));

        panel.replace(propValuePanel);
    }

    private String getString(Object o){
        if (o == null) {
            return "";
        }
        return o.toString();
    }

    /*基础属性*/
    JScrollPane baseProperties(Layer layer, List<IdAble> idInputs) {

        List<Object> list = new ArrayList<>();
        list.add("");
        list.add("create");
        list.add("update");
        list.add("list");
        list.add("delete");
        list.add("menu");
        list.add("index");
        list.add("detail");
        list.add("data");

        LayerFormPanel formPanel = new LayerFormPanel();
        formPanel.setLayer(layer);

        formPanel.setRowInput("ID",DBConstants.id,getString(layer.getId()));
        formPanel.setRowInput("组ID",DBConstants.group_id,getString(layer.getGroupId()));
        formPanel.setRowInput("表",DBConstants.table_name,layer.getTableName());
        formPanel.setRowInput("名称",DBConstants.name,layer.getName());
        formPanel.setRowComboBox("类型",DBConstants.type,layer.getType(),list);
        formPanel.setRowInput("特殊标记",DBConstants.tag,layer.getTag());
        formPanel.setRowCheckBox("可见",DBConstants.visiable_flag,layer.getVisiableFlag());
        formPanel.end();
        idInputs.addAll(formPanel.getValues());

        formPanel.setBorder(BorderFactory.createEmptyBorder());
        JScrollPane scrollPane = new JScrollPane(formPanel);
        scrollPane.setBorder(BorderFactory.createEmptyBorder());
        return scrollPane;
    }

    private void bindConfig(){
        if (currentLayer != null) {
            S.b(ConfigDialogController.class)
                    .bindLayerConfig(currentLayer);
        }

    }

    private void bindLink() {
        if (currentLayer != null) {
            S.getBean(LinkDialogController.class)
                    .bindLayerLink(currentLayer);
        }
    }
}
