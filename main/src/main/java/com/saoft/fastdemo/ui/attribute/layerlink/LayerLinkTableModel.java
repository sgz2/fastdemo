package com.saoft.fastdemo.ui.attribute.layerlink;

import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

import java.util.List;

public class LayerLinkTableModel extends DefaultTableModel<LayerLink> {

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "目标层",
                "显示名",
                "位置标记",
                "参数",
                "cssClass",
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LayerLink layer = entities.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return layer.getLinkLayerId();
            case 1:
                return layer.getTitle();
            case 2:
                return layer.getTag();
            case 3:
                return layer.getParameters();
            case 4:
                return layer.getCssClass();
        }
        return null;
    }

    public List<LayerLink> getList() {
        return super.entities;
    }

    //    @Override
//    public Class<?> getColumnClass(int columnIndex) {
//        Class<?> aClass = getValueAt(0, columnIndex).getClass();
//        return aClass;
//    }
}
