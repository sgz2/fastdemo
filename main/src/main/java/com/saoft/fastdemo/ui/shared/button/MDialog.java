package com.saoft.fastdemo.ui.shared.button;

import com.saoft.fastdemo.Application;

import javax.swing.*;
import java.awt.*;

public class MDialog extends JDialog {

    public MDialog() {
        setTitle(Application.M_PROJECT_NAME);
        this.setIconImages(ImageComponent.imageList);
    }

    public MDialog(Frame owner) {
        super(owner);
        setTitle(Application.M_PROJECT_NAME);
        this.setIconImages(ImageComponent.imageList);
    }

    public MDialog(Frame owner, boolean modal) {
        super(owner, modal);
        setTitle(Application.M_PROJECT_NAME);
        this.setIconImages(ImageComponent.imageList);
    }

    public MDialog(JDialog owner) {
        setTitle(Application.M_PROJECT_NAME);
        this.setIconImages(ImageComponent.imageList);
    }

    /**
     * 设置二级标题
     * @param subTitle
     */
    public void setSubTitle(String subTitle){
        setTitle(subTitle + " - " + Application.M_PROJECT_NAME);
    }
}
