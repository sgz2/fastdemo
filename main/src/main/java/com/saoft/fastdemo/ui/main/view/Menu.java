/*
 * Created by JFormDesigner on Fri Jun 07 08:15:15 CST 2019
 */

package com.saoft.fastdemo.ui.main.view;

import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.panel.LinePanelProxy;
import javafx.scene.layout.Border;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;

/**
 * @author Brainrain
 */
@Component("Menu")
public class Menu extends JPanel {

    private JMenuBar menuBar = new JMenuBar();

    private JMenuItem help;

    private JMenuItem titleSetting;

    private JMenuItem about;
    private JMenuItem refresh;
    private JMenuItem otherSave;
    private JMenuItem open;
    private JMenuItem newCreate;
    private JMenuItem exportSetting;



    public Menu() {

        initComponents();
        initAfter();
    }

    private void initAfter(){

//        ButtonUtil.setIcon(login, ButtonUtil.LOGIN);
        ButtonUtil.convertIconButton(logout, ButtonUtil.LOGOUT);

        menuBar.setBorder(new EmptyBorder(0,0,0,0));

        JMenu menu = new JMenu("文件");
        newCreate = new JMenuItem("新建");
        menu.add(newCreate);

        open = new JMenuItem("打开");
        menu.add(open);

        refresh = new JMenuItem("刷新");
        menu.add(refresh);

        otherSave = new JMenuItem("另存为");
        menu.add(otherSave);

        titleSetting = new JMenuItem("标题设置");
        menu.add(titleSetting);

        exportSetting = new JMenuItem("导出");
        menu.add(exportSetting);

        help = new JMenuItem("帮助");
        about = new JMenuItem("关于");

        menuBar.add(menu);
        menuBar.add(help);
        menuBar.add(about);


        add(menuBar,BorderLayout.WEST);

    }

    public JMenuItem getTitleSetting() {
        return titleSetting;
    }

    public JMenuItem getAbout() {
        return about;
    }

    public JMenuItem getHelp() {
        return help;
    }

    public JMenuItem getNewCreate() {
        return newCreate;
    }

    public JMenuItem getOpen() {
        return open;
    }

    public JMenuItem getOtherSave() {
        return otherSave;
    }

    public JMenuItem getRefresh() {
        return refresh;
    }

    public JMenuItem getExportSetting() {
        return exportSetting;
    }

    private void initComponents() {
        setLayout(new BorderLayout());

        loginPanel = new JPanel();
        onlinePanel = new JPanel();

        {
            loginPanel.setLayout(new BoxLayout(loginPanel, BoxLayout.X_AXIS));

            login = new JButton("登录");
            register = new JButton("注册");
            loginPanel.add(login);
            loginPanel.add(register);
        }
        {
            onlinePanel.setLayout(new BoxLayout(onlinePanel, BoxLayout.X_AXIS));

            logout = new JButton("登出");
            username = new JButton("User");
            ButtonUtil.setIcon(username, ButtonUtil.USER);
            onlinePanel.add(username);
            onlinePanel.add(logout);
        }

        JPanel content = new JPanel();
        content.setLayout(new BoxLayout(content, BoxLayout.X_AXIS));

        content.add(loginPanel);
        content.add(onlinePanel);
        add(content,BorderLayout.EAST);
        PreferencesUtil preferencesUtil = new PreferencesUtil();
        //登录面板
        String token = preferencesUtil.getToken();
        if (!StringUtils.hasLength(token)) {
            render_login();
        }else{
            render_online();
        }

    }


    public void render_login() {
        onlinePanel.setVisible(false);
        loginPanel.setVisible(true);
    }

    public void render_online(){

        PreferencesUtil preferencesUtil = new PreferencesUtil();
        String username = preferencesUtil.getUsername();
        this.username.setText(username);

        onlinePanel.setVisible(true);
        loginPanel.setVisible(false);
    }

    JPanel loginPanel;
    JPanel onlinePanel;
    private JButton login;
    private JButton username;
    private JButton logout;
    private JButton register;

    public JButton getLogout() {
        return logout;
    }

    public JButton getLogin() {
        return login;
    }

    public JButton getRegister() {
        return register;
    }
}
