package com.saoft.fastdemo.ui.db;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.bean.JColumn;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.TableRelation;
import com.saoft.fastdemo.ui.db.model.AddTableModel;
import com.saoft.fastdemo.ui.db.model.EditTableModel;
import com.saoft.fastdemo.ui.db.model.TableRelationTableModel;
import com.saoft.fastdemo.ui.db.util.TableUtil;
import com.saoft.fastdemo.ui.db.view.TableTabPanel;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * 打开表详情
 */
@org.springframework.stereotype.Component("TableTabController")
public class TableTabController {

    @Autowired
    PreviewController previewController;

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void initRelation(){

        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableTabPanel currentTab1 = (TableTabPanel) currentTab;
            TableRelationTableModel model = (TableRelationTableModel) currentTab1.getRelationTable().getModel();

            TableInfo tableInfo = currentTab1.getTableInfo();
            TableRelation query = new TableRelation();
            List<TableRelation> ftableId = query.selectList(new QueryWrapper<TableRelation>().eq(DBConstants.ftable_id, tableInfo.getName()));
            model.clear();
            model.addEntities(ftableId);
        }
    }

    public void initTable(){
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableTabPanel currentTab1 = (TableTabPanel) currentTab;
            JTable dataTable = currentTab1.getDataTable();

            TableInfo tableInfo = currentTab1.getTableInfo();
            DefaultTableModel model = new DefaultTableModel<Map>() {

                @Override
                public String[] getColumnLabels() {
                    List<TableField> fields = tableInfo.getFields();
                    List<String> collect = fields.stream().map(f -> f.getName()).collect(Collectors.toList());
                    return collect.toArray(new String[]{});
                }

                @Override
                public Object getValueAt(int rowIndex, int columnIndex) {
                    Map map = entities.get(rowIndex);
                    List<TableField> fields = tableInfo.getFields();
                    TableField field = fields.get(columnIndex);
                    return map.get(field.getName());
                }
            };

            dataTable.setModel(model);
            String tableColumn = TableUtil.tableColumn(tableInfo);
            String sql = "select " + tableColumn + " from `" + tableInfo.getName() + "` limit 0,1000";
            List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
            model.addEntities(list);
        }

        initStuct();
    }

    /**
     * 初始化结构
     */
    public void initStuct(){
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableTabPanel currentTab1 = (TableTabPanel) currentTab;
            //结构
            JTable stuctTable = currentTab1.getStuctTable();

            TableInfo tableInfo = currentTab1.getTableInfo();
            EditTableModel model = new EditTableModel();

            stuctTable.setModel(model);

            List<JTableColumn> jTableColumns = new JTableColumn().selectList(
                    new QueryWrapper<JTableColumn>()
                            .eq(DBConstants.table_name, tableInfo.getName()));

            model.addEntities(jTableColumns);
        }
    }


}
