/*
 * Created by JFormDesigner on Fri Aug 30 11:50:53 CST 2019
 */

package com.saoft.fastdemo.ui.main.login;

import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("LoginFrame")
public class LoginFrame extends MDialog {

    public LoginFrame(MainFrame owner) {
        super(owner);
        initComponents();
        after();
    }

    public void after(){

    }

    public JTextField getUsername() {
        return username;
    }

    public JPasswordField getPassword() {
        return password;
    }

    public JButton getLoginBtn() {
        return loginBtn;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        label1 = new JLabel();
        loginBtn = new JButton();
        label6 = new JLabel();
        label7 = new JLabel();
        content = new JPanel();
        label3 = new JLabel();
        username = new JTextField();
        label4 = new JLabel();
        password = new JPasswordField();

        //======== this ========
        setMinimumSize(new Dimension(300, 340));
        setResizable(false);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //---- label1 ----
        label1.setText("\u767b\u5f55");
        label1.setHorizontalAlignment(SwingConstants.CENTER);
        label1.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 30));
        contentPane.add(label1, BorderLayout.NORTH);

        //---- loginBtn ----
        loginBtn.setText("\u767b\u5f55");
        loginBtn.setMinimumSize(new Dimension(200, 60));
        loginBtn.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 16));
        contentPane.add(loginBtn, BorderLayout.SOUTH);

        //---- label6 ----
        label6.setText("     ");
        contentPane.add(label6, BorderLayout.WEST);

        //---- label7 ----
        label7.setText("      ");
        contentPane.add(label7, BorderLayout.EAST);

        //======== content ========
        {
            content.setLayout(new GridLayout(6, 1));

            //---- label3 ----
            label3.setText("\u90ae\u7bb1\uff1a");
            content.add(label3);
            content.add(username);

            //---- label4 ----
            label4.setText("\u5bc6\u7801\uff1a");
            content.add(label4);
            content.add(password);
        }
        contentPane.add(content, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JLabel label1;
    private JButton loginBtn;
    private JLabel label6;
    private JLabel label7;
    private JPanel content;
    private JLabel label3;
    private JTextField username;
    private JLabel label4;
    private JPasswordField password;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

}
