package com.saoft.fastdemo.ui.shared.form;

import javax.swing.*;
import java.util.Objects;

public class IdComboBox extends JComboBox implements IdAble{

    private String id;

    public String getId() {
        return id;
    }

    @Override
    public Object getValue() {
        return getSelectedItem();
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdComboBox idInput = (IdComboBox) o;
        return Objects.equals(id, idInput.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
