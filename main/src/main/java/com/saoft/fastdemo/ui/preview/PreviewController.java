package com.saoft.fastdemo.ui.preview;

import com.saoft.fastdemo.ui.preview.view.PreviewPanel;
import com.saoft.fastdemo.ui.shared.controller.AbstractPanelController;
import org.springframework.beans.factory.annotation.Autowired;

import javax.swing.*;
import java.awt.*;

@org.springframework.stereotype.Component("PreviewController")
public class PreviewController extends AbstractPanelController {

    @Autowired
    PreviewPanel previewPanel;

    @Override
    public JPanel getPanel() {
        return previewPanel;
    }

    public void addTab(String title, JComponent component){
        JTabbedPane tabbedPane = previewPanel.getjTabbedPane();
        int i = hasComponent(component);
        if (i >= 0) {
            tabbedPane.setSelectedIndex(i);
        }else{
            component.setBorder(BorderFactory.createEmptyBorder());
            tabbedPane.addTab(title, component);
            int count = tabbedPane.getComponentCount();
            tabbedPane.setSelectedIndex(count-1);
        }
    }


    private int hasComponent(JComponent component){
        JTabbedPane tabbedPane = previewPanel.getjTabbedPane();
        Component[] components = tabbedPane.getComponents();
        for (int i = 0; i < components.length; i++) {
            if (components[i].equals(component)) {
                return i;
            }
        }
        return -1;
    }

    public Component getCurrentTab(){
        JTabbedPane tabbedPane = previewPanel.getjTabbedPane();
        return tabbedPane.getSelectedComponent();
    }
}
