package com.saoft.fastdemo.ui.shared.button;

import org.springframework.core.io.ClassPathResource;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

public class ImageComponent {

    public static final java.util.List<Image> imageList = new ArrayList<>();

    static {
        try {
            URL url = new ClassPathResource("/icon/fastdemo@0,16x.png").getURL();
            URL url1 = new ClassPathResource("/icon/fastdemo@0,32x.png").getURL();
            URL url3 = new ClassPathResource("/icon/fastdemo@0,64x.png").getURL();
            imageList.add(ImageIO.read(url3));
            imageList.add(ImageIO.read(url1));
            imageList.add(ImageIO.read(url));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
