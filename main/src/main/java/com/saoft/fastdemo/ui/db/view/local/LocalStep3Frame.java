/*
 * Created by JFormDesigner on Sun Jun 16 21:08:36 CST 2019
 */

package com.saoft.fastdemo.ui.db.view.local;

import com.saoft.fastdemo.ui.db.model.TableListModel;
import com.saoft.fastdemo.ui.shared.button.MFrame;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("LocalStep3Frame")
public class LocalStep3Frame extends MFrame {

    public LocalStep3Frame() {
        setSubTitle("选择表（可多选）");
        initComponents();
        init();
    }

    void init() {
        tlist.setModel(new TableListModel());
    }

    public JButton getPbtn() {
        return pbtn;
    }


    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        scrollPane1 = new EmptyBorderScrollPane();
        tlist = new JList();
        panel1 = new JPanel();
        pbtn = new JButton();
        fbtn = new JButton();
        cbtn = new JButton();

        //======== this ========
        Container contentPane = getContentPane();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(tlist);
        }
        contentPane.add(scrollPane1);

        //======== panel1 ========
        {
            panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));

            //---- pbtn ----
            pbtn.setText("\u4e0a\u4e00\u6b65");
            panel1.add(pbtn);

            //---- fbtn ----
            fbtn.setText("\u5b8c\u6210");
            panel1.add(fbtn);

            //---- cbtn ----
            cbtn.setText("\u53d6\u6d88");
            panel1.add(cbtn);
        }
        contentPane.add(panel1);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JScrollPane scrollPane1;
    private JList tlist;
    private JPanel panel1;
    private JButton pbtn;
    private JButton fbtn;
    private JButton cbtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JList getTlist() {
        return tlist;
    }

    public JPanel getPanel1() {
        return panel1;
    }

    public JButton getFbtn() {
        return fbtn;
    }

    public JButton getCbtn() {
        return cbtn;
    }
}
