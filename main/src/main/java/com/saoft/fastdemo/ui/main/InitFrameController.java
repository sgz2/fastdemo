package com.saoft.fastdemo.ui.main;

import com.saoft.fastdemo.Application;
import com.saoft.fastdemo.util.CheckProjUtil;
import com.saoft.fastdemo.system.ChooserUtil;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.main.view.NewProjectFrame;
import com.saoft.fastdemo.ui.main.view.NoProjectFrame;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;

import javax.swing.*;
import java.io.File;

public class InitFrameController {

    private NoProjectFrame noProjectFrame = new NoProjectFrame();
    private NewProjectFrame newProjectFrame = new NewProjectFrame();
    private PreferencesUtil preferencesUtil = new PreferencesUtil();

    public InitFrameController() {
        init();
    }

    public void init(){
        ControllerUtil.registerAction(noProjectFrame.getNewBtn(), e -> newProj());
        ControllerUtil.registerAction(noProjectFrame.getOpenBtn(), e -> open());

        ControllerUtil.registerAction(newProjectFrame.getOpenBtn(), e -> openDir());
        ControllerUtil.registerAction(newProjectFrame.getOkButton(),e -> ok());
        ControllerUtil.registerAction(newProjectFrame.getCancelButton(),e -> cancel());
    }

    public NoProjectFrame getNoProjectFrame() {
        return noProjectFrame;
    }

    public NewProjectFrame getNewProjectFrame() {
        return newProjectFrame;
    }

    private void newProj(){
        newProjectFrame.setVisible(true);
    }

    void openDir(){

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(newProjectFrame);
        File file = chooser.getSelectedFile();
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            newProjectFrame.getDirPath().setText(absolutePath);
        }
    }

    void open(){
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
        ChooserUtil.acceptFd(chooser);
        chooser.showOpenDialog(noProjectFrame);
        File file = chooser.getSelectedFile();
        if (file != null) {
            String path = file.getAbsolutePath();
            String s = path.replaceAll("\\\\", "/");
            //设置临时变量
            preferencesUtil.setH2File(s);

            Application.startBoot();
//

        }
    }

    void ok(){
        String text = newProjectFrame.getDirPath().getText();
        String text1 = newProjectFrame.getFname().getText();

        //todo 路劲判断
        String s = text.replaceAll("\\\\", "/");
        s += "/" + text1;
        //新建数据库文件
        CheckProjUtil.createIfNotExists(s);
        PreferencesUtil util = new PreferencesUtil();
        util.setH2File(s);
        //启动springboot
        cancel();
        //刷新界面
        Application.startBoot();
    }

    void cancel(){
        newProjectFrame.setVisible(false);
    }

}
