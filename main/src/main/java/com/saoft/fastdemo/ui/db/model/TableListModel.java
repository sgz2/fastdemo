package com.saoft.fastdemo.ui.db.model;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.shared.model.SaoftDefaultListModel;

import java.util.List;

public class TableListModel extends SaoftDefaultListModel<TableInfo> {

    public List<TableInfo> getData() {
        return entities;
    }
}
