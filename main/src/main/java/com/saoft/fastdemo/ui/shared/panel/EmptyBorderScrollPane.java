package com.saoft.fastdemo.ui.shared.panel;

import javax.swing.*;

public class EmptyBorderScrollPane extends JScrollPane {

    public EmptyBorderScrollPane() {
        setBorder(BorderFactory.createEmptyBorder());
    }
}
