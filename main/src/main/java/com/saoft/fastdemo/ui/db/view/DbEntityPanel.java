package com.saoft.fastdemo.ui.db.view;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.db.model.JEntityInfoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component("DbEntityPanel")
public class DbEntityPanel extends JPanel {

    private JList<TableInfo> jList;

    JEntityInfoModel entityInfoModel;

    @Autowired
    DbEntityPanel(JEntityInfoModel entityInfoModel){
        this.entityInfoModel = entityInfoModel;
        initComponents();
    }

    private void initComponents() {
        jList = new JList(entityInfoModel);
        jList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane paneWithTable = new JScrollPane(jList);
        add(paneWithTable, BorderLayout.CENTER);
    }

    public JList<TableInfo> getjList() {
        return jList;
    }
}
