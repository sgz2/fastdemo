package com.saoft.fastdemo.ui.shared;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.saoft.fastdemo.db.entity.Layer;

import java.lang.reflect.Field;

public class ObjectUtil {

    public static void setValue(Object src, String key, Object value) {
        Class<?> aClass = src.getClass();
        try {
            Field[] fields = aClass.getDeclaredFields();
            for (Field field : fields) {
                TableField annotation = field.getAnnotation(TableField.class);
                if (annotation != null && annotation.value().equals(key)) {
                    Class<?> type = field.getType();
                    if (value == null || type.equals(value.getClass())) {
                        field.setAccessible(true);
                        field.set(src, value);
                    }else{
                        if (value instanceof Layer) {
                            field.setAccessible(true);
                            field.set(src, ((Layer) value).getId());
                        }
                    }
                }
            }
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
