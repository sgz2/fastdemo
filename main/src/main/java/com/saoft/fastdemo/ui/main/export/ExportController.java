package com.saoft.fastdemo.ui.main.export;

import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.file.FileCopier;
import cn.hutool.core.util.ZipUtil;
import cn.hutool.http.HttpException;
import cn.hutool.http.HttpRequest;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.Method;
import com.saoft.fastdemo.db.entity.Role;
import com.saoft.fastdemo.db.entity.Setting;
import com.saoft.fastdemo.util.CheckProjUtil;
import com.saoft.fastdemo.system.FastdemoProperties;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.ConsoleController;
import com.saoft.fastdemo.ui.shared.DirUtil;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.ui.shared.UiRequestUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.util.FileContentUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

@Component("ExportController")
public class ExportController {

    @Autowired
    ExportFrame exportFrame;

    @Autowired
    PreferencesUtil preferencesUtil;

    FastdemoProperties fastdemoProperties;

    //导出设置
    ExportSetting exportSetting = new ExportSetting();

    @PostConstruct
    private void init() {
        ControllerUtil.registerAction(exportFrame.getExportBtn(), e -> export());
    }

    /*导出前检查*/
    private boolean exportBefore() {
        this.exportSetting = extraExportSetting();
        String codeDir = getExportDir();
        //生成sql文件

        //导入sql

        return true;
    }

    /*框架文件*/
    private void exportAfter() {
        String framework = getFramework();
        String webRoot = getWebRoot();
        //maven
        if (isMaven()){
            //源文件
            File file = new File("common/pomxml/"+framework+"/pom.xml");
            if (!file.exists()) {
                file = new File("common/pomxml/bkmansh/pom.xml");
            }

            //目标文件
            String projectDir = getExportProjectDir();
            String webxml = projectDir  + "/pom.xml";
            File file1 = new File(webxml);
            if (!file1.exists()) {
                FileCopier.create(file, file1).copy();

                //替换内容
                FileContentUtil.replace(file1, "comsaoft", exportSetting.getBase_pkg());
                FileContentUtil.replace(file1, "fastdemo_project", getProjectName());

            }

        }else{//copy lib 判断文件夹是否存在
            File file = new File("common/libs/" + framework);
            if (file.exists() && file.isDirectory()) {
                String projectDir = getExportProjectDir();
                String libPath = webRoot + "/WEB-INF/lib";
                File lib = new File(projectDir, libPath);
                FileUtil.copyFilesFromDir(file, lib, true);
            }
        }

        /*根据编辑器设置配置文件*/
        ideConfig();

        //copy 文件 js框架文件
        {
            String uiFramework = getUiFramework();
            File file = new File("common/views/" + uiFramework);
            if (file.exists() && file.isDirectory()) {
                String projectDir = getExportProjectDir();
                File webRootFile = new File(projectDir, webRoot);
                FileCopier.create(file, webRootFile)
                        .setCopyContentIfDir(true)
                        .setOnlyCopyFile(false)
                        .setOverride(true)
                        .copy();
            }
        }

        { //copy tld框架文件
            String uiFramework = getUiFramework();
            File file = new File("common/tld/" + uiFramework);
            if (file.exists() && file.isDirectory()) {
                String projectDir = getExportProjectDir();
                File webRootFile = new File(projectDir, webRoot);
                File tldFile = new File(webRootFile, "WEB-INF");
                FileCopier.create(file, tldFile)
                        .setCopyContentIfDir(true)
                        .setOnlyCopyFile(false)
                        .setOverride(true)
                        .copy();
            }
        }

        //copy webxml
        {
            //源文件
            File file = new File("common/web/"+framework+"/web.xml");
            if (!file.exists()) {
                file = new File("common/web/arh619ssm/web.xml");
            }

            //目标文件
            String projectDir = getExportProjectDir();
            String webxml = projectDir + "/" + webRoot + "/WEB-INF/web.xml";
            File file1 = new File(ExportUtil.replaceLine(webxml));
//            if (!file1.exists()) {
//                FileCopier.create(file, file1).copy();
//            }
            FileCopier.create(file, file1)
                    .setOverride(true)
                    .copy();

        }
    }



    public void export() {
        if (!exportBefore()) {
            return;
        }
        String codeDir = getExportDir();
        LayoutManager layout = exportFrame.getContentPane().getLayout();

        if (layout instanceof CardLayout) {
            CardLayout cardLayout = (CardLayout) layout;
            cardLayout.last(exportFrame.getContentPane());
        }

        new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                try {

                    if (fastdemoProperties == null) {
                        fastdemoProperties = S.getBean(FastdemoProperties.class);
                    }

                    String url = fastdemoProperties.getApiServer()+"/api/export";

                    HttpRequest request = new HttpRequest(url);
                    request.addHeaders(UiRequestUtil.headers());
                    request.setMethod(Method.POST);
                    //发送文件 文件被占用无法发送
                    request.form("version", fastdemoProperties.getVersion());
                    request.form("file", FileUtil.file(preferencesUtil.getH2File()));
                    //释放连接
                    S.closeDataSource();


                    HttpResponse response = request.executeAsync();

                    //重新连接
                    String dbUrl = CheckProjUtil.getH2Url(preferencesUtil.getH2File());
                    S.replaceDataSource(dbUrl);

                    if (response.isOk()) {
                        File tempFile = File.createTempFile("export_", ".zip");
                        response.writeBody(tempFile);

                        ConsoleController.log("download File:"+tempFile.getAbsolutePath());

                        ZipUtil.unzip(tempFile, new File(codeDir));

                        FileUtil.del(tempFile);

                        ConsoleController.log("delete File:"+tempFile.getAbsolutePath());
                        //copy框架文件
                        exportAfter();

                        //if(idea){} 尝试用idea打开
                        if (!DirUtil.openByIdea(getExportProjectDir())) {
                            DirUtil.open(codeDir);
                        }

                    }else{
                        JOptionPane.showMessageDialog(null,"请求失败："+response.getStatus()+"\n更多详情，查看帮助");
                    }


                } catch (HttpException e){
                    //重新连接
                    String dbUrl = CheckProjUtil.getH2Url(preferencesUtil.getH2File());
                    S.replaceDataSource(dbUrl);
                    //不能连接
                    JOptionPane.showMessageDialog(null,"连接服务器失败\n更多详情，查看帮助");
//            e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    //恢复设置页面
                    if (layout instanceof CardLayout) {
                        CardLayout cardLayout = (CardLayout) layout;
                        cardLayout.first(exportFrame.getContentPane());
                    }
                }
                return null;
            }
        }.execute();
    }

    private String getWebRoot() {

        if(isMaven()){
            return  "/src/main/webapp/";
        }else{
            return  "/web/";
        }
    }

    private boolean isMaven() {
        String maven = exportSetting.getMaven();
        return "maven".equalsIgnoreCase(maven);
    }

    private boolean isIdea() {
        return true;
    }

    /*可能存在别名*/
    private String getUiFramework() {
        String templateCode = getTemplateCode();
        if ("aceadmin".equals(templateCode)) {
            return "ace";
        }else if ("huiadmin".equals(templateCode)) {
            return "hui";
        }else if ("adminlte".equals(templateCode)) {
            return "lte";
        }
        return "layui";
    }

    private String getTemplateCode(){
        List<Role> roles = new Role().selectAll();
        for (Role role : roles) {
            String templateCode = role.getTemplateCode();
            if (StringUtils.hasLength(templateCode)) {
                return templateCode;
            }
        }
        return "layui";
    }

    /*后台采用的框架*/
    private String getFramework() {
        String framework = exportSetting.getFramework();
        if (StringUtils.hasLength(framework)) {
            return framework.trim();
        }
        return "arh619ssm";
    }

    /*解压用的导出目录*/
    private String getExportDir() {
        String value = exportSetting.getPosition();
        if (StringUtils.hasLength(value)) {
            return value;
        } else {
            //检测项目文件路径
            String h2File = preferencesUtil.getH2File();
            File parentFile = new File(h2File).getParentFile();
            return parentFile.getAbsolutePath();
        }
    }

    private void ideConfig(){
        //判断风格 ide eclipse idea 设置配置文件
        // 普通web
        if (isIdea()) {
            if (!isMaven()) {
                ideaWebConfig();
            } else {
                ideaMavenConfig();
            }
        }
    }

    private void ideaWebConfig(){
//        if (false){
//            File file = new File("common/idea/web");
//            if (file.exists() && file.isDirectory()) {
//                String projectDir = getExportProjectDir();
//                String idea = ".idea";
//                File lib = new File(projectDir, idea);
//                FileCopier.create(file, lib)
//                        .setCopyContentIfDir(true)
//                        .setOnlyCopyFile(false)
//                        .setOverride(true)
//
//                        .copy();
//                //iml
//                String projectName = getProjectName();
//                File imlFile = new File(lib, "idea.iml");
//                //替换内容
//                FileContentUtil.replace(imlFile, "fastdemo_project", projectName);
//                //重命名
//                FileUtil.rename(imlFile,projectName, true, true);
//
//                //修改 modules.xml
//                File modxml = new File(lib, "modules.xml");
//                FileContentUtil.replace(modxml, "fastdemo_project", projectName);
//            }
//        }
    }

    private void ideaMavenConfig(){

    }

    private void eclipseWebConfig(){

    }

    private void eclipseMavenConfig(){

    }

    private String getExportProjectDir() {
        String dir = getExportDir() + "/" + getProjectName();
        return dir;
    }

    private String getProjectName() {
        String value = exportSetting.getProject_name();

        if (StringUtils.hasLength(value)) {
            return value;
        } else {
            //检测项目文件路径
            String h2File = preferencesUtil.getH2File();
            File file = new File(h2File);
            String name = file.getName();
            String stripName = StringUtils.stripFilenameExtension(name);
            return stripName;
        }
    }

    /**
     * 将客户端的设置，设置到对象里面
     * @return
     */
    private ExportSetting extraExportSetting(){
        ExportSetting exportSetting = new ExportSetting();
        List<Setting> settings = new Setting().selectAll();
        Map<String, String> map = MapUtil.listToMap(settings, "code", "value");
        MapUtil.setValueByMap(exportSetting, map);
        return exportSetting;
    }
}
