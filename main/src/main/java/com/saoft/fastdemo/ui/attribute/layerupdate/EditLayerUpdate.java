/*
 * Created by JFormDesigner on Thu Jul 18 21:57:47 CST 2019
 */

package com.saoft.fastdemo.ui.attribute.layerupdate;

import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import org.springframework.stereotype.Component;

import java.awt.*;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
@Component("EditLayerUpdate")
public class EditLayerUpdate extends MDialog {

    private LayerItem layerItem;

    public EditLayerUpdate(MainFrame owner) {
        super(owner);
        initComponents();
        after();
    }

    private void after(){
        type.addItem("");
        type.addItem("pk");
        type.addItem("text");
        type.addItem("select");
        type.addItem("fk-select");
        type.addItem("radio");
        type.addItem("img");
        type.addItem("date");
        type.addItem("datetime");
        type.addItem("idx-count");
        type.addItem("idx-user");
        type.addItem("idx-weather");
        type.addItem("from-server");

        query.addItem("");
        query.addItem("=");
        query.addItem("like");
    }

    public JButton getKvBtn() {
        return kvBtn;
    }

    public JButton getOkButton() {
        return okButton;
    }

    public JButton getCancelButton() {
        return cancelButton;
    }

    public LayerItem getLayerItem() {
        return layerItem;
    }

    public void setLayerItem(LayerItem layerItem) {
        this.layerItem = layerItem;
    }

    public JButton getHelpBtn() {
        return helpBtn;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        panel1 = new JPanel();
        label1 = new JLabel();
        aliasLab = new JLabel();
        label2 = new JLabel();
        label10 = new JLabel();
        label5 = new JLabel();
        label6 = new JLabel();
        label7 = new JLabel();
        label8 = new JLabel();
        label9 = new JLabel();
        label3 = new JLabel();
        label11 = new JLabel();
        label12 = new JLabel();
        contentPanel = new JPanel();
        field = new JTextField();
        alias = new JTextField();
        title = new JTextField();
        must_flag = new JCheckBox();
        type = new JComboBox();
        type_kvlist = new JTextField();
        validate = new JTextField();
        readonly_flag = new JCheckBox();
        hidden_flag = new JCheckBox();
        query = new JComboBox();
        width = new JTextField();
        sort_by = new JTextField();
        buttonBar = new JPanel();
        kvBtn = new JButton();
        helpBtn = new JButton("帮助");
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(400, 39));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== panel1 ========
            {
                panel1.setPreferredSize(new Dimension(100, 204));
                panel1.setMinimumSize(new Dimension(100, 204));
                panel1.setLayout(new GridLayout(14, 1, 0, 10));

                //---- label1 ----
                label1.setText("\u5b57\u6bb5");
                panel1.add(label1);

                //---- aliasLab ----
                aliasLab.setText("\u522b\u540d");
                panel1.add(aliasLab);

                //---- label2 ----
                label2.setText("\u6807\u9898");
                panel1.add(label2);

                //---- label10 ----
                label10.setText("\u5fc5\u586b");
                panel1.add(label10);

                //---- label5 ----
                label5.setText("\u7c7b\u578b");
                panel1.add(label5);

                //---- label6 ----
                label6.setText("\u53ef\u9009\u7c7b\u578b\u503c");
                panel1.add(label6);

                //---- label7 ----
                label7.setText("\u9a8c\u8bc1");
                panel1.add(label7);

                //---- label8 ----
                label8.setText("\u53ea\u8bfb");
                panel1.add(label8);

                //---- label9 ----
                label9.setText("\u9690\u85cf");
                panel1.add(label9);

                //---- label3 ----
                label3.setText("\u67e5\u8be2\u6761\u4ef6");
                panel1.add(label3);

                //---- label11 ----
                label11.setText("\u5bbd\u5ea6");
                panel1.add(label11);

                //---- label12 ----
                label12.setText("\u6392\u5e8f\u53f7");
                panel1.add(label12);
            }
            dialogPane.add(panel1, BorderLayout.WEST);

            //======== contentPanel ========
            {
                contentPanel.setLayout(new GridLayout(14, 0, 0, 10));
                contentPanel.add(field);
                contentPanel.add(alias);
                contentPanel.add(title);
                contentPanel.add(must_flag);
                contentPanel.add(type);
                contentPanel.add(type_kvlist);
                contentPanel.add(validate);
                contentPanel.add(readonly_flag);
                contentPanel.add(hidden_flag);
                contentPanel.add(query);
                contentPanel.add(width);
                contentPanel.add(sort_by);

            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0, 0.0};

                buttonBar.add(helpBtn, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                        GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                        new Insets(0, 0, 0, 5), 0, 0));
                //---- kvBtn ----
                kvBtn.setText("kv\u6e85\u5c04");
                buttonBar.add(kvBtn, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- okButton ----
                okButton.setText("OK");
                buttonBar.add(okButton, new GridBagConstraints(3, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                buttonBar.add(cancelButton, new GridBagConstraints(4, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel panel1;
    private JLabel label1;
    private JLabel aliasLab;
    private JLabel label2;
    private JLabel label10;
    private JLabel label5;
    private JLabel label6;
    private JLabel label7;
    private JLabel label8;
    private JLabel label9;
    private JLabel label3;
    private JLabel label11;
    private JLabel label12;
    private JPanel contentPanel;
    @TableField(value = DBConstants.field)
    private JTextField field;
    @TableField(value = DBConstants.alias)
    private JTextField alias;
    @TableField(value = DBConstants.title)
    private JTextField title;
    @TableField(value = DBConstants.must_flag)
    private JCheckBox must_flag;
    @TableField(value = DBConstants.type)
    private JComboBox type;
    @TableField(value = DBConstants.type_kvlist)
    private JTextField type_kvlist;
    @TableField(value = DBConstants.validate)
    private JTextField validate;
    @TableField(value = DBConstants.readonly_flag)
    private JCheckBox readonly_flag;
    @TableField(value = DBConstants.hidden_flag)
    private JCheckBox hidden_flag;
    @TableField(value = DBConstants.query)
    private JComboBox query;
    @TableField(value = DBConstants.width)
    private JTextField width;
    @TableField(value = DBConstants.sort_by)
    private JTextField sort_by;
    private JPanel buttonBar;
    private JButton kvBtn;
    private JButton helpBtn;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
