package com.saoft.fastdemo.ui.db;

import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.db.entity.TableRelation;
import com.saoft.fastdemo.ui.db.model.TableListModel;
import com.saoft.fastdemo.ui.db.view.DbTablePanel;
import com.saoft.fastdemo.ui.db.view.RelationFrame;
import com.saoft.fastdemo.ui.db.view.local.dto.RelationDTO;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.List;

/**
 * 添加表的映射关系
 */
@Component("RelationFrameController")
public class RelationFrameController {

    @Autowired
    RelationFrame relationFrame;

    @Autowired
    DbTablePanel dbTablePanel;

    @Autowired
    TableTabController tableTabController;

    @PostConstruct
    void init(){
        ControllerUtil.registerAction(relationFrame.getOkBtn(),(e)->add());
        ControllerUtil.registerAction(relationFrame.getCelBtn(),(e)->cel());
    }

    /*添加关系*/
    void add(){
        //添加关系到数据库
        TableRelation relation = relationFrame.getRelation();
        relation.insert();
        relationFrame.setVisible(false);
        //刷新关系表格
        tableTabController.initRelation();
    }

    void cel(){
        relationFrame.setVisible(false);
    }

    /*init 添加关系的选择项*/
    public void initForm(TableInfo info){
        relationFrame.setFromTable(info);
        JComboBox getrSel = relationFrame.getrSel();
        getrSel.removeAllItems();
        getrSel.addItem(RelationDTO.m2o());
        getrSel.addItem(RelationDTO.o2m());

        JComboBox formSel = relationFrame.getFormSel();
        formSel.removeAllItems();
        List<TableField> fields = info.getFields();
        for (TableField field : fields) {
            formSel.addItem(field);
        }

        TableListModel model = (TableListModel) dbTablePanel.getTableInfoJList().getModel();
        List<TableInfo> data = model.getData();
        JComboBox tableSel = relationFrame.getToTableSel();
        tableSel.removeAllItems();
        for (TableInfo datum : data) {
            tableSel.addItem(datum);
        }
        ItemListener[] itemListeners = tableSel.getItemListeners();
        if(itemListeners.length==1){
            tableSel.addItemListener(new ItemListener() {
                @Override
                public void itemStateChanged(ItemEvent e) {
                    TableInfo selectedItem = (TableInfo) tableSel.getSelectedItem();
                    initToSel(selectedItem);
                }
            });
        }

    }

    void initToSel(TableInfo info){
        JComboBox toSel = relationFrame.getToSel();

        toSel.removeAllItems();
        List<TableField> fields = info.getFields();

        for (TableField field : fields) {
            toSel.addItem(field);
        }
    }
}
