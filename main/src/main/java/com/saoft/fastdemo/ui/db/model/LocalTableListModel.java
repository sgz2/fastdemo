package com.saoft.fastdemo.ui.db.model;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.shared.model.SaoftDefaultListModel;
import org.springframework.stereotype.Component;

@Component("LocalTableListModel")
public class LocalTableListModel extends SaoftDefaultListModel<TableInfo> {
}
