/*
 * Created by JFormDesigner on Tue Jun 25 19:40:15 CST 2019
 */

package com.saoft.fastdemo.ui.attribute.menu;

import java.awt.*;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.ui.attribute.model.MenuTableModel;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;

import java.util.Objects;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class MenuFrame extends JPanel {

    private MenuFrameController controller;
    private Layer layer;

    public MenuFrame() {
        initComponents();
        initAfter();
    }


    void initAfter(){
        menuTable.setModel(new MenuTableModel());
    }


    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MenuFrame that = (MenuFrame) o;
        return Objects.equals(layer.getId(), that.layer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(layer.getId());
    }

    public MenuFrameController getController() {
        return controller;
    }

    public void setController(MenuFrameController controller) {
        this.controller = controller;
    }

    public JTable getMenuTable() {
        return menuTable;
    }

    public JButton getAddBtn() {
        return addBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        scrollPane1 = new JScrollPane();
        menuTable = new JTable();
        panel1 = new JPanel();
        addBtn = new JButton();
        editBtn = new JButton();
        deleteBtn = new JButton();

        //======== this ========
        setLayout(new BorderLayout());

        //======== scrollPane1 ========
        {
            scrollPane1.setViewportView(menuTable);
        }
        add(scrollPane1, BorderLayout.CENTER);

        //======== panel1 ========
        {
            panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));

            //---- addBtn ----
            addBtn.setText("\u65b0\u589e");
            panel1.add(addBtn);

            //---- editBtn ----
            editBtn.setText("\u7f16\u8f91");
            panel1.add(editBtn);

            //---- deleteBtn ----
            deleteBtn.setText("\u5220\u9664");
            panel1.add(deleteBtn);
        }
        add(panel1, BorderLayout.NORTH);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JScrollPane scrollPane1;
    private JTable menuTable;
    private JPanel panel1;
    private JButton addBtn;
    private JButton editBtn;
    private JButton deleteBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
