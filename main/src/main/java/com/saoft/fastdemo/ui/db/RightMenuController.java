package com.saoft.fastdemo.ui.db;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.db.entity.TemplateItem;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.db.view.DbTablePanel;
import com.saoft.fastdemo.ui.db.view.menu.DBTablePopupMenu;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.InsertMethod;
import com.saoft.fastdemo.ui.outline.OutlineController;
import com.saoft.fastdemo.ui.outline.view.OutLinePanel;
import com.saoft.fastdemo.ui.outline.view.RolePanel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.util.List;

@Component("RightMenuController")
public class RightMenuController {

    OutlineController outlineController;

    @Autowired
    DBTablePopupMenu dbTablePopupMenu;

    /*Dialog*/
    @Autowired
    InsertMethod insertMethod;

    @Autowired
    DbTablePanel dbTablePanel;

    @Autowired
    OutLinePanel outLinePanel;

    JMenu newMenu;

    @PostConstruct
    public void init() {
        /*初始化右键菜单*/
        newMenu = dbTablePopupMenu.addMenu("新建");
        dbTablePopupMenu.addSeparator();
        dbTablePopupMenu.addMenuItem("插入方法", e -> insertMethod.setVisible(true));
        dbTablePopupMenu.addSeparator();
        dbTablePopupMenu.addMenuItem("首页统计", e -> indexCount());
    }

    //将选中表加入当前角色的首页统计
    void indexCount(){
        //查找current Role index
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        String code = rolePanel.getRole().getCode();
        List<Layer> layerList = new Layer().selectList(new QueryWrapper<Layer>()
                .eq(DBConstants.role_code, code)
                .eq(DBConstants.type, "index"));
        if (layerList == null || layerList.size() == 0) {
            JOptionPane.showMessageDialog(null,"当前角色没有首页");
            return;
        }
        Layer layer = layerList.get(0);


        JList<TableInfo> tableInfoJList = dbTablePanel.getTableInfoJList();
        TableInfo tableInfo = tableInfoJList.getSelectedValue();
        LayerItem item = new LayerItem();
        item.setType("idx-count");
        item.setTypeKvlist(String.format("{t:%s}", tableInfo.getName()));
        item.setTitle(tableInfo.getComment());
        item.setLayerId(layer.getId());
        item.insert();
    }

    /**
     * 通过数据库初始化
     */
    public void initByDB(){
        //私有模板
//        Template template = new Template().selectOne(new QueryWrapper<Template>().eq(DBConstants.code,templateId));
//        List<TemplateItem> itemList = new TemplateItem().selectList(new QueryWrapper<TemplateItem>().eq(DBConstants.template_code, template.getCode()));
//        itemList2.addAll(itemList);

        //公共模板
        List<TemplateItem> itemList2 = new TemplateItem().selectList(new QueryWrapper<TemplateItem>()
                .eq(DBConstants.template_code, "base").orderByAsc(DBConstants.sort_code));

        for (TemplateItem templateItem : itemList2) {
            dbTablePopupMenu.addSubMenu(newMenu, templateItem.getName(),e -> addLayerAction(templateItem));
        }
    }

    public void addLayerAction(TemplateItem templateItem){
        if (outlineController == null) {
            outlineController = S.getBean(OutlineController.class);
        }
        JList<TableInfo> tableInfoJList = dbTablePanel.getTableInfoJList();
        TableInfo tableInfo = tableInfoJList.getSelectedValue();
        if (templateItem.getCode().contains("series")) {
            outlineController.addSeriesLayer(templateItem, tableInfo.getName());
        }else{
            outlineController.addControllerTreeLayer(templateItem,tableInfo.getName());
        }
    }

}
