package com.saoft.fastdemo.ui.preview.view;

import com.saoft.fastdemo.uic.NoInsetTabbedPaneUI;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import java.awt.*;

public class PreviuewJTabbedPane extends JTabbedPane {

    public PreviuewJTabbedPane() {
        NoInsetTabbedPaneUI ui = new NoInsetTabbedPaneUI();
        setUI(ui);
        ui.overrideContentBorderInsetsOfUI();
    }
}

