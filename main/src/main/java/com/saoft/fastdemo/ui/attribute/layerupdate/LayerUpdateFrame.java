/*
 * Created by JFormDesigner on Thu Jul 18 16:50:16 CST 2019
 */

package com.saoft.fastdemo.ui.attribute.layerupdate;

import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;

import java.awt.*;
import java.util.Objects;
import javax.swing.*;
import javax.swing.border.*;

/**
 * @author Brainrain
 */
public class LayerUpdateFrame extends JPanel {

    private Layer layer;

    private LayeUpdateFrameController controller;

    public LayerUpdateFrame() {
        initComponents();
        after();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LayerUpdateFrame that = (LayerUpdateFrame) o;
        return Objects.equals(layer.getId(), that.layer.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hash(layer.getId());
    }

    public LayeUpdateFrameController getController() {
        return controller;
    }

    public void setController(LayeUpdateFrameController controller) {
        this.controller = controller;
    }

    private void after(){
        updateTable.setModel(new LayerUpdateTableModel());
    }

    public JTable getUpdateTable() {
        return updateTable;
    }


    public JButton getAddBtn() {
        return addBtn;
    }

    public JButton getAddAllBtn() {
        return addAllBtn;
    }

    public JButton getEditBtn() {
        return editBtn;
    }

    public JButton getDeleteBtn() {
        return deleteBtn;
    }

    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        scrollPane1 = new JScrollPane();
        updateTable = new JTable();
        panel1 = new JPanel();
        addAllBtn = new JButton();
        addBtn = new JButton();
        editBtn = new JButton();
        deleteBtn = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(0, 0));
        setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setLayout(new BorderLayout());

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(updateTable);
                }
                contentPanel.add(scrollPane1, BorderLayout.CENTER);

                //======== panel1 ========
                {
                    panel1.setLayout(new BoxLayout(panel1, BoxLayout.X_AXIS));

                    //---- addAllBtn ----
                    addAllBtn.setText("add all");
                    panel1.add(addAllBtn);

                    //---- addBtn ----
                    addBtn.setText("add");
                    panel1.add(addBtn);

                    //---- editBtn ----
                    editBtn.setText("edit");
                    panel1.add(editBtn);

                    //---- deleteBtn ----
                    deleteBtn.setText("delete");
                    panel1.add(deleteBtn);
                }
                contentPanel.add(panel1, BorderLayout.NORTH);
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);
        }
        add(dialogPane, BorderLayout.CENTER);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JScrollPane scrollPane1;
    private JTable updateTable;
    private JPanel panel1;
    private JButton addAllBtn;
    private JButton addBtn;
    private JButton editBtn;
    private JButton deleteBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
