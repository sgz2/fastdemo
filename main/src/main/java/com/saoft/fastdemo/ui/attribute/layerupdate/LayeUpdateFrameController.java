package com.saoft.fastdemo.ui.attribute.layerupdate;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.FormUtil;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

public class LayeUpdateFrameController {


    LayerUpdateFrame layerUpdateFrame;

    EditLayerUpdate editLayerUpdate;

    public LayeUpdateFrameController() {
        layerUpdateFrame = new LayerUpdateFrame();
        layerUpdateFrame.setController(this);
        editLayerUpdate = S.getBean(EditLayerUpdate.class);
        init();
        rowEdit();
    }

    void init(){
        ControllerUtil.registerAction(layerUpdateFrame.getDeleteBtn(),e -> delete());
        ControllerUtil.registerAction(layerUpdateFrame.getAddAllBtn(),e -> addAll());
        ControllerUtil.registerAction(layerUpdateFrame.getAddBtn(),e -> add1());
        ControllerUtil.registerAction(layerUpdateFrame.getEditBtn(),e -> edit());

    }

    private void rowEdit(){
        JTable menuTable = layerUpdateFrame.getUpdateTable();
        menuTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    edit();
                }
            }
        });
    }

    public void initByLayer(Layer layer){

        List<LayerItem> layerUpdates = new LayerItem()
                .selectList(new QueryWrapper<LayerItem>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .orderByAsc(DBConstants.sort_by)
                );

        LayerUpdateTableModel model = (LayerUpdateTableModel) layerUpdateFrame.getUpdateTable().getModel();
        model.clear();
        model.addEntities(layerUpdates);

        layerUpdateFrame.setLayer(layer);
    }

    public void show(){
        PreviewController bean = S.getBean(PreviewController.class);
        Layer layer = layerUpdateFrame.getLayer();
        bean.addTab(layer.getName(),layerUpdateFrame);
    }


    public void addAll(){
        Layer layer = layerUpdateFrame.getLayer();
        String tableName = layer.getTableName();

        List<JTableColumn> tableColumns = new JTableColumn()
                .selectList(new QueryWrapper<JTableColumn>().eq(DBConstants.table_name, tableName));

        for (int i = 0; i < tableColumns.size(); i++) {
            JTableColumn tableColumn = tableColumns.get(i);
            LayerItem layerCheck = new LayerItem();
            layerCheck.setField(tableColumn.getName());
            layerCheck.setTitle(tableColumn.getShowName());
            layerCheck.setLayerId(layer.getId());

            layerCheck.setSortBy(new Double(i));
//            layerCheckCacl(layerCheck);
            if (Boolean.parseBoolean(tableColumn.getKeyFlag())) {
                layerCheck.setHiddenFlag(true);
            }

            layerCheck.insert();
        }

        initByLayer(layer);
        LayerUpdateTableModel model = (LayerUpdateTableModel) layerUpdateFrame.getUpdateTable().getModel();
        model.fireTableDataChanged();
    }


    public void add1(){
        LayerItem layerItem = new LayerItem();
        editLayerUpdate.setLayerItem(layerItem);
        FormUtil.bindData(editLayerUpdate, layerItem);
        editLayerUpdate.setVisible(true);
    }


    public void edit(){
        JTable listTable = layerUpdateFrame.getUpdateTable();
        int selectedRow = listTable.getSelectedRow();
        LayerUpdateTableModel model = (LayerUpdateTableModel) listTable.getModel();
        LayerItem layerCheck = model.getEntityByRow(selectedRow);

        editLayerUpdate.setLayerItem(layerCheck);
        FormUtil.bindData(editLayerUpdate, layerCheck);

        editLayerUpdate.setVisible(true);
    }

    public void delete(){
        JTable listTable = layerUpdateFrame.getUpdateTable();
        LayerUpdateTableModel model = (LayerUpdateTableModel) listTable.getModel();
        model.removeRow(listTable);
    }

}
