package com.saoft.fastdemo.ui.db.model;

import com.saoft.fastdemo.bean.TableInfoEx;
import com.saoft.fastdemo.ui.shared.model.SaoftDefaultListModel;
import org.springframework.stereotype.Component;


@Component("JEntityInfoModel")
public class JEntityInfoModel extends SaoftDefaultListModel<TableInfoEx> {


}
