package com.saoft.fastdemo.ui.attribute.form;

import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.ui.shared.form.IdAble;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class PropValuePanel extends JTabbedPane {

    private Layer layer;

    private List<IdAble> idInputList = new ArrayList<>();

    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }

    public List<IdAble> getIdInputList() {
        return idInputList;
    }

}
