package com.saoft.fastdemo.ui.main.view;

import com.saoft.fastdemo.ui.attribute.AttributeController;
import com.saoft.fastdemo.ui.db.DbController;
import com.saoft.fastdemo.ui.main.TopController;
import com.saoft.fastdemo.ui.outline.OutlineController;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.button.MFrame;
import com.saoft.fastdemo.ui.shared.button.MyButton;
import com.saoft.fastdemo.ui.shared.button.RotatedIcon;
import com.saoft.fastdemo.ui.shared.button.TextIcon;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.annotation.PostConstruct;
import javax.swing.*;

@Component("MainFrame")
public class MainFrame extends MFrame {

    @Autowired
    private DbController dbController;
    @Autowired
    private OutlineController outlineController;
    @Autowired
    private AttributeController attributeController;
    @Autowired
    private PreviewController previewController;

    @Autowired
    TopController topController;

    private JSplitPane mainRight;

    public MainFrame() {

    }

    @PostConstruct
    public void init(){
        before();
        initComponents();
        after();
    }

    private void before(){
//        topController = S.getBean(TopController.class);
//        dbController = S.getBean(DbController.class);
//        componentController = S.getBean(ComponentController.class);
//        outlineController = S.getBean(OutlineController.class);
//        attributeController = S.getBean(AttributeController.class);
//        previewController = S.getBean(PreviewController.class);
    }

    private void after(){

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowOpened(WindowEvent e) {
                super.windowOpened(e);
                mainRight.setDividerLocation(0.45);
            }
        });
        /*左上片区竞争*/
        ControllerUtil.registerAction(dbSwithBtn, e->dbSwitch());
    }

    private String left_top_flag = "db";

    public JTextArea getTextArea() {
        return textArea;
    }

    void dbSwitch(){
        if("db".equals(left_top_flag)){
            left_top_flag = "";
        }else{
            left_top_flag = "db";
        }
    }

    private void initComponents() {
        centerPanel = new JPanel();
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        centerAndBottom = new JPanel();
        centerAndBottomSplite = new JSplitPane();
        mainLeft = new JSplitPane();
        mainCenterAndRight = new JSplitPane();
        mainRight = new JSplitPane();

        {
            //设置边框
            centerAndBottomSplite.setBorder(BorderFactory.createEtchedBorder());
            mainLeft.setBorder(BorderFactory.createEmptyBorder());
            mainRight.setBorder(BorderFactory.createEmptyBorder());
            mainCenterAndRight.setBorder(BorderFactory.createEmptyBorder());
        }

        mainCenterAndRight.setResizeWeight(1.0d);
        mainRight.setResizeWeight(0.0d);

        mainLeft.setDividerSize(5);
        mainCenterAndRight.setDividerSize(5);
        mainRight.setDividerSize(5);
        centerAndBottomSplite.setDividerSize(5);

        {
            centerPanel.setLayout(new BorderLayout());
            centerAndBottom.setLayout(new BorderLayout());
        }

        //======== this ========
        setPreferredSize(new Dimension(1280, 800));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== splitPane1 ========
        {

            mainLeft.setLeftComponent(dbController.getPanel());

            //======== splitPane2 ========
            {
                mainCenterAndRight.setLastDividerLocation(-1);
                {
                    mainCenterAndRight.setLeftComponent(previewController.getPanel());

                    mainRight.setOrientation(JSplitPane.VERTICAL_SPLIT);
                    mainRight.setTopComponent(attributeController.getPanel());
                    mainRight.setBottomComponent(outlineController.getPanel());
                    mainRight.setDividerLocation(0.5);
                    mainCenterAndRight.setRightComponent(mainRight);
                }
            }
            mainLeft.setRightComponent(mainCenterAndRight);
        }

        //
        {

            leftBtns = new JPanel();
            dbSwithBtn = new MyButton();
            dbSwithBtn.setIcon(getLeftIcon(dbSwithBtn, "数据源"));


            leftBtns.setLayout(new BoxLayout(leftBtns, BoxLayout.Y_AXIS));

            leftBtns.add(dbSwithBtn);
//            leftBtns.add(blockSwithBtn);
        }

        {
            centerAndBottomSplite.setResizeWeight(1);
            centerAndBottomSplite.setOrientation(JSplitPane.VERTICAL_SPLIT);
            //设置下面控制台打印信息
            centerAndBottomSplite.setTopComponent(mainLeft);
            JPanel panel = new JPanel();
            panel.setLayout(new BorderLayout());

            textArea = new JTextArea();
            textArea.setEditable(false);
            textArea.setRows(5);

            textAreaScrollPane = new EmptyBorderScrollPane();
            textAreaScrollPane.setViewportView(textArea);
            panel.add(textAreaScrollPane,BorderLayout.CENTER);

            centerAndBottomSplite.setBottomComponent(panel);
        }

        {
            centerAndBottom.add(centerAndBottomSplite, BorderLayout.CENTER);

            JPanel buttons = new JPanel();
            buttons.setLayout(new BoxLayout(buttons, BoxLayout.X_AXIS));

            consoleBtn = new JButton("调试信息");
            buttons.add(consoleBtn);
            centerAndBottom.add(buttons, BorderLayout.SOUTH);
        }

        JPanel footer = new JPanel();
        footer.add(new JLabel("footer"));

        contentPane.add(topController.getPanel(), BorderLayout.NORTH);
        centerPanel.add(centerAndBottom, BorderLayout.CENTER);
        centerPanel.add(leftBtns, BorderLayout.WEST);
        centerPanel.add(footer, BorderLayout.SOUTH);
        contentPane.add(centerPanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
    }

    private Icon getLeftIcon(JButton button,String title){
        TextIcon t1 = new TextIcon(button, title, TextIcon.Layout.HORIZONTAL);
        RotatedIcon r1 = new RotatedIcon(t1, RotatedIcon.Rotate.UP);
        return r1;
    }

    public JScrollPane getTextAreaScrollPane() {
        return textAreaScrollPane;
    }

    private JButton dbSwithBtn;

    private JButton consoleBtn;
    private JTextArea textArea;
    private JScrollPane textAreaScrollPane;

    private JPanel leftBtns;
    private JPanel centerPanel;
    private JPanel centerAndBottom;
    private JSplitPane centerAndBottomSplite;
    private JSplitPane mainLeft;
    private JSplitPane mainCenterAndRight;

}
