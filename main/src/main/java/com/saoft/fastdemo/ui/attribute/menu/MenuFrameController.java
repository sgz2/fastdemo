package com.saoft.fastdemo.ui.attribute.menu;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.Menu;
import com.saoft.fastdemo.service.RoleService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.attribute.model.MenuTableModel;
import com.saoft.fastdemo.ui.outline.OutlineController;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.util.StringUtils;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class MenuFrameController {

    MenuFrame menuFrame;

    EditMenu editMenu;

    public MenuFrameController(Layer layer) {
        menuFrame = new MenuFrame();
        menuFrame.setController(this);
        menuFrame.setLayer(layer);
        editMenu = S.getBean(EditMenu.class);
        init();
        rowEdit();
    }

    void init(){
        ControllerUtil.registerAction(menuFrame.getAddBtn(),e -> addBtn());
        ControllerUtil.registerAction(menuFrame.getEditBtn(),e -> editBtn());
        ControllerUtil.registerAction(menuFrame.getDeleteBtn(),e -> deleteBtn());

        ControllerUtil.registerAction(editMenu.getOkButton(),e -> save());
        ControllerUtil.registerAction(editMenu.getCancelButton(),e -> cancel());

//        ControllerUtil.registerKeyDelete(menuFrame.getMenuTable(), new AbstractAction() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                System.out.println(e.getActionCommand());
//            }
//        });

    }

    private void rowEdit(){
        JTable menuTable = menuFrame.getMenuTable();
        menuTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    editBtn();
                }
            }
        });
    }

    public void initByDb(){
        String activeRole = S.b(RoleService.class).currentRoleCode();
        JTable menuTable = menuFrame.getMenuTable();
        MenuTableModel model = (MenuTableModel) menuTable.getModel();
        List<Menu> menuList = new Menu().selectList(
                        new QueryWrapper<Menu>().orderByAsc(DBConstants.sort_by)
                                .eq(DBConstants.role, activeRole));
        model.clear();
        model.addEntities(menuList);
    }

    public void show(){
        String activeRole = S.b(RoleService.class).currentRoleCode();
        PreviewController bean = S.getBean(PreviewController.class);
        String title = activeRole+"菜单";
        bean.addTab(title,menuFrame);
    }

    void save(){
        Menu data = editMenu.getData();

        if(!StringUtils.hasLength(data.getUrl())){
            if (data.getLayerId() != null) {
                //设置url
                Layer layer = new Layer().selectById(data.getLayerId());
                String type = layer.getType();
                String url = layer.getRoleCode() + "/" +
                        layer.getTableName() + "/" +
                        layer.getTableName() + "_" + MenuLayerKeyValue.map.get(type)
                        ;
                data.setUrl(url);
            }
        }

        data.insertOrUpdate();
        editMenu.setVisible(false);
        initByDb();
    }

    void cancel(){
        editMenu.setVisible(false);
    }

    void addBtn(){
        editMenu.loadData(new Menu());
        editMenu.setVisible(true);
    }

    void editBtn(){
        JTable menuTable = menuFrame.getMenuTable();
        MenuTableModel model = (MenuTableModel) menuTable.getModel();
        int selectedRow = menuTable.getSelectedRow();
        Menu menu = model.getEntityByRow(selectedRow);

        editMenu.loadData(menu);
        editMenu.setVisible(true);
    }

    void deleteBtn(){
        JTable menuTable = menuFrame.getMenuTable();
        MenuTableModel model = (MenuTableModel) menuTable.getModel();
        model.removeRow(menuTable);

    }
}
