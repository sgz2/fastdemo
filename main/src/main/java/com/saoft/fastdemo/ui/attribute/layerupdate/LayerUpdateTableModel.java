package com.saoft.fastdemo.ui.attribute.layerupdate;

import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

import java.util.List;

public class LayerUpdateTableModel extends DefaultTableModel<LayerItem> {

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "字段",
                "别名",
                "标题",
                "必填",
                "类型",
                "类型值",
                "验证",
                "只读",
                "隐藏",
                "查询条件",
                "宽度",
                "排序",
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        LayerItem layer = entities.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return layer.getField();
            case 1:
                return layer.getAlias();
            case 2:
                return layer.getTitle();
            case 3:
                return layer.getMustFlag();
            case 4:
                return layer.getType();
            case 5:
                return layer.getTypeKvlist();
            case 6:
                return layer.getValidate();
            case 7:
                return layer.getReadonlyFlag();
            case 8:
                return layer.getHiddenFlag();
            case 9:
                return layer.getQuery();
            case 10:
                return layer.getWidth();
            case 11:
                return layer.getSortBy();
        }
        return null;
    }

    public List<LayerItem> getList() {
        return super.entities;
    }

}
