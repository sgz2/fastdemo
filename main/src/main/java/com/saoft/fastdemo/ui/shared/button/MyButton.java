package com.saoft.fastdemo.ui.shared.button;

import javax.swing.*;
import java.awt.*;

public class MyButton extends JButton {

    public MyButton() {
        Insets bm = UIManager.getInsets("Button.margin");
        Insets margin = new Insets(bm.left, bm.top, bm.left, bm.top);
        setMargin(margin);
    }
}
