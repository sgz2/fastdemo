package com.saoft.fastdemo.ui.db;

import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.JTableInfo;
import com.saoft.fastdemo.service.db.TableInfoService;
import com.saoft.fastdemo.ui.db.model.TableListModel;
import com.saoft.fastdemo.ui.db.view.AddTable;
import com.saoft.fastdemo.ui.db.view.DbTablePanel;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.LinkedList;
import java.util.List;

/**
 * 数据源上面的控制按钮
 */
@Component("DBTableController")
public class DBTableController {

    @Autowired
    DbTablePanel dbTablePanel;

    @Autowired
    PreviewController previewController;

    @Autowired
    AddTable addTable;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TableInfoService tableInfoService;

    @PostConstruct
    public void init(){

        ControllerUtil.registerAction(dbTablePanel.getMf(), (e)-> mf());
        ControllerUtil.registerAction(dbTablePanel.getEdit(), (e)-> edit());
        ControllerUtil.registerAction(dbTablePanel.getDelete(), (e)-> delete());
        ControllerUtil.registerAction(dbTablePanel.getLocal(), (e)-> local());
        ControllerUtil.registerAction(dbTablePanel.getRemote(), (e)-> remote());
        ControllerUtil.registerAction(dbTablePanel.getMf(), (e)-> mf());
    }

    public void initByDb(){
        TableListModel model = (TableListModel) dbTablePanel.getTableInfoJList().getModel();

        List<JTableInfo> jTableInfos = tableInfoService.getAll();

        List<TableInfo> infoList = new LinkedList<>();
        for (JTableInfo jTableInfo : jTableInfos) {
            TableInfo info = new TableInfo();
            infoList.add(info);
            info.setName(jTableInfo.getName());
            info.setComment(jTableInfo.getShowName());

            List<JTableColumn> table_name = tableInfoService.tableColumns(info.getName());

            List<TableField> tableFields = new LinkedList<>();
            info.setFields(tableFields);
            for (JTableColumn jTableColumn : table_name) {
                TableField field = new TableField();
                field.setName(jTableColumn.getName());
                field.setType(jTableColumn.getType());
                field.setComment(jTableColumn.getShowName());
                tableFields.add(field);
            }


        }
        model.clear();
        model.addEntities(infoList);
        dbTablePanel.getTableInfoJList().updateUI();

    }

    private void mf(){
        previewController.addTab("newtable",addTable);
    }


    private void edit(){

    }

    private void delete(){
        TableInfo selectedValue = dbTablePanel.getTableInfoJList().getSelectedValue();
        if (selectedValue != null) {
            //删除表 todo 提示删除
            tableInfoService.tableDelete(selectedValue.getName());
            initByDb();
        }
    }

    private void local(){
        //是否有配置

    }

    private void remote(){

    }
}
