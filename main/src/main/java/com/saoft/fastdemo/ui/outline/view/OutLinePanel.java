package com.saoft.fastdemo.ui.outline.view;

import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.panel.BorderUtil;
import org.springframework.stereotype.Component;

import javax.swing.*;
import java.awt.*;

@Component("OutLinePanel")
public class OutLinePanel extends JPanel {

    private JPanel top;
    private JLabel titleText;

    private JButton addRole = new JButton("addRole");

    private JTabbedPane roleTabbedPane = new JTabbedPane();

    public OutLinePanel() {
        initComponents();
    }

    public JButton getAddRole() {
        return addRole;
    }

    private void initComponents() {

        top = new JPanel();
        titleText = new JLabel();

        //======== this ========
       setLayout(new BorderLayout());
       setBorder(BorderFactory.createEmptyBorder());

        //======== top ========
        {
            top.setLayout(new BorderLayout());

            //---- title ----
            titleText.setText("角色");
            top.add(titleText,BorderLayout.WEST);
            top.setBorder(BorderUtil.createTitleBorder());

            JPanel panel = new JPanel();
            panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

            panel.add(addRole);


            JButton rightClose = new JButton("RightClose");
            panel.add(rightClose);
            ButtonUtil.convertIconButton(rightClose, ButtonUtil.RIGHT_CLOSE);

            top.add(panel, BorderLayout.EAST);
        }
        add(top,BorderLayout.NORTH);

        add(roleTabbedPane,BorderLayout.CENTER);
    }

    public JTabbedPane getRoleTabbedPane() {
        return roleTabbedPane;
    }
}
