package com.saoft.fastdemo.ui.shared.form;

import com.saoft.fastdemo.ui.shared.GBC;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FormPanel extends JPanel {

    private int rows = 0;

    private List<IdAble> values = new ArrayList();

    private GridBagLayout gridBag = new GridBagLayout();

    public FormPanel() {
        init();
    }

    void init(){
        setLayout(gridBag);
    }


    public void setTitle(String title){
        JLabel label = new JLabel(title);
        gridBag.addLayoutComponent(label, new GBC(0,rows,1,1).setIpad(20,5).
                setFill(GBC.NONE).setWeight(0, 0));
        add(label);
    }

    /**
     *
     * @param title
     * @param id 数据库列名
     * @param value
     */
    public void setRowInput(String title,String id,String value){
        setTitle(title);
        setInput(id,value);
    }

    public void setRowComboBox(String title, String id, Object value, List items){
        setTitle(title);
        setComboBox(id,value,items);
    }

    public void setRowCheckBox(String title,String id,Boolean value){
        setTitle(title);
        setCheckbox(id,value);
    }
    public void setInput(String id,String value){
        IdInput field = new IdInput();
        field.setId(id);
        field.setText(value);

        if ("id".equals(id)) {
            field.setEditable(false);
        }

        gridBag.addLayoutComponent(field, new GBC(1,rows,1,1).setIpad(90,5).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        values.add(field);
        rows++;
        add(field);
    }

    public void setComboBox(String id,Object value,List<Object> items){
        IdComboBox field = new IdComboBox();
        for (Object item : items) {
            field.addItem(item);
        }
        field.setId(id);
        field.setSelectedItem(value);

        if ("id".equals(id)) {
            field.setEditable(false);
        }

        gridBag.addLayoutComponent(field, new GBC(1,rows,1,1).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        values.add(field);
        rows++;
        add(field);
    }

    public void setCheckbox(String id,Boolean value){
        IdCheckBox field = new IdCheckBox();
        field.setId(id);
        field.setSelected(value==null?false:value);
        gridBag.addLayoutComponent(field, new GBC(1,rows,1,1).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        values.add(field);
        rows++;
        add(field);
    }

    public void setButton(JButton button){
        gridBag.addLayoutComponent(button, new GBC(1,rows,1,1).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));
        rows++;
        add(button);
    }

    public void setInfo(String info){
        JTextField field = new JTextField();
        field.setText(info);
        field.setEditable(false);
        gridBag.addLayoutComponent(field, new GBC(1,rows,1,1).
                setFill(GBC.HORIZONTAL).setWeight(0, 0));

        rows++;
        add(field);
    }

    public void end(){
        add(new JLabel(), new GBC(2, rows, 1, 1).setWeight(1, 1).setFill(GBC.BOTH));
    }

    public List<IdAble> getValues() {
        return values;
    }
}
