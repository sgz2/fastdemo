package com.saoft.fastdemo.ui.shared.controller;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;


public class ControllerUtil {

    /**
     * 注册监听事件
     * @param button
     * @param listener
     */
    public static void registerAction(JButton button, ActionListener listener) {
        button.addActionListener(listener);
    }

    private static String DELETE = "fast-delete";
    /**
     * 注册删除键
     * @param table
     * @param action
     */
    public static void registerKeyDelete(JTable table,AbstractAction action){
//        int condition = JComponent.WHEN_IN_FOCUSED_WINDOW;
        int condition = JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT;
        InputMap inputMap = table.getInputMap(condition);
        ActionMap actionMap = table.getActionMap();

        // DELETE is a String constant that for me was defined as "Delete"
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0),DELETE );
        actionMap.put(DELETE, action);
    }
}
