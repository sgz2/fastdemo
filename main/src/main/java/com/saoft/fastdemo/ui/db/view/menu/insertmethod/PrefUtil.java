package com.saoft.fastdemo.ui.db.view.menu.insertmethod;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Setting;
import org.springframework.util.StringUtils;

public class PrefUtil {

    public final static String current_project_dir = "insertmethod_current_project_dir";
    public final static String current_dao = "insertmethod_current_dao";
    public final static String current_dao_xml = "insertmethod_current_dao_xml";
    public final static String current_service = "insertmethod_current_service";
    public final static String current_service_impl = "insertmethod_current_service_impl";

    public final static String name_dao = "insertmethod_name_dao";
    public final static String name_dao_xml = "insertmethod_name_dao_xml";
    public final static String name_service = "insertmethod_name_service";
    public final static String name_service_impl = "insertmethod_name_service_impl";
    public final static String name_style = "insertmethod_name_styel";

    public final static String title_full_name = "title_full_name";
    public final static String title_short_name = "title_short_name";
    public final static String title_footer = "title_footer";


    public static int getInteger(String code) {
        String s = get(code);
        if (StringUtils.hasLength(s)) {
            return Integer.parseInt(s);
        }
        return 0;
    }

    public static String get(String code) {
        Setting setting = new Setting()
                .selectOne(new QueryWrapper<Setting>()
                        .eq(DBConstants.code, code));
        if (setting != null) {
            if (StringUtils.hasLength(setting.getValue())) {
                return setting.getValue();
            }
            return "";
        }
        return null;
    }

    public static void set(String code,String value) {

        String s = get(code);
        if (s == null) {
            Setting setting = new Setting();
            setting.setCode(code);
            setting.setValue(value);
            setting.insert();
        }else{
            UpdateWrapper<Setting> set = new UpdateWrapper<Setting>()
                    .eq(DBConstants.code, code)
                    .set(DBConstants.value, value);
            new Setting().update(set);
        }

    }
}
