package com.saoft.fastdemo.ui.attribute.model;

import com.saoft.fastdemo.db.entity.Menu;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

public class MenuTableModel extends DefaultTableModel<Menu> {

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "角色",
                "Layer",
                "编码",
                "父级编码",
                "名称",
                "地址",
                "target",
                "排序"
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        Menu menu = entities.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return menu.getRole();
            case 1:
                return menu.getLayerId();
            case 2:
                return menu.getCode();
            case 3:
                return menu.getParentCode();
            case 4:
                return menu.getName();
            case 5:
                return menu.getUrl();
            case 6:
                return menu.getTarget();
            case 7:
                return menu.getSortBy();
        }
        return null;
    }

//    @Override
//    public Class<?> getColumnClass(int columnIndex) {
//        Class<?> aClass = getValueAt(0, columnIndex).getClass();
//        return aClass;
//    }
}
