package com.saoft.fastdemo.ui.shared.rightclick;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;

/**
 * 初始化 表格的右键菜单
 */
public class RightClickPopupMenu extends JPopupMenu {

    /**
     * 添加菜单并编辑
     * @param title
     * @param listener
     */
    public JMenuItem addMenuItem(String title, ActionListener listener){
        JMenuItem item = new JMenuItem(title);
        item.addActionListener(listener);
        return add(item);
    }

    public void showMe(JComponent component, MouseEvent event) {
        Point point = event.getPoint();
        show(component, point.x,point.y);
    }
}
