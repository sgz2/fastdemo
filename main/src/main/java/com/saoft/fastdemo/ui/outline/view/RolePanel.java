/*
 * Created by JFormDesigner on Fri Jun 07 00:22:53 CST 2019
 */

package com.saoft.fastdemo.ui.outline.view;

import com.saoft.fastdemo.db.entity.Role;
import com.saoft.fastdemo.ui.outline.model.ControllerTreeModel;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */

public class RolePanel extends JPanel {

    private Role role;

    public RolePanel() {
        initComponents();
        init();

    }

    private void init(){
        type.addItem("front-end");
        type.addItem("back-end");

        templateCode.addItem("layui");
        templateCode.addItem("adminlte");
        templateCode.addItem("aceadmin");
        templateCode.addItem("huiadmin");
        templateCode.addItem("fangcms");
        templateCode.addItem("layui-front");

        controlerTree.setModel(new ControllerTreeModel());
        controlerTree.setRootVisible(false);


//        controlerTree.setCellRenderer(new ControllerNodeRender());
        //
//        tabbedPane1.setTabPlacement(SwingConstants.LEFT);
        tabbedPane.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
    }

    public JTabbedPane getTabbedPane() {
        return tabbedPane;
    }

    public JButton getDeleteRoleBtn() {
        return deleteRoleBtn;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public JButton getSaveRoleBtn() {
        return saveRoleBtn;
    }

    public JTextField getRoleIdTxt() {
        return roleIdTxt;
    }

    public JTextField getRoleNameTxt() {
        return roleNameTxt;
    }

    public JButton getViewTpl() {
        return viewTpl;
    }

    public JComboBox getType() {
        return type;
    }

    public JComboBox getTemplateCode() {
        return templateCode;
    }

    public JTree getControlerTree() {
        return controlerTree;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        tabbedPane = new JTabbedPane();
        scrollPane1 = new JScrollPane();
        controlerTree = new JTree();
        panel3 = new JPanel();
        label5 = new JLabel();
        roleIdTxt = new JTextField();
        label6 = new JLabel();
        roleNameTxt = new JTextField();
        label7 = new JLabel();
        type = new JComboBox();
        label8 = new JLabel();
        templateCode = new JComboBox();
        viewTpl = new JButton();
        saveRoleBtn = new JButton();
        deleteRoleBtn = new JButton();

        //======== this ========
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));

        //======== tabbedPane ========
        {

            //======== scrollPane1 ========
            {
                scrollPane1.setViewportView(controlerTree);
            }
            tabbedPane.addTab("\u63a7\u5236\u5c42", scrollPane1);

            //======== panel3 ========
            {
                panel3.setLayout(new GridBagLayout());
                ((GridBagLayout)panel3.getLayout()).columnWidths = new int[] {0, 0, 0, 0};
                ((GridBagLayout)panel3.getLayout()).rowHeights = new int[] {0, 0, 0, 0, 0, 0, 0};
                ((GridBagLayout)panel3.getLayout()).columnWeights = new double[] {0.0, 1.0, 0.0, 1.0E-4};
                ((GridBagLayout)panel3.getLayout()).rowWeights = new double[] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

                //---- label5 ----
                label5.setText("\u89d2\u8272ID\uff1a");
                panel3.add(label5, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- roleIdTxt ----
                roleIdTxt.setColumns(20);
                panel3.add(roleIdTxt, new GridBagConstraints(1, 0, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- label6 ----
                label6.setText("\u89d2\u8272\u540d\u79f0\uff1a");
                panel3.add(label6, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- roleNameTxt ----
                roleNameTxt.setColumns(20);
                panel3.add(roleNameTxt, new GridBagConstraints(1, 1, 2, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- label7 ----
                label7.setText("\u89d2\u8272\u7c7b\u578b");
                panel3.add(label7, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
                panel3.add(type, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- label8 ----
                label8.setText("\u6a21\u677f");
                panel3.add(label8, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
                panel3.add(templateCode, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- viewTpl ----
                viewTpl.setText("\u6a21\u677f\u4ecb\u7ecd");
                panel3.add(viewTpl, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- saveRoleBtn ----
                saveRoleBtn.setText("\u4fdd\u5b58");
                panel3.add(saveRoleBtn, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.WEST, GridBagConstraints.VERTICAL,
                    new Insets(0, 0, 0, 0), 0, 0));

                //---- deleteRoleBtn ----
                deleteRoleBtn.setText("\u5220\u9664\u89d2\u8272");
                panel3.add(deleteRoleBtn, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            tabbedPane.addTab("\u89d2\u8272\u4fe1\u606f", panel3);
        }
        add(tabbedPane);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JTabbedPane tabbedPane;
    private JScrollPane scrollPane1;
    private JTree controlerTree;

    private JPanel panel3;
    private JLabel label5;
    private JTextField roleIdTxt;
    private JLabel label6;
    private JTextField roleNameTxt;
    private JLabel label7;
    private JComboBox type;
    private JLabel label8;
    private JComboBox templateCode;
    private JButton viewTpl;
    private JButton saveRoleBtn;
    private JButton deleteRoleBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
