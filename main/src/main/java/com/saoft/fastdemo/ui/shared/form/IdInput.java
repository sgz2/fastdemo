package com.saoft.fastdemo.ui.shared.form;

import javax.swing.*;
import java.util.Objects;

public class IdInput extends JTextField implements IdAble{

    private String id;

    public String getId() {
        return id;
    }

    @Override
    public Object getValue() {
        return getText();
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdInput idInput = (IdInput) o;
        return Objects.equals(id, idInput.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }
}
