/*
 * Created by JFormDesigner on Wed Aug 21 16:12:42 CST 2019
 */

package com.saoft.fastdemo.ui.main.view;

import com.saoft.fastdemo.ui.shared.button.MFrame;

import java.awt.*;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class NoProjectFrame extends MFrame {
    public NoProjectFrame() {
        initComponents();
        after();
    }

    private void after(){

    }

    public JButton getNewBtn() {
        return newBtn;
    }

    public JButton getOpenBtn() {
        return openBtn;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel1 = new JPanel();
        newBtn = new JButton();
        openBtn = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(600, 39));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== panel1 ========
        {
            panel1.setLayout(new GridLayout(5, 0));

            //---- newBtn ----
            newBtn.setText("\u65b0\u5efa\u9879\u76ee");
            panel1.add(newBtn);

            //---- openBtn ----
            openBtn.setText("\u6253\u5f00\u9879\u76ee");
            panel1.add(openBtn);
        }
        contentPane.add(panel1, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel1;
    private JButton newBtn;
    private JButton openBtn;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
