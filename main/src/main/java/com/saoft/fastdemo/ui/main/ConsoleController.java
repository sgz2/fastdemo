package com.saoft.fastdemo.ui.main;

import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.view.MainFrame;

import javax.swing.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ConsoleController {

    static MainFrame mainFrame;

    final static DateFormat FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:sss");

    private static MainFrame getMainFrame() {
        if (mainFrame == null) {
            mainFrame = S.getBean(MainFrame.class);
        }
        return mainFrame;
    }

    public static void log(String info){
        JTextArea textArea = getMainFrame().getTextArea();

        textArea.append(FORMAT.format(new Date()));
        textArea.append(" - ");
        textArea.append(info);
        textArea.append("\n");

//        JScrollPane scrollPane = getMainFrame().getTextAreaScrollPane();
//        try {
//            Thread.sleep(100);
//        } catch (InterruptedException ex) {
//        }
//        JScrollBar scrollBar = scrollPane.getHorizontalScrollBar();
//        scrollBar.setValue(scrollBar.getMaximum());
    }
}
