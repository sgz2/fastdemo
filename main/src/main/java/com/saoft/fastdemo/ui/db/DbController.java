package com.saoft.fastdemo.ui.db;

import com.saoft.dbinfo.po.TableField;
import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.db.entity.TableRelation;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.db.model.JTableInfoModel;
import com.saoft.fastdemo.ui.db.model.TableRelationTableModel;
import com.saoft.fastdemo.ui.db.view.*;
import com.saoft.fastdemo.ui.db.view.menu.DBTablePopupMenu;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.controller.AbstractPanelController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;

import com.saoft.fastdemo.ui.db.view.data.DataForm;
import com.saoft.fastdemo.ui.db.view.data.RowData;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;

@org.springframework.stereotype.Component("DbController")
public class DbController extends AbstractPanelController {

    @Autowired
    PreviewController previewController;

    @Autowired
    RelationFrameController relationFrameController;
    @Autowired
    TableTabController tableTabController;
    @Autowired
    LocalController localController;

    @Autowired
    DbPanel dbPanel;


    @Autowired
    JTableInfoModel jTableInfoModel;

    @Autowired
    DbTablePanel dbTablePanel;

    @Autowired
    RelationFrame relationFrame;

    DataForm dataForm;

    @Autowired
    DBTablePopupMenu dbTablePopupMenu;

    @Override
    public JPanel getPanel() {
        return dbPanel;
    }

    @PostConstruct
    public void init(){

        dbTablePanel.getTableInfoJList().addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(e.getButton() == MouseEvent.BUTTON1){
                    if (e.getClickCount() == 2) {
                        TableInfo tableInfo = dbTablePanel.getTableInfoJList().getSelectedValue();

                        TableTabPanel tabPanel = new TableTabPanel(tableInfo);
                        ControllerUtil.registerAction(tabPanel.getAddRBtn(),(e2)-> addRBtn());
                        ControllerUtil.registerAction(tabPanel.getDelRBtn(),(e2)-> delRBtn());

                        ControllerUtil.registerAction(tabPanel.getDataAddBtn(),(e2)-> dataAddBtn());
                        ControllerUtil.registerAction(tabPanel.getDataDeleteBtn(),(e2)-> dataDeleteBtn());
                        ControllerUtil.registerAction(tabPanel.getDataEditBtn(),(e2)-> dataEditBtn());
                        previewController.addTab(tableInfo.getName(), tabPanel);
                        /*打开表格面板 初始化数据*/
                        tableTabController.initRelation();
                        tableTabController.initTable();
                        rowEdit();
                    }
                }
                if (e.getButton() == MouseEvent.BUTTON3) {
                    Point point = e.getPoint();
                    JList<TableInfo> tableInfoJList = dbTablePanel.getTableInfoJList();

                    int toIndex = tableInfoJList.locationToIndex(point);
                    tableInfoJList.setSelectedIndex(toIndex);
                    dbTablePopupMenu.show(dbTablePanel.getTableInfoJList(),point.x,point.y);
                }

            }
        });

        ControllerUtil.registerAction(dbTablePanel.getLocaleBtn(), (e)-> local());
    }

    private void rowEdit(){
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableTabPanel tableTabPanel = (TableTabPanel) currentTab;
            JTable dataTable = tableTabPanel.getDataTable();
            dataTable.addMouseListener(new MouseAdapter() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    if (e.getClickCount() == 2) {
                        dataEditBtn();
                    }
                }
            });

        }


    }


    /*数据添加按钮*/
    private void dataAddBtn(){
        if (dataForm == null) {
            dataForm = S.getBean(DataForm.class);
        }
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableInfo tableInfo = ((TableTabPanel) currentTab).getTableInfo();
            dataForm.clean();
            for (TableField field : tableInfo.getFields()) {
                RowData rowData = new RowData();
                rowData.setId(field.getName());
                if (StringUtils.hasLength(field.getComment())) {
                    rowData.setTitle(field.getComment());
                }else{
                    rowData.setTitle(field.getName());
                }
//                rowData.setIsnotnull(field.get);
                rowData.setComponent(new JTextField());
                dataForm.addItem(rowData);
            }
            dataForm.setVisible(true);
        }
    }

    /**
     * 编辑一行数据
     */
    private void dataEditBtn(){
        if (dataForm == null) {
            dataForm = S.getBean(DataForm.class);
        }
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableTabPanel tableTabPanel = (TableTabPanel)currentTab;
            TableInfo tableInfo = tableTabPanel.getTableInfo();

            DefaultTableModel model = (DefaultTableModel) tableTabPanel.getDataTable().getModel();
            Map entityByRow = (Map) model.getEntityByRow(tableTabPanel.getDataTable().getSelectedRow());

            dataForm.clean();
            for (TableField field : tableInfo.getFields()) {
                RowData rowData = new RowData();
                rowData.setId(field.getName());
                if (StringUtils.hasLength(field.getComment())) {
                    rowData.setTitle(field.getComment());
                }else{
                    rowData.setTitle(field.getName());
                }
//                rowData.setIsnotnull(field.get);
                rowData.setComponent(new JTextField());
                rowData.setValue(entityByRow.get(field.getName()));
                dataForm.addItem(rowData);
            }
            dataForm.setVisible(true);
        }
    }
    private void dataDeleteBtn(){}

    /*删除关系*/
    void delRBtn(){
        Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            JTable relationTable = ((TableTabPanel) currentTab).getRelationTable();
            TableRelationTableModel model = (TableRelationTableModel) relationTable.getModel();
            int selectedRow = relationTable.getSelectedRow();
            TableRelation entityByRow = model.getEntityByRow(selectedRow);
            entityByRow.deleteById();
        }
        tableTabController.initRelation();
    }

    void addRBtn(){
        Component currentTab = previewController.getCurrentTab();
        if(currentTab instanceof TableTabPanel){
            TableInfo tableInfo = ((TableTabPanel) currentTab).getTableInfo();
            relationFrameController.initForm(tableInfo);
            relationFrame.setVisible(true);
        }
    }


    void local(){
        localController.preOpen();
    }

}
