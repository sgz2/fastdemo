package com.saoft.fastdemo.ui.shared.controller;

import javax.swing.*;

public abstract class AbstractPanelController {

    public abstract JComponent getPanel();
}
