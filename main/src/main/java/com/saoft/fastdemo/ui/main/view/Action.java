/*
 * Created by JFormDesigner on Fri Jun 07 08:17:44 CST 2019
 */

package com.saoft.fastdemo.ui.main.view;

import com.saoft.fastdemo.ui.shared.ButtonUtil;
import com.saoft.fastdemo.ui.shared.panel.LinePanelProxy;
import org.springframework.stereotype.Component;

import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("Action")
public class Action extends JPanel {

    public Action() {
        initComponents();
        after();
    }

    void after(){
        ButtonUtil.convertIconButton(refresh, ButtonUtil.REFRESH);
        ButtonUtil.convertIconButton(brower, ButtonUtil.RUN);
        ButtonUtil.convertIconButton(export, ButtonUtil.EXPORT);
    }

    private void initComponents() {
        LinePanelProxy proxy = new LinePanelProxy(this);

        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        brower = new JButton();
        refresh = new JButton();
        export = new JButton("导出");

        //---- button1 ----
        brower.setText("\u9884\u89c8");
        refresh.setText("刷新");
        proxy.add(brower);
        proxy.add(refresh);
        proxy.add(export);

        // JFormDesigner - End of component initialization  //GEN-END:initComponents
        proxy.right();
    }



    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JButton brower;
    private JButton refresh;
    private JButton export;

    public JButton getRefresh() {
        return refresh;
    }

    public JButton getBrower() {
        return brower;
    }

    public JButton getExport() {
        return export;
    }
}
