package com.saoft.fastdemo.ui.db.model;

import com.saoft.fastdemo.db.entity.TableRelation;
import com.saoft.fastdemo.ui.shared.model.DefaultTableModel;

public class TableRelationTableModel extends DefaultTableModel<TableRelation> {

    @Override
    public String[] getColumnLabels() {
        return new String[]{
                "编码",
                "类型",
                "字段",
                "参考表",
                "参考字段"
        };
    }

    @Override
    public Object getValueAt(int rowIndex, int relationIndex) {
        TableRelation relation = entities.get(rowIndex);

        switch (relationIndex) {
            case 0:
                return relation.getName();
            case 1:
                return relation.getType();
            case 2:
                return relation.getFcolumnId();
            case 3:
                return relation.getTtableId();
            case 4:
                return relation.getTcolumnId();
        }
        return null;
    }
}
