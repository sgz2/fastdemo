package com.saoft.fastdemo.ui.db.view.menu.insertmethod;

import com.saoft.fastdemo.system.ChooserUtil;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.bean.ServiceDaoData;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.File;

@Component("DictoryConfigController")
public class DictoryConfigController {

    @Autowired
    DictoryConfig dictoryConfig;

    @Autowired
    InsertMethod insertMethod;


    @PostConstruct
    private void init(){
        ControllerUtil.registerAction(insertMethod.getDictoryBtn(),e->loadConfig());

        ControllerUtil.registerAction(dictoryConfig.getCancelButton(),e->dictoryConfig.setVisible(false));

        ControllerUtil.registerAction(dictoryConfig.getDirBtn(),e->selectDir());
        ControllerUtil.registerAction(dictoryConfig.getDaoBtn(),e->selectDao());
        ControllerUtil.registerAction(dictoryConfig.getDao_xmlBtn(),e->selectDaoXml());
        ControllerUtil.registerAction(dictoryConfig.getService_implBtn(),e->selectServiceIMpl());
        ControllerUtil.registerAction(dictoryConfig.getServiceBtn(),e->selectService());
        ControllerUtil.registerAction(dictoryConfig.getOkButton(),e->save());

    }

    public void loadConfig(){
        dictoryConfig.getDir().setText(PrefUtil.get(PrefUtil.current_project_dir));
        dictoryConfig.getDao().setText(PrefUtil.get(PrefUtil.current_dao));
        dictoryConfig.getDao_xml().setText(PrefUtil.get(PrefUtil.current_dao_xml));
        dictoryConfig.getService().setText(PrefUtil.get(PrefUtil.current_service));
        dictoryConfig.getService_impl().setText(PrefUtil.get(PrefUtil.current_service_impl));
        dictoryConfig.setVisible(true);
    }

    /*保存配置*/
    void save(){
        String dir = dictoryConfig.getDir().getText();
        String dao = dictoryConfig.getDao().getText();
        String ser = dictoryConfig.getService().getText();
        String dao_impl = dictoryConfig.getDao_xml().getText();
        String ser_impl = dictoryConfig.getService_impl().getText();

        PrefUtil.set(PrefUtil.current_project_dir,dir);
        PrefUtil.set(PrefUtil.current_dao,dao);
        PrefUtil.set(PrefUtil.current_dao_xml,dao_impl);
        PrefUtil.set(PrefUtil.current_service,ser);
        PrefUtil.set(PrefUtil.current_service_impl,ser_impl);

        dictoryConfig.setVisible(false);
    }

    void selectDir(){
        File file = ChooserUtil.getDirectoryFile(PrefUtil.get(PrefUtil.current_project_dir));
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            absolutePath.replaceAll("\\\\", "/");
            PrefUtil.set(PrefUtil.current_project_dir, absolutePath);
            dictoryConfig.getDir().setText(absolutePath);
            //扫描其他包路径
            ServiceDaoData scan = ScanUtil.scan(absolutePath);

            if (scan.getDao() != null) {
                dictoryConfig.getDao().setText(scan.getDao());
            }

            if (scan.getDao_xml() != null) {
                dictoryConfig.getDao_xml().setText(scan.getDao_xml());
            }

            if (scan.getService() != null) {
                dictoryConfig.getService().setText(scan.getService());
            }

            if (scan.getService_impl() != null) {
                dictoryConfig.getService_impl().setText(scan.getService_impl());
            }
        }

    }

    void selectDao(){
        File file = ChooserUtil.getDirectoryFile(PrefUtil.get(PrefUtil.current_project_dir));
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            dictoryConfig.getDao().setText(absolutePath);
        }
    }

    void selectDaoXml(){
        File file = ChooserUtil.getDirectoryFile(PrefUtil.get(PrefUtil.current_project_dir));
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            dictoryConfig.getDao_xml().setText(absolutePath);

        }
    }

    void selectService(){
        File file = ChooserUtil.getDirectoryFile(PrefUtil.get(PrefUtil.current_project_dir));
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            dictoryConfig.getService().setText(absolutePath);

        }
    }

    void selectServiceIMpl(){
        File file = ChooserUtil.getDirectoryFile(PrefUtil.get(PrefUtil.current_project_dir));
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            dictoryConfig.getService_impl().setText(absolutePath);

        }
    }

}
