package com.saoft.fastdemo.ui.db.util;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.JTableColumn;

import java.util.List;

public class TableInfoUtil {


    /**
     * 获取主键 列
     * @param tableName
     * @return
     */
    public static String pk(String tableName) {
        if (tableName == null) {
            return null;
        }
        List<JTableColumn> aTrue = new JTableColumn().selectList(new QueryWrapper<JTableColumn>()
                .eq(DBConstants.table_name, tableName)
                .eq(DBConstants.key_flag, "true"));

        if (aTrue == null) {
            return null;
        }

        for (JTableColumn column : aTrue) {
            return column.getName();
        }
        return null;
    }
}
