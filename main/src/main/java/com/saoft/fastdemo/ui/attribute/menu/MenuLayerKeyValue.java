package com.saoft.fastdemo.ui.attribute.menu;

import java.util.HashMap;
import java.util.Map;

public class MenuLayerKeyValue {

    public static Map<String, String> map = new HashMap<>();

    static {
        map.put("create", "add");
        map.put("list", "list");
        map.put("update", "edit");
        map.put("delete", "delete");
    }
}
