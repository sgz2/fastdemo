/*
 * Created by JFormDesigner on Tue Jun 18 20:27:31 CST 2019
 */

package com.saoft.fastdemo.ui.db.view;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.db.model.TableRelationTableModel;
import com.saoft.fastdemo.ui.shared.panel.EmptyBorderScrollPane;

import java.awt.*;
import java.util.Objects;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class TableTabPanel extends JPanel {

    private TableInfo tableInfo;

    public TableTabPanel(TableInfo tableInfo) {
        this.tableInfo = tableInfo;
        initComponents();
        initAfter();
    }

    public JTable getRelationTable() {
        return relationTable;
    }

    public TableInfo getTableInfo() {
        return tableInfo;
    }

    private void initAfter(){
        relationTable.setModel(new TableRelationTableModel());
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        tabbedPane1 = new JTabbedPane();
        panel1 = new JPanel();
        actionPanel = new JPanel();
        scrollPane1 = new JScrollPane();
        dataTable = new JTable();
        panel5 = new JPanel();
        panel6 = new JPanel();
        dataAddBtn = new JButton();
        dataEditBtn = new JButton();
        dataDeleteBtn = new JButton();
        pagePanel = new JPanel();
        button1 = new JButton();
        button2 = new JButton();
        panel2 = new JPanel();
        structPanel = new JPanel();
        scrollPane2 = new JScrollPane();
        stuctTable = new JTable();
        panel3 = new JPanel();
        panel4 = new JPanel();
        addRBtn = new JButton();
        delRBtn = new JButton();
        scrollPane3 = new JScrollPane();
        relationTable = new JTable();

        //======== this ========
        setLayout(new BorderLayout());

        //======== tabbedPane1 ========
        {

            //======== panel1 ========
            {
                panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));

                //======== actionPanel ========
                {
                    actionPanel.setLayout(new BoxLayout(actionPanel, BoxLayout.X_AXIS));
                }
                panel1.add(actionPanel);

                //======== scrollPane1 ========
                {
                    scrollPane1.setViewportView(dataTable);
                }
                panel1.add(scrollPane1);

                //======== panel5 ========
                {
                    panel5.setMaximumSize(new Dimension(2147483647, 30));
                    panel5.setLayout(new BorderLayout());

                    //======== panel6 ========
                    {
                        panel6.setLayout(new BoxLayout(panel6, BoxLayout.X_AXIS));

                        //---- dataAddBtn ----
                        dataAddBtn.setText("add");
                        panel6.add(dataAddBtn);

                        //---- dataEditBtn ----
                        dataEditBtn.setText("edit");
                        panel6.add(dataEditBtn);

                        //---- dataDeleteBtn ----
                        dataDeleteBtn.setText("delete");
                        panel6.add(dataDeleteBtn);
                    }
                    panel5.add(panel6, BorderLayout.WEST);

                    //======== pagePanel ========
                    {
                        pagePanel.setLayout(new BoxLayout(pagePanel, BoxLayout.X_AXIS));

                        //---- button1 ----
                        button1.setText("\u4e0a\u4e00\u9875");
                        pagePanel.add(button1);

                        //---- button2 ----
                        button2.setText("\u4e0b\u4e00\u9875");
                        pagePanel.add(button2);
                    }
                    panel5.add(pagePanel, BorderLayout.EAST);
                }
                panel1.add(panel5);
            }
            tabbedPane1.addTab("\u6570\u636e", panel1);

            //======== panel2 ========
            {
                panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));

                //======== structPanel ========
                {
                    structPanel.setLayout(new BoxLayout(structPanel, BoxLayout.X_AXIS));
                }
                panel2.add(structPanel);

                //======== scrollPane2 ========
                {
                    scrollPane2.setViewportView(stuctTable);
                }
                panel2.add(scrollPane2);
            }
            tabbedPane1.addTab("\u7ed3\u6784", panel2);

            //======== panel3 ========
            {
                panel3.setLayout(new BoxLayout(panel3, BoxLayout.Y_AXIS));

                //======== panel4 ========
                {
                    panel4.setLayout(new BoxLayout(panel4, BoxLayout.X_AXIS));

                    //---- addRBtn ----
                    addRBtn.setText("\u6dfb\u52a0");
                    panel4.add(addRBtn);

                    //---- delRBtn ----
                    delRBtn.setText("\u5220\u9664");
                    panel4.add(delRBtn);
                }
                panel3.add(panel4);

                //======== scrollPane3 ========
                {
                    scrollPane3.setViewportView(relationTable);
                }
                panel3.add(scrollPane3);
            }
            tabbedPane1.addTab("\u5173\u7cfb", panel3);
        }
        add(tabbedPane1, BorderLayout.CENTER);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel actionPanel;
    private JScrollPane scrollPane1;
    private JTable dataTable;
    private JPanel panel5;
    private JPanel panel6;
    private JButton dataAddBtn;
    private JButton dataEditBtn;
    private JButton dataDeleteBtn;
    private JPanel pagePanel;
    private JButton button1;
    private JButton button2;
    private JPanel panel2;
    private JPanel structPanel;
    private JScrollPane scrollPane2;
    private JTable stuctTable;
    private JPanel panel3;
    private JPanel panel4;
    private JButton addRBtn;
    private JButton delRBtn;
    private JScrollPane scrollPane3;
    private JTable relationTable;
    // JFormDesigner - End of variables declaration  //GEN-END:variables


    public JTable getDataTable() {
        return dataTable;
    }

    public JTable getStuctTable() {
        return stuctTable;
    }

    public JButton getAddRBtn() {
        return addRBtn;
    }

    public JButton getDelRBtn() {
        return delRBtn;
    }

    public JButton getDataAddBtn() {
        return dataAddBtn;
    }

    public JButton getDataEditBtn() {
        return dataEditBtn;
    }

    public JButton getDataDeleteBtn() {
        return dataDeleteBtn;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TableTabPanel that = (TableTabPanel) o;
        return Objects.equals(tableInfo, that.tableInfo);
    }

    @Override
    public int hashCode() {

        return Objects.hash(tableInfo);
    }
}
