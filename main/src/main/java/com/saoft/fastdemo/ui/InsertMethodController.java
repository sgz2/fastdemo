package com.saoft.fastdemo.ui;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.gen.InsertFtlBean;
import com.saoft.fastdemo.gen.MethodData;
import com.saoft.fastdemo.ui.db.view.DbTablePanel;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.InsertMethod;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.PrefUtil;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.bean.NameData;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.bean.ServiceDaoData;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.io.File;

@Component("InsertMethodController")
public class InsertMethodController {

    /*Dialog*/
    @Autowired
    InsertMethod insertMethod;

    @Autowired
    DbTablePanel dbTablePanel;

    @PostConstruct
    void init(){
        /*绑定insertmethod的方法*/
        ControllerUtil.registerAction(insertMethod.getReload(), e -> reload());
        ControllerUtil.registerAction(insertMethod.getOkButton(),e -> imok());
        ControllerUtil.registerAction(insertMethod.getCancelButton(),e -> insertMethod.setVisible(false));
    }

    /**
     * 重新加载 识别需要注入的类
     */
    private void reload() {
        //读取两个配置
        ServiceDaoData serviceDaoData = new ServiceDaoData();
        serviceDaoData.setDao(PrefUtil.get(PrefUtil.current_dao));
        serviceDaoData.setDao_xml(PrefUtil.get(PrefUtil.current_dao_xml));
        serviceDaoData.setService(PrefUtil.get(PrefUtil.current_service));
        serviceDaoData.setService_impl(PrefUtil.get(PrefUtil.current_service_impl));

        NameData nameData = new NameData();
        nameData.setDao(PrefUtil.get(PrefUtil.name_dao));
        nameData.setDao_xml(PrefUtil.get(PrefUtil.name_dao_xml));
        nameData.setService(PrefUtil.get(PrefUtil.name_service));
        nameData.setService_impl(PrefUtil.get(PrefUtil.name_service_impl));
        nameData.setStyle(PrefUtil.get(PrefUtil.name_style));

        //当前点击的类名
        TableInfo tableInfo = dbTablePanel.getTableInfoJList().getSelectedValue();
        String name = tableInfo.getName();

        String style = nameData.getStyle();
        if ("UpperCamelCase".equals(style)) {
            name = com.baomidou.mybatisplus.core.toolkit.StringUtils.underlineToCamel(name);
            name = name.substring(0, 1).toUpperCase() + name.substring(1, name.length());
        }
        //得到几个文件名
        String dao = String.format(nameData.getDao(), name);
        String daox = String.format(nameData.getDao_xml(), name);
        String ser = String.format(nameData.getService(), name);
        String seri = String.format(nameData.getService_impl(), name);

        //查看文件是否存在
        if (new File(serviceDaoData.getDao(),dao).exists()) {
            insertMethod.getDao().setText(dao);
            insertMethod.getDao().setToolTipText(serviceDaoData.getDao()+"/"+dao);
        }else{
            insertMethod.getDao().setText("");
        }
        if (new File(serviceDaoData.getDao_xml(),daox).exists()) {
            insertMethod.getDao_xml().setText(daox);
            insertMethod.getDao_xml().setToolTipText(serviceDaoData.getDao_xml()+"/"+daox);
        }else{
            insertMethod.getDao_xml().setText("");
        }
        if (new File(serviceDaoData.getService(),ser).exists()) {
            insertMethod.getService().setText(ser);
            insertMethod.getService().setToolTipText(serviceDaoData.getService()+"/"+ser);
        }else{
            insertMethod.getService().setText("");
        }
        if (new File(serviceDaoData.getService_impl(),seri).exists()) {
            insertMethod.getService_impl().setText(seri);
            insertMethod.getService_impl().setToolTipText(serviceDaoData.getService_impl()+"/"+seri);
        }else{
            insertMethod.getService_impl().setText("");
        }


    }

    private void imok() {
        //插入
        System.out.println("save imok");
        boolean d = insertMethod.getDao_chk().isSelected();
        boolean dx = insertMethod.getDao_xml_chk().isSelected();
        boolean s= insertMethod.getService_chk().isSelected();
        boolean si = insertMethod.getService_impl_chk().isSelected();

        String name = insertMethod.getName1().getText();
        String parm = insertMethod.getParm().getText();
        String back = insertMethod.getBack().getText();
        String type = insertMethod.getCrud().getSelectedItem().toString();

        MethodData methodData = new MethodData();
        methodData.setName(name);
        methodData.setParm(parm);
        methodData.setResultType(back);
        methodData.setType(type);

        //1.获取模板配置 2.组装数据 3.生成目标
        InsertFtlBean bean = new InsertFtlBean();
        JList<TableInfo> tableInfoJList = dbTablePanel.getTableInfoJList();

        TableInfo tableInfo = tableInfoJList.getSelectedValue();
        bean.getData().put("table", tableInfo);
        bean.getData().put("entityFL"
                , com.baomidou.mybatisplus.core.toolkit.StringUtils.underlineToCamel(tableInfo.getName()));
        bean.getData().put("method", methodData);

        {
            if (d){
                JTextField dao = insertMethod.getDao();
                if(StringUtils.hasLength(dao.getText())){
                    String toolTipText = dao.getToolTipText();
                    bean.appendMethodAndImport(toolTipText,"ssm/dao.ftl",methodData);
                }
            }
        }

        {
            if (dx){
                JTextField dao = insertMethod.getDao_xml();
                if(StringUtils.hasLength(dao.getText())){
                    String toolTipText = dao.getToolTipText();
                    bean.appendMethodAndImportXml(toolTipText,"ssm/dao_xml.ftl",methodData);
                }
            }
        }

        {
            if (s){
                JTextField service = insertMethod.getService();
                if(StringUtils.hasLength(service.getText())){
                    String toolTipText = service.getToolTipText();
                    bean.appendMethodAndImport(toolTipText,"ssm/service.ftl",methodData);
                }
            }
        }

        {
            if (si){
                JTextField service_impl = insertMethod.getService_impl();
                if(StringUtils.hasLength(service_impl.getText())){
                    String toolTipText = service_impl.getToolTipText();
                    bean.appendMethodAndImport(toolTipText,"ssm/service_impl.ftl",methodData);
                }
            }
        }

    }

}
