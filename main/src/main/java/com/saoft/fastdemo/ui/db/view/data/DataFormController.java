package com.saoft.fastdemo.ui.db.view.data;

import com.saoft.dbinfo.po.TableInfo;
import com.saoft.fastdemo.ui.db.TableTabController;
import com.saoft.fastdemo.ui.db.util.TableInfoUtil;
import com.saoft.fastdemo.ui.db.view.TableTabPanel;
import com.saoft.fastdemo.ui.preview.PreviewController;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import com.saoft.fastdemo.web.component.SqlComponent;
import com.saoft.fastdemo.web.component.TableComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Map;

@Component("DataFormController")
public class DataFormController {

    @Autowired
    DataForm dataForm;

    @Autowired
    TableComponent tableComponent;

    @Autowired
    PreviewController previewController;

    @Autowired
    TableTabController tableTabController;

    @Autowired
    SqlComponent sqlComponent;

    @PostConstruct
    private void init(){
        ControllerUtil.registerAction(dataForm.getOkButton(),e->ok());
        ControllerUtil.registerAction(dataForm.getCancelButton(),e->cancle());
    }

    private void ok(){
        Map<String, Object> map = dataForm.extraData();
        //tableinfo
        java.awt.Component currentTab = previewController.getCurrentTab();
        if (currentTab instanceof TableTabPanel) {
            TableInfo tableInfo = ((TableTabPanel) currentTab).getTableInfo();
            String tableInfoName = tableInfo.getName();
            sqlComponent.executeInsertSql(tableInfoName, map,TableInfoUtil.pk(tableInfoName));
        }
        //刷新table
        tableTabController.initTable();
        cancle();
    }

    private void cancle(){
        dataForm.setVisible(false);
    }
}
