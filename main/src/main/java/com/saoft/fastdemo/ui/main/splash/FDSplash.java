/*
 * Created by JFormDesigner on Sun Aug 25 18:13:40 CST 2019
 */

package com.saoft.fastdemo.ui.main.splash;

import com.saoft.fastdemo.Application;
import com.saoft.fastdemo.ui.shared.button.MFrame;
import org.springframework.core.io.ClassPathResource;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

/**
 * @author Brainrain
 */
public class FDSplash extends MFrame {

    private BufferedImage img;

    public FDSplash() {
        setUndecorated(true);
        before();
        initComponents();
        after();
    }

    private void after() {
        version.setText("当前版本："+Application.VERSION);
        version.setForeground(Color.white);
        version.setBounds(800 - 240, 600 - 40, 250, 25);
    }

    private void before(){
        try {
            img = ImageIO.read(new ClassPathResource("/icon/fastdemo-splash.png").getURL());
//            image.setIcon(new ImageIcon());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void setProgress(Integer progress){
        this.progress.setValue(progress);
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        progress = new JProgressBar();
        imagePanel = new MyPanel();
        version = new JLabel();

        //======== this ========
        setMinimumSize(new Dimension(800, 600));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());
        contentPane.add(progress, BorderLayout.PAGE_END);

        //======== imagePanel ========
        {
            imagePanel.setPreferredSize(new Dimension(0, 0));
            imagePanel.setMinimumSize(new Dimension(0, 0));
            imagePanel.setLayout(null);

            //---- version ----
            version.setHorizontalAlignment(SwingConstants.LEFT);
            version.setVerticalAlignment(SwingConstants.TOP);
            version.setText("\u5f53\u524d\u7248\u672c\uff1a0.0.00000000");
            version.setFont(new Font("Microsoft YaHei UI", Font.PLAIN, 18));
            imagePanel.add(version);
            version.setBounds(120, 230, 250, 25);

            { // compute preferred size
                Dimension preferredSize = new Dimension();
                for(int i = 0; i < imagePanel.getComponentCount(); i++) {
                    Rectangle bounds = imagePanel.getComponent(i).getBounds();
                    preferredSize.width = Math.max(bounds.x + bounds.width, preferredSize.width);
                    preferredSize.height = Math.max(bounds.y + bounds.height, preferredSize.height);
                }
                Insets insets = imagePanel.getInsets();
                preferredSize.width += insets.right;
                preferredSize.height += insets.bottom;
                imagePanel.setMinimumSize(preferredSize);
                imagePanel.setPreferredSize(preferredSize);
            }
        }
        contentPane.add(imagePanel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    public JLabel getVersion() {
        return version;
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JProgressBar progress;
    private JPanel imagePanel;
    private JLabel version;
    // JFormDesigner - End of variables declaration  //GEN-END:variables

    private class MyPanel extends JPanel{
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            g.drawImage(img, 0, 0, getWidth(), getHeight(), this);
        }
    }
}
