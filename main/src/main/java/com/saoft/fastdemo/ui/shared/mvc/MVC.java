package com.saoft.fastdemo.ui.shared.mvc;

public interface MVC<T> {

    void initValue();
    T getData();
    void buildData();
}
