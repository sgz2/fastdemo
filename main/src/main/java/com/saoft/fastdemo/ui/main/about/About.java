/*
 * Created by JFormDesigner on Wed Sep 04 20:56:18 CST 2019
 */

package com.saoft.fastdemo.ui.main.about;

import cn.hutool.http.HttpUtil;
import com.saoft.fastdemo.Application;
import com.saoft.fastdemo.system.FastdemoProperties;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.shared.button.HrefButton;
import com.saoft.fastdemo.ui.shared.button.MDialog;
import com.saoft.fastdemo.util.BrowseUtil;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

/**
 * @author Brainrain
 */
@Component("About")
public class About extends MDialog {

    public About(MainFrame owner) {
        super(owner);
        initComponents();
        after();
        bind();
    }

    private void after() {
        version.setText(Application.VERSION);
    }

    private void bind(){
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BrowseUtil.openUrl("http://doc.saoft.com/1263655");
            }
        });
    }

    public JLabel getServer_version() {
        return server_version;
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        panel1 = new JPanel();
        label1 = new JLabel();
        label2 = new JLabel();
        label4 = new JLabel();
        panel2 = new JPanel();
        version = new JLabel();
        server_version = new JLabel();
        button1 = new JButton();

        //======== this ========
        setMinimumSize(new Dimension(300, 200));
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== panel1 ========
        {
            panel1.setLayout(new GridLayout(10, 0));

            //---- label1 ----
            label1.setText("\u5f53\u524d\u7248\u672c\uff1a");
            panel1.add(label1);

            //---- label2 ----
            label2.setText("\u6700\u65b0\u7248\u672c\uff1a");
            panel1.add(label2);

            //---- label4 ----
            label4.setText("\u66f4\u65b0\u8bb0\u5f55\uff1a");
            panel1.add(label4);
        }
        contentPane.add(panel1, BorderLayout.WEST);

        //======== panel2 ========
        {
            panel2.setLayout(new GridLayout(10, 0));

            //---- version ----
            version.setText("0.0");
            panel2.add(version);

            //---- server_version ----
            server_version.setText("\u5df2\u7ecf\u662f\u6700\u65b0\u7248\u672c");
            panel2.add(server_version);

            //---- button1 ----
            button1.setText("\u70b9\u51fb\u67e5\u770b");
            panel2.add(button1);
        }
        contentPane.add(panel2, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    private JPanel panel1;
    private JLabel label1;
    private JLabel label2;
    private JLabel label4;
    private JPanel panel2;
    private JLabel version;
    private JLabel server_version;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
