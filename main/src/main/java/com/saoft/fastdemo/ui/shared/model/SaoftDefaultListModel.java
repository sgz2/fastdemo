package com.saoft.fastdemo.ui.shared.model;


import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

public class SaoftDefaultListModel<T> extends AbstractListModel {

    protected List<T> entities = new ArrayList<>();

    public void addEntities(List<T> entities) {
        this.entities.addAll(entities);
        fireIntervalAdded(this,0,entities.size());
    }
    public void clear() {
        entities.clear();
    }

    @Override
    public int getSize() {
        return entities.size();
    }

    @Override
    public Object getElementAt(int index) {
        return entities.get(index);
    }
}
