package com.saoft.fastdemo.ui.db.view.local.dto;

public class RelationDTO {

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public static RelationDTO m2o(){
        RelationDTO dto = new RelationDTO();
        dto.setCode("m2o");
        dto.setName("多对一");
        return dto;
    }

    public static RelationDTO o2m(){
        RelationDTO dto = new RelationDTO();
        dto.setCode("o2m");
        dto.setName("一对多");
        return dto;
    }

    @Override
    public String toString() {
        return name;
    }
}
