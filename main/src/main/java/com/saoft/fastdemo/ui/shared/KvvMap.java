package com.saoft.fastdemo.ui.shared;

import java.util.HashMap;
import java.util.Map;

public class KvvMap<T,T1,T2> {

    private Map<T, T1> map1 = new HashMap<>();
    private Map<T, T2> map2 = new HashMap<>();

    public void put1(T key,T1 value){
        map1.put(key, value);
    }

    public void put2(T key,T2 value){
        map2.put(key, value);
    }

    public T1 get1(T key){
        return map1.get(key);
    }

    public T2 get2(T key){
        return map2.get(key);
    }

    public void setMap1(Map<T, T1> map1) {
        this.map1 = map1;
    }

    public void setMap2(Map<T, T2> map2) {
        this.map2 = map2;
    }
}
