package com.saoft.fastdemo.ui.shared;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;

import java.io.IOException;
import java.util.LinkedList;

public class DirUtil {

    /**
     * <p>
     * 打开输出目录
     * </p>
     */
    public static void open(String outDir) {
        if (StringUtils.isNotEmpty(outDir)) {
            try {
                String osName = System.getProperty("os.name");
                if (osName != null) {
                    if (osName.contains("Mac")) {
                        Runtime.getRuntime().exec("open " + outDir);
                    } else if (osName.contains("Windows")) {
                        Runtime.getRuntime().exec("cmd /c start " + outDir);
                    } else {

                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static boolean openByIdea(String outDir) {
        if (StringUtils.isNotEmpty(outDir)) {
            try {
                LinkedList<String> list = new LinkedList<>();
                list.add("idea64.exe");
                list.add(outDir);
                CmdRunner.run(list,null);
                return true;
            } catch (Exception e) {
                return false;
            }
        }
        return false;
    }
}
