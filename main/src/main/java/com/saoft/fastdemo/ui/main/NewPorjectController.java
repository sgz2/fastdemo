package com.saoft.fastdemo.ui.main;

import com.saoft.fastdemo.util.CheckProjUtil;
import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.main.view.MainFrame;
import com.saoft.fastdemo.ui.main.view.NewProjectFrame;
import com.saoft.fastdemo.ui.shared.controller.ControllerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.swing.*;
import java.io.File;

@Component("NewPorjectController")
public class NewPorjectController {

    TopController topController;

    @Autowired
    NewProjectFrame newProjectFrame;

    @Autowired
    PreferencesUtil preferencesUtil;

    MainFrame mainFrame;

    @PostConstruct
    void init(){
        ControllerUtil.registerAction(newProjectFrame.getOpenBtn(),e -> openDir());
        ControllerUtil.registerAction(newProjectFrame.getOkButton(),e -> ok());
        ControllerUtil.registerAction(newProjectFrame.getCancelButton(),e -> cancel());
    }

    void openDir(){
        if(mainFrame==null){
            mainFrame = S.getBean(MainFrame.class);
        }

        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.showOpenDialog(mainFrame);
        File file = chooser.getSelectedFile();
        if (file != null) {
            String absolutePath = file.getAbsolutePath();
            newProjectFrame.getDirPath().setText(absolutePath);
        }
    }

    void ok(){
        String text = newProjectFrame.getDirPath().getText();
        String text1 = newProjectFrame.getFname().getText();

        String s = text.replaceAll("\\\\", "/");
        s += "/" + text1;
        //新建数据库文件
        CheckProjUtil.createIfNotExists(s);

        String dbUrl = CheckProjUtil.getH2Url(s);

        //设置jdbctemplate datasource
        S.replaceDataSource(dbUrl);

        //刷新界面
        if (topController == null) {
            topController = S.getBean(TopController.class);
        }
        topController.refresh();
        //设置最近文件
        preferencesUtil.setH2File(s);
        newProjectFrame.setVisible(false);
    }


    void cancel(){
        newProjectFrame.setVisible(false);
    }
}
