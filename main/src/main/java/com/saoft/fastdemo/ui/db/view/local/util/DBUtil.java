package com.saoft.fastdemo.ui.db.view.local.util;

import com.saoft.fastdemo.ui.db.view.local.dto.DBConfigDTO;
import org.springframework.util.StringUtils;

import java.sql.*;
import java.util.LinkedList;
import java.util.List;

public class DBUtil {

    /**
     * 获取连接
     */
    public static Connection getConn(DBConfigDTO dto) {
        return getConn(dto, "");
    }
    public static Connection getConn(DBConfigDTO dto,String db) {

        if (StringUtils.hasLength(db)) {
            db = "/" + db;
        }else{
            db = "";
        }
        String driverName = "com.mysql.jdbc.Driver";
        String url = "jdbc:mysql://localhost:3306"+db+"?useUnicode=true&useSSL=false&characterEncoding=utf8&serverTimezone=UTC";
        String username = dto.getUsername();
        String password = dto.getPassword();

        Connection conn = null;
        try {
            Class.forName(driverName);
            conn = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException | SQLException e) {
            e.printStackTrace();
        }
        return conn;
    }

    public static List<String> getAllDB(DBConfigDTO dto) {
        String sql = "show databases";
        Connection conn = getConn(dto);
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            List<String> dbs = new LinkedList<>();
            while (resultSet.next()) {
                String string = resultSet.getString(1);
                dbs.add(string);
            }
            return dbs;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                conn.close();
            } catch (Exception e) {
            }
        }

        return null;
    }

    public static List<String> getAllTable(DBConfigDTO dto, String dbname) {
        String use = "use "+dbname;
        String sql = "show table status";
        Connection conn = getConn(dto);
        try {
            PreparedStatement ps = conn.prepareStatement(use);
            ps.execute();

            ps = conn.prepareStatement(sql);
            ResultSet resultSet = ps.executeQuery();
            List<String> tables = new LinkedList<>();
            while (resultSet.next()) {
                String string = resultSet.getString(1);
                tables.add(string);
            }
            return tables;
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                conn.close();
            } catch (Exception e) {
            }
        }

        return null;
    }
}

