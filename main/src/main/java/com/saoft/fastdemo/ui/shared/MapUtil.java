package com.saoft.fastdemo.ui.shared;

import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.ui.main.export.ExportSetting;
import org.springframework.cglib.beans.BeanMap;
import org.springframework.util.StringUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class MapUtil {

    /**
     *
     * @param map
     * @param key
     * @param value
     * @param <T>
     */
    public static <T,T2> void  putListValue(Map<T2, LinkedList<T>> map,T2 key,T value){
        if (map.get(key) == null) {
            LinkedList<T> objects = new LinkedList<>();
            map.put(key, objects);
        }
        LinkedList o = map.get(key);
        o.add(value);
    }

    /**
     *
     * @param list
     * @param keyAttr
     * @param valueAttr
     */
    public static  Map<String,String>  listToMap(List list,String keyAttr,String valueAttr){
        Map<String,String> listToMap = new HashMap<>();
        for (Object o : list) {
            BeanMap map = BeanMap.create(o);
            Object o1 = map.get(keyAttr);
            Object o2 = map.get(valueAttr);
            if (o1 != null && o2 != null) {
                listToMap.put(o1.toString(), o2.toString());
            }

        }
        return listToMap;
    }

    public static Map<String,Object> getFieldsByAttribute(Object t) {
        Map<String, Object> map = new HashMap<>();
        Class<?> aClass = t.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            TableField annotation = field.getAnnotation(TableField.class);
            if (annotation != null) {
                try {
                    field.setAccessible(true);
                    //这里是通过属性名称
                    map.put(field.getName(), field.get(t));
                } catch (IllegalAccessException e) {
                    continue;
                }
            }
        }
        return map;
    }

    public static Map<String,Object> getFields(Object t) {
        Map<String, Object> map = new HashMap<>();
        Class<?> aClass = t.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {
            TableField annotation = field.getAnnotation(TableField.class);
            if (annotation != null) {
                try {
                    field.setAccessible(true);
                    map.put(annotation.value(), field.get(t));
                } catch (IllegalAccessException e) {
                    continue;
                }
            }
        }
        return map;
    }

    public static <T> Map<String,T>  listToMap(List<T> list,String keyAttr){
        Map<String,T> listToMap = new HashMap<>();
        for (T o : list) {
            Map<String, Object> map = getFields(o);
            Object o1 = map.get(keyAttr);
            if (o1 != null) {
                listToMap.put(o1.toString(), o);
            }
        }
        return listToMap;
    }

    /**
     * 设置map的值到 对象中
     * @param t
     * @param value
     */
    public static void setValueByMap(Object t,Map<String,String> value){
        Class<?> aClass = t.getClass();
        Field[] fields = aClass.getDeclaredFields();

        for (Field field : fields) {

            String name = field.getName();
            String mapValue = value.get(name);
            if (StringUtils.hasLength(mapValue)) {
                try {
                    if (field.getType().equals(String.class) ) {
                        field.setAccessible(true);
                        field.set(t,mapValue);
                    }
                } catch (IllegalAccessException e) {
                    continue;
                }

            }
        }

    }
}
