package com.saoft.fastdemo.system;

import com.saoft.fastdemo.Application;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.prefs.Preferences;

@Order(1)
@Component("PreferencesUtil")
public class PreferencesUtil {

    private String default_proj = "default_proj";
    private String token = "token";
    private String username = "username";

    public Preferences getPreferences(){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        return pref;
    }

    public String getToken(){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        String s = pref.get(token, null);
        return s;
    }

    public void setToken(String nToken,String musername){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        pref.put(token, nToken);
        pref.put(username, musername);
    }

    public void setUsername(String musername){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        pref.put(username, musername);
    }


    public String getUsername(){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        String s = pref.get(username, null);
        return s;
    }

    /*获取最近文件*/
    public String getH2File(){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        String s = pref.get(default_proj, null);
        return s;
    }

    public boolean hasProj(){
        String h2File = getH2File();
        if (h2File == null) {
            return false;
        }
        return new File(h2File).exists();
    }

    /*设置最近文件*/
    public void setH2File(String h2File){
        Preferences pref = Preferences.userNodeForPackage(Application.class);
        pref.put(default_proj, h2File);
    }

}
