package com.saoft.fastdemo.system;

import com.saoft.fastdemo.util.CheckProjUtil;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import java.sql.SQLException;

@Order(1)
@Component("S")
public class S implements ApplicationContextAware {
    // 上下文对象实例
    private static ApplicationContext applicationContext;

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        S.applicationContext = applicationContext;
    }

    public static ApplicationContext getApplicationContext(){
        return applicationContext;
    }

    public static <T> T getBean(Class<T> clazz){
        return applicationContext.getBean(clazz);
    }
    public static <T> T b(Class<T> clazz){
        return applicationContext.getBean(clazz);
    }

    public static String getProperty(String property) {
        Environment bean = applicationContext.getBean(Environment.class);
        return bean.getProperty(property);
    }

    @SuppressWarnings("unchecked")
    public static <T> T getBean(String name){
        return (T)applicationContext.getBean(name);
    }

    public static <T> T getBean(String name, Class<T> clazz){
        return getApplicationContext().getBean(name, clazz);
    }

    public static void replaceDataSource(String dburl) {
        RuntimeDataSource bean = getBean(RuntimeDataSource.class);
        closeDataSource();
        HikariConfig config = getConfig(dburl);
        bean.setDataSource(new HikariDataSource(config));
    }

    //关闭当前连接
    public static void closeDataSource() {
        RuntimeDataSource bean = getBean(RuntimeDataSource.class);
        if (bean.getDataSource() != null) {
            HikariDataSource dataSource = (HikariDataSource) bean.getDataSource();

            if(!dataSource.isClosed()){
                try {
                    dataSource.getConnection().createStatement().execute("SHUTDOWN");
                    dataSource.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        }
    }

    private static HikariConfig getConfig(String dburl) {
        RuntimeDataSource bean = getBean(RuntimeDataSource.class);
        if (bean.getDataSource() != null) {
            HikariDataSource dataSource = (HikariDataSource) bean.getDataSource();

            HikariConfig jdbcConfig = new HikariConfig();
            jdbcConfig.setPoolName(dburl);
            jdbcConfig.setDriverClassName(dataSource.getDriverClassName());
            jdbcConfig.setJdbcUrl(dburl);
            jdbcConfig.setUsername(dataSource.getUsername());
            jdbcConfig.setPassword(dataSource.getPassword());
            jdbcConfig.setMaximumPoolSize(dataSource.getMaximumPoolSize());
            jdbcConfig.setMaxLifetime(dataSource.getMaxLifetime());
            jdbcConfig.setConnectionTimeout(dataSource.getConnectionTimeout());
            jdbcConfig.setIdleTimeout(dataSource.getIdleTimeout());

            return jdbcConfig;
        }else{
            HikariConfig config = new HikariConfig();
            config.setUsername(CheckProjUtil.USERNAME);
            config.setPassword(CheckProjUtil.PASSWORD);
            config.setJdbcUrl(CheckProjUtil.getH2Url(dburl));
            config.setDriverClassName("org.h2.Driver");
            return config;
        }

    }

}
