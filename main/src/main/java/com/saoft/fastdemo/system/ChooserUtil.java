package com.saoft.fastdemo.system;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.io.File;

public class ChooserUtil {

    public static void acceptFd(JFileChooser chooser){
        chooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if(f.getName().endsWith(".fdproj") || f.isDirectory()){
                    return true;
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "FastDemo文件";
            }
        });
    }

    public static File getFile(){
        return getFile(null);
    }
    public static File getFile(File currentFile){
        return getFile(currentFile,JFileChooser.FILES_ONLY);
    }

    public static File getDirectoryFile(){
        return getFile(null,JFileChooser.DIRECTORIES_ONLY);
    }

    public static File getDirectoryFile(String currentFile){
        try {
            File file = new File(currentFile);
            return getFile(file,JFileChooser.DIRECTORIES_ONLY);
        } catch (Exception e) {

        }
        return getDirectoryFile();
    }

    public static File getFile(File currentFile,int model){
        JFileChooser chooser = new JFileChooser();
        chooser.setFileSelectionMode(model);
        if (currentFile != null) {
            if(currentFile.isDirectory()){
                chooser.setCurrentDirectory(currentFile);
            }else{
                chooser.setCurrentDirectory(currentFile.getParentFile());
            }
        }
        chooser.showOpenDialog(null);

        File file = chooser.getSelectedFile();
        return file;
    }
}
