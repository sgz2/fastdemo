package com.saoft.fastdemo.system;

import com.saoft.fastdemo.util.CheckProjUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@EnableConfigurationProperties
@Configuration("ApplicationConfiguration")
@ComponentScan(basePackages = "com.saoft.fastdemo")
public class ApplicationConfiguration {

    @Autowired
    PreferencesUtil preferencesUtil;

    @Bean
    public DataSource dataSource() {

        RuntimeDataSource ds = new RuntimeDataSource();
        DataSource dataSource = CheckProjUtil.bootMysqlFile();
        ds.setDataSource(dataSource);
        //启动文件
//        String h2File = preferencesUtil.getH2File();
//        if(StringUtils.hasLength(h2File)){
//            DataSource dataSource = CheckProjUtil.bootH2File(preferencesUtil.getH2File());
//            ds.setDataSource(dataSource);
//        }
        return ds;
    }

}
