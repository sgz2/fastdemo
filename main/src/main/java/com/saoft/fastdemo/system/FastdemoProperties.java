package com.saoft.fastdemo.system;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component("FastdemoProperties")
@ConfigurationProperties(prefix = "fastdemo")
public class FastdemoProperties {

    private String apiServer = "http://localhost:18080";
    private String version = "1.0";

    public String getApiServer() {
        return apiServer;
    }

    public void setApiServer(String apiServer) {
        this.apiServer = apiServer;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
