package com.saoft.fastdemo.uic;


import com.saoft.fastdemo.ui.shared.tab.MyCloseButton;

import javax.swing.*;
import javax.swing.plaf.LayerUI;
import java.awt.*;
import java.awt.event.MouseEvent;

public class CloseableTabbedPaneLayerUI extends LayerUI<JTabbedPane> {
    JPanel p = new JPanel();
    Point pt = new Point(-100, -100);

    private JTabbedPane target;

    MyCloseButton button = new MyCloseButton();

    public CloseableTabbedPaneLayerUI() {
        super();
    }

    public void setTarget(JTabbedPane target) {
        this.target = target;
    }

    @Override
    public void paint(Graphics g, JComponent c) {
        super.paint(g, c);
        if (c instanceof JLayer == false) {
            return;
        }
        JLayer jlayer = (JLayer) c;
        JTabbedPane tabPane = (JTabbedPane) jlayer.getView();
        int tabCount = tabPane.getTabCount();
        for (int i = 0; i < tabCount; i++) {
            Rectangle rect = tabPane.getBoundsAt(i);
            Dimension d = button.getPreferredSize();
            int x = rect.x + rect.width - d.width - 2;
            int y = rect.y + (rect.height - d.height) / 2;
            Rectangle r = new Rectangle(x, y, d.width, d.height);
            button.setForeground(r.contains(pt) ? Color.RED : Color.BLACK);
            SwingUtilities.paintComponent(g, button, p, r);
        }
    }

    @Override
    public void installUI(JComponent c) {
        super.installUI(c);
        ((JLayer) c).setLayerEventMask(AWTEvent.MOUSE_EVENT_MASK
                | AWTEvent.MOUSE_MOTION_EVENT_MASK);
    }

    @Override
    public void uninstallUI(JComponent c) {
        ((JLayer) c).setLayerEventMask(0);
        super.uninstallUI(c);
    }

    protected Point convertPoint(MouseEvent e){
        Point point = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), target);
        return point;
    }

    @Override
    protected void processMouseEvent(MouseEvent e, JLayer<? extends JTabbedPane> l) {
        if (e.getID() != MouseEvent.MOUSE_CLICKED) {
            return;
        }
        pt.setLocation(convertPoint(e));
        JTabbedPane tabbedPane = (JTabbedPane) l.getView();
        int index = tabbedPane.indexAtLocation(pt.x, pt.y);
        if (index >= 0) {
            Rectangle rect = tabbedPane.getBoundsAt(index);
            Dimension d = button.getPreferredSize();
            int x = rect.x + rect.width - d.width - 2;
            int y = rect.y + (rect.height - d.height) / 2;
            Rectangle r = new Rectangle(x, y, d.width, d.height);
            if (r.contains(pt)) {
                tabbedPane.removeTabAt(index);
            }
        }
        l.getView().repaint();
    }

    @Override
    protected void processMouseMotionEvent(MouseEvent e,
                                           JLayer<? extends JTabbedPane> l) {

        pt.setLocation(convertPoint(e));
        JTabbedPane tabbedPane = (JTabbedPane) l.getView();
        int index = tabbedPane.indexAtLocation(pt.x, pt.y);
        if (index >= 0) {
            tabbedPane.repaint(tabbedPane.getBoundsAt(index));
        } else {
            tabbedPane.repaint();
        }
    }
}
