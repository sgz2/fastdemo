package com.saoft.fastdemo.uic;

import javax.swing.plaf.basic.BasicTabbedPaneUI;
import java.awt.*;

public class NoInsetTabbedPaneUI extends BasicTabbedPaneUI {
    /**
     * Create tabbed-pane-UI object to allow fine control of the
     * L&F of this specific object.
     */
    public NoInsetTabbedPaneUI(){
        super();
    }
    /**
     * Override the content border insets of the UI which represent
     * the L&F of the border around the pane. In this case only care
     * about having a bottom inset.
     */
    public void overrideContentBorderInsetsOfUI(){
        this.tabInsets = new Insets(2,2,2,50);
    }
}
