package com.saoft.fastdemo.init;

import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.db.DBTableController;
import com.saoft.fastdemo.ui.db.RightMenuController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;


@Component("DBRunner")
public class DBRunner implements CommandLineRunner {

    @Autowired
    DBTableController dbTableController;

    @Autowired
    RightMenuController rightMenuController;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    PreferencesUtil preferencesUtil;

    @Override
    public void run(String... args) throws Exception {

        if(preferencesUtil.hasProj()){
            //初始化
            dbTableController.initByDb();
        }
        rightMenuController.initByDB();

    }
}
