package com.saoft.fastdemo.init;

import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.main.export.ExportFrameController;
import com.saoft.fastdemo.ui.outline.OutlineController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component("OutRunner")
public class OutRunner implements CommandLineRunner {

    @Autowired
    OutlineController outlineController;

    @Autowired
    ExportFrameController exportFrameController;

    @Autowired
    PreferencesUtil preferencesUtil;
    @Override
    public void run(String... args) throws Exception {

        if(preferencesUtil.hasProj()){
            outlineController.initByDb();
            exportFrameController.initByDB();
        }
    }
}
