package com.saoft.fastdemo.util;

import org.springframework.beans.factory.config.YamlPropertiesFactoryBean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.util.Properties;

public class YamlConfigurerUtil {

    private static Properties ymlProperties = new Properties();

    public YamlConfigurerUtil(String ymlFile) {
        Resource app = new ClassPathResource(ymlFile);
        YamlPropertiesFactoryBean yamlPropertiesFactoryBean = new YamlPropertiesFactoryBean();
        // 2:将加载的配置文件交给 YamlPropertiesFactoryBean
        yamlPropertiesFactoryBean.setResources(app);
        // 3：将yml转换成 key：val
        Properties properties = yamlPropertiesFactoryBean.getObject();
        ymlProperties = properties;
    }

    public YamlConfigurerUtil(Properties properties){
        ymlProperties = properties;
    }

    public String getStrYmlVal(String key){
        return ymlProperties.getProperty(key);
    }

    public Integer getIntegerYmlVal(String key){
        return Integer.valueOf(ymlProperties.getProperty(key));
    }

}
