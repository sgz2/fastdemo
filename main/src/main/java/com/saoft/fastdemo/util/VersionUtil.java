package com.saoft.fastdemo.util;

import cn.hutool.http.HttpUtil;
import com.saoft.fastdemo.system.FastdemoProperties;
import com.saoft.fastdemo.system.S;

public class VersionUtil {

    public static String getServerVersion(){
        try {
            FastdemoProperties bean = S.getBean(FastdemoProperties.class);
            String apiServer = bean.getApiServer();
            String version = HttpUtil.get(apiServer + "/api/update/current_version");
            return version;
        } catch (Exception e) {
            return "0.0.00000000";
        }
    }
}
