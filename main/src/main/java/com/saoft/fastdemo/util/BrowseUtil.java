package com.saoft.fastdemo.util;

import java.awt.Desktop;
import java.net.URI;

public class BrowseUtil {
    public static Exception openUrl(String url) {
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    URI uri = new URI(url);
                    desktop.browse(uri);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return e;
        }
        return null;
    }
}
