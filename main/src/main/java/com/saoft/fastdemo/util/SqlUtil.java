package com.saoft.fastdemo.util;

import java.util.Map;
import java.util.Set;

public class SqlUtil {

    public static String toStr(Object obj){
        if (obj == null) {
            return "";
        }
        return obj.toString();
    }
    //动态表数据替换
    public static String ddlReplace(String sql, Map<String, String> repalce) {
        Set<String> strings = repalce.keySet();
        for (String string : strings) {
            sql = sql.replaceAll(string, repalce.get(string));
        }
        return sql;
    }
}
