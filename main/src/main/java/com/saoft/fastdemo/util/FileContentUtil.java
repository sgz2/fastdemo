package com.saoft.fastdemo.util;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * 复制文件时本地操作
 */
public class FileContentUtil {

    /**
     * 本地替换
     * @param src
     * @param regex "fastdemo_project"
     * @param replacement
     */
    public static void replace(File src,String regex,String replacement) {
        try {
            Path path = Paths.get(src.getAbsolutePath());
            Charset charset = StandardCharsets.UTF_8;

            String content = new String(Files.readAllBytes(path), charset);
            content = content.replaceAll(regex, replacement);
            Files.write(path, content.getBytes(charset));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
