package com.saoft.fastdemo.util;

import com.saoft.fastdemo.system.S;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.RunScript;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.io.*;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public class CheckProjUtil {

    public static final String SCHEMA = "fastdemo";
    public static final String USERNAME = "fastdemo";
    public static final String PASSWORD = "omedtsaf";

    public static DataSource bootMysqlFile() {
        String property = "123456";

        HikariConfig config = new HikariConfig();
        config.setUsername("root");
        config.setPassword(property);
        config.setJdbcUrl("jdbc:mysql://127.0.0.1:3306/fastdemo_v3_1?autoReconnect=true&useUnicode=true&characterEncoding=utf8&zeroDateTimeBehavior=CONVERT_TO_NULL&useSSL=false&serverTimezone=UTC&allowPublicKeyRetrieval=true");
        config.setDriverClassName("com.mysql.cj.jdbc.Driver");
        return new HikariDataSource(config);
    }

    public static DataSource bootH2File(String proj_path) {
        HikariConfig config = new HikariConfig();
        config.setUsername(USERNAME);
        config.setPassword(PASSWORD);
        config.setJdbcUrl(getH2Url(proj_path));
        config.setDriverClassName("org.h2.Driver");
        return new HikariDataSource(config);

    }

    /**
     * h2 文件路径去掉后缀
     * @return
     */
    private static String pathToFile(String path){
        if (path.endsWith(".fdproj")) {
            return path.split(".fdproj")[0];
        }else{
            return path;
        }
    }

    public static String getH2Url(String path){
        return "jdbc:h2:file:" + pathToFile(path) + ";MODE=MYSQL;TRACE_LEVEL_FILE=0;FILE_LOCK=NO;SCHEMA=" + SCHEMA;
//        return "jdbc:h2:file:" + path + ";MODE=MYSQL;AUTO_SERVER=TRUE;TRACE_LEVEL_FILE=0;FILE_LOCK=NO;SCHEMA=" + SCHEMA;
    }

    public static void createIfNotExists(String dbfile) {

        String url = "jdbc:h2:file:"+pathToFile(dbfile)+";MODE=MYSQL";
//        HikariDataSource dataSource = new HikariDataSource();
//        dataSource.setUsername(USERNAME);
//        dataSource.setJdbcUrl(url);
//        dataSource.setPassword(PASSWORD);

        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(url);
        dataSource.setUser(USERNAME);
        dataSource.setPassword(PASSWORD);

        JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        String sql = "select * from `information_schema`.`SCHEMATA`";
        try {
            List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
            boolean hasFastdemo = false;
            for (Map<String, Object> map : list) {
                Object o = map.get("schema_name");
                if (o != null) {
                    String s = o.toString();
                    if (s.toUpperCase().equals(SCHEMA.toUpperCase())) {
                        hasFastdemo = true;
                        break;
                    }
                }
            }
            if (!hasFastdemo) {
                createDb(dataSource);
            }
        } catch (Exception e) {
            createDb(dataSource);
        }

    }

    private static void createDb(JdbcDataSource dataSource){
        //运行初始化
        try {
            System.out.println("createDb-start");
            InputStream inputStream = new ClassPathResource("obs.sql").getInputStream();
            RunScript.execute(dataSource.getConnection(), new InputStreamReader(inputStream,Charset.forName("UTF-8")));
            System.out.println("createDb-complete");
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
