package com.saoft.fastdemo;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.Subject;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.concurrent.atomic.AtomicInteger;

@Component("ProgressBeanPostProcessor")
public class ProgressBeanPostProcessor implements BeanPostProcessor,ApplicationListener<ContextRefreshedEvent> {

    private Integer total = 240;

    private AtomicInteger count = new AtomicInteger(0);

    private final static Subject<Integer> beans = BehaviorSubject.create();


    @Override
    public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
        count.incrementAndGet();
        beans.onNext(count.get()*100/total);
        return bean;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent applicationEvent) {
        System.out.println("total:");
        System.out.println(count.get());
        beans.onComplete();
    }

    public static Observable<Integer> observe() {
        return beans;
    }
}
