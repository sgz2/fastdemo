package com.saoft.fastdemo.gen;

import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.stereotype.Component;

import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.Map;

@Component("TemplateFtlFactory")
public class TemplateFtlFactory {

    private Configuration configuration;

    public TemplateFtlFactory() {
        init();
    }

    private void init(){
        /*初始化freemarker模板*/
        configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setDefaultEncoding(Charset.forName("UTF-8").name());
        configuration.setTemplateLoader(new StringTemplateLoader());
    }

    public String genStr(Map<String,Object> objectMap,String templateString){
        try {
            Template template = new Template("template", templateString, configuration);
//            Template template = configuration.getTemplate(templateString);
            StringWriter stringBuffer = new StringWriter();
            template.process(objectMap,stringBuffer);
            return stringBuffer.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }
}
