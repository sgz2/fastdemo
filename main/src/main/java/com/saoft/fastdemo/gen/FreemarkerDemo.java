package com.saoft.fastdemo.gen;

import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;

import java.io.IOException;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;

public class FreemarkerDemo {

    public static void main(String[] args) throws IOException, TemplateException {
        /*初始化freemarker模板*/
        Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setDefaultEncoding(Charset.forName("UTF-8").name());

        //这里的ClassTemplateLoader 的basePackagePath  如果是 / 开头就是绝对路径。如果不是/ 开头就是相对于
        //com.saoft.fastdemo.gen.FreemarkerDemo.java 这个类的相对路径，会以com.saoft.fastdemo.gen这个目录开始找
        //FileTemplateLoader
        configuration.setTemplateLoader(new ClassTemplateLoader(FreemarkerDemo.class,"/templates/"));

        //获取模板
        Template template = configuration.getTemplate("ssm/demo.ftl");

        //需要注入的数据
        Map<String, Object> dataMap = new HashMap<>();
        dataMap.put("testKey", "This is key");

        //输出位置 这里用字符串输出，如果要输出文件自行替换
        StringWriter writer = new StringWriter();
        template.process(dataMap, writer);

        System.out.println(writer.getBuffer());
    }
}
