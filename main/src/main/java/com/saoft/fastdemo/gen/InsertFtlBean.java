package com.saoft.fastdemo.gen;

import cn.hutool.core.io.FileUtil;
import com.github.javaparser.Position;
import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import freemarker.cache.ClassTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class InsertFtlBean {

    private Configuration configuration;
    private Map<String,Object> data = new HashMap<>();

    public InsertFtlBean() {
        init();
    }

    private void init(){
        /*初始化freemarker模板*/
        configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
        configuration.setDefaultEncoding(Charset.forName("UTF-8").name());
        configuration.setTemplateLoader(new ClassTemplateLoader(this.getClass(),"/templates/"));
    }

    public Configuration getConfiguration() {
        return configuration;
    }

    public Map<String, Object> getData() {
        return data;
    }

    /*输出*/
    public void out(Map<String,Object> objectMap,String templatePath, Writer out){
        try {
            Template template = configuration.getTemplate(templatePath);
            template.process(objectMap,out);
        } catch (Exception e) {

        }
    }


    public void appendMethodAndImport(String path,String templatePath,MethodData methodData){

        StringWriter writer = new StringWriter();
        //"ssm/dao.ftl"
        this.out(data,templatePath,writer);
        StringBuffer buffer = writer.getBuffer();
        System.out.println(buffer);
        String join = join(path, buffer, methodData);

        if (StringUtils.hasLength(join)) {
            //覆盖原来的文件
            FileUtil.writeUtf8String(join, path);
        }
    }

    public void appendMethodAndImportXml(String path,String templatePath,MethodData methodData){

        StringWriter writer = new StringWriter();
        //"ssm/dao.ftl"
        this.out(data,templatePath,writer);
        StringBuffer buffer = writer.getBuffer();
        System.out.println(buffer);
        String join = joinXml(path, buffer, methodData);

        if (StringUtils.hasLength(join)) {
            //覆盖原来的文件
            FileUtil.writeUtf8String(join, path);
        }

    }


    private String joinXml(String path,StringBuffer buffer,MethodData methodData){
        //拼接字符 todo 目前找不到好的解析xml的方法只能 判断</mapper>
        try {
            StringBuilder sb = new StringBuilder();
            // 源文件
            List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            for (int i = 0; i < lines.size(); i++) {
                String s = lines.get(i);
                //类结束行 position 第一行是1
                if(s.contains("</mapper>")){
                    int i1 = s.indexOf("</mapper>");
                    String start = s.substring(0, i1);
                    String endstr = s.substring(i1, s.length());
                    sb.append(start);
                    sb.append("\n");
                    sb.append(buffer);
                    sb.append("\n");
                    sb.append(endstr);
                    sb.append("\n");
                }else{
                    sb.append(s);
                    sb.append("\n");
                }
            }

            System.out.println(sb);
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    /**
     *
     * @param path
     * @param buffer
     * @param methodData 获取其中的返回值类型，看是否需要加入到import中 todo
     * @return
     */
    private String join(String path,StringBuffer buffer,MethodData methodData){
        //拼接字符
        File javaFile = new File(path);
        try {
            System.out.println(javaFile.exists());
            CompilationUnit cu = StaticJavaParser.parse(javaFile);
            String name1 = FileUtil.getName(javaFile);
            String shortName = name1.split("\\.")[0];

            //加入点的位置
            Position position;
            try {
                Optional<ClassOrInterfaceDeclaration> classByName = cu.getClassByName(shortName);
                Optional<Position> end = classByName.get().getEnd();
                position = end.get();
            } catch (Exception e) {
                Optional<ClassOrInterfaceDeclaration> classByName = cu.getInterfaceByName(shortName);
                Optional<Position> end = classByName.get().getEnd();
                position = end.get();
            }

            StringBuilder sb = new StringBuilder();

            // 源文件
            List<String> lines = Files.readAllLines(Paths.get(path), StandardCharsets.UTF_8);
            for (int i = 0; i < lines.size(); i++) {
                String s = lines.get(i);
                //类结束行 position 第一行是1
                if (i == position.line - 1) {
                    String start = s.substring(0, position.column - 1);
                    String endstr = s.substring(position.column - 1, s.length());
                    sb.append(start);
                    sb.append("\n");
                    sb.append(buffer);
                    sb.append("\n");
                    sb.append(endstr);
                    sb.append("\n");
                }else{
                    sb.append(s);
                    sb.append("\n");
                }
            }

            System.out.println(sb);
            return sb.toString();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
