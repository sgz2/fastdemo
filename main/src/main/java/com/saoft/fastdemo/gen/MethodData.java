package com.saoft.fastdemo.gen;

public class MethodData {

    /**
     * insert update delete select
     */
    private String type;
    private String resultType;
    private String name;
    private String parm;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
        if (resultType == null || resultType.trim().length() == 0) {
            this.resultType = "void";
        }

    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
        if (name == null || name.trim().length() == 0) {
            this.name = "methodName";
        }
    }

    public String getParm() {
        return parm;
    }

    public void setParm(String parm) {
        this.parm = parm;
    }

}
