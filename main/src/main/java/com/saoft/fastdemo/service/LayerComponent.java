package com.saoft.fastdemo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.web.bean.Msg;
import org.springframework.stereotype.Component;

import java.util.List;

@Component("LayerComponent")
public class LayerComponent {


    public List<LayerItem> getLayerItemByLayerId(Integer layerId){
        List<LayerItem> layerItems = new LayerItem().selectList(
                new QueryWrapper<LayerItem>()
                        .orderByAsc(DBConstants.sort_by)
                        .eq(DBConstants.layer_id, layerId)
        );
        return layerItems;
    }

    public Layer getTagLayer(String role,String type,String tag){
        List<Layer> layerList = new Layer().selectList(new QueryWrapper<Layer>()
                .eq(DBConstants.role_code, role)
                .eq(DBConstants.type, type)
                .eq(DBConstants.tag, tag)
        );

        if (layerList == null || layerList.size() == 0) {
            return null;
        }
        //todo 超过一个

        return layerList.get(0);
    }
}
