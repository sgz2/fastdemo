package com.saoft.fastdemo.service.db;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.JTableInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("TableInfoService")
public class TableInfoService {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public List<JTableInfo> getAll(){
        JTableInfo tableInfo = new JTableInfo();
        List<JTableInfo> jTableInfos = tableInfo.selectAll();
        return jTableInfos;
    }

    public List<JTableColumn> tableColumns(String tableName){
        List<JTableColumn> table_name = new JTableColumn()
                .selectList(new QueryWrapper<JTableColumn>()
                        .eq(DBConstants.table_name, tableName));
        return table_name;
    }

    public void tableDelete(String tableName) {
        String sql = "DROP TABLE IF EXISTS `"+tableName+"`";
        jdbcTemplate.execute(sql);

        JTableInfo tableInfo = new JTableInfo();
        tableInfo.delete(new QueryWrapper<JTableInfo>().eq(DBConstants.name, tableName));

        JTableColumn column = new JTableColumn();
        column.delete(new QueryWrapper<JTableColumn>().eq(DBConstants.table_name, tableName));
    }
}
