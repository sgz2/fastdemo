package com.saoft.fastdemo.service.logic.bean;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;

public class IdNameValues {

    private String id;
    private String name;
    private String value;
    private List<IdName> values = new LinkedList<>();


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<IdName> getValues() {
        return values;
    }

    public void setValues(List<IdName> values) {
        this.values = values;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public void clearRepeat(){
        HashSet<IdName> idNames = new HashSet<>(values);
        values.clear();
        values.addAll(idNames);
    }
}
