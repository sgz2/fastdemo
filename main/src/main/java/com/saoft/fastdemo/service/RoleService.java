package com.saoft.fastdemo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.Role;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.outline.view.OutLinePanel;
import com.saoft.fastdemo.ui.outline.view.RolePanel;
import org.springframework.stereotype.Service;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;

@Service("RoleService")
public class RoleService {

    public Role getByLayerId(Integer layerId) {
        Layer layer = new Layer().selectById(layerId);
        return getByCode(layer.getRoleCode());
    }

    public Role getByCode(String roleCode) {
        QueryWrapper<Role> roleQueryWrapper = new QueryWrapper<>();
        roleQueryWrapper.eq(DBConstants.code, roleCode);
        Role role = new Role().selectOne(roleQueryWrapper);
        return role;
    }

    public Role currentRole(){
        OutLinePanel outLinePanel = S.b(OutLinePanel.class);
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        return rolePanel.getRole();
    }

    public RolePanel currentRolePanel(){
        OutLinePanel outLinePanel = S.b(OutLinePanel.class);
        RolePanel rolePanel = (RolePanel) outLinePanel.getRoleTabbedPane().getSelectedComponent();
        return rolePanel;
    }

    public JTree currentControllerTree(){
        return currentRolePanel().getControlerTree();
    }

    public String currentRoleCode(){
        return currentRole().getCode();
    }
}
