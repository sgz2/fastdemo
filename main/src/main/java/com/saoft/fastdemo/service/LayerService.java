package com.saoft.fastdemo.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerGroup;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.outline.model.LayerGroupNode;
import com.saoft.fastdemo.ui.outline.view.OutLinePanel;
import com.saoft.fastdemo.ui.outline.view.RolePanel;
import org.springframework.stereotype.Service;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;

@Service
public class LayerService {

    public Layer getById(Integer id) {
        Layer layer = new Layer().selectById(id);
        return layer;
    }

    public List<Layer> roleMenu(String role){
        List<Layer> layerList = new Layer().selectList(
                new QueryWrapper<Layer>()
                        .eq(DBConstants.type,"menu")
                        .eq(DBConstants.role_code, role));
        return layerList;
    }

    public List<Layer> listByTableName(String tableName) {
        List<Layer> layerList = new Layer().selectList(
                new QueryWrapper<Layer>().eq(DBConstants.table_name, tableName));
        return layerList;
    }


    public List<LayerGroup> roleGroupList() {
        String roleCode = S.b(RoleService.class).currentRoleCode();
        return roleGroupList(roleCode);

    }

    public List<LayerGroup> roleGroupList(String roleCode) {

        List<LayerGroup> layerGroupList = new LayerGroup()
                .selectList(new QueryWrapper<LayerGroup>()
                        .eq(DBConstants.role_code, roleCode));
        return layerGroupList;

    }

    public List<Layer> roleLayerList() {
        String roleCode = S.b(RoleService.class).currentRoleCode();
        return roleLayerList(roleCode);
    }


    public List<Layer> roleLayerList( String roleCode) {

        List<Layer> layerList = new Layer().selectList(
                new QueryWrapper<Layer>()
                        .eq(DBConstants.role_code, roleCode));
        return layerList;
    }

    public Layer getCurrentLayer(){
        OutLinePanel outLinePanel = S.b(OutLinePanel.class);
        JTabbedPane tabbedPane = outLinePanel.getRoleTabbedPane();
        RolePanel rolePanel = (RolePanel) tabbedPane.getSelectedComponent();
        JTree controlerTree = rolePanel.getControlerTree();
        DefaultMutableTreeNode node = (DefaultMutableTreeNode) controlerTree.getLastSelectedPathComponent();
        if(node == null){
            return null;
        }
        if(node.getUserObject() instanceof Layer) {
            Layer userObject = (Layer) node.getUserObject();
            return userObject;
        }
        return null;
    }

    public void deleteLayer(Object o){
        if (o instanceof Layer) {
            Layer layer = (Layer) o;
            layer.deleteById();
            deleteSubData(layer);
        }else{
            LayerGroupNode cnode = (LayerGroupNode) o;
            deleteGroup(cnode);
        }
    }

    private void deleteSubData(Layer layer){
        new LayerItem().delete(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layer.getId()));
        new LayerLink().delete(new QueryWrapper<LayerLink>().eq(DBConstants.layer_id, layer.getId()));
    }

    private void deleteGroup(LayerGroupNode node){
        LayerGroup layerGroup = node.getLayerGroup();
        layerGroup.deleteById();

        for (Object o : node.getChilds()) {
            deleteLayer(o);
        }
    }

    public void newGroup(LayerGroup layerGroup, List<Layer> layers) {
        layerGroup.insert();
        Integer id = layerGroup.getId();
        if (layers != null) {
            for (Layer layer : layers) {
                layer.setGroupId(id);
                layer.updateById();
            }
        }
    }

    public void updateGroupName(LayerGroup layer) {
        layer.updateById();
    }

    public void updateLayer(Layer currentLayer) {
        currentLayer.updateById();
    }

}
