package com.saoft.fastdemo.service.logic.bean;

import java.util.Objects;

public class IdName {

    private String id;
    private String name;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IdName idName = (IdName) o;
        return Objects.equals(id, idName.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
