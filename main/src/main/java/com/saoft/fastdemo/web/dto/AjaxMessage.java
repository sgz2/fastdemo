package com.saoft.fastdemo.web.dto;

public class AjaxMessage {

    private int code;
    private String msg;
    private Object data;

    public static AjaxMessage success(){
        AjaxMessage ajaxMessage = new AjaxMessage();
        ajaxMessage.setCode(0);
        ajaxMessage.setMsg("成功");
        return ajaxMessage;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
