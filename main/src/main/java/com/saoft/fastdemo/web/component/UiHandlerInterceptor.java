package com.saoft.fastdemo.web.component;

import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Role;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.web.SessionUtil;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;


public class UiHandlerInterceptor implements HandlerInterceptor {

    private String[] whiteList = new String[]{"login","register"};

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {
        if (modelAndView != null) {
            String view = modelAndView.getViewName();
            if(!view.startsWith("redirect:")){
                //
                String ui = getUiByRole(view);
                if(ui.startsWith("redirect:")){
                    modelAndView.setViewName(ui);
                }else if (view.endsWith(".html")) {
                    modelAndView.setViewName(view);
                } else{
                    modelAndView.setViewName(ui + view);
                }

            }
        }
    }

    private Map<String,Role> roleMap(){
        List<Role> roles = new Role().selectAll();
        Map<String, Role> code = MapUtil.listToMap(roles, DBConstants.code);
        return code;
    }

    /**
     * 不用登录的路径
     * @param view
     * @return
     */
    private boolean whiteList(String view){
        for (String s : whiteList) {
            if (view.startsWith(s)) {
                return true;
            }
        }

        return false;
    }
    /**
     * 获取当前角色的模板
     * @return
     */
    private String getUiByRole(String viewName){
        Map<String, Role> stringRoleMap = roleMap();
        String role = SessionUtil.checkRole();
        Role roleRlease = null;
        //未登录
        if (role == null || "".equals(role.trim())) {
            //查验是否有游客角色
            Role role2 = stringRoleMap.get("visitor");
            //有游客角色
            if (role2 != null) {
                //游客
                role = "visitor";
                roleRlease = role2;
            }else{
                //没有游客角色 又没有登录
                if(whiteList(viewName)){
                    for (Role role3 : stringRoleMap.values()) {
                        System.out.println(roleRlease);
                        roleRlease = role3;
                    }
                }else{
                    return "redirect:/login";
                }
            }
        }else{
            roleRlease = stringRoleMap.get(role);
        }

        //标记当前页面属于哪个角色配置的
        SessionUtil.setAttribute("fastdemo_roleFlag", role);
        return roleRlease.getTemplateCode()+"/";
    }
}
