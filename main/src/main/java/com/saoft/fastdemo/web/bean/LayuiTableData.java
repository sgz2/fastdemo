package com.saoft.fastdemo.web.bean;

import java.util.List;

public class LayuiTableData {

    private int code;
    private int count;
    private List data;
    private String msg;

    private List rows;
    private int total;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getTotal() {
        return count;
    }

    public List getRows() {
        return data;
    }
}
