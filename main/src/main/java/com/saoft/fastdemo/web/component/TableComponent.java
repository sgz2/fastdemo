package com.saoft.fastdemo.web.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.*;

import com.saoft.fastdemo.service.LayerService;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.web.SessionUtil;
import com.saoft.fastdemo.web.dto.LayerLinkDTO;
import com.saoft.fastdemo.web.dto.TableDTO;
import com.saoft.fastdemo.web.util.KvTypeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Component("TableComponent")
public class TableComponent {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CommonComponent commonComponent;

//    @Autowired
//    SqlComponent sqlComponent;

    @Autowired
    LayerService layerService;


    public void setTable(Integer layerId,boolean filterServerData){
        TableDTO table = getTable(layerId);
        setTable(table, filterServerData);
    }

    public void setTable(TableDTO table,boolean filterServerData){
        SessionUtil.setAttribute("tableInfo", table.getTableInfo());
        SessionUtil.setAttribute("columnList", table.getColumnList());
        SessionUtil.setAttribute("layer",table.getLayer());

        if (filterServerData) {
            List rlt = new ArrayList();

            List columnList = table.getColumnList();
            for (Object o : columnList) {
                if (o instanceof Map) {
                    Object o1 = ((Map) o).get("type");

                    if (o1 == null || o1.toString().length() == 0) {
                        rlt.add(o);
                    }else{
                        if (!"from-server".equals(o1.toString())) {
                            rlt.add(o);
                        }
                    }
                } else if (o instanceof LayerItem) {
                    String type = ((LayerItem) o).getType();
                    if (!"from-server".equals(type)) {
                        rlt.add(o);
                    }
                }
            }
            SessionUtil.setAttribute("columnList", rlt);
        }
    }


    public TableDTO getTable(Integer layerId){
        TableDTO tableDTO = new TableDTO();
        Layer layer = layerService.getById(layerId);
        /*table*/
        JTableInfo tableInfo = new JTableInfo();
        tableInfo = tableInfo.selectOne(
                new QueryWrapper<JTableInfo>()
                        .eq(DBConstants.name, layer.getTableName()));

        String pk = commonComponent.getPk(layer.getTableName());
        SessionUtil.setAttribute("fastdemo_pk",pk);

        /*所有列*/
        String type = layer.getType();
        if ("list".equals(type)) {
            buildList(tableDTO, layer);
        } else if("create".equals(type)){
            buildCreate(tableDTO, layer);
        } else if("update".equals(type)){
            buildUpdate(tableDTO, layer);
        }else if("detail".equals(type)){
            buildDetail(tableDTO, layer);
        }
        SessionUtil.setAttribute("fastdemo_layerId", layerId);
        tableDTO.setLayer(layer);
        tableDTO.setTableInfo(tableInfo);
        return tableDTO;
    }

    public String getWhereByQuery(Integer layerId,List paremeterList){
        List<LayerItem> layerItems = new LayerItem()
                .selectList(new QueryWrapper<LayerItem>()
                        .eq(DBConstants.layer_id, layerId)
                        .isNotNull(DBConstants.query)
                );

        StringBuffer where = new StringBuffer();

        HttpServletRequest request = SessionUtil.getHttpServletRequest();

        for (LayerItem layerItem : layerItems) {
            if (StringUtils.hasLength(layerItem.getQuery())) {
                String parameter = request.getParameter(layerItem.getField());
                if(StringUtils.hasLength(parameter)){
                    where.append("`" + layerItem.getField() + "` ");
                    where.append(layerItem.getQuery());
                    where.append("? and ");
                    if ("like".equals(layerItem.getQuery())) {
                        paremeterList.add("%"+parameter+"%");
                    }else{
                        paremeterList.add(parameter);
                    }

                }
            }
        }
        if (where.length() == 0) {
            return "";
        }else{
            return " where " +where.substring(0, where.length() - 4);
        }
    }

    /**
     * 别名返回结果，多用于无序结构展示
     * @param layerId
     * @return
     */
    public String getListSqlAlias(Integer layerId){
        List<LayerItem> layer_id = new LayerItem().selectList(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
        StringBuffer columns = new StringBuffer();
        for (LayerItem layerItem : layer_id) {
            columns.append("`");
            columns.append(layerItem.getField());
            columns.append("`");

            if(StringUtils.hasLength(layerItem.getAlias())){
                columns.append(" as `");
                columns.append(layerItem.getAlias());
                columns.append("`");
            }else{
                columns.append(" as `");
                columns.append(layerItem.getField());
                columns.append("`");
            }
            columns.append(",");
        }
        return columns.substring(0, columns.length() - 1);
    }

    /**
     * 当前层的所有别名
     * @param layerId
     * @return
     */
    public List<String> getCovertKey(Integer layerId){
        List<LayerItem> layer_id = new LayerItem().selectList(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
        List<String> list = new ArrayList<>();
        for (LayerItem layerItem : layer_id) {
            if(StringUtils.hasLength(layerItem.getAlias())){
                list.add(layerItem.getAlias());
            }else{
                list.add(layerItem.getField());
            }
        }
        return list;
    }

    /**
     * 由于jdbc返回结果会被转换成忽略大小写的map，转成json返回时又默认是大写所以需要装换
     * @param data
     * @param covertKey
     * @return
     */
    public List<Map<String,Object>> covertSelectMap(List<Map<String,Object>> data,List<String> covertKey){

        List<Map<String, Object>> collect = data.stream().map(objectMap -> {
            Map<String, Object> map = new HashMap<>();
            covertKey.forEach(key -> map.put(key, objectMap.get(key)));
            return map;
        }).collect(Collectors.toList());

        return collect;
    }

    /**
     * 设置多对一的对象
     * @param tabelName 用于检查外键设置
     * @param orgManyPointMap
     */
    public void setRelationManyToOne(String tabelName,Map<String,Object> orgManyPointMap){
        List<TableRelation> ftable_id = new TableRelation()
                .selectList(new QueryWrapper<TableRelation>()
                        .eq(DBConstants.type, "m2o")
                        .eq(DBConstants.ftable_id, tabelName)
                );
        if(!CollectionUtils.isEmpty(ftable_id)){
            for (TableRelation relation : ftable_id) {
                //todo 别名未生效
                String sql = "select * from `%s` where `%s`=?";
                Object idValue = orgManyPointMap.get(relation.getFcolumnId());
                if (idValue != null) {
                    String releaseSql = String.format(sql, relation.getTtableId(),relation.getTcolumnId());
                    Map<String, Object> reslut = jdbcTemplate.queryForMap(releaseSql, idValue);
                    orgManyPointMap.put(relation.getName(), reslut);
                }
            }

        }
    }

    /**
     * 组装插入的sql
     * @param table
     * @param parmeterList
     * @return
     */
    public String insertSql(TableDTO table,List<Object> parmeterList){
        Layer layer = table.getLayer();
        String sql = "insert into `" + layer.getTableName() + "` (";
        String columnStr = "";
        String what = "";
        List<LayerItem> columnList = table.getOrgColumnList();

        Map<String, Object> insertDataMap = commonComponent.getInsertDataMap(columnList,null);

        for (String s : insertDataMap.keySet()) {
            columnStr += ",`"+s+"`";
            what += ",?";
            parmeterList.add(insertDataMap.get(s));
        }

        sql += columnStr.substring(1);
        sql += ")values(";
        sql += what.substring(1) + ")";
        return sql;
    }

    /**
     * 构建公共 type 的解析
     * @param tableDTO
     * @param layer
     */
    private void buildCommonType(TableDTO tableDTO,Layer layer){
        List<LayerItem> layerCreates = new LayerItem().selectList(
                new QueryWrapper<LayerItem>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .orderByAsc(DBConstants.sort_by)
        );

        List<Map<String,Object>> list = new ArrayList<>();

        layerCreates.forEach(layerCreate -> {
            Map<String, Object> map = MapUtil.getFieldsByAttribute(layerCreate);
            //
            String type = layerCreate.getType();
            if ("radio".equals(type) || "select".equals(type)) {
                KvTypeUtil kvTypeUtil = new KvTypeUtil(layerCreate.getTypeKvlist());
                Map<String, Object> stringMap = kvTypeUtil.getMap();
                map.put("kvMap", stringMap);
            }
            if ("fk-select".equals(type)) {
                fkSelect(map,layerCreate.getTypeKvlist());
            }
            list.add(map);
        });

        tableDTO.setColumnList(list);
        tableDTO.setOrgColumnList(layerCreates);
    }

    private void buildCreate(TableDTO tableDTO,Layer layer){

        buildCommonType(tableDTO, layer);

//            返回到列表的连接
        List<LayerLink> linkList = new LayerLink().selectList(
                new QueryWrapper<LayerLink>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .eq(DBConstants.tag, "return_to_list"));
        if(linkList!=null && linkList.size()>0){
            LayerLink layerLink = linkList.get(0);
            SessionUtil.setAttribute("fastdemo_return_to_list_layerId", layerLink.getLinkLayerId());
        }
    }


    private void buildDetail(TableDTO tableDTO, Layer layer) {
        buildCommonType(tableDTO, layer);

        //订单相关layer
        List<LayerLink> linkList = new LayerLink().selectList(
                new QueryWrapper<LayerLink>()
                        .eq(DBConstants.layer_id, layer.getId()));

        Map<String,LayerLink> orderMap = new HashMap<>();
        linkList.stream().forEach(layerLink -> orderMap.put(layerLink.getTag(), layerLink));


        SessionUtil.setAttribute("fastdemo_tagList", linkList);
        SessionUtil.setAttribute("fastdemo_tagMap", orderMap);
    }

    private void buildUpdate(TableDTO tableDTO,Layer layer){
        buildCommonType(tableDTO, layer);

//            返回到列表的连接
        List<LayerLink> linkList = new LayerLink().selectList(
                new QueryWrapper<LayerLink>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .eq(DBConstants.tag, "return_to_list"));
        if (linkList != null && linkList.size() > 0) {
            LayerLink layerLink = linkList.get(0);
            SessionUtil.setAttribute("fastdemo_return_to_list_layerId", layerLink.getLinkLayerId());
        }
    }

    /**
     * 外键下拉选择
     * @param map
     * @param kvlist
     */
    private void fkSelect(Map<String,Object> map,String kvlist){

        //todo where
        //todo where
        //todo where

        KvTypeUtil kvTypeUtil = new KvTypeUtil(kvlist);

        String sql = "select * from `%s` ";
        List<Map<String, Object>> mapList = jdbcTemplate.queryForList(String.format(sql, kvTypeUtil.getTableName()));
        map.put("kvList", mapList);

        //key value 的建
        if (kvTypeUtil.getKey() == null) {
            map.put("kvKey", "id");
        }else{
            map.put("kvKey", kvTypeUtil.getKey());
        }
        if (kvTypeUtil.getValue() == null) {
            map.put("kvValue", "name");
        }else{
            map.put("kvValue", kvTypeUtil.getValue());
        }

    }

    private void buildList(TableDTO tableDTO,Layer layer){
        buildCommonType(tableDTO, layer);

        //设置新增链接
        List<LayerLink> layerLinks = new LayerLink()
                .selectList(new QueryWrapper<LayerLink>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .like(DBConstants.tag,"link%")
                );
        List<LayerLinkDTO> collect = layerLinks.stream().map(layerLink -> {
            LayerLinkDTO linkDTO = new LayerLinkDTO();
            linkDTO.setLayerLink(layerLink);
            return linkDTO;
        }).collect(Collectors.toList());
        SessionUtil.setAttribute("fastdemo_links", collect);

        Map<String, LayerLink> linkMap = MapUtil.listToMap(layerLinks, DBConstants.tag);
        SessionUtil.setAttribute("fastdemo_linkMap", linkMap);

        //行内连接
        List<LayerLink> rowLinks = new LayerLink()
                .selectList(new QueryWrapper<LayerLink>()
                        .eq(DBConstants.layer_id, layer.getId())
                        .like(DBConstants.tag,"row%")
                );

        Map<String,LayerLink> tagMap = new HashMap<>();

        List<LayerLinkDTO> rowLink = rowLinks.stream().map(layerLink -> {
            tagMap.put(layerLink.getTag(), layerLink);

            LayerLinkDTO linkDTO = new LayerLinkDTO();
            linkDTO.setLayerLink(layerLink);
            return linkDTO;
        }).collect(Collectors.toList());
        SessionUtil.setAttribute("fastdemo_rowLinks", rowLink);
        SessionUtil.setAttribute("fastdemo_tagMap", tagMap);

        //查询条件
        List<Map<String, Object>> columnList = tableDTO.getColumnList();
        List<Map<String, Object>> collect1 = columnList.stream()
                .filter(item -> !ObjectUtils.isEmpty(item.get("query")))
                .collect(Collectors.toList());
        SessionUtil.setAttribute("fastdemo_queryList", collect1);

    }
}
