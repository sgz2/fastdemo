package com.saoft.fastdemo.web.util;

import cn.hutool.json.JSONUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * 解析kvList
 */
public class KvTypeUtil {

    private String kvlist;
    private Map<String,Object> map = new HashMap<>();

    public KvTypeUtil(String kvlist) {
        this.kvlist = kvlist;
        initMap();
    }

    private void initMap(){
        try {
            if(kvlist!=null){
                this.map= JSONUtil.parseObj(kvlist);
            }
        } catch (Exception e) {

        }
    }

    /**
     * 服务器填充的数据类型
     * @return
     */
    public String getServerData(){
        Object o = map.get("type");
        if (o == null) {
            return null;
        }
        return o.toString();
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public String getKey(){
        Object o = map.get("key");
        if (o == null) {
            return null;
        }
        return o.toString();
    }

    public String getValue(){
        Object o = map.get("value");
        if (o == null) {
            return null;
        }
        return o.toString();
    }

    public String getTableName(){
        Object o = map.get("t");
        if (o == null) {
            return null;
        }
        return o.toString();
    }
}
