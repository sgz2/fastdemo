package com.saoft.fastdemo.web;


import com.saoft.fastdemo.web.bean.LoginUser;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

public class SessionUtil {

    public static final String LOGIN_USER = "loginUser";
    public static final String LOGIN_ROLE = "loginRole";

    public static void setAttribute(String key,Object value){
        HttpServletRequest request = getHttpServletRequest();
        request.setAttribute(key, value);
    }

    public static LoginUser getUser(){
        HttpServletRequest request = getHttpServletRequest();
        Object attribute = request.getSession().getAttribute(LOGIN_USER);
        if (attribute == null) {
            return null;
        }
        return (LoginUser) attribute;
    }

    public static String getRole(){
        HttpServletRequest request = getHttpServletRequest();
        Object attribute = request.getSession().getAttribute(LOGIN_ROLE);
        if (attribute == null) {
            return null;
        }
        return (String) attribute;
    }

    public static HttpServletRequest getHttpServletRequest() {
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder
                .getRequestAttributes()).getRequest();
        return request;

    }

    public static String checkRole() {
        HttpServletRequest request = getHttpServletRequest();
        Object role = request.getSession().getAttribute(LOGIN_ROLE);
        if (role != null && StringUtils.hasLength(role.toString())) {
            return role.toString();
        }
        String role1 = request.getParameter("role");
        return role1;
    }

    public static String getIpAddress(HttpServletRequest request) {
                 String ip = request.getHeader("x-forwarded-for");
                 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                         ip = request.getHeader("Proxy-Client-IP");
                     }
                 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                         ip = request.getHeader("WL-Proxy-Client-IP");
                    }
                 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                         ip = request.getHeader("HTTP_CLIENT_IP");
                     }
                 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                         ip = request.getHeader("HTTP_X_FORWARDED_FOR");
                     }
                 if (ip == null || ip.length() == 0 || "unknown".equalsIgnoreCase(ip)) {
                         ip = request.getRemoteAddr();
                     }
                 return ip;
    }

}
