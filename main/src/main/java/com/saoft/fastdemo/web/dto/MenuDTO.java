package com.saoft.fastdemo.web.dto;

import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.db.entity.Menu;

import java.util.LinkedList;

public class MenuDTO {

    @KeepThis
    private Menu menu;
    @KeepThis
    private LinkedList<MenuDTO> childs = new LinkedList<>();
    @KeepThis
    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }
    @KeepThis
    public LinkedList<MenuDTO> getChilds() {
        return childs;
    }

    public void setChilds(LinkedList<MenuDTO> childs) {
        this.childs = childs;
    }
}
