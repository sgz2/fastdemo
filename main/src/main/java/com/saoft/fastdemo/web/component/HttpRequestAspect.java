package com.saoft.fastdemo.web.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.db.entity.Menu;
import com.saoft.fastdemo.service.LayerService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.ui.db.view.menu.insertmethod.PrefUtil;
import com.saoft.fastdemo.web.SessionUtil;
import com.saoft.fastdemo.web.dto.MenuDTO;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Aspect
@Component("HttpRequestAspect")
public class HttpRequestAspect {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TableComponent tableComponent;


    @Autowired
    CommonComponent commonComponent;

    public HttpRequestAspect() {
        System.out.println("Aop-----------------------");
    }

    @KeepThis
    @Pointcut(value = "@annotation(org.springframework.web.bind.annotation.GetMapping)")
    /*切点签名*/
    public void print() {
        System.out.println("test");
    }

    /*@After注解表示在方法执行之后执行*/
    @After("print()")
    public void after() {
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        //标题
        String fullname = PrefUtil.get(PrefUtil.title_full_name);
        String shortname = PrefUtil.get(PrefUtil.title_short_name);
        String footer = PrefUtil.get(PrefUtil.title_footer);
        HashMap<String, Object> map = new HashMap<>();
        map.put("fullname", fullname);
        map.put("shortname", shortname);
        map.put("footer", footer);
        request.setAttribute("fastdemo_name",map);

        //所有菜单 role
        String role = SessionUtil.checkRole();
        if (role == null) {
            role = "visitor";
        }
        Menu menu = new Menu();
        List<Menu> menus = menu.selectList(
                new QueryWrapper<Menu>()
                        .eq(DBConstants.role, role)
                        .orderByAsc(DBConstants.sort_by)
        );

        List<MenuDTO> root = new LinkedList<>();
        Map<String,MenuDTO> all = new HashMap<>();
        for (Menu menu1 : menus) {
            MenuDTO dto = new MenuDTO();
            all.put(menu1.getCode(), dto);
            dto.setMenu(menu1);
            if(!StringUtils.hasLength(menu1.getParentCode())){
                root.add(dto);
            }
        }
        for (Menu menu1 : menus) {
            if(StringUtils.hasLength(menu1.getParentCode())){
                MenuDTO dto = all.get(menu1.getParentCode());
                MenuDTO subDto = all.get(menu1.getCode());
                dto.getChilds().add(subDto);
            }
        }

        request.setAttribute("fastdemoMenuList", root);

        //菜单连接的数据
        List<Layer> layerList = S.b(LayerService.class).roleMenu(role);
        if (layerList != null && layerList.size() > 0) {
            Layer mylayer = layerList.get(0);

            LayerLink layerLInk = commonComponent.getLinkLayerIdByTag(mylayer,"include_data");
            if (layerLInk == null) {
                return;
            }
            Integer layerId = layerLInk.getLinkLayerId();
            Layer layer = S.b(LayerService.class).getById(layerId);

            //查询条件
            List<Object> objectList = new LinkedList<>();
            String whereByQuery = tableComponent.getWhereByQuery(layerId, objectList);
            //别名拼接
            String alias = tableComponent.getListSqlAlias(layerId);
            String sql = "select "+alias+" from `" + layer.getTableName() + "` "+whereByQuery+" limit 1000";

            List<Map<String, Object>> list = jdbcTemplate.queryForList(sql,objectList.toArray());

            List<String> covertKey = tableComponent.getCovertKey(layerId);
            List<Map<String, Object>> list1 = tableComponent.covertSelectMap(list, covertKey);

            request.setAttribute("fastdemoMenuInclude", list1);


            LayerLink data_link = commonComponent.getLinkLayerIdByTag(layer, "data_link");
            request.setAttribute("fastdemoMenuIncludeLink", data_link);
        }

    }

}
