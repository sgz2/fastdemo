package com.saoft.fastdemo.web;

import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;

import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.db.entity.Role;
import com.saoft.fastdemo.service.RoleService;
import com.saoft.fastdemo.system.S;
import com.saoft.fastdemo.web.bean.LayuiTableData;
import com.saoft.fastdemo.web.bean.Msg;
import com.saoft.fastdemo.web.component.CommonComponent;
import com.saoft.fastdemo.web.component.TableComponent;
import com.saoft.fastdemo.web.dto.TableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.*;

@Controller("CrudController")
public class CrudController {

    @Autowired
    JdbcTemplate template;

    @Autowired
    TableComponent tableComponent;

    @Autowired
    CommonComponent commonComponent;

    @GetMapping("layer/{fastdemo_layerId}")
    public String layer(@PathVariable("fastdemo_layerId")Integer layerId,
                        HttpServletRequest request,
                        RedirectAttributes model){

        //判断登录
        if (!hasLogin(layerId)) {
            return loginString(layerId);
        }

        Layer layer = new Layer().selectById(layerId);
        Map<String, String> map = new HashMap<>();
        map.put("create", "add");
        map.put("list", "list");
        map.put("detail", "detail");
        map.put("index", "index");
        //todo other

        LayerLink accept_from_row = commonComponent.getLinkLayerIdByTag(layer, "accept_from_row");
        if (accept_from_row != null) {
            model.addAttribute(accept_from_row.getParameters(), request.getParameter(accept_from_row.getParameters()));
        }

        String key = map.get(layer.getType());
        return "redirect:/" + key + "/" + layerId;
    }

    /**
     * 列表页
     * @return
     */
    @GetMapping("list/{fastdemo_layerId}")
    public String list(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            return loginString(layerId);
        }
        //设置配置信息
        tableComponent.setTable(layerId,false);

        /**
         * 是否需要注入数据
         * 门户模式使用
         * 方便jsp代码同步到fastdemoadmin
         */
        if (true) {
            syncData(layerId);
        }


        return "list";
    }

    /**
     * 在getList 中同步数据,                                       不半年报你 美女
     */
    private void syncData(Integer layerId){
        Layer layer = new Layer().selectById(layerId);

        //查询条件
        List<Object> objectList = new LinkedList<>();
        String whereByQuery = tableComponent.getWhereByQuery(layerId, objectList);
        //别名拼接
        String alias = tableComponent.getListSqlAlias(layerId);
        String sql = "select "+alias+" from `" + layer.getTableName() + "` "+whereByQuery+getLimit();
        String countSql = "select count(*) total from `" + layer.getTableName() + "` "+whereByQuery;

        List<Map<String, Object>> list = template.queryForList(sql,objectList.toArray());

        List<String> covertKey = tableComponent.getCovertKey(layerId);
        List<Map<String, Object>> list1 = tableComponent.covertSelectMap(list, covertKey);

        //关联外键
        for (Map<String, Object> map : list1) {
            tableComponent.setRelationManyToOne(layer.getTableName(),map);
        }

        //统计总数
        Map<String, Object> map = template.queryForMap(countSql,objectList.toArray());
        String total = map.get("total").toString();
        int anInt = Integer.parseInt(total);

        String currentPage = getCurrentPage();

        Map<String, Object> pageData = new HashMap<>();
        pageData.put("rows", list1);
        pageData.put("total", anInt);
        pageData.put("currentPage", currentPage);

        SessionUtil.setAttribute("pageData",pageData);
    }

    @ResponseBody
    @PostMapping("list/{fastdemo_layerId}")
    public Object listData(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            String loginString = loginString(layerId,false);
            return Msg.error().setMsg("not login").setData(loginString);
        }
        Layer layer = new Layer().selectById(layerId);

        //查询条件
        List<Object> objectList = new LinkedList<>();
        String whereByQuery = tableComponent.getWhereByQuery(layerId, objectList);
        //别名拼接
        String alias = tableComponent.getListSqlAlias(layerId);
        String sql = "select "+alias+" from `" + layer.getTableName() + "` "+whereByQuery+getLimit();
        String countSql = "select count(*) total from `" + layer.getTableName() + "` "+whereByQuery;

        List<Map<String, Object>> list = template.queryForList(sql,objectList.toArray());

        List<String> covertKey = tableComponent.getCovertKey(layerId);
        List<Map<String, Object>> list1 = tableComponent.covertSelectMap(list, covertKey);

        //关联外键
        for (Map<String, Object> map : list1) {
            tableComponent.setRelationManyToOne(layer.getTableName(),map);
        }

        //统计总数
        Map<String, Object> map = template.queryForMap(countSql,objectList.toArray());
        String total = map.get("total").toString();
        int anInt = Integer.parseInt(total);

        LayuiTableData data = new LayuiTableData();
        data.setCode(0);
        data.setCount(anInt);
        data.setData(list1);
        data.setMsg("success");

        return data;
    }

    /**
     * 当前页码
     * @return
     */
    private String getCurrentPage(){
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        Object page = request.getParameter("page");
        if (page != null && page.toString().length() > 0) {
            return page.toString();
        }
        return "1";
    }

    /**
     * 构建分页limit
     * @return
     */
    private String getLimit(){
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        Integer limitInt = 10;
        Integer offsetInt = 0;

        Object limit = request.getParameter("limit");
        if (limit != null && limit.toString().length() > 0) {
            limitInt = Integer.parseInt(limit.toString());
        }

        Object page = request.getParameter("page");
        if (page != null && page.toString().length() > 0) {
            int pageInt = Integer.parseInt(page.toString());
            offsetInt = (pageInt - 1) * limitInt;
        }

        Object offset = request.getParameter("offset");
        if (offset != null && offset.toString().length() > 0) {
            offsetInt = Integer.parseInt(offset.toString());
        }
        return String.format(" limit %d,%d", offsetInt,limitInt);
    }

    @GetMapping("add/{fastdemo_layerId}")
    public String add(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            return loginString(layerId);
        }

        tableComponent.setTable(layerId,true);
        return "add";
    }


    @ResponseBody
    @PostMapping("add/{fastdemo_layerId}")
    public Msg addData(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            String loginString = loginString(layerId,false);
            return Msg.error().setMsg("not login").setData(loginString);
        }
        TableDTO table = tableComponent.getTable(layerId);
        List<Object> parmeterList = new LinkedList<>();
        String sql = tableComponent.insertSql(table, parmeterList);

        template.update(sql, parmeterList.toArray());
        return Msg.success();
    }

    @GetMapping("edit/{fastdemo_layerId}")
    public String edit(@PathVariable("fastdemo_layerId")Integer layerId,String oid){
        TableDTO table = tableComponent.getTable(layerId);
        tableComponent.setTable(table,true);
        //主键
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        String pk = commonComponent.getPk(table.getLayer().getTableName());
        String sql = "select * from `" + table.getLayer().getTableName() + "` where `" + pk + "`=?";
        Map<String, Object> map = template.queryForMap(sql, oid);
        SessionUtil.setAttribute("obj", map);
        return "edit";
    }

    @GetMapping("detail/{fastdemo_layerId}")
    public String detail(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            return loginString(layerId);
        }

        //别名拼接
        String alias = tableComponent.getListSqlAlias(layerId);

        TableDTO table = tableComponent.getTable(layerId);
        tableComponent.setTable(table,false);
        //主键
        String pk = commonComponent.getPk(table.getLayer().getTableName());

        String oid = commonComponent.getParameter();

        String sql = "select "+alias+" from `" + table.getLayer().getTableName() + "` where `" + pk + "`=?";
        Map<String, Object> map = template.queryForMap(sql, oid);
        SessionUtil.setAttribute("obj", map);
        return "detail";
    }

    @ResponseBody
    @PostMapping("edit/{fastdemo_layerId}")
    public Msg editData(@PathVariable("fastdemo_layerId")Integer layerId){
        if (!hasLogin(layerId)) {
            String loginString = loginString(layerId,false);
            return Msg.error().setMsg("not login").setData(loginString);
        }

        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        TableDTO table = tableComponent.getTable(layerId);

        String sql = "update `" + table.getLayer().getTableName() + "` set ";
        String columnStr = "";
        List<LayerItem> columnList = table.getOrgColumnList();

        Map<String, Object> insertDataMap = commonComponent.getInsertDataMap(columnList, null);

        List<Object> parmeterList = new LinkedList<>();
        for (String s : insertDataMap.keySet()) {
            columnStr += "`" + s + "`=?,";
            parmeterList.add(insertDataMap.get(s));
        }

        String pk = commonComponent.getPk(table.getLayer().getTableName());
        sql += columnStr.substring(0,columnStr.length()-1);
        sql += " where `" + pk + "`='" + request.getParameter(pk) + "'";
        template.update(sql, parmeterList.toArray());
        return Msg.success();
    }


    @ResponseBody
    @PostMapping("delete/{fastdemo_layerId}")
    public Msg deleteData(@PathVariable("fastdemo_layerId")Integer layerId,String oid){
        if (!hasLogin(layerId)) {
            String loginString = loginString(layerId,false);
            return Msg.error().setMsg("not login").setData(loginString);
        }

        TableDTO table = tableComponent.getTable(layerId);

        String pk = commonComponent.getPk(table.getLayer().getTableName());

        String sql = "delete from `" + table.getLayer().getTableName() + "` where `" + pk + "`=?";
        template.update(sql,oid);
        return Msg.success();
    }



    /**
     * 重定向登录页面
     * @param layerId
     * @return
     */
    private String loginString(Integer layerId,boolean redirect){
        Layer layer = new Layer().selectById(layerId);
        if(redirect){
            String login = "redirect:/login?role=" + layer.getRoleCode();
            return login;
        }else{
            String login = "/login?role=" + layer.getRoleCode();
            return login;
        }

    }

    private String loginString(Integer layerId){
        return loginString(layerId, true);
    }

    /**
     * 检查用户调用url时 是否登录
     * @param layerId
     * @return
     */
    private boolean hasLogin(Integer layerId){
        Layer layer = new Layer().selectById(layerId);
        String roleCode = layer.getRoleCode();
        String role = SessionUtil.checkRole();
        if(StringUtils.hasLength(roleCode)){
            if(!"visitor".equals(roleCode)){
                if (roleCode.equals(role)) {
                    return true;
                }
            }else{
                return true;
            }
        }
        return false;


    }
}
