package com.saoft.fastdemo.web.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * 不依赖数据
 */
@Component("SqlComponent")
public class SqlComponent {

    @Autowired
    JdbcTemplate jdbcTemplate;

    /**
     * 修改数据
     * @param tableName
     * @param insertDataMap
     * @param pk
     * @return
     */
    public int executeUpdateSql(String tableName, Map<String,Object> insertDataMap, String pk){
        String sql = "update " + tableName + " set ";

        String columnStr = "";
        List<Object> parmeterList = new LinkedList<>();
        for (String s : insertDataMap.keySet()) {
            columnStr += ",`" + s + "`=?";
            Object o = insertDataMap.get(s);
            if (o instanceof String) {
                if (StringUtils.hasLength((String) o)) {
                    parmeterList.add(o);
                } else {
                    parmeterList.add(null);
                }
            } else {
                parmeterList.add(insertDataMap.get(s));
            }

        }
        sql += columnStr.substring(1);

        sql += " where " + pk + "=?";
        parmeterList.add(insertDataMap.get(pk));

        return jdbcTemplate.update(sql, parmeterList.toArray());
    }


    public int executeUpdateSqlWithWhere(String tableName, Map<String, Object> dataMap, Map<String, Object> whereMap) {
        String sql = "update " + tableName + " set ";

        String columnStr = "";
        List<Object> parmeterList = new LinkedList<>();
        for (String s : dataMap.keySet()) {
            columnStr += ",`" + s + "`=?";
            Object o = dataMap.get(s);
            if (o instanceof String) {
                if (StringUtils.hasLength((String) o)) {
                    parmeterList.add(o);
                } else {
                    parmeterList.add(null);
                }
            } else {
                parmeterList.add(dataMap.get(s));
            }
        }
        sql += columnStr.substring(1);

        String whereStr = " 1=1 ";
        for (String s : whereMap.keySet()) {
            whereStr += " and `" + s + "`=?";
            Object o = whereMap.get(s);
            if (o instanceof String) {
                if (StringUtils.hasLength((String) o)) {
                    parmeterList.add(o);
                } else {
                    parmeterList.add(null);
                }
            } else {
                parmeterList.add(whereMap.get(s));
            }
        }

        sql += " where " + whereStr;

        return jdbcTemplate.update(sql, parmeterList.toArray());
    }


    /**
     * 执行插入操作,如果存在则改为修改
     *
     * @param tableName
     * @param insertDataMap
     * @param pk
     * @return
     */
    public int executeInsertSql(String tableName, Map<String, Object> insertDataMap, String pk) {

        boolean b = hasDataByPk(tableName, insertDataMap, pk);
        if (b) {
            return executeUpdateSql(tableName, insertDataMap, pk);
        }

        String sql = "insert into `" + tableName + "` (";
        String columnStr = "";
        String what = "";
        List<Object> parmeterList = new LinkedList<>();

        for (String s : insertDataMap.keySet()) {
            columnStr += ",`" + s + "`";
            what += ",?";
            Object o = insertDataMap.get(s);
            if (o instanceof String) {
                if (StringUtils.hasLength((String) o)) {
                    parmeterList.add(o);
                } else {
                    parmeterList.add(null);
                }
            } else {
                parmeterList.add(insertDataMap.get(s));
            }

        }

        sql += columnStr.substring(1);
        sql += ")values(";
        sql += what.substring(1) + ")";

        return jdbcTemplate.update(sql, parmeterList.toArray());
    }


    /**
     * 根据主键查找数据
     *
     * @param tableName
     * @param insertDataMap
     * @param pk
     * @return
     */
    public boolean hasDataByPk(String tableName, Map<String, Object> insertDataMap, String pk) {
        if (pk == null) {
            return false;
        }
        String sql = "select * from " + tableName + " where " + pk + "=?";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, insertDataMap.get(pk));
        if (list != null && list.size() > 0) {
            return true;
        }
        return false;
    }

}
