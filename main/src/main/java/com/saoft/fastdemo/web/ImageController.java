package com.saoft.fastdemo.web;

import cn.hutool.core.io.FileUtil;
import com.saoft.fastdemo.system.ProjectInfoUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.nio.file.Files;
import java.util.UUID;

@Controller("ImageController")
public class ImageController {


    /**
     * 上传图片
     */
    @RequestMapping(method = RequestMethod.POST, value = "/uploadImage")
    @ResponseBody
    public String upload(@RequestPart("file") MultipartFile picture, HttpServletRequest request) {

        String pictureName = UUID.randomUUID().toString() + "." + getFileSuffix(picture.getOriginalFilename());
        try {
            File projectPath = new File("templates/"+ProjectInfoUtil.getProjectId() + "/upload-images");
            File file = new File(projectPath, pictureName);
            if (!file.getParentFile().exists()) {
                file.getParentFile().mkdirs();
            }
            FileUtil.writeFromStream(picture.getInputStream(), file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "/upload-images/"+pictureName;
    }

    private String getFileSuffix(String fileWholeName) {
        if (fileWholeName == null || "".equals(fileWholeName)) {
            return "none";
        } else {
            int lastIndexOf = fileWholeName.lastIndexOf(".");
            return fileWholeName.substring(lastIndexOf + 1);
        }
    }
}
