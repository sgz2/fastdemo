package com.saoft.fastdemo.web.config;

import com.alibaba.fastjson.serializer.SerializerFeature;
import com.alibaba.fastjson.support.config.FastJsonConfig;
import com.alibaba.fastjson.support.spring.FastJsonHttpMessageConverter;
import com.saoft.fastdemo.system.ProjectInfoUtil;
import com.saoft.fastdemo.web.component.UiHandlerInterceptor;
import org.apache.tomcat.util.descriptor.web.ServletDef;
import org.apache.tomcat.util.descriptor.web.WebXml;
import org.apache.tomcat.util.descriptor.web.WebXmlParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.xml.sax.InputSource;

import javax.servlet.ServletRegistration;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Configuration("WebConfig")
public class WebConfig implements WebMvcConfigurer {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        UiHandlerInterceptor interceptor = new UiHandlerInterceptor();
        registry.addInterceptor(interceptor).addPathPatterns("/**");
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        registry.addResourceHandler("/static/**")
                .addResourceLocations(
                        fileLocation("static"),
                        fileLocation("templates/"+ProjectInfoUtil.getProjectId()+"/static")
                );

        registry.addResourceHandler("/zj/**")
                .addResourceLocations( fileLocation("zj"));

        registry.addResourceHandler("/upload-images/**")
                .addResourceLocations( fileLocation("templates/"+ProjectInfoUtil.getProjectId()+"/upload-images"));
    }

    /*本地文件路径*/
    public String fileLocation(String dir){
        File file = new File("");
        String path = file.getAbsolutePath();
        String s = path.replaceAll("\\\\", "/");
        String rh = "file:" + s + "/" + dir + "/";
        return rh;
    }

    @Override
    public void configureMessageConverters(List<HttpMessageConverter<?>> converters) {
        FastJsonHttpMessageConverter converter = new FastJsonHttpMessageConverter();
        FastJsonConfig config = new FastJsonConfig();
        config.setSerializerFeatures(
                // 保留 Map 空的字段
                SerializerFeature.WriteMapNullValue,
                // 将 String 类型的 null 转成""
                SerializerFeature.WriteNullStringAsEmpty,
                // 将 Number 类型的 null 转成 0
                SerializerFeature.WriteNullNumberAsZero,
                // 将 List 类型的 null 转成 []
                SerializerFeature.WriteNullListAsEmpty,
                // 将 Boolean 类型的 null 转成 false
                SerializerFeature.WriteNullBooleanAsFalse,
                // 避免循环引用
                SerializerFeature.DisableCircularReferenceDetect);

        converter.setFastJsonConfig(config);
        converter.setDefaultCharset(Charset.forName("UTF-8"));
        List<MediaType> mediaTypeList = new ArrayList<>();
        // 解决中文乱码问题，相当于在 Controller 上的 @RequestMapping 中加了个属性 produces = "application/json"
        mediaTypeList.add(MediaType.APPLICATION_JSON);
        converter.setSupportedMediaTypes(mediaTypeList);
        converters.add(converter);
    }

    @Bean
    public ServletContextInitializer registerPreCompiledJsps() {
        return servletContext -> {
            ClassPathResource resource = new ClassPathResource("/web.xml");
            InputStream inputStream = null;
            try {
                inputStream = resource.getInputStream();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (inputStream == null) {
                logger.info("web.xml konnte nicht gelesen werden");
                return;
            }
            try {
                WebXmlParser parser = new WebXmlParser(false, false, true);
                WebXml webXml = new WebXml();
                boolean success = parser.parseWebXml(new InputSource(inputStream), webXml, false);
                if (!success) {
                    logger.error("Error registering precompiled JSPs");
                }
                for (ServletDef def : webXml.getServlets().values()) {
                    logger.info("Registering precompiled JSP: {} = {} -> {}", def.getServletName(), def.getServletClass());
                    ServletRegistration.Dynamic reg = servletContext.addServlet(def.getServletName(), def.getServletClass());
                    reg.setLoadOnStartup(99);
                }

                for (Map.Entry<String, String> mapping : webXml.getServletMappings().entrySet()) {
                    logger.info("Mapping servlet: {} -> {}", mapping.getValue(), mapping.getKey());
                    servletContext.getServletRegistration(mapping.getValue()).addMapping(mapping.getKey());
                }
            } catch (Exception e) {
                logger.error("Error registering precompiled JSPs", e);
            }
        };
    }
}
