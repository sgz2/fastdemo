package com.saoft.fastdemo.web.component;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.JTableColumn;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.db.entity.LayerLink;
import com.saoft.fastdemo.web.SessionUtil;
import com.saoft.fastdemo.web.bean.LoginUser;
import com.saoft.fastdemo.web.util.KvTypeUtil;
import com.saoft.fastdemo.web.util.OrderUtil;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component("CommonComponent")
public class CommonComponent {

    public String getPk(String tableName) {
        List<JTableColumn> columns = new JTableColumn().selectList(
                new QueryWrapper<JTableColumn>()
                        .eq(DBConstants.table_name, tableName)
                        .eq(DBConstants.key_flag, "true")
        );
        if (columns != null && columns.size() > 0) {
            return columns.get(0).getName();
        }else{
            return "id";
        }
    }

    /**
     * 获取层接收参数
     * @return
     */
    public String getParameter(){
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        Object fastdemo_tagMap = request.getAttribute("fastdemo_tagMap");
        if (fastdemo_tagMap != null && fastdemo_tagMap instanceof Map) {
            Object o = ((Map) fastdemo_tagMap).get("accept_from_row");
            if (o != null && o instanceof LayerLink) {
                String parameters = ((LayerLink) o).getParameters();
                return request.getParameter(parameters);
            }
        }
        return "";
    }
    /**
     * org - 别名
     * @param layerId
     * @return
     */
    public Map<String, String> getOrgAliasMap(Integer layerId) {
        List<LayerItem> layer_id = new LayerItem().selectList(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
        Map<String,String> map = new HashMap<>();
        for (LayerItem layerItem : layer_id) {
            if(StringUtils.hasLength(layerItem.getAlias())){
                map.put(layerItem.getField(), layerItem.getAlias());
            }else{
                map.put(layerItem.getField(), layerItem.getField());
            }

        }
        return map;
    }

    public Map<String, String> getOrgAliasMap(List<LayerItem> layer_id) {
        Map<String,String> map = new HashMap<>();
        for (LayerItem layerItem : layer_id) {
            if(StringUtils.hasLength(layerItem.getAlias())){
                map.put(layerItem.getField(), layerItem.getAlias());
            }else{
                map.put(layerItem.getField(), layerItem.getField());
            }

        }
        return map;
    }

    /**
     * 别名 外键
     * @param layerId
     * @return
     */
    public Map<String, String> getAliasOrgMap(Integer layerId) {
        List<LayerItem> layer_id = new LayerItem().selectList(new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
        Map<String,String> map = new HashMap<>();
        for (LayerItem layerItem : layer_id) {
            if(StringUtils.hasLength(layerItem.getAlias())){
                map.put(layerItem.getAlias(), layerItem.getField());
            }else{
                map.put(layerItem.getField(), layerItem.getField());
            }

        }
        return map;
    }

    public Map<String, String> getAliasOrgMap(List<LayerItem> layer_id) {
        Map<String,String> map = new HashMap<>();
        for (LayerItem layerItem : layer_id) {
            if(StringUtils.hasLength(layerItem.getAlias())){
                map.put(layerItem.getAlias(), layerItem.getField());
            }else{
                map.put(layerItem.getField(), layerItem.getField());
            }

        }
        return map;
    }


    /**
     * 组装 key value
     * @param columnList
     * @param runtimeMap 备选值，比如后台订单子项，需要应用订单号，这个值是 有订单那边产生，需要再运行是传递的数据
     * @return
     */
    public Map<String, Object> getInsertDataMap(List<LayerItem> columnList,Map<String,Object> runtimeMap) {
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        Map<String, Object> dataMap = new HashMap<>();
        for (LayerItem column : columnList) {
            String key = column.getField();
            //判断别名
            if (StringUtils.hasLength(column.getAlias())) {
                key = column.getAlias();
            }
            Object putData = request.getParameter(key);

            if (putData==null || !StringUtils.hasLength(putData.toString())) {
                putData = insertServerData(column, runtimeMap);
            }

            dataMap.put(column.getField(), putData);
        }
        return dataMap;
    }

    /**
     * 解析 FromUser 类型进行填充数据
     * @param item
     * @param runtimeMap
     * @return
     */
    public Object insertServerData(LayerItem item,Map<String,Object> runtimeMap){
        String type = item.getType();
        if (!StringUtils.hasLength(type)) {
            return null;
        }
        LoginUser user = SessionUtil.getUser();

        KvTypeUtil typeUtil = new KvTypeUtil(item.getTypeKvlist());
        String serverData= typeUtil.getServerData();

        if ("datetime".equals(serverData)) {
            Date date = new Date();
            return date;
        } else if ("user_id".equals(serverData)) {
            return user.getUserId();
        } else if ("user_name".equals(serverData)) {
            return user.getUsername();
        } else if ("order_no".equals(serverData)) {
            String orderNo = OrderUtil.getOrderNo(user.getUserId());
            return orderNo;
        } else if("map_alias_key".equals(serverData)){
            if (runtimeMap == null) {
                return null;
            }
            Object o = runtimeMap.get(item.getAlias());
            return o;
        }
        return null;
    }


    /**
     * 获取当前层连接的层
     * @param layer
     * @param tag
     * @return
     */
    public LayerLink getLinkLayerIdByTag(Layer layer, String tag) {
        if (layer != null) {
            List<LayerLink> linkList = new LayerLink().selectList(
                    new QueryWrapper<LayerLink>()
                            .eq(DBConstants.layer_id, layer.getId())
                            .eq(DBConstants.tag, tag)
            );
            if (linkList != null && linkList.size() > 0) {
                return linkList.get(0);
            }
        }
        return null;
    }
}
