package com.saoft.fastdemo.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.ui.shared.MapUtil;
import com.saoft.fastdemo.web.bean.LoginUser;
import com.saoft.fastdemo.web.component.CommonComponent;
import com.saoft.fastdemo.service.LayerComponent;
import com.saoft.fastdemo.web.util.KvTypeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@Controller("IndexController")
public class IndexController {

    @Autowired
    LayerComponent layerComponent;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    CommonComponent commonComponent;

    @GetMapping({"","index","/","index/**"})
    public String index() {

        String role = SessionUtil.checkRole();
        if (StringUtils.hasLength(role)) {
            List<Layer> layerList = new Layer().selectList(
                    new QueryWrapper<Layer>().eq(DBConstants.role_code,role)
                            .eq(DBConstants.type, "index"));
            if (layerList.size() > 0) {
                Layer layer = layerList.get(0);
                List<LayerItem> itemList = layerComponent.getLayerItemByLayerId(layer.getId());
                //分组
                boolean hasWeather = false;
                List<Map<String,Object>> idxCount = new LinkedList<>();
                List<LayerItem> info = new LinkedList<>();

                for (LayerItem layerItem : itemList) {
                    if ("idx-weather".equals(layerItem.getType())) {
                        hasWeather = true;
                    }
                    if ("idx-count".equals(layerItem.getType())) {
                        KvTypeUtil kvTypeUtil = new KvTypeUtil(layerItem.getTypeKvlist());
                        String sql = "select count(*) as total from `" + kvTypeUtil.getTableName() + "`";
                        Map<String, Object> map = jdbcTemplate.queryForMap(sql);
                        Object total = map.get("total");
                        Map<String, Object> map1 = MapUtil.getFieldsByAttribute(layerItem);
                        map1.put("total", total);
                        idxCount.add(map1);
                    }
                    if ("idx-user".equals(layerItem.getType())) {
                        //
                        info.add(layerItem);
                    }
                }

                //查询 用户信息
                HttpServletRequest request = SessionUtil.getHttpServletRequest();
                LoginUser loginUser = SessionUtil.getUser();
                String pk = commonComponent.getPk(loginUser.getTableName());
                String sql = String.format("select * from `%s` where %s = ?", loginUser.getTableName(), pk);
                Map<String, Object> map = jdbcTemplate.queryForMap(sql, loginUser.getUserId());
                request.setAttribute("obj", map);

                request.setAttribute("hasWeather", hasWeather);
                request.setAttribute("infoItem", info);
                request.setAttribute("idxCount",idxCount);
            }

        }else{
            return "redirect:/login";
        }
        return "index";
    }

//
//        //判断当前角色
//        String s = SessionUtil.checkRole();
//        if (s != null) {
//
//        }
//
//        //判断是否存在游客角色
//        List<Role> roleList = new Role().selectList(new QueryWrapper<Role>().eq("type", "front-end"));
//        if (roleList != null && roleList.size() > 0) {
//            return "index";
//        }
//
//        //判断是否有登录的配置
//        List<Layer> layers = new Layer().selectList(
//                new QueryWrapper<Layer>().eq(DBConstants.tag, "login"));
//
//        //有登录功能
//        if (layers != null && layers.size() > 0) {
//            SessionUtil.setAttribute("layers", layers);
//            return "login";
//        }
////        return "index";
//    }


}
