package com.saoft.fastdemo.web;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.saoft.fastdemo.bean.DBConstants;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;
import com.saoft.fastdemo.web.bean.LoginUser;
import com.saoft.fastdemo.web.bean.Msg;
import com.saoft.fastdemo.web.component.CommonComponent;
import com.saoft.fastdemo.web.component.TableComponent;
import com.saoft.fastdemo.web.dto.TableDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * @item login
 * */
@Controller("LoginController")
public class LoginController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    TableComponent tableComponent;

    @Autowired
    CommonComponent commonComponent;

    /*约定入口菜单*/
    /*入口*/
    /*菜单*/
    /**
     * @prop
     * @Group ds ;table 数据表
     * */
    @GetMapping("login")
    public String login(){
        /*所有角色*/
        List<Layer> layers = new Layer().selectList(
                new QueryWrapper<Layer>().eq(DBConstants.tag, "login"));

        List<Layer> registerLayers = new Layer().selectList(
                new QueryWrapper<Layer>().eq(DBConstants.tag, "register"));
        //有登录功能
        if (layers != null && layers.size() > 0) {
            SessionUtil.setAttribute("layers", layers);
        }

        /*有注册功能*/
        if (registerLayers != null && registerLayers.size() > 0) {
            SessionUtil.setAttribute("registerLayers", registerLayers);
        }

        return "login";
    }

    @ResponseBody
    @PostMapping("login")
    public Msg loginPost(HttpServletRequest request){

        String role = request.getParameter("role");
        QueryWrapper<Layer> wrapper = new QueryWrapper<>();
        wrapper.eq(DBConstants.tag, "login");
        if (StringUtils.hasLength(role)) {
            wrapper.eq(DBConstants.role_code, role);
        }
        List<Layer> layers = new Layer().selectList(wrapper);
        if (layers != null && layers.size() > 0) {
            Layer layer = layers.get(0);
            role = layer.getRoleCode();
            List<LayerItem> layerChecks = new LayerItem().selectList(
                    new QueryWrapper<LayerItem>()
                            .eq(DBConstants.layer_id, layer.getId()));
            LayerItem usernameC = null;
            LayerItem passwordC = null;
            for (LayerItem layerCheck : layerChecks) {
                if ("username".equals(layerCheck.getAlias())) {
                    usernameC = layerCheck;
                }else if("password".equals(layerCheck.getAlias())){
                    passwordC = layerCheck;
                }
            }
            String uField = null;
            String pwdField = null;
            try {
                uField = usernameC.getField();
                pwdField = passwordC.getField();
            } catch (Exception e) {
                return Msg.error().setMsg("检查登录配置");
            }


            String username = request.getParameter("username");
            String password = request.getParameter("password");

            boolean validateUser = validateUser(role, layer.getTableName()
                    , uField, pwdField, username, password);

            if (validateUser) {
                return Msg.success();
            }
        }

        return Msg.error().setMsg("用户名或密码错误");
    }

    @GetMapping("logout")
    public String logout(){
        /*所有角色*/
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        request.getSession().setAttribute(SessionUtil.LOGIN_USER, null);
        request.getSession().setAttribute("loginRole", null);
        return "redirect:/login";
    }

    @GetMapping("register")
    public String register(){
        /*所有角色*/
        List<Layer> layers = new Layer().selectList(
                new QueryWrapper<Layer>().eq(DBConstants.tag, "register"));

        //有登录功能
        if (layers != null && layers.size() > 0) {
            SessionUtil.setAttribute("layers", layers);

            Integer layerId = layers.get(0).getId();
            List<LayerItem> layerCreates = new LayerItem().selectList(
                    new QueryWrapper<LayerItem>()
                            .eq(DBConstants.layer_id, layerId));

            SessionUtil.setAttribute("columnList", layerCreates);
        }

        return "register";
    }

    @ResponseBody
    @PostMapping("register")
    public Msg registerDate(HttpServletRequest request){

        String role = request.getParameter("role");
        QueryWrapper<Layer> wrapper = new QueryWrapper<>();
        wrapper.eq(DBConstants.tag, "register");
        if (StringUtils.hasLength(role)) {
            wrapper.eq(DBConstants.role_code, role);
        }
        List<Layer> layers = new Layer().selectList(wrapper);
        if (layers != null && layers.size() > 0) {
            Layer layer = layers.get(0);

            TableDTO table = tableComponent.getTable(layer.getId());

//            String sql = "insert into `" + layer.getTableName() + "` (";
//            String columnStr = "";
//            String what = "";
//            List<Map<Object, Object>> columnList = table.getColumnList();
//            Map<String, Object> insertDataMap = commonComponent.getInsertDataMap(columnList,null);
//            List<Object> parmeterList = new LinkedList<>();
//            for (String s : insertDataMap.keySet()) {
//                columnStr += ",`"+s+"`";
//                what += ",?";
//                parmeterList.add(insertDataMap.get(s));
//            }
//
//            sql += columnStr.substring(1);
//            sql += ")values(";
//            sql += what.substring(1) + ")";
//            jdbcTemplate.update(sql, parmeterList.toArray());

            List<Object> parmeterList = new LinkedList<>();
            String sql = tableComponent.insertSql(table, parmeterList);

            jdbcTemplate.update(sql, parmeterList.toArray());
        }
        return Msg.success();
    }

    private boolean validateUser(
            String role
            ,String tableName
            ,String uField,String pwdField
            ,String username,String password){



        String sql = "select * from `" + tableName + "` where `" + uField + "`=?";
        List<Map<String, Object>> list = jdbcTemplate.queryForList(sql, username);
        if (list.size() > 0) {
            if(password!=null && password.equals(list.get(0).get(pwdField))){
                //登录成功 注入session
                LoginUser loginUser = new LoginUser();
                Map<String, Object> map = list.get(0);
                String pk = commonComponent.getPk(tableName);
                Object o = map.get(pk);

                loginUser.setUserId((Integer) o);
                loginUser.setUsername(map.get(uField).toString());
                loginUser.setShowname(map.get(uField).toString());
                loginUser.setTableName(tableName);
                HttpServletRequest request = SessionUtil.getHttpServletRequest();
                request.getSession().setAttribute(SessionUtil.LOGIN_USER, loginUser);
                request.getSession().setAttribute(SessionUtil.LOGIN_ROLE, role);
                return true;
            }
        }
        return false;
    }

    /**
     * 个人信息 实际属于编辑
     * @return
     */
    @GetMapping("profile")
    public String profile(){

        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        LoginUser loginUser = SessionUtil.getUser();
        String loginRole = (String) request.getSession().getAttribute("loginRole");
        String pk = commonComponent.getPk(loginUser.getTableName());


        //获取layerId 设置字段
        Integer layerId = getTagLayerId(loginRole,"profile");
        if (layerId != null) {
            TableDTO table = tableComponent.getTable(layerId);
            tableComponent.setTable(table,true);
        }

        String sql = String.format("select * from `%s` where %s = ?", loginUser.getTableName(), pk);
        Map<String, Object> map = jdbcTemplate.queryForMap(sql, loginUser.getUserId());
        request.setAttribute("obj", map);
        return "profile";
    }

    @GetMapping("password")
    public String password(){
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        String loginRole = (String) request.getSession().getAttribute("loginRole");

        //获取layerId 设置字段
        Integer layerId = getTagLayerId(loginRole,"update_password");
        SessionUtil.setAttribute("layer", new Layer().selectById(layerId));
        return "password";
    }

    @ResponseBody
    @PostMapping("password")
    public Msg passwordSave(String password,String newPassword){
        HttpServletRequest request = SessionUtil.getHttpServletRequest();
        LoginUser loginUser = SessionUtil.getUser();
        String loginRole = SessionUtil.getRole();
        String pk = commonComponent.getPk(loginUser.getTableName());

        //获取layerId 设置字段
        Integer layerId = getTagLayerId(loginRole,"update_password");
        String passwordColumn = getPasswordColumn(layerId);
        //验证旧密码
        String sql = "select * from `%s` where `%s`=?";
        Map<String, Object> map = jdbcTemplate.queryForMap(String.format(sql, loginUser.getTableName(), pk), loginUser.getUserId());
        String o = (String) map.get(passwordColumn);

        if(!o.equals(password)){
            return Msg.error().setMsg("旧密码错误");
        }
        //修改新密码
        String updateSql = "update `%s` set `%s`=? where `%s`=?";
        jdbcTemplate.update(
                String.format(updateSql, loginUser.getTableName(), passwordColumn, pk)
                ,newPassword,loginUser.getUserId()
        );

        return Msg.success();
    }

    private String getPasswordColumn(Integer layerId){
        List<LayerItem> list = new LayerItem().selectList(
                new QueryWrapper<LayerItem>().eq(DBConstants.layer_id, layerId));
        if (list != null && list.size() > 0) {
            return list.get(0).getField();
        }
        return null;
    }

    /**
     * 获取角色的个人信息配置的layerId
     * @param role
     * @return
     */
    private Integer getTagLayerId(String role,String tab){
        List<Layer> layers = new Layer().selectList(new QueryWrapper<Layer>()
                .eq(DBConstants.role_code, role)
                .eq(DBConstants.tag, tab));

        if (layers != null && layers.size() > 0) {
            return layers.get(0).getId();
        }
        return null;
    }




}
