package com.saoft.fastdemo.web.dto;

import com.saoft.fastdemo.db.entity.JTableInfo;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerItem;

import java.util.List;
import java.util.Map;

public class TableDTO {

    private Layer out;
    private JTableInfo tableInfo;
    private List<Map<String, Object>> columnList;
    private List<LayerItem> orgColumnList;

    public Layer getLayer() {
        return out;
    }

    public void setLayer(Layer out) {
        this.out = out;
    }

    public JTableInfo getTableInfo() {
        return tableInfo;
    }

    public void setTableInfo(JTableInfo tableInfo) {
        this.tableInfo = tableInfo;
    }

    public List<Map<String, Object>> getColumnList() {
        return columnList;
    }

    public void setColumnList(List<Map<String, Object>> columnList) {
        this.columnList = columnList;
    }

    public List<LayerItem> getOrgColumnList() {
        return orgColumnList;
    }

    public void setOrgColumnList(List<LayerItem> orgColumnList) {
        this.orgColumnList = orgColumnList;
    }
}
