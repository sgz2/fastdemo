package com.saoft.fastdemo.web.dto;

import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.db.entity.Layer;
import com.saoft.fastdemo.db.entity.LayerLink;

public class LayerLinkDTO {

    @KeepThis
    private LayerLink layerLink;
    /*目标层*/
    @KeepThis
    private Layer layer;
    @KeepThis
    public LayerLink getLayerLink() {
        return layerLink;
    }

    public void setLayerLink(LayerLink layerLink) {
        this.layerLink = layerLink;
        if (layerLink != null) {
            Integer linkLayerId = layerLink.getLinkLayerId();
            Layer layer = new Layer().selectById(linkLayerId);
            this.layer = layer;
        }
    }
    @KeepThis
    public Layer getLayer() {
        return layer;
    }

    public void setLayer(Layer layer) {
        this.layer = layer;
    }
}
