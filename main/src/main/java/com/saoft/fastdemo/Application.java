package com.saoft.fastdemo;

import com.saoft.fastdemo.system.PreferencesUtil;
import com.saoft.fastdemo.ui.main.ConsoleController;
import com.saoft.fastdemo.ui.main.InitFrameController;
import com.saoft.fastdemo.ui.main.MainController;
import com.saoft.fastdemo.ui.main.splash.FDSplash;
import com.saoft.fastdemo.util.LookAndFeelUtils;
import com.saoft.fastdemo.util.YamlConfigurerUtil;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.json.YamlJsonParser;
import org.springframework.context.ConfigurableApplicationContext;

import javax.swing.*;


@SpringBootApplication
@MapperScan("com.saoft.fastdemo.dao")
public class Application {

    public final static String M_PROJECT_NAME = "FastDemo代码助手";

    @Value("${fastdemo.version}")
    public static String VERSION = "1.0";

    public static String[] args;
    public static  InitFrameController frame = new InitFrameController();
    public static  FDSplash fdSplash = null;
    public static  PreferencesUtil util = new PreferencesUtil();

    public static void main(String[] args) {
        Application.args = args;
        readVersion();
        fdSplash = new FDSplash();
        for (String arg : args) {
            if ("-version".equals(arg) || "-v".equals(arg)) {
                //返回版本号
                System.out.println(VERSION);
                return;
            }
        }

        //检查数据库阻止springboot 启动
        if(util.hasProj()){
            fdSplash.setVisible(true);
            startBoot();
        }else{
            //弹出新建界面
            frame.getNoProjectFrame().setVisible(true);
        }
    }

    public static void readVersion(){
        if (Application.args != null) {
            for (String arg : Application.args) {
                if (arg.contains("active=dev")) {
                    YamlConfigurerUtil util = new YamlConfigurerUtil("application-dev.yml");
                    Application.VERSION = util.getStrYmlVal("fastdemo.version");
                    return;
                }
            }
        }
        YamlConfigurerUtil util = new YamlConfigurerUtil("application-prod.yml");
        Application.VERSION = util.getStrYmlVal("fastdemo.version");
    }

    public static void banner(){
        fdSplash.setVisible(true);
        ProgressBeanPostProcessor.observe().subscribe(integer -> fdSplash.setProgress(integer)
                ,e->{}
                ,() ->fdSplash.setVisible(false));
    }

    public static void startBoot(){
        banner();
        frame.getNoProjectFrame().setVisible(false);
        LookAndFeelUtils.setWindowsLookAndFeel();

        long start = System.currentTimeMillis();

        new SwingWorker() {
            @Override
            protected Object doInBackground() throws Exception {
                ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class).headless(false).run(args);
                MainController mainMenuController = context.getBean(MainController.class);
                mainMenuController.prepareAndOpenFrame();

                long end = System.currentTimeMillis();
                ConsoleController.log("time:"+(end-start)+"ms");
                return null;
            }
        }.execute();

    }

}
