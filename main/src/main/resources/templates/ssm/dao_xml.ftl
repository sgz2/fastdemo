<#if method.type=='select'>
    <#--根据 xx 获取结果-->
    <select id="${method.name!}" resultMap="BaseResultMap" >
        select * from `${table.name}` where `xxxx` = ${r'#{'}}
    </select>
</#if>
<#if method.type=='update'>
<#--修改-->
    <update id="${method.name!}" parameterType="xxx" >
        update `${table.name}`
        <set >

        </set>
        where `${pkField.name}` = ${r'#{'}id,jdbcType=xxx}
    </update>
</#if>
<#if method.type=='insert'>
<#--新增-->
    <insert id="insert" parameterType="com.cjh.hotel.entity.VipLevel" >
        insert into `${table.name}` (`id`,`xxx`)
        values (${r'#{'}id,jdbcType=INTEGER},${r'#{'}xxx,jdbcType=VARCHAR})
    </insert>
</#if>
<#if method.type=='delete'>
<#--删除-->
    <delete id="${method.name!}" parameterType="java.lang.Integer" >
      delete from `${table.name}` where `xxxx` = ${r'#{'}id,jdbcType=xxx}
    </delete>
</#if>