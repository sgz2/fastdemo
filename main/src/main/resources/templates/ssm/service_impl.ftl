<#assign returnstr= (method.resultType=='void')?string("","return ")>
<#if method.type=='select'>
    /*
     * 查询${table.comment!}
     */
    ${method.resultType!} ${method.name!}(${method.parm!}){
        ${returnstr}${entityFL}Dao.${method.name!}(${method.parm!});
    }
</#if>
<#if method.type=='insert'>
    /*
     * 新增${table.comment!}
     */
    ${method.resultType!} ${method.name!}(${method.parm!}){
        ${returnstr}${entityFL}Dao.${method.name!}(${method.parm!});
    }
</#if>
<#if method.type=='update'>
    /*
     * 修改${table.comment!}
     */
    ${method.resultType!} ${method.name!}(${method.parm!}){
        ${returnstr}${entityFL}Dao.${method.name!}(${method.parm!});
    }
</#if>
<#if method.type=='delete'>
    /*
     * 删除${table.comment!}
     */
    ${method.resultType!}  ${method.name!}(${method.parm!}){
        ${returnstr}${entityFL}Dao.${method.name!}(${method.parm!});
    }
</#if>