function _callJava(actionName,value,success,fail) {
    var obj = {
        request: value+"",
        persistent:false,
        onSuccess: function(response) {
            // alert("返回的数据:"+response);
        },
        onFailure: function(error_code, error_message) {}
    }
    if (success) {
        obj.onSuccess = success;
    }
    if (fail) {
        obj.onFailure = fail;
    }
    window[actionName](obj);
}