package com.saoft.fastdemo.bean;

import com.saoft.dbinfo.po.TableInfo;

public class TableInfoEx extends TableInfo {

    @Override
    public String toString() {
        return getName();
    }
}
