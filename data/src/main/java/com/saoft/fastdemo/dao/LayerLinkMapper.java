package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.LayerLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 层链接 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface LayerLinkMapper extends BaseMapper<LayerLink> {

}
