package com.saoft.fastdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saoft.fastdemo.db.entity.Menu;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-26
 */
public interface MenuMapper extends BaseMapper<Menu> {

}
