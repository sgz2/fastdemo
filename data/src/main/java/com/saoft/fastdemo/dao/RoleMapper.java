package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface RoleMapper extends BaseMapper<Role> {

}
