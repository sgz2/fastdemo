package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.Template;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模板 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface TemplateMapper extends BaseMapper<Template> {

}
