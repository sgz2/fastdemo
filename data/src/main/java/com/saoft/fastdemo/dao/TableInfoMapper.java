package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.JTableInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 表信息 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface TableInfoMapper extends BaseMapper<JTableInfo> {

}
