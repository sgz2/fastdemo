package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.Setting;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 全局配置 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface SettingMapper extends BaseMapper<Setting> {

}
