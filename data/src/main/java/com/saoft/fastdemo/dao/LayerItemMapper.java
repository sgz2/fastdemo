package com.saoft.fastdemo.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.saoft.fastdemo.db.entity.LayerItem;

/**
 * <p>
 * layer详细列配置 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-26
 */
public interface LayerItemMapper extends BaseMapper<LayerItem> {

}
