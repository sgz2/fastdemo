package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.LayerGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 分组 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface LayerGroupMapper extends BaseMapper<LayerGroup> {

}
