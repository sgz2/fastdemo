package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.JTableColumn;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 列信息 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface TableColumnMapper extends BaseMapper<JTableColumn> {

}
