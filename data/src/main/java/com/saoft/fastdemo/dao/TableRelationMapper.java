package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.TableRelation;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 模型特殊字段 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface TableRelationMapper extends BaseMapper<TableRelation> {

}
