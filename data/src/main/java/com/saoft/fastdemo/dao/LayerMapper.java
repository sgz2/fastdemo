package com.saoft.fastdemo.dao;

import com.saoft.fastdemo.db.entity.Layer;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 输出层 Mapper 接口
 * </p>
 *
 * @author saoft
 * @since 2019-08-19
 */
public interface LayerMapper extends BaseMapper<Layer> {

}
