package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* layer详细列配置
* </p>
*
* @author saoft
* @since 2019-08-26
*/
@TableName(DBConstants.okgfd_layer_item)
public class LayerItem extends Model<LayerItem> {

    private static final long serialVersionUID = 1L;

        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 层ID
        */
    @TableField(DBConstants.layer_id)
    private Integer layerId;


        /**
        * 字段
        */
    @TableField(DBConstants.field)
    private String field;

        /**
        * 别名
        */
    @TableField(DBConstants.alias)
    private String alias;

        /**
        * 显示名
        */
    @TableField(DBConstants.title)
    private String title;

        /**
        * 必填项
        */
    @TableField(DBConstants.must_flag)
    private Boolean mustFlag;


        /**
        * 查询条件
        */
    @TableField(DBConstants.query)
    private String query;

        /**
        * 类型 text,password,repassword,email,number,textarea,img,radio,checkbox,select
        */
    @TableField(DBConstants.type)
    private String type;

        /**
        * 可选类型的可选值 json
        */
    @TableField(DBConstants.type_kvlist)
    private String typeKvlist;

    @TableField(DBConstants.validate)
    private String validate;

        /**
        * 只读
        */
    @TableField(DBConstants.readonly_flag)
    private Boolean readonlyFlag;

        /**
        * 隐藏
        */
    @TableField(DBConstants.hidden_flag)
    private Boolean hiddenFlag;

    @TableField(DBConstants.width)
    private Integer width;

        /**
        * 编号
        */
    @TableField(DBConstants.sort_by)
    private Double sortBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public Integer getLayerId() {
        return layerId;
    }

    public void setLayerId(Integer layerId) {
    this.layerId = layerId;
    }
    public String getField() {
        return field;
    }

    public void setField(String field) {
    this.field = field;
    }
    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
    this.alias = alias;
    }
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
    this.title = title;
    }
    public Boolean getMustFlag() {
        return mustFlag;
    }

    public void setMustFlag(Boolean mustFlag) {
    this.mustFlag = mustFlag;
    }
    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
    this.query = query;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
    this.type = type;
    }
    public String getTypeKvlist() {
        return typeKvlist;
    }

    public void setTypeKvlist(String typeKvlist) {
    this.typeKvlist = typeKvlist;
    }
    public String getValidate() {
        return validate;
    }

    public void setValidate(String validate) {
    this.validate = validate;
    }
    public Boolean getReadonlyFlag() {
        return readonlyFlag;
    }

    public void setReadonlyFlag(Boolean readonlyFlag) {
    this.readonlyFlag = readonlyFlag;
    }
    public Boolean getHiddenFlag() {
        return hiddenFlag;
    }

    public void setHiddenFlag(Boolean hiddenFlag) {
    this.hiddenFlag = hiddenFlag;
    }
    public Integer getWidth() {
        return width;
    }

    public void setWidth(Integer width) {
    this.width = width;
    }
    public Double getSortBy() {
        return sortBy;
    }

    public void setSortBy(Double sortBy) {
    this.sortBy = sortBy;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

@Override
public String toString() {
return title;
}
}
