package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 全局配置
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_setting)
public class Setting extends Model<Setting> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

    @TableField(DBConstants.group_code)
    private String groupCode;

    @TableField(DBConstants.name)
    private String name;

        /**
        * 编码
        */
    @TableField(DBConstants.code)
    private String code;

        /**
        * 值
        */
    @TableField(DBConstants.value)
    private String value;

    @TableField(DBConstants.value_type)
    private String valueType;

        /**
        * 可选值
        */
    @TableField(DBConstants.value_option)
    private String valueOption;

    @TableField(DBConstants.memo)
    private String memo;

    @TableField(DBConstants.sort_code)
    private Integer sortCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getGroupCode() {
        return groupCode;
    }

    public void setGroupCode(String groupCode) {
    this.groupCode = groupCode;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
    this.code = code;
    }
    public String getValue() {
        return value;
    }

    public void setValue(String value) {
    this.value = value;
    }
    public String getValueType() {
        return valueType;
    }

    public void setValueType(String valueType) {
    this.valueType = valueType;
    }
    public String getValueOption() {
        return valueOption;
    }

    public void setValueOption(String valueOption) {
    this.valueOption = valueOption;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
    this.memo = memo;
    }
    public Integer getSortCode() {
        return sortCode;
    }

    public void setSortCode(Integer sortCode) {
    this.sortCode = sortCode;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
