package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 模型特殊字段
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_table_relation)
public class TableRelation extends Model<TableRelation> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 显示名
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 类型 m2o o2m
        */
    @TableField(DBConstants.type)
    private String type;

        /**
        * 模型外键
        */
    @TableField(DBConstants.ftable_id)
    private String ftableId;

        /**
        * 列
        */
    @TableField(DBConstants.fcolumn_id)
    private String fcolumnId;

        /**
        * 目标表
        */
    @TableField(DBConstants.ttable_id)
    private String ttableId;

        /**
        * 目标列
        */
    @TableField(DBConstants.tcolumn_id)
    private String tcolumnId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
    this.type = type;
    }
    public String getFtableId() {
        return ftableId;
    }

    public void setFtableId(String ftableId) {
    this.ftableId = ftableId;
    }
    public String getFcolumnId() {
        return fcolumnId;
    }

    public void setFcolumnId(String fcolumnId) {
    this.fcolumnId = fcolumnId;
    }
    public String getTtableId() {
        return ttableId;
    }

    public void setTtableId(String ttableId) {
    this.ttableId = ttableId;
    }
    public String getTcolumnId() {
        return tcolumnId;
    }

    public void setTcolumnId(String tcolumnId) {
    this.tcolumnId = tcolumnId;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
