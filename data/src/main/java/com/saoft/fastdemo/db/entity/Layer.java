package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.baomidou.mybatisplus.extension.activerecord.Model;

import java.io.Serializable;
import java.util.Objects;

import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 输出层
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_layer)
public class Layer extends Model<Layer> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @KeepThis
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

    @KeepThis
    @TableField(DBConstants.role_code)
    private String roleCode;

        /**
        * 表
        */
    @TableField(DBConstants.table_name)
    private String tableName;

        /**
        * 名称
        */
    @KeepThis
    @TableField(DBConstants.name)
    private String name;

        /**
        * 类型 1增加 2删除 3列表 4修改 5详情 6验证
        */
        @KeepThis
    @TableField(DBConstants.type)
    private String type;

        /**
        * 特殊标记 登录注册等特殊情况
        */
    @TableField(DBConstants.tag)
    private String tag;

    @TableField(DBConstants.mvc_type)
    private String mvcType;

    @TableField(value = DBConstants.group_id,fill = FieldFill.UPDATE)
    private Integer groupId;

        /**
        * 可见
        */
    @TableField(DBConstants.visiable_flag)
    private Boolean visiableFlag;

        /**
        * 锁定
        */
    @TableField(DBConstants.lock_flag)
    private Boolean lockFlag;



    @KeepThis
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }

    @KeepThis
    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
    }
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
    this.tableName = tableName;
    }
    @KeepThis
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }

    @KeepThis
    public String getType() {
        return type;
    }

    public void setType(String type) {
    this.type = type;
    }
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
    this.tag = tag;
    }
    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
    this.groupId = groupId;
    }
    public Boolean getVisiableFlag() {
        return visiableFlag;
    }

    public void setVisiableFlag(Boolean visiableFlag) {
    this.visiableFlag = visiableFlag;
    }
    public Boolean getLockFlag() {
        return lockFlag;
    }

    public void setLockFlag(Boolean lockFlag) {
    this.lockFlag = lockFlag;
    }

    public String getMvcType() {
        return mvcType;
    }

    public void setMvcType(String mvcType) {
        this.mvcType = mvcType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Layer layer = (Layer) o;
        return Objects.equals(id, layer.id);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id);
    }

    @Override
protected Serializable pkVal() {
    return this.id;
}

@Override
public String toString() {
return name;
}
}
