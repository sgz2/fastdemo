package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 模板
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_template)
public class Template extends Model<Template> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 编号
        */
    @TableField(DBConstants.code)
    private String code;

        /**
        * 名称
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 详情链接
        */
    @TableField(DBConstants.info_href)
    private String infoHref;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
    this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getInfoHref() {
        return infoHref;
    }

    public void setInfoHref(String infoHref) {
    this.infoHref = infoHref;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
