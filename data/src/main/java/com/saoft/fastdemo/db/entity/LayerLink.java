package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 层链接
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_layer_link)
public class LayerLink extends Model<LayerLink> {

    private static final long serialVersionUID = 1L;
    @KeepThis
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 所属层
        */
        @KeepThis
    @TableField(DBConstants.layer_id)
    private Integer layerId;

        /**
        * 目标层
        */
        @KeepThis
    @TableField(DBConstants.link_layer_id)
    private Integer linkLayerId;

        /**
        * 显示名
        */
        @KeepThis
    @TableField(DBConstants.title)
    private String title;

        /**
        * 参数
        */
        @KeepThis
    @TableField(DBConstants.parameters)
    private String parameters;

        /**
        * 标记
        */
        @KeepThis
    @TableField(DBConstants.tag)
    private String tag;

        /**
        * css样式class
        */
        @KeepThis
    @TableField(DBConstants.css_class)
    private String cssClass;
    @KeepThis
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }@KeepThis
    public Integer getLayerId() {
        return layerId;
    }

    public void setLayerId(Integer layerId) {
    this.layerId = layerId;
    }@KeepThis
    public Integer getLinkLayerId() {
        return linkLayerId;
    }

    public void setLinkLayerId(Integer linkLayerId) {
    this.linkLayerId = linkLayerId;
    }@KeepThis
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
    this.title = title;
    }@KeepThis
    public String getParameters() {
        return parameters;
    }

    public void setParameters(String parameters) {
    this.parameters = parameters;
    }@KeepThis
    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
    this.tag = tag;
    }@KeepThis
    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
    this.cssClass = cssClass;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
