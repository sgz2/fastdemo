package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 模板项
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_template_item)
public class TemplateItem extends Model<TemplateItem> {

    private static final long serialVersionUID = 1L;

        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

    @TableField(DBConstants.template_code)
    private String templateCode;

        /**
        * 编码
        */
    @TableField(DBConstants.code)
    private String code;

        /**
        * 名称
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 必选
        */
    @TableField(DBConstants.if_must)
    private String ifMust;

        /**
        * 排序
        */
    @TableField(DBConstants.sort_code)
    private String sortCode;

        /**
        * 备注
        */
    @TableField(DBConstants.memo)
    private String memo;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
    this.templateCode = templateCode;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
    this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getIfMust() {
        return ifMust;
    }

    public void setIfMust(String ifMust) {
    this.ifMust = ifMust;
    }
    public String getSortCode() {
        return sortCode;
    }

    public void setSortCode(String sortCode) {
    this.sortCode = sortCode;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
    this.memo = memo;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

@Override
public String toString() {
return name;
}
}
