package com.saoft.fastdemo.db;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.util.StringUtils;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

/**
 * 服务端跟客户端不同
 * 服务端需要同时处理多个生成请求
 * 所以服务端的动态数据源需要根据：
 *      当前登录用户的特征设置
 *
 */
public class RuntimeDataSource implements DataSource {

    //本地线程，获取当前正在执行的currentThread
    public static final ThreadLocal<String> contextHolder = new ThreadLocal<String>();

    private DataSource hikariDataSource;

    Map<String,DataSource> generatorDataSource = new HashMap<>();

    public DataSource getDataSource() {
        return hikariDataSource;
    }

    /**
     * 设置用户临时数据连接
     * @param userId
     * @param hikariDataSource
     */
    public void setDataSource(String userId,DataSource hikariDataSource) {
        contextHolder.set(userId);
        generatorDataSource.put(userId, hikariDataSource);
    }


    public void setDataSource(DataSource hikariDataSource) {
        if (this.hikariDataSource != null) {
            try {
                Connection connection = this.hikariDataSource.getConnection();
                connection.close();
            } catch (SQLException e) {
//                e.printStackTrace();
            }
        }
        this.hikariDataSource = hikariDataSource;
    }

    /**
     * jdbc template 会调用该方法
     *
     * @return
     * @throws SQLException
     */
    @Override
    public Connection getConnection() throws SQLException {
        //判断是否是用户进行的导出操作
        String userId = contextHolder.get();
        if (StringUtils.hasLength(userId)) {
            return generatorDataSource.get(userId).getConnection();
        }
        return hikariDataSource.getConnection();
    }

    /**
     * 关闭临时连接
     */
    public void close(){
        String userId = contextHolder.get();
        if (StringUtils.hasLength(userId)) {
            try {
                HikariDataSource dataSource = (HikariDataSource) generatorDataSource.get(userId);
                dataSource.getConnection().createStatement().execute("SHUTDOWN");
                dataSource.close();
                generatorDataSource.remove(userId);
                contextHolder.remove();
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return hikariDataSource.getConnection(username, password);
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return hikariDataSource.unwrap(iface);
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return hikariDataSource.isWrapperFor(iface);
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return hikariDataSource.getLogWriter();
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {
        hikariDataSource.setLogWriter(out);
    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {
        hikariDataSource.setLoginTimeout(seconds);
    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return hikariDataSource.getLoginTimeout();
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return hikariDataSource.getParentLogger();
    }
}
