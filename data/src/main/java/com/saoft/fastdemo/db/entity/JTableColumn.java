package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.KeepThis;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 列信息
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@KeepThis
@TableName(DBConstants.okgfd_table_column)
public class JTableColumn extends Model<JTableColumn> {

    private static final long serialVersionUID = 1L;

        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 表
        */
    @TableField(DBConstants.table_name)
    private String tableName;

        /**
        * 列
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 主键
        */
    @TableField(DBConstants.key_flag)
    private String keyFlag;

        /**
        * 显示名
        */
    @TableField(DBConstants.show_name)
    private String showName;

        /**
        * 类型
        */
    @TableField(DBConstants.type)
    private String type;

        /**
        * 允许为空
        */
    @TableField(DBConstants.if_null)
    private String ifNull;

        /**
        * 默认值
        */
    @TableField(DBConstants.default_value)
    private String defaultValue;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
    this.tableName = tableName;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getKeyFlag() {
        return keyFlag;
    }

    public void setKeyFlag(String keyFlag) {
    this.keyFlag = keyFlag;
    }
    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
    this.showName = showName;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
    this.type = type;
    }
    public String getIfNull() {
        return ifNull;
    }

    public void setIfNull(String ifNull) {
    this.ifNull = ifNull;
    }
    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(String defaultValue) {
    this.defaultValue = defaultValue;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
