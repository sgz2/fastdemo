package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 分组
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_layer_group)
public class LayerGroup extends Model<LayerGroup> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

    @TableField(DBConstants.role_code)
    private String roleCode;

        /**
        * 名称
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 展开
        */
    @TableField(DBConstants.expand)
    private Boolean expand;

        /**
        * 父级
        */
    @TableField(DBConstants.parent_id)
    private Integer parentId;

        /**
        * 可见
        */
    @TableField(DBConstants.visiable_flag)
    private Boolean visiableFlag;

        /**
        * 锁
        */
    @TableField(DBConstants.lock_flag)
    private Boolean lockFlag;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getRoleCode() {
        return roleCode;
    }

    public void setRoleCode(String roleCode) {
    this.roleCode = roleCode;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public Boolean getExpand() {
        return expand;
    }

    public void setExpand(Boolean expand) {
    this.expand = expand;
    }
    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
    this.parentId = parentId;
    }
    public Boolean getVisiableFlag() {
        return visiableFlag;
    }

    public void setVisiableFlag(Boolean visiableFlag) {
    this.visiableFlag = visiableFlag;
    }
    public Boolean getLockFlag() {
        return lockFlag;
    }

    public void setLockFlag(Boolean lockFlag) {
    this.lockFlag = lockFlag;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

    @Override
    public String toString() {
        return name;
    }
}
