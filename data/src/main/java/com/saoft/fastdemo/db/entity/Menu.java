package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 菜单表
* </p>
*
* @author saoft
* @since 2019-08-26
*/
@TableName(DBConstants.okgfd_menu)
public class Menu extends Model<Menu> {

    private static final long serialVersionUID = 1L;

        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * layer
        */
    @TableField(DBConstants.layer_id)
    private Integer layerId;

        /**
        * 角色
        */
    @TableField(DBConstants.role)
    private String role;

        /**
        * 编码
        */
    @TableField(DBConstants.code)
    private String code;

        /**
        * 父级编码
        */
    @TableField(DBConstants.parent_code)
    private String parentCode;

        /**
        * 显示名
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 链接
        */
    @TableField(DBConstants.url)
    private String url;

        /**
        * target
        */
    @TableField(DBConstants.target)
    private String target;

    @TableField(DBConstants.sort_by)
    private Double sortBy;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public Integer getLayerId() {
        return layerId;
    }

    public void setLayerId(Integer layerId) {
    this.layerId = layerId;
    }
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
    this.role = role;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
    this.code = code;
    }
    public String getParentCode() {
        return parentCode;
    }

    public void setParentCode(String parentCode) {
    this.parentCode = parentCode;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
    this.url = url;
    }
    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
    this.target = target;
    }
    public Double getSortBy() {
        return sortBy;
    }

    public void setSortBy(Double sortBy) {
    this.sortBy = sortBy;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

@Override
public String toString() {
return name;
}
}
