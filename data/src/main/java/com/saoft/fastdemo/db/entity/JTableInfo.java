package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 表信息
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_table_info)
public class JTableInfo extends Model<JTableInfo> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 表
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 显示名
        */
    @TableField(DBConstants.show_name)
    private String showName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getShowName() {
        return showName;
    }

    public void setShowName(String showName) {
    this.showName = showName;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
