package com.saoft.fastdemo.db.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import java.io.Serializable;
import com.baomidou.mybatisplus.annotation.TableField;
import com.saoft.fastdemo.bean.DBConstants;

/**
* <p>
* 角色
* </p>
*
* @author saoft
* @since 2019-08-19
*/
@TableName(DBConstants.okgfd_role)
public class Role extends Model<Role> {

    private static final long serialVersionUID = 1L;

        /**
        * ID
        */
        @TableId(value = DBConstants.id, type = IdType.AUTO)
    private Integer id;

        /**
        * 编号
        */
    @TableField(DBConstants.code)
    private String code;

        /**
        * 名称
        */
    @TableField(DBConstants.name)
    private String name;

        /**
        * 类型
        */
    @TableField(DBConstants.type)
    private String type;

        /**
        * 说明
        */
    @TableField(DBConstants.memo)
    private String memo;

        /**
        * 模板
        */
    @TableField(DBConstants.template_code)
    private String templateCode;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
    this.id = id;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
    this.code = code;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
    this.name = name;
    }
    public String getType() {
        return type;
    }

    public void setType(String type) {
    this.type = type;
    }
    public String getMemo() {
        return memo;
    }

    public void setMemo(String memo) {
    this.memo = memo;
    }
    public String getTemplateCode() {
        return templateCode;
    }

    public void setTemplateCode(String templateCode) {
    this.templateCode = templateCode;
    }

@Override
protected Serializable pkVal() {
    return this.id;
}

}
