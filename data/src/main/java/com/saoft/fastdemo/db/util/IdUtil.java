package com.saoft.fastdemo.db.util;

import com.saoft.fastdemo.bean.DBConstants;
import org.springframework.jdbc.core.JdbcTemplate;

public class IdUtil {

    public static Integer maxId(String table, JdbcTemplate jdbcTemplate){
        String sql = "select max("+DBConstants.id +") from `" + table+"`";
        Integer integer = jdbcTemplate.queryForObject(sql, Integer.class);
        return integer;
    }
}
