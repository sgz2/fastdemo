package com.saoft.fastdemo.zujian;
/**
 * 组件的每一个属性
 */
public class ZuJianItem {

    //属性显示的名称
    private String name;
    //值类型 input,image,richtext,link,list,color,slist
    //list 默认是动态数据list
    private String type;
    //如果值类型为list，这里的value是List<ZuJianItem>
    private Object value;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    @Override
    public String toString() {
        if ("input".equals(type)) {
            if (value != null) {
                return value.toString();
            }
        }
        return name;
    }
}
